/* jshint node: true */
"use strict";

//Go to this link if query making is an issue
//https://www.npmjs.com/package/mysql-wrap

var mysql = require('mysql');
var config = require('../../config');


// Connect to database
var pool = mysql.createPool({
    canRetry: true,
    database: config.mysql.database,
    host: config.mysql.host,
    user: config.mysql.user,
    password: config.mysql.password,
    connectionLimit: 100,
    supportBigNumbers: true,
    bigNumberStrings: true,
    charset: 'utf8mb4_unicode_ci',
    multipleStatements: true,
    //debug: true,
    //debug: (process.env.NODE_ENV === 'dev') ? ['ComQueryPacket'] : false,
    //debug: ['RowDataPacket'],
    waitForConnections: true,
    queueLimit: 0
});

// Attempt to catch disconnects 
pool.on('connection', function (connection) {

    // Below never get called
    connection.on('error', function (err) {
        console.error(new Date(), 'MySQL error', err.code);
    });
    connection.on('close', function (err) {
        console.error(new Date(), 'MySQL close', err);
    });
});

//and pass it into the node-mysql-wrap constructor
var createMySQLWrap = require('mysql-wrap');
var sql = createMySQLWrap(pool);

module.exports = sql;