var nodemailer = require('nodemailer');
var ejs = require('ejs');
var fs = require('fs');


module.exports = {

    /**
     * Used to create transport layer
     * @author karthick.k
     * @param   {object}   service&auth     email service, user name and password
     */
    transporter: function () {
        return nodemailer.createTransport({
            service: "Gmail",
            auth: {
                user: "v3@campaygn.com",
                pass: ")m3g_V'^v`>W-J3F;K2}sGUy/2WaxU;NU?"
            }
        });
    },

    /**
     * Used to prepare and return template
     * @author karthick.k
     * @param   {object}   params, template
     * @returns   {string} htmlstring creating ejs template using the user information and return the html string
     */
    get_template: function (template, obj) {
        var email_template_string = fs.readFileSync(__dirname + '/' + template + '.ejs', 'utf8');
        return ejs.render(email_template_string, obj);
    }
};
