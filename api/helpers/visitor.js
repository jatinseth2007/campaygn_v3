/* jshint node: true */
"use strict";

var ua = require('universal-analytics');
var config = require('../../config');

var visitor = ua(config.analytics_id);

module.exports = visitor;