/* jshint node: true */
"use strict";

var cassandra = require('cassandra-driver');
var client = new cassandra.Client({
    contactPoints: ['localhost'],
    keyspace: 'campaygn3_' + process.env.NODE_ENV
});
