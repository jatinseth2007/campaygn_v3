/* jshint node: true */
"use strict";

var YouTube = require('youtube-api');

var key_array = [{
    "key_name": "Browser Key 1 -- Narayan Account",
    key: "AIzaSyDjsjsXqNs35VAAq3IbTOGTbfugPdHE4vI"
}, {
    "key_name": "Browser Key 2 -- Narayan Account",
    key: "AIzaSyCJA0WAqkxhAVodyEqF4h_NnKV3KqBQvRk"
}, {
    "key_name": "Browser Key 3 -- Narayan Account",
    key: "AIzaSyCr4xte0uuh4OOMavdIkXupjHAX6duQF6I"
}, {
    "key_name": "Server Key 1 -- Jatin Account",
    key: "AIzaSyD2wvKmhu8UFE8bEGKbNhdndrRGq7ixT3U"
}, {
    "key_name": "Server Key 2 -- Jatin Account",
    key: "AIzaSyDS48uk5fCiTSresNcSGVAt5B-SFX8VoBE"
}, {
    "key_name": "Server Key 3 -- Jatin Account",
    key: "AIzaSyCiKRkhU0drkyG7cgRXgh8JbNl53P0fH6U"
}, {
    "key_name": "Server Key 4 -- Jatin Account",
    key: "AIzaSyCdrn8HPCG1l39-ij0JD8v0G6ogSeenMAI"
}, {
    "key_name": "Server Key 5 -- Jatin Account",
    key: "AIzaSyCKMQO7gx9Kte0OW30-oNVVn_2Zrzdlcvo"
}, {
    "key_name": "Server Key 6 -- Jatin Account",
    key: "AIzaSyB8mdKimrAQqybEEuWa_PSEdi5qExCA6QY"
}, {
    "key_name": "Server Key 7 -- Jatin Account",
    key: "AIzaSyB75k_0I05vqIoRT39Ro4IRexw5yTMcoMU"
}, {
    "key_name": "Server Key 8 -- Jatin Account",
    key: "AIzaSyDmUAbk91K6mTkv2C5GiNiA_PGXUF4ThCI"
}, {
    "key_name": "Server Key 9 -- Jatin Account",
    key: "AIzaSyD-TEf_YxrZN1hx4Bj6nvmyiS2pVngnhUQ"
}, {
    "key_name": "Server Key 10 -- Jatin Account",
    key: "AIzaSyBPW5Nh_XInNUw4vssWGxu9xUQvZne3Tb0"
}, {
    "key_name": "Server Key 1 -- Karthick Account",
    key: "AIzaSyDKaxj_MExcaT-d9_VgUX0ruv_25ww_o6s"
}, {
    "key_name": "Server Key 2 -- Karthick Account",
    key: "AIzaSyBDA_sF84J24anNtpAVuPOXRuol2qtxz8A"
}, {
    "key_name": "Server Key 3 -- Karthick Account",
    key: "AIzaSyB6BW6t3FuWHhFyEB3GImNtsPPuD21mSfI"
}, {
    "key_name": "Server Key 4 -- Karthick Account",
    key: "AIzaSyDo9eCjsSmy8LndMub-XHzY-aA1kSfbDG0"
}, {
    "key_name": "Server Key 5 -- Karthick Account",
    key: "AIzaSyAolIC1d8zemyZ6kB0ZWJvRUGdf86S0v_Q"
}, {
    "key_name": "Server Key 6 -- Karthick Account",
    key: "AIzaSyC6auxyXFMETR-wb66Wl_RTXKmIMBWTJIw"
}, {
    "key_name": "Server Key 7 -- Karthick Account",
    key: "AIzaSyCY3tQuWMzQF34PZvltpoR4GGjx8gwj6zE"
}, {
    "key_name": "Server Key 8 -- Karthick Account",
    key: "AIzaSyCmQDcJUD5G_tcxy8QkFPs-krntKqjjHLk"
}, {
    "key_name": "Server Key 9 -- Karthick Account",
    key: "AIzaSyCt65RkhOmNCPg71vqZuuVKd8FkWXuWXzM"
}, {
    "key_name": "Server Key 10 -- Karthick Account",
    key: "AIzaSyBlG7oSRtc80LxZAaR02mBe9mkZxUnXRxI"
}];

// need this base url on multiple occasions...
YouTube.youtube_baseurl = 'https://www.youtube.com/watch?v=';

YouTube.shuffle_key = function () {
    var random_key = Math.floor(Math.random() * key_array.length);

    YouTube.authenticate({
        type: "key",
        key: key_array[random_key].key
    });

};

module.exports = YouTube;
