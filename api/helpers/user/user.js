var local_startegy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');
var db = require('../db');

module.exports = function (passport) {


    /**
     * Used to serialize user for the session
     * @author karthick.k
     * @param   {object}   user     user information, id, email
     * @param   {function} callback can be invoked after serializing and it passes id in the callback by passing error and user id
     */
    passport.serializeUser(function (user, callback) {
        callback(null, user.id);
    });

    /**
     * Used to deserialize user for the session
     * @author karthick.k
     * @param   {string}   id     user id
     * @param   {function} callback can be invoked after deserializing and it passes user object by passing error and user information
     */
    passport.deserializeUser(function (id, callback) {
        db.query("SELECT firstname, lastname, email, id, is_verified, role_id, expires_at FROM users WHERE id = ? ", [id], function (err, rows) {
            db.query("SELECT * FROM roles WHERE id = ? ", [rows[0].role_id], function (err, roles) {
                rows[0].role = roles[0].role;
                if (rows[0].firstname && rows[0].lastname) {
                    rows[0].fullname = rows[0].firstname + " " + rows[0].lastname;
                }
                callback(err, rows[0]);
            });
        });
    });


    /**
     * Used to create account after validating the credentials firstname, lastname, role, email
     * @author karthick.k
     * @param   {object}   req      request object along with form data, email, password
     * @param   {string}   email    email address of the user
     * @param   {string}   password password for the user account from form
     * @param   {function} callback can be invoked after ceating account or in case of err by passing err and user information * in the function
     */
    passport.use(
        'create_user',
        new local_startegy({
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true
            },
            function (req, email, password, callback) {
                if (!req.body) {
                    return done("Please fill mandatory fields");
                }

                if (req.body && !req.body.firstname) {
                    return done("Firstname is required");
                }

                if (req.body.firstname.length < 3) {
                    return done("Firstname should be atleast 3 characters");
                }

                if (req.body && !req.body.lastname) {
                    return done("Lastname is required");
                }

                if (!/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(email)) {
                    return done("Email address is incorrect");
                }

                if (req.body && !req.body.role_id) {
                    return done("Role is required");
                }
                db.query("SELECT * FROM users WHERE email = ?", [email], function (err, rows) {
                    if (err) {
                        return callback("Error while creating account, Please retry");
                    }
                    if (rows.length) {
                        return callback("Email address already exist");
                    } else {
                        var user = {
                            email: email,
                            role_id: req.body.role_id,
                            firstname: req.body.firstname,
                            lastname: req.body.lastname,
                            expires_at: req.body.expires_at
                        };

                        var query = "INSERT INTO users (email, firstname, lastname, expires_at, role_id) values (?,?,?,?,?)";

                        db.query(query, [user.email, user.firstname, user.lastname, user.expires_at, user.role_id], function (err, rows) {
                            if (err) {
                                return callback("Error while creating account, Please retry");
                            }
                            user.id = rows.insertId;
                            return callback(null, user);
                        });
                    }
                });
            })
    );


    /**
     * Used to login after validating the credentials email and password
     * @author karthick.k
     * @param   {object}   req      request object along with form data, email, password
     * @param   {string}   email    email address of the user
     * @param   {string}   password password for the user account from form
     * @param   {function} callback can be invoked after creating account or in case of error by passing error and user  information in the function
     */
    passport.use(
        'login',
        new local_startegy({
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true
            },
            function (req, email, password, callback) {
                if (!/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(email)) {
                    return callback("Email address is incorrect");
                }
                // Password should contain atleast 8 characters with atleast one letter, one digit and one special character.
                if (!/^.{5,}$/.test(password)) {
                    return callback("Password is wrong");
                }

                db.query("SELECT * FROM users WHERE email = ?", [email], function (err, rows) {
                    if (err) {
                        return callback("Error while logging in, Please retry");
                    }
                    if (!rows.length) {
                        return callback("Email address is not found, Please enter correct email address");
                    }
                    if (rows[0].is_verified === 'N' || rows[0].password === null) {
                        return callback("Now, you need to verify your email address, we have send an email to " + email + " to verify your address, Please click the activate my account link in the email to continue");
                    }

                    if (rows[0].expires_at !== null) {
                        var current_date = new Date().getTime();
                        var expiry = new Date(rows[0].expires_at).getTime();
                        if (expiry < current_date) {
                            return callback("Your account got expired");
                        }
                    }

                    if (!bcrypt.compareSync(password, rows[0].password)) {
                        return callback("Password is incorrect, Please retry or reset your password by using forgot password option");
                    }
                    db.query("SELECT * FROM roles WHERE id = ? ", [rows[0].role_id], function (err, roles) {
                        if (err) {
                            return callback("Error while logging in, Please retry");
                        } else if (roles.length) {
                            rows[0].role = roles[0].role;
                            return callback(null, rows[0]);
                        }
                    });
                });
            })
    );
};

module.exports.get_host = function () {
    if (process.env.NODE_ENV === "production") {
        return "v3.campaygn.com";
    } else if (process.env.NODE_ENV === "staging") {
        return "v3.campaygn.com";
    } else {
        return "localhost:9002";
    }
};