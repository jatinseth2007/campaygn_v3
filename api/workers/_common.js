/* jshint node: true */
"use strict";

var Q = require('q');
var redis = require('../helpers/redis');
var db = require('../helpers/db');

module.exports = {
    /**
     * Used to clear the cache/delete the key in the redis
     * @author karthick.k
     * @param   {string} key key for redis cache
     * @returns {object} error/result
     */
    clear_cache: function (key) {
        return redis.del(key);
    },
    cache: function (key, value, time_to_expire) {
        return redis.set(key, JSON.stringify(value), 'EX', time_to_expire);
    },
    send_res: function (req, res, data) {
        res.json(data);
        if (!(req.query.cache && req.query.cache === 'false')) {
            this.cache(req.url, data, 86400);
        }
    },
    send_err: function (req, res, code, err) {
        res.status(code).json(err);
    },
    get_data: function (table, columns_to_fetch, column, value) {
        var def = Q.defer();

        var whereObject = {};

        whereObject[column] = value;
        db.select({
                table: table,
                'fields': [columns_to_fetch]
            }, whereObject

        ).then(function (result) {
            if (!result) {
                def.reject({
                    code: 404,
                    msg: "No result found"
                });
            } else {
                def.resolve(result);
            }
        }, function (err) {
            def.reject({
                code: err.statusCode,
                msg: err
            });
        });
        return def.promise;
    },
    check_limit: function (limit) {
        if (limit && !isNaN(limit) && limit > 0 && limit < 1000) {
            return Math.ceil(limit);
        } else {
            return 30;
        }
    },
    check_page: function (page) {
        if (page && !isNaN(page) && page > 0) {
            return Math.ceil(page);
        } else {
            return 1;
        }
    },
    to_slug: function (text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    },
    generate_worker_error: function (err, custom_error, filename) {
        // need to add code for keep a check on err object also
        if (typeof err !== 'object') {
            err = {
                message: err
            };
        }

        // need to check custom_error also
        if (typeof custom_error === 'string') {
            err.custom_message = custom_error;
        } else if (typeof custom_error === 'number') {
            err.custom_id = custom_error;
        } else {
            //In case of object pass it as it is.
            err.custom_error = custom_error;
        }
        err.filename = filename ? filename : "No Filenmae was provided";

        return err;
    }
};
