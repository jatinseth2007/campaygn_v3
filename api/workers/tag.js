var Q = require("q");
var _ = require("lodash");
var moment = require("moment");
var db = require('../helpers/db');
var config = require('../../config');
var _common_workers = require('../workers/_common');

module.exports = {

    /**
     * Used to fetch post based on the tag id and post creation date (six month old)
     * @author karthick.k
     * @param   {object}   req request header
     * @returns {Array} List of posts
     */
    get_posts: function (req) {
        var query = "SELECT * FROM ?? WHERE ?? =  ? AND ?? > ?";
        var six_month = moment().subtract(6, 'months').unix();
        return Q.all([
            db.query(query, ["facebook_tags_view", "tag_name", req.params.tag_name, "post_created_time", six_month]),
            db.query(query, ["instagram_tags_view", "tag_name", req.params.tag_name, "post_created_time", six_month]),
            db.query(query, ["youtube_tags_view", "tag_name", req.params.tag_name, "video_published_at", six_month])
        ]);
    }

};