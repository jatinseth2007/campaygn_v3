var Q = require("q");
var _ = require("lodash");
var moment = require("moment");
var db = require('../helpers/db');
var config = require('../../config');
var _common_workers = require('../workers/_common');

module.exports = {

    /**
     * Used to register the events
     * @author karthick.k
     * @returns {object} Promise object of db query
     */
    register_event: function () {
        var args = Array.prototype.slice.call(arguments);
        var event = {};
        event.category = args[0];
        event.action = args[1];
        event.label = args[2];
        event.value = args[3];
        event.creator_email = args[4];
        event.creator_fullname = args[5];
        return db.insert("analytics_events", event);
    },

    /**
     * Used to fetch events
     * @author karthick.k
     * @param   {object} req request object
     * @returns {object} list of event or error message
     */
    get_events: function (req) {
        var filter = {};
        if (req.query && !!req.query.from && !!req.query.to) {
            filter.from = req.query.from;
            filter.to = req.query.to;
        }
        var select_query = "SELECT ae.`category`, ae.`action`, COUNT(`action`) as count FROM `analytics_events` ae GROUP BY `category`, `action`";
        if (!!filter.hasOwnProperty("from") && !!filter.hasOwnProperty("to")) {
            select_query = "SELECT ae.`category`, ae.`action`, COUNT(`action`) as count FROM `analytics_events` ae WHERE DATE(ae.`created_at`) BETWEEN DATE('" + filter.from + "') AND DATE('" + filter.to + "') GROUP BY `category`, `action`";
        }
        return db.query(select_query);
    },

    /**
     * Used to export the data in xlsx file
     * @author karthick.k
     * @param   {object}   req request object
     * @returns {Object} list of data based on the category
     */
    export: function (req) {
        var form_details = req.query;
        var query = "SELECT pv.`profile_name` as `name`, pv.`profile_country` as `country`, pv.`profile_gender` as `gender`, pv.`profile_age` as `age`, pv.`profile_age_min` as `followers_minimum_age`, pv.`profile_age_max` as `followers_maximum_age`, IFNULL(pv.`ig_followed_by`,0) as `instagram_followers`, pv.`total_followers` as `total_followers`, IFNULL(pv.`facebook_likes`,0) as `facebook_followers`, IFNULL(pv.`youtube_subscriber_count`,0) as `youtube_followers` FROM `profile_view` pv WHERE DATE(`pv`.`profile_created_at`) BETWEEN DATE('" + form_details.from + "') AND DATE('" + form_details.to + "')";
        return db.query(query);
    }
};