/* jshint node: true */
"use strict";

var user = require('./facebook-user');
var post = require('./facebook-post');
var tags = require('./facebook-tags');
var insights = require('./facebook-insights');


module.exports = {
    user:user,
    post:post,
    tags:tags,
    insights:insights
};
