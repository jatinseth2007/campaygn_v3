/* jshint node: true */
"use strict";

var Q = require('q');
var moment = require('moment');
var db = require('../../helpers/db');
var _ = require('lodash');
var _common_worker = require('../_common');


module.exports = {
    save_tags: function (tags) {
        if (tags && tags.length > 0) {
            // need to prepare the array so that we can easily pass it to query;
            var prepared_array = [];

            tags.map(function (tag) {
                prepared_array.push([tag]);
            });

            // pepare query to save in db...
            var query = "INSERT INTO tags (tag) VALUES ? ON DUPLICATE KEY UPDATE tag = VALUES(tag);";

            //Save the hash_tag first then insert its relation with post to avoid non existance error
            return db.query(query, [prepared_array]).then(function (result) {
                return result;
            }, function (err) {
                throw _common_worker.generate_worker_error(err, {
                    error_type: 'custom',
                    message: "Error in saving tags in db.save - tags table"
                }, __filename);
            });

        } else {
            return "No tags to save";
        }
    },
    save_name_tags: function (name_tags, post_id) {
        if (name_tags && name_tags.length > 0) {
            // tags are done now we need to fetch tags id so that we can save relationship also
            var select_tags_query = "SELECT id FROM tags WHERE tag IN (?);";

            return db.query(select_tags_query, [name_tags]).then(function (result_tags) {
                var data_to_save = [];

                result_tags.map(function (result_tag) {
                    data_to_save.push([result_tag.id, post_id]);
                });

                //save tag,post relationship...
                var relationship_query = "INSERT INTO facebook_name_tags_posts (tag_id,post_id) VALUES ? ON DUPLICATE KEY UPDATE tag_id = VALUES(tag_id), post_id = VALUES(post_id);";

                return db.query(relationship_query, [data_to_save]).then(function (data) {
                    return data;
                }, function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        fb_post_id: post_id,
                        error_type: 'custom',
                        message: "Error in saving post tags in save_tag db.save - facebook_name_tags_posts"
                    }, __filename);
                });
            }, function (err) {
                throw _common_worker.generate_worker_error(err, {
                    fb_post_id: post_id,
                    error_type: 'custom',
                    message: "Error in selecting tags from tags table - tags"
                }, __filename);
            });
        } else {
            return "No tags to save";
        }
    },
    save_hash_tags: function (hash_tags, post_id) {
        if (hash_tags && hash_tags.length > 0) {
            // tags are done now we need to fetch tags id so that we can save relationship also
            var select_tags_query = "SELECT id FROM tags WHERE tag IN (?);";

            return db.query(select_tags_query, [hash_tags]).then(function (result_tags) {
                var data_to_save = [];

                result_tags.map(function (result_tag) {
                    data_to_save.push([result_tag.id, post_id]);
                });

                //save tag,post relationship...
                var relationship_query = "INSERT INTO facebook_hash_tags_posts (tag_id,post_id) VALUES ? ON DUPLICATE KEY UPDATE tag_id = VALUES(tag_id), post_id = VALUES(post_id);";

                return db.query(relationship_query, [data_to_save]).then(function (data) {
                    return data;
                }, function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        fb_post_id: post_id,
                        error_type: 'custom',
                        message: "Error in saving post tags in save_tag db.save - facebook_hash_tags_posts"
                    }, __filename);
                });
            }, function (err) {
                throw _common_worker.generate_worker_error(err, {
                    fb_post_id: post_id,
                    error_type: 'custom',
                    message: "Error in selecting tags from tags table - tags"
                }, __filename);
            });

        } else {
            return "No tags to save";
        }
    },
    //Data for growth
    get_growth: function (tag_id) {

        return Q.all([
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    facebook_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    facebook_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(created_time, '%Y-%m-%d') IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "');    "
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    facebook_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    facebook_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(created_time, '%Y-%m-%d') >= '" + moment().subtract(8, 'd').format('YYYY-MM-DD') + "'      " +
                    "  AND     FROM_UNIXTIME(created_time, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "'; "
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    facebook_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    facebook_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(created_time, '%Y-%m-%d') >= '" + moment().subtract(31, 'd').format('YYYY-MM-DD') + "'" +
                    "  AND     FROM_UNIXTIME(created_time, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "';"
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts, FROM_UNIXTIME(MIN(created_time), '%Y-%m-%d') as oldest_date, FROM_UNIXTIME(MAX(created_time), '%Y-%m-%d') as latest_date, DATEDIFF(FROM_UNIXTIME(MAX(created_time)),FROM_UNIXTIME(MIN(created_time))) as no_days                  " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    facebook_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    facebook_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   created_time >= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 6 MONTH)); "
            })
        ]);

    }
};
