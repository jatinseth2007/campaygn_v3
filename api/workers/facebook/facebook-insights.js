/* jshint node: true */
"use strict";

var Q = require('q');
var moment = require('moment');
var db = require('../../helpers/db');
var _ = require('lodash');
var FB = require('../../helpers/facebook');
var _common_worker = require('../_common');


module.exports = {
    get_fb_daily_insights: function (id) {
        var def = Q.defer();

        //Everytime a new request is made.. Make sure to randomize the tokens so that we dont run out of limit.
        FB.randomize_tokens();

        FB.api(id, {
            fields: ["fan_count", "name", "picture.width(1024).height(1024)"]
        }, function (res) {

            if (!res || res.error) {
                if (res.error.code === 100) {
                    //This special case is made for batch request. If no record found. We resolve the promise with empty data. which is equal to array. lol
                    def.resolve([]);
                } else {
                    def.reject(_common_worker.generate_worker_error((!res ? 'unknown error occurred' : res.error), {
                        fb_id: id,
                        error_type: 'fb',
                        message: "Error in fetching data from fb api - get_fb_daily_insights",
                        fb_information: (!res) ? 'No information received from FB' : res
                    }, __filename));
                }
            } else {
                def.resolve(res);
            }
        });

        return def.promise;
    },
    save_fb_daily_insights: function (id) {
        return this.get_fb_daily_insights(id).then(function (data) {

            if (data.length <= 0) {
                return Q.defer().resolve('just ignore');
            }

            var data_to_save = {
                user_id: id,
                date: moment().format('YYYY-MM-DD'),
                likes: data.fan_count
            };
            return Q.all([
                db.save('facebook', {
                    id: id,
                    likes: data.fan_count,
                    name: data.name ? data.name : null,
                    picture: data.picture.data.url ? data.picture.data.url : null
                }),
                db.save('facebook_insights', data_to_save)
            ]).then(function (data) {
                return data;
            }, function (err) {
                throw _common_worker.generate_worker_error(err, {
                    fb_id: id,
                    error_type: 'custom',
                    message: "Error in saving data fetched from fb api - save_fb_daily_insights"
                }, __filename);
            });

        });

    },
    get_fb_page_fans_country: function (id) {
        var def = Q.defer();

        //Everytime a new request is made.. Make sure to randomize the tokens so that we dont run out of limit.
        FB.randomize_tokens();

        FB.api(id + '/insights/page_fans_country/lifetime?since=' + moment().subtract(1, 'd').startOf('day').unix() + '&until=' + moment().endOf('day').unix(), function (res) {

            if (!res || res.error) {
                if (!res) {
                    def.resolve([]);
                } else if (res.error.code === 100) {
                    //This special case is made for batch request. If no record found. We resolve the promise with empty data. which is equal to array. lol
                    def.resolve([]);
                } else if (res.error.code === 21) {
                    //The user has migrated to a different ID
                    var IDs = res.error.message.match(/\d+/g);
                    fb_users.update_user_migration(parseInt(IDs[0]), parseInt(IDs[1])).then(function () {
                        //Return with empty result if the migration is successull.
                        def.resolve([]);
                    }, function (err) {
                        def.reject(_common_worker.generate_worker_error(err, {
                            fb_id: id,
                            error_type: 'fb',
                            message: "Error in migrating new ID - update_user_migration"
                        }, __filename));
                    });
                } else {
                    def.reject(_common_worker.generate_worker_error((!res ? 'unknown error occurred' : res.error), {
                        fb_id: id,
                        fb_res: JSON.stringify(res),
                        error_type: 'fb',
                        message: "Error in fetching data from fb api - get_fb_page_fans_country"
                    }, __filename));
                }
            } else {
                // For now we only want 
                // console.log(res)
                def.resolve(res.data[0]);
            }
        });

        return def.promise;

    },
    save_fb_page_fans_country: function (id) {

        return this.get_fb_page_fans_country(id).then(function (data) {


            //If data is undefined than dont reject.. Instead reply with a proper message
            if (!data) {
                return 'No data to save for this user : ' + id;
            }

            //If no data found to save than resolve with empty array //This happens because the users page has not given permission for reading his insights
            if (data.length === 0) {
                return [];
            }
            var saved_data = data.values.map(function (day) {

                //returning save of all countries 
                var saved_day = _.map(day.value, function (fans, country) {

                    //Returning the promise of saving the data
                    return db.save('facebook_page_fans_country', {
                        user_id: id,
                        date: moment(day.end_time).format('YYYY-MM-DD'),
                        country: country,
                        fans: fans
                    }).then(function (data) {
                        return data;
                    }, function (err) {
                        throw _common_worker.generate_worker_error(err, {
                            fb_id: id,
                            error_type: 'custom',
                            message: "Error in saving data in facebook_page_fans_country - save_fb_page_fans_country"
                        }, __filename);
                    });

                });

                return Q.all(saved_day);

            });
            return Q.all(saved_data);

        });
    }
};

var fb_users = require("./facebook-user");
