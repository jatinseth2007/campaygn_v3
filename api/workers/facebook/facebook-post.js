/* jshint node: true */
"use strict";

var Q = require('q');
var _ = require('lodash');
var moment = require('moment');
var db = require('../../helpers/db');
var FB = require('../../helpers/facebook');
var _common_worker = require('../_common');
var async = require("async");
var fb_tag = require("./facebook-tags");

module.exports = {
    get_fb_reccent_posts: function (id) {
        var def = Q.defer();

        //Everytime a new request is made.. Make sure to randomize the tokens so that we dont run out of limit.
        FB.randomize_tokens();

        FB.api(id + '/posts/', {
            fields: [
                'id', 'message', 'message_tags', 'full_picture', 'link', 'shares', 'type', 'application', 'description',
                'created_time', 'likes.limit(0).summary(true)', 'comments.limit(0).summary(true)'
            ]
        }, function (res) {
            if (!res || res.error) {
                if (res.error.code === 100) {
                    //This special case is made for batch request. If no record found. We resolve the promise with empty data. which is equal to array. lol
                    def.resolve([]);
                } else if (res.error.code === 1) {
                    //This is to handle stupid things from FB - "Unknown error occured" - we can't do anything unless we know the error. lol
                    def.resolve([]);
                } else if (res.error.code === 21) {
                    //The user has migrated to a different ID
                    var IDs = res.error.message.match(/\d+/g);
                    fb_users.update_user_migration(parseInt(IDs[0]), parseInt(IDs[1])).then(function () {
                        //Return with empty result if the migration is successull.
                        def.resolve([]);
                    }, function (err) {
                        def.reject(_common_worker.generate_worker_error(err, {
                            fb_id: id,
                            error_type: 'custom',
                            message: "Error in migrating new ID - update_user_migration"
                        }, __filename));
                    });
                } else {
                    if (!res) {
                        def.resolve([]);
                    } else {
                        def.reject(_common_worker.generate_worker_error(res.error, {
                            fb_id: id,
                            fb_res: JSON.stringify(res),
                            error_type: 'FB',
                            message: "Error in fetching posts from FB - get_fb_reccent_posts"
                        }, __filename));
                    }
                }
            } else {
                // console.log(res.data);
                try {
                    _.map(res.data, function (post) {

                        post.user_id = post.id.split("_")[0];
                        post.id = post.id.split("_")[1];

                        post.created_time = moment(post.created_time).unix();
                        if (post.application) {
                            post.app_link = post.application.link;
                            post.app_name = post.application.namme;
                            post.app_id = post.application.id;
                            delete post.application;
                        }
                        if (post.shares) {
                            post.shares_count = post.shares ? post.shares.count : null;
                            delete post.shares;
                        }

                        if (post.likes) {
                            post.likes_count = post.likes.summary.total_count;
                            delete post.likes;
                        }

                        if (post.comments) {
                            post.comments_count = post.comments ? post.comments.summary.total_count : null;
                            delete post.comments;
                        }

                        return post;
                    });

                    def.resolve(res.data);
                } catch (error) {
                    def.reject(_common_worker.generate_worker_error(error, {
                        fb_id: id,
                        error_type: 'custom',
                        message: "Error in fetching posts from FB - get_fb_reccent_posts - else"
                    }, __filename));
                }

            }
        });
        return def.promise;
    },
    save_engagement_rate: function (post) {
        try {
            return db.selectOne('facebook_live_users_view', {
                fb_id: post.user_id
            }).then(function (fb_user) {
                return _.round((((post.likes_count ? post.likes_count : 0) + (post.comments_count ? post.comments_count : 0) + (post.shares_count ? post.shares_count : 0)) / fb_user.fb_likes), 2);
            }, function (err) {
                throw _common_worker.generate_worker_error(err, {
                    fb_information: JSON.stringify(post),
                    error_type: 'custom',
                    message: "Error in updating post engagement rate in save_engagement_rate in db.selectOne"
                }, __filename);
            }).then(function (ER) {
                return db.update('facebook_posts', {
                    engagement_rate: (isNaN(ER) || ER === Infinity) ? null : ER
                }, {
                    id: post.id
                }).then(function (data) {
                    return data;
                }, function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        fb_information: JSON.stringify(post),
                        error_type: 'custom',
                        message: "Error in updating post engagement rate in save_engagement_rate in db.update"
                    }, __filename);
                });
            });
        } catch (error) {
            throw _common_worker.generate_worker_error(error, {
                fb_information: JSON.stringify(post),
                error_type: 'custom',
                message: "Error in updating post engagement rate in save_engagement_rate"
            }, __filename);
        }
    },
    save_post: function (data) {
        try {

            var self = this;
            return db.save('facebook_posts', data)
                .then(function (result) {
                    //Returning a promise with eigther success or error
                    return self.save_engagement_rate(data);
                }, function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        fb_information: JSON.stringify(data),
                        error_type: 'custom',
                        message: "Error in saving post in save_post method db.save"
                    }, __filename);
                });
        } catch (error) {
            throw _common_worker.generate_worker_error(error, {
                fb_information: JSON.stringify(data),
                error_type: 'custom',
                message: "Error in saving post in save_post method"
            }, __filename);
        }
    },
    save_posts: function (posts) {
        var def = Q.defer();
        var self = this;

        //Iterate through each post and upload them.
        //we need to run each post synchrosly, one by one, so, i am going to use asyn lib
        async.eachSeries(posts, function (post, callback) {
            var name_tags = [];

            //need to prepare an array for this...
            if (post.message_tags && post.message_tags.length > 0) {
                name_tags = post.message_tags.map(function (message_tag) {
                    return _.trim(message_tag.name.trim(), '#');
                });

                // need to delete otherwise it will create problem while saving post...
                delete post.message_tags;
            }

            var hash_tags_arr = post.message ? post.message.match(/(?:^|\W)#(\w+)(?!\w)/g) : [];
            var hash_tags = [];

            //need to prepare an array for this...
            if (hash_tags_arr && hash_tags_arr.length > 0) {
                hash_tags = hash_tags_arr.map(function (hash_tag) {
                    return _.trim(hash_tag.trim(), '#');
                });
            }

            // need to save post first...
            self.save_post(post).then(function (data) {
                var all_tags = name_tags.concat(hash_tags);

                if (all_tags && all_tags.length <= 0) {
                    callback();
                }

                // save tags in tags table, if we will do it separately for both deadlock occur...
                fb_tag.save_tags(all_tags).then(function (data) {
                    var tags_promise = [];

                    // hash tags to save
                    if (hash_tags && hash_tags.length > 0) {
                        tags_promise.push(fb_tag.save_hash_tags(hash_tags, post.id));
                    } else {
                        tags_promise.push(true);
                    }

                    // message tags to save...
                    if (name_tags && name_tags.length > 0) {
                        tags_promise.push(fb_tag.save_name_tags(name_tags, post.id));
                    } else {
                        tags_promise.push(true);
                    }

                    //save hash-tags first, if we run it parallel then deadlock will happen...
                    Q.all(tags_promise).then(function (data) {
                        callback();
                    }, function (err) {
                        callback(err);
                    });
                }, function (err) {
                    callback(err);
                });
            }, function (err) {
                callback(err);
            });


        }, function (err) {
            if (err) {
                def.reject(err);
            } else {
                def.resolve("done");
            }
        });

        return def.promise;
    }
};

var fb_users = require("./facebook-user");
