/* jshint node: true */
"use strict";

var Q = require('q');
var moment = require('moment');
var _ = require('lodash');
var db = require('../../helpers/db');
var FB = require('../../helpers/facebook');


module.exports = {
    get_facebook_user_details: function (id) {
        var def = Q.defer();

        //Everytime a new request is made.. Make sure to randomize the tokens so that we dont run out of limit.
        FB.randomize_tokens();

        FB.api(id, {
            fields: [
                'id', 'username', 'name', 'picture.width(1024).height(1024)', 'birthday', 'category', 'checkins', 'description',
                'fan_count', 'were_here_count', 'talking_about_count', 'link', 'website', 'mission', 'company_overview', 'global_brand_root_id',
                'general_info', 'personal_info', 'bio', 'personal_interests', 'awards', 'phone', 'affiliation',
                'products', 'cover', 'has_added_app', 'is_community_page', 'release_date', 'location'
            ]
        }, function (res) {
            if (!res || res.error) {
                def.reject(!res ? 'error occurred' : res.error);
            } else {

                var to_return = {
                    id: res.id,
                    username: res.username ? res.username : null,
                    name: res.name ? res.name : null,
                    picture: res.picture.data.url ? res.picture.data.url : null,
                    birthday: res.birthday ? moment(res.birthday, 'MM/DD/YYYY').format('YYYY-MM-DD') : null,
                    category: res.category ? res.category : null,
                    checkins: res.checkins ? res.checkins : null,
                    description: res.description ? res.description : null,
                    likes: res.fan_count ? res.fan_count : null,
                    were_here_count: res.were_here_count ? res.were_here_count : null,
                    talking_about_count: res.talking_about_count ? res.talking_about_count : null,
                    link: res.link ? res.link : null,
                    website: res.website ? res.website : null,
                    mission: res.mission ? res.mission : null,
                    company_overview: res.company_overview ? res.company_overview : null,
                    global_brand_root_id: res.global_brand_root_id ? res.global_brand_root_id : null,
                    general_info: res.general_info ? res.general_info : null,
                    personal_info: res.personal_info ? res.personal_info : null,
                    bio: res.bio ? res.bio : null,
                    personal_interests: res.personal_interests ? res.personal_interests : null,
                    awards: res.awards ? res.awards : null,
                    phone: res.phone ? res.phone : null,
                    affiliation: res.affiliation ? res.affiliation : null,
                    products: res.products ? res.products : null
                };
                if (res.cover) {
                    to_return.cover_id = res.cover.id;
                    to_return.cover_offset_x = res.cover.offset_x;
                    to_return.cover_offset_y = res.cover.offset_y;
                    to_return.cover_source = res.cover.source;
                }
                to_return.has_added_app = res.has_added_app ? res.has_added_app.toString() : null;
                to_return.is_community_page = res.is_community_page ? res.is_community_page.toString() : null;
                to_return.release_date = res.release_date ? moment(res.release_date, 'MMMM Do YYYY').format('YYYY-MM-DD') : null;
                //Sometime date comes super weird.
                if (to_return.release_date === 'Invalid date') {
                    delete to_return.release_date;
                }
                to_return.founded = _.isInteger(_.parseInt(res.founded)) ? _.parseInt(res.founded) : null;
                if (res.location) {
                    to_return.location_located_in = res.location.located_in ? res.location.located_in : null;
                    to_return.location_latitude = res.location.latitude ? res.location.latitude : null;
                    to_return.location_longitude = res.location.longitude ? res.location.longitude : null;
                    to_return.location_name = res.location.name ? res.location.name : null;
                    to_return.location_street = res.location.street ? res.location.street : null;
                    to_return.location_region = res.location.region ? res.location.region : null;
                    to_return.location_city = res.location.city ? res.location.city : null;
                    to_return.location_state = res.location.state ? res.location.state : null;
                    to_return.location_country = res.location.country ? res.location.country : null;
                    to_return.location_zip = res.location.zip ? res.location.zip : null;

                }
                def.resolve(to_return);

            }
        });
        return def.promise;
    },
    update_user_migration: function (old_id, new_id) {

        if (!_.isNumber(old_id) || !_.isNumber(new_id)) {
            return new Error("Old id or new id for migration is not a number");
        }

        return db.update('profiles', {
            facebook: new_id
        }, {
            facebook: old_id
        });
    },
    save_facebook_user_details: function (id) {
        return this.get_facebook_user_details(id).then(function (fb_data) {
            return db.save('facebook', fb_data).then(function (data) {
                console.log("Changes saved for :", fb_data.name);
                return true;
            }, function (err) {
                console.log("Error : ", fb_data.id, err);
                throw err;
            });
        }, function (err) {
            console.log("Facebook API error :", err);
            throw err;
        });

    },
    update_engagement_rate: function (id) {

        return db.one("SELECT AVG(`engagement_rate`) as `avg_engagement_rate` FROM (SELECT `engagement_rate` FROM `facebook_posts` WHERE `user_id` = " + id + " ORDER BY `created_time` DESC LIMIT 0,10) x")
            .then(function (data) {

                //Update only if engagement rate is available.
                if (data.avg_engagement_rate) {
                    return db.save('facebook', {
                        id: id,
                        engagement_rate: data.avg_engagement_rate
                    });
                } else {
                    return "No facebook engagement rate to update";
                }
            });
    },
    //Data for growth box
    get_growth: function (facebook_id) {

        return Q.all([
            db.query({
                sql: "" +
                    "   SELECT      CONCAT(ifnull(Z.`media_posted`,0), ',', 0) as media_info, X.`followed_by_info`                                 " +
                    "   FROM        (                                                                                                                 " +
                    "                   SELECT  `user_id`, GROUP_CONCAT(`likes` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info                                                                                    " +
                    "                   FROM    `facebook_insights`                                                                                   " +
                    "                   WHERE   `user_id` =" + facebook_id +
                    "                   AND     `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(2, 'd').format('YYYY-MM-DD') + "') " +
                    "               )as X                                                                              " +
                    "   LEFT JOIN    (                                                                                                                " +
                    "                   SELECT  `user_id`, COUNT(`user_id`) as media_posted " +
                    "                   FROM    `facebook_posts` " +
                    "                   WHERE   `user_id` = " + facebook_id +
                    "                   AND     FROM_UNIXTIME(`created_time`, '%Y-%m-%d') IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "') " +
                    "                 )as Z ON (X.`user_id` = Z.`user_id`); "


            }),
            db.query({
                sql: "" +
                    "   SELECT      CONCAT(ifnull(Z.`media_posted`,0), ',', 0) as media_info, X.`followed_by_info`                                 " +
                    "   FROM        (                                                                                                                 " +
                    "                   SELECT  `user_id`, GROUP_CONCAT(`likes` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info " +
                    "                   FROM    `facebook_insights`                                                                                   " +
                    "                   WHERE   `user_id` =" + facebook_id +
                    "                   AND     `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(8, 'd').format('YYYY-MM-DD') + "') " +
                    "               )as X                                                                              " +
                    "   LEFT JOIN    (                                                                                                                " +
                    "                   SELECT  `user_id`, COUNT(`user_id`) as media_posted " +
                    "                   FROM    `facebook_posts` " +
                    "                   WHERE   `user_id` = " + facebook_id +
                    "                   AND     FROM_UNIXTIME(`created_time`, '%Y-%m-%d') >= '" + moment().subtract(8, 'd').format('YYYY-MM-DD') + "'   " +
                    "                   AND     FROM_UNIXTIME(`created_time`, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "'   " +
                    "                 )as Z ON (X.`user_id` = Z.`user_id`); "


            }),

            db.query({
                sql: "" +
                    "   SELECT      CONCAT(ifnull(Z.`media_posted`,0), ',', 0) as media_info, X.`followed_by_info`                                 " +
                    "   FROM        (                                                                                                                 " +
                    "                   SELECT  `user_id`, GROUP_CONCAT(`likes` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info " +
                    "                   FROM    `facebook_insights`                                                                                   " +
                    "                   WHERE   `user_id` =" + facebook_id +
                    "                   AND     `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(31, 'd').format('YYYY-MM-DD') + "') " +
                    "               )as X                                    " +
                    "   LEFT JOIN    (                                                                                                                " +
                    "                   SELECT  `user_id`, COUNT(`user_id`) as media_posted " +
                    "                   FROM    `facebook_posts` " +
                    "                   WHERE   `user_id` = " + facebook_id +
                    "                   AND     FROM_UNIXTIME(`created_time`, '%Y-%m-%d') >= '" + moment().subtract(31, 'd').format('YYYY-MM-DD') + "'    " +
                    "                   AND     FROM_UNIXTIME(`created_time`, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "'     " +
                    "                 )as Z ON (X.`user_id` = Z.`user_id`); "
            }),
            db.query({
                sql: "" +
                    "   SELECT  latest_date, oldest_date, no_days, followers_diff" +
                    "   FROM    `facebook_profiles_average_view` a " +
                    "   WHERE   a.`user_id` ='" + facebook_id +
                    "'   ; "
            }),
            db.query({
                sql: "SELECT COUNT(id) as media_diff, FROM_UNIXTIME(MIN(created_time), '%Y-%m-%d') as oldest_date, FROM_UNIXTIME(MAX(created_time), '%Y-%m-%d') as latest_date, DATEDIFF(FROM_UNIXTIME(MAX(created_time)),FROM_UNIXTIME(MIN(created_time))) as no_days FROM facebook_posts WHERE user_id ='" + facebook_id + "' AND created_time >= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 6 MONTH));"
            })
        ]);

    }
};
