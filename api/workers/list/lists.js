/* jshint node: true */
"use strict";

var db = require('../../helpers/db');

module.exports = {
    /**
     * Used to fetch the user's base lists having role owner only
     * @author Jatin Seth
     * @param   {object}   req     user id
     * @param   {object} res returns result of query
     */
    fetch_user_owner_root_lists: function (user_id) {

        // prepare query to fetch lists assosiated with a user by parent list.
        var query = "SELECT b.`id`, b.`name` FROM `lists_users` a JOIN `lists` b ON(a.`list_id` = b.`id`) WHERE a.`user_id` = '" + user_id + "' AND a.`user_role` IN ('owner') AND b.`parent_id` = 0 AND b.`deleted` = 'N';";

        return db.query(query);
    },
    /**
     * Used to fetch the user's base lists having role member and admin only
     * @author Jatin Seth
     * @param   {object}   req     user id
     * @param   {object} res returns result of query
     */
    fetch_user_other_root_lists: function (user_id) {

        // prepare query to fetch lists assosiated with a user by parent list.
        var query = "SELECT b.`id`, b.`name` FROM `lists_users` a JOIN `lists` b ON(a.`list_id` = b.`id`) WHERE a.`user_id` = '" + user_id + "' AND a.`user_role` IN ('member','admin') AND b.`deleted` = 'N' AND b.`parent_id` NOT IN (SELECT b.`id` FROM `lists_users` a JOIN `lists` b ON(a.`list_id` = b.`id`) WHERE a.`user_id` = '" + user_id + "' AND b.`deleted` = 'N');";

        return db.query(query);
    },
    /**
     * Used to fetch the user's base lists having role owner only with details
     * @author Jatin Seth
     * @param   {object}   req     user id
     * @param   {object} res returns result of query
     */
    user_owner_root_lists_query: function (req, count_query) {

        var query = " ";
        var user_id = req.user.id;
        var sort_order = (req.query.sort_order) ? req.query.sort_order : 'DESC';
        var sort_by = (req.query.sort_by) ? req.query.sort_by : 'a.`id`';

        query += (count_query) ? " SELECT    COUNT(X.`id`) as total_records FROM (" : '';

        // prepare query to fetch owner lists assosiated with a user.
        query += " " +
            " SELECT    a.`id`,COUNT(c.`profile_id`) as number_of_influencers, a.`name`, a.`description`, SUM(`facebook_likes`) as fb_reach, SUM(`ig_followed_by`) as ig_reach, SUM(`youtube_subscriber_count`) as yt_reach, IFNULL(e.`total_subchilds`, 0) as total_subchilds" +
            " FROM		`lists`			    a " +
            " JOIN		`lists_users`		b ON (b.`user_id` = '" + user_id + "' AND a.`parent_id` = 0 AND a.`deleted` = 'N' AND b.`user_role` = 'owner' AND a.`id` = b.`list_id`) " +
            " LEFT JOIN	 (       " +
            "             SELECT    `parent_id`, COUNT(`id`) as total_subchilds      " +
            "             FROM      `lists`                                           " +
            "             GROUP BY  `parent_id`                                       " +
            "            )as e ON (a.`id` = e.`parent_id`)                 " +
            " LEFT JOIN	`lists_profiles`	c ON (a.`id` = c.`list_id`) " +
            " LEFT JOIN	`profile_view`	d ON (c.`profile_id` = d.`profile_id`) " +
            " GROUP BY	a.`id` " +
            " ORDER BY	" + sort_by + " " + sort_order;

        query += (count_query) ? " )as X;" : '';

        return query;
    },
    /**
     * Used to fetch the user's base lists having role member and admin only with details
     * @author Jatin Seth
     * @param   {object}   req     user id
     * @param   {object} res returns result of query
     */
    fetch_user_other_root_lists_details: function (user_id) {

        // prepare query to fetch owner lists assosiated with a user.
        var query = " " +
            " SELECT    a.`id`,COUNT(c.`profile_id`) as number_of_influencers, a.`name`, a.`description`, SUM(`facebook_likes`) as fb_reach, SUM(`ig_followed_by`) as ig_reach, SUM(`youtube_subscriber_count`) as yt_reach" +
            " FROM		    `lists`			    a " +
            " JOIN		    `lists_users`		b ON (b.`user_id` = '" + user_id + "' AND a.`parent_id` NOT IN (SELECT a.`id` FROM `lists` a JOIN `lists_users` b ON (a.`id` = b.`list_id`) WHERE b.`user_id` = '" + user_id + "' AND a.`deleted` = 'N') AND a.`deleted` = 'N' AND b.`user_role` IN ('admin', 'member') AND a.`id` = b.`list_id`) " +
            " LEFT JOIN	    `lists_profiles`	c ON (a.`id` = c.`list_id`) " +
            " LEFT JOIN	    `profile_view`	d ON (c.`profile_id` = d.`profile_id`) " +
            " GROUP BY	    a.`id` ";

        return db.query(query);
    }
};
