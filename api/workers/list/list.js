/* jshint node: true */
"use strict";

var db = require('../../helpers/db');
var Q = require("q");
var user_worker = require('./../user');
var email_helper = require('../../helpers/email/email');
var analytics = require("../analytics");

module.exports = {
    /**
     * Used to add new list in system
     * @author Jatin Seth
     * @param   {object}   req object containing input from user
     * @param   {object} res returns promise accordingly
     */
    add: function (input) {
        try {
            var self = this;

            // need to validate input data before moving forward...
            return self.validate_list_params(input).then(function (validation_output) {
                // if got error need to throw error...
                if (Object.keys(validation_output).length > 0) {
                    throw validation_output;
                }

                // if input is fine then need to move forward...
                return db.save('lists', {
                    name: input.body.name.trim(),
                    parent_id: (input.body.parent_id) ? input.body.parent_id : 0,
                    description: (input.body.description) ? input.body.description : ''
                }).then(function (list_info) {
                    //Analytics - register list addition
                    analytics.register_event("list", "added", "list_name", input.body.name, input.user.email, input.user.fullname);
                    // after list has been added, need to added further entries...
                    var promises = [];

                    // Add influencers in DB for this list...
                    if (input.body.profiles && Object.keys(input.body.profiles).length > 0) {
                        // need to prepare the array first to add into the DB
                        var influencers = self.prepare_influencers_array(list_info.insertId, input.body.profiles);
                        promises.push(self.add_influencers(influencers));
                    }

                    // Add user in owner's list...
                    var user_list_info = [{
                        list_id: list_info.insertId,
                        user_id: input.user.id,
                        user_role: 'owner'
                        }];

                    promises.push(self.add_list_user(user_list_info));

                    return Q.all(promises);

                }, function (err) {
                    throw err;
                });
            }, function (err) {
                throw err;
            });

        } catch (err) {
            throw err;
        }
    },
    /**
     * Used to update list in system
     * @author Jatin Seth
     * @param   {object}   req object containing input from user
     * @param   {object} res returns promise accordingly
     */
    update: function (input) {
        try {
            var self = this;

            // need to validate input data before moving forward...
            return self.validate_list_params(input).then(function (validation_output) {

                // if got error need to throw error...
                if (Object.keys(validation_output).length > 0) {
                    throw validation_output;
                }

                // if input is fine then need to move forward...
                return db.save('lists', input.body);
            }, function (err) {
                throw err;
            });

        } catch (err) {
            throw err;
        }
    },
    /**
     * Used to validate list params
     * @author Jatin Seth
     * @param   {object}   req object containing input from user
     * @param   {object} res returns promise according to validations
     */
    validate_list_params: function (input) {
        var errors = {};
        var promises = [];

        if (typeof input.body.name === 'undefined' || input.body.name.trim().length === 0) {
            errors.name = "Please add valid name for list";
        }

        if (typeof input.body.parent_id !== 'undefined' && isNaN(input.body.parent_id)) {
            errors.parent_id = "Please select a valid parent list";
        }

        if (typeof input.body.profiles !== 'undefined' && typeof input.body.profiles !== 'object') {
            errors.profiles = "Please select valid influencers to add in list";
        }

        if (typeof input.user.id === 'undefined' || isNaN(input.user.id)) {
            errors.profiles = "Please add valid user";
        }

        promises.push(this.check_duplicacy(input.body, input.user.id));


        return Q.all(promises).then(function (data) {
            if (data[0].length > 0 && !errors.name) {
                errors.name = 'The list with same name already exist';
            }

            return errors;

        }, function (err) {
            throw err;
        });
    },
    /**
     * Used to add profiles into a list
     * @author Jatin Seth
     * @param   {object}   req object containing values for e.g (list_id, user_id)
     * @param   {object} res returns result of query
     */
    add_influencers: function (value) {
        var promises = [];

        value.forEach(function (item, index) {
            promises.push(db.save('lists_profiles', item));
        });

        return Q.all(promises);
    },
    /**
     * Used to prepare influencers array so that we can add it directly into query
     * @author Jatin Seth
     * @param   {object}   req list id, array of objects for e.g [{list_id, profile_id}]
     * @param   {object} res returns result of query
     */
    prepare_influencers_array: function (list_id, input) {
        var output = [];

        for (var key in input) {
            output.push({
                list_id: list_id,
                profile_id: input[key]
            });
        }

        return output;
    },
    /**
     * Used to prepare users array so that we can add it directly into query
     * @author Jatin Seth
     * @param   {object}   req list id, array of objects for e.g [{list_id, user_id, user_role}]
     * @param   {object} res returns array
     */
    prepare_list_users_obj: function (list_id, input) {
        var output = [];

        if (input.users && input.users.length > 0) {
            input.users.forEach(function (user, index) {
                output.push({
                    list_id: list_id,
                    user_role: input.role,
                    user_id: user
                });
            });
        }

        return output;
    },
    /**
     * Used to add user for list
     * @author Jatin Seth
     * @param   {object}   req object containing values for e.g (list_id, user_id, user_role)
     * @param   {object} res returns result of query
     */
    add_list_user: function (value) {
        return db.insert('lists_users', value);
    },
    /**
     * Used to check the authority of user on list
     * @author Jatin Seth
     * @param   {object}   req user id and list id
     * @param   {object} res returns result of query
     */
    check_user_authorization: function (user_id, list_id) {
        return db.query("SELECT `user_role` FROM `lists_users` WHERE `user_id` = '" + user_id + "' AND `list_id` = '" + list_id + "'");
    },
    /**
     * Used to remove profiles from a list
     * @author Jatin Seth
     * @param   {object}   req  body contains object of profiles
     * @param   {object} res returns result of query
     */
    remove_list_profiles: function (body) {
        var self = this;

        var promises = [];

        for (var key in body.profiles) {
            promises.push(db.delete('lists_profiles', {
                list_id: body.parent_id,
                profile_id: body.profiles[key]
            }));
        }

        return Q.all(promises).then(function (data) {
            return true;
        }, function (err) {
            throw err;
        });
    },
    /**
     * Used to check the duplicasy, if a list with same name by same user is already there in system
     * @author Jatin Seth
     * @param   {object}   req     list name, user id
     * @param   {object} res returns result of query
     */
    check_duplicacy: function (input, user_id) {
        var where = " a.`name` = '" + input.name.trim() + "' ";

        // need to add condition if
        if (input.id) {
            where += " AND a.`id` != '" + input.id + "' ";
        }

        return db.query("SELECT a.`id` FROM `lists` a JOIN `lists_users` b ON (" + where + " AND b.`user_id` = '" + user_id + "' AND a.`id` = b.`list_id`)");
    },

    /**
     * Used to check if list has any child
     * @author Jatin Seth
     * @param   {object}   req     list-id
     * @param   {object} res returns result of query
     */
    have_children: function (list_id) {
        return db.query("SELECT COUNT(`id`) as children_no FROM `lists` WHERE `parent_id` = '" + list_id + "';");
    },

    /**
     * Used to check if list has any child
     * @author Jatin Seth
     * @param   {object}   req     list-id
     * @param   {object} res returns result of query
     */
    delete_list: function (list_id, user) {
        //Analytics - register list deletion
        if (user.hasOwnProperty("email")) {
            analytics.register_event("list", "deleted", "list_id", list_id, user.email, user.fullname);
        }
        return db.query("UPDATE `lists` SET `deleted` = 'Y' WHERE `id` = '" + list_id + "' OR `parent_id` = '" + list_id + "' ;");
    },

    /**
     * Used to check if input is valid or not
     * @author Jatin Seth
     * @param   {object}   req     list-id
     * @param   {object} res returns true or errors
     */
    validate_list_user_params: function (input) {
        var errors = {};

        if (!(input.role) || ['member', 'admin'].indexOf(input.role) < 0) {
            errors.user_role = "Please select valid user role.";
        }

        if (!(input.users) || input.users.length <= 0) {
            errors.users = "Please select atleast one user.";
        }


        if (Object.keys(errors).length <= 0) {
            return true;
        }

        return errors;
    },
    /**
     * Used to send emails for lists module
     * @param   {object}   user     consist of template_to_use, params to set in template
     * @returns {function} promise
     */
    send_email: function (subject, template_to_use, params) {
        // I want to make it promisify
        var def = Q.defer();

        var transporter = email_helper.transporter();
        var template = email_helper.get_template(template_to_use, params);
        var mail_options = {
            from: 'hi',
            to: params.email,
            subject: subject,
            text: '',
            html: template
        };

        transporter.sendMail(mail_options, function (err, data) {
            if (err) {
                console.log(err);
                def.reject("Error while sending confirmation email to your account, Please retry");
            } else {
                def.resolve('done');
            }
        });

        return def.promise;
    },
    /**
     * Used to send email notification when user is added in list
     * @param   {object}   user     input
     * @returns {function} callback can be invoked after sending email
     */
    send_list_notification: function (req) {
        var self = this;

        // fetch user details to send email.
        return user_worker.fetch_users_by_ids(req.body.users).then(function (users) {
            var promises = [];
            var to_emails = [];

            if (users.length > 0) {
                users.forEach(function (user, index) {
                    to_emails.push(user.email);
                });

                promises.push(self.send_email('Ykone: List sharing', 'list/list_sharing', {
                    email: to_emails,
                    list_url: req.protocol + '://' + req.get('host') + '/#/list/' + req.params.list_id,
                    role: req.body.role
                }));
            }

            return Q.all(promises);
        }, function (err) {
            throw err;
        });
    },
    /**
     * Used to validate list comment params
     * @author Jatin Seth
     * @param   {object}   req object containing input from user
     * @param   {object} res returns error or true
     */
    validate_comment_params: function (input) {
        var errors = {};

        if (typeof input.comment === 'undefined' || input.comment.trim().length === 0) {
            errors.comment = "Please provide valid comment.";
        }

        if (Object.keys(errors).length > 0) {
            return errors;
        }

        return true;
    },
    /**
     * Used to prepare data for excel export
     * @author Jatin Seth
     * @param   {object}   input query result
     * @param   {object} res returns data or empty data
     */
    prepare_list_export_data: function (input) {
        var output = [];

        if (input.length > 1) {
            input.forEach(function (item, index) {
                output.push({
                    Name: (item.profile_name) ? item.profile_name : 'NA',
                    Country: (item.profile_country) ? item.profile_country : 'NA',
                    URL: (item.profile_url) ? item.profile_url : 'NA',
                    Engagement_Rate: (item.total_engagement_rate) ? parseFloat(item.total_engagement_rate).toFixed(2) : 'NA',
                    Total_Followers: (item.total_followers) ? item.total_followers : 'NA',
                    Total_Followers_Growth: (item.total_growth) ? (item.total_growth + '%') : 'NA',
                    Facebook_Followers: (item.facebook_likes) ? item.facebook_likes : 'NA',
                    Instagram_Followers: (item.ig_followed_by) ? item.ig_followed_by : 'NA',
                    Youtube_Followers: (item.youtube_subscriber_count) ? item.youtube_subscriber_count : 'NA'
                });
            });
        }

        return output;
    }
};