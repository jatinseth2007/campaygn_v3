/* jshint node: true */
"use strict";

var list = require('./list');
var lists = require('./lists');

module.exports = {
    list: list,
    lists: lists
};
