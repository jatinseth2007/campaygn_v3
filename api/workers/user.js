/* jshint node: true */
"use strict";

var user_helper = require('../helpers/user/user');
var email_helper = require('../helpers/email/email');
var db = require('../helpers/db');
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');

module.exports = {
    /**
     * Used to generate unique token
     * @author karthick.k
     * @returns {string} hexadecimal unique string
     */
    get_validation_token: function () {
        return crypto.randomBytes(20).toString('hex');
    },

    /**
     * Generates the expiry date for confirming the account, it is 24 hours
     * @author karthick.k
     * @returns {object} object consist of timestamp and date of expiry, with interval of 24 hours from current date
     */
    get_expiry: function () {
        var result = {};
        var date = new Date();
        var timestamp = date.setDate(date.getDate() + 1);
        result.timestamp = timestamp;
        result.full_date = new Date(date);
        return result;
    },

    /**
     * Updates the validation token in table based on the email address
     * @author karthick.k
     * @param   {string} email            email address of the user
     * @param   {string} validation_token unique validation token generated by get_validation_token method
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    update_validation_token: function (email, validation_token, callback) {
        var update_query = "UPDATE users SET validation_token = ? WHERE email = ?";
        db.query(update_query, [validation_token, email], function (err, rows) {
            if (err) {
                return callback("Error while creating account, Please retry");
            }
            return callback(null, rows);
        });
    },

    /**
     * Used to send verification email to the account and to update validation token in the table
     * @param   {object}   user     consist of id, email, create_at time
     * @param {boolean}     is_forgot_password whether this method is accessed for forgot password option                        
     * @param   {function} callback callback can be invoked after sending email
     * @returns {function} callback can be invoked after sending email
     */
    send_email: function (req, user, is_forgot_password, callback) {
        var expiry = this.get_expiry();
        var validation_token = this.get_validation_token() + "-" + expiry.timestamp;
        var transporter = email_helper.transporter();
        user.validation_url = null;
        if (!!is_forgot_password) {
            user.validation_url = req.protocol + "://" + user_helper.get_host() + "/#/user/reset_password/?token=" + validation_token + "&email=" + user.email;
        } else {
            user.validation_url = req.protocol + "://" + user_helper.get_host() + "/#/user/verification/?token=" + validation_token + "&email=" + user.email;
        }
        user.name = user.firstname + " " + user.lastname;
        user.expiry = expiry.full_date;
        var subject = !!is_forgot_password ? 'Reset your password' : 'Verification of Account';
        var template = null;
        if (!!is_forgot_password) {
            template = email_helper.get_template("/user/forgot-password", {
                user: user
            });
        } else {
            template = email_helper.get_template("/user/verify-user", {
                user: user
            });
        }
        var mail_options = {
            from: 'hi',
            to: user.email,
            subject: subject,
            text: '',
            html: template
        };
        this.update_validation_token(user.email, validation_token, function (err, data) {
            if (err) {
                return callback(err);
            }
            transporter.sendMail(mail_options, function (err, data) {
                if (err) {
                    console.log(err);
                    return callback("Error while sending confirmation email to your account, Please retry");
                } else {
                    return callback(null, data);
                }
            });
        });
    },

    /**
     * Updates the validation token in table based on the email address
     * @author karthick.k
     * @param   {string} email            email address of the user
     * @param   {string} validation_token unique validation token generated by get_validation_token method
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    update_is_verified: function (email, token, callback) {
        var update_query = "UPDATE users SET is_verified = ? WHERE email = ? AND validation_token = ?";
        db.query(update_query, ['Y', email, token], function (err, rows) {
            if (err) {
                return callback("Error while creating account, Please retry");
            }
            return callback(null, rows);
        });
    },

    /**
     * Fetch user using email address
     * @author karthick.k
     * @param   {string} email            email address of the user
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    get_user_by_email: function (email, callback) {
        var select_query = "SELECT * FROM users WHERE email = ?";
        db.query(select_query, [email], function (err, rows) {
            if (err) {
                return callback("Error while fetching associated user");
            }
            if (!rows.length) {
                return callback("User is not found");
            }
            return callback(null, rows[0]);
        });
    },

    /**
     * Fetching the user using email and token
     * @author karthick.k
     * @param   {string} email            email address of the user
     * @param   {string} token unique validation token generated by get_validation_token method
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    get_user_using_email_token: function (email, token, callback) {
        var select_query = "SELECT * FROM users WHERE email = ? AND validation_token = ?";
        db.query(select_query, [email, token], function (err, rows) {
            if (err) {
                return callback("Your verification link is malformed, Please retry");
            }
            if (!rows.length) {
                return callback("Your verification link is not associated, Please follow correct verification link");
            }
            return callback(null, rows[0]);
        });
    },

    /**
     * Delete the user using email and token if the verification link got expired
     * @author karthick.k
     * @param   {string} email            email address of the user
     * @param   {string} validation_token unique validation token generated by get_validation_token method
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    delete_user_using_email_token: function (email, token, callback) {
        var delete_query = "DELETE FROM users WHERE email = ? AND validation_token = ?";
        db.query(delete_query, [email, token], function (err, rows) {
            if (err) {
                return callback("Your verification link is malformed, Please retry");
            }
            return callback(null, rows);
        });
    },


    /**
     * Used to handle forgot password, sending an email to reset the password after verifying the credentials
     * @param   {object}   req     consist of request header, verification token
     * @param   {function} callback callback can be invoked after verifying account
     */
    forgot_password: function (req, callback) {
        var scope = this;
        if (!req || !req.query || !/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(req.query.email)) {
            return callback("Your verification link is malformed, Please retry");
        }
        scope.get_user_by_email(req.query.email, function (err, user) {
            if (err) {
                return callback(err);
            }
            if (user.is_verified === 'N') {
                return callback("Now, you need to verify your email address, we have send an email to " + user.email + " to verify your address, Please click the activate my account link in the email to continue");
            }

            if (user.expires_at !== null) {
                var current_date = new Date().getTime();
                var expiry = new Date(user.expires_at).getTime();
                if (expiry < current_date) {
                    return callback("Your account got expired, You cannot reset the password");
                }
            }
            scope.send_email(req, user, true, function (err, data) {
                if (err) {
                    return callback(err);
                }
                return callback(null, "We have send an email to " + user.email + " to reset the password, Please click the reset password link in the email to change password");
            });
        });
    },

    /**
     * Updates the new password in the table based on the email address
     * @author karthick.k
     * @param   {string} email            email address of the user
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    update_password: function (email, new_password, callback) {
        var update_query = "UPDATE users SET password = ? WHERE email = ?";
        db.query(update_query, [new_password, email], function (err, rows) {
            if (err) {
                return callback("Error while updating password, Please retry");
            }
            return callback(null, rows);
        });
    },

    /**
     * Updates the last login in the table
     * @author karthick.k
     * @param   {string} email            email address of the user
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    update_last_login: function (email, callback) {
        var update_query = "UPDATE users SET last_login = NOW() WHERE email = ?";
        db.query(update_query, [email], function (err, rows) {
            if (err) {
                console.log(err);
                return callback("Error while updating password, Please retry");
            }
            return callback(null, rows);
        });
    },

    /**
     * Used to handle reset password, update the new password to the table after verifying credentials
     * @param   {object}   req     consist of request header, verification token
     * @param   {function} callback callback can be invoked after verifying account
     */
    reset_password: function (req, callback) {
        var scope = this;
        if (!req || !req.body || !req.body.token || !/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(req.body.email)) {
            return callback("Your reset password link is malformed, Please retry");
        }

        if (!req.body.password || !req.body.confirm_password) {
            return callback("Password and confirm password is required");
        }

        if (req.body.password !== req.body.confirm_password) {
            return callback("Password and confirm password is not matching");
        }

        if (!/^.{5,}$/.test(req.body.password)) {
            return callback("Password should contain atleast 5 characters");
        }

        scope.get_user_using_email_token(req.body.email, req.body.token, function (err, user) {
            if (err) {
                return callback(err);
            }
            var validation_token = user.validation_token;
            var expiry_timestamp = validation_token.split("-")[1];
            var date = new Date();
            var current_timestamp = date.getTime();
            if (current_timestamp > expiry_timestamp) {
                return callback("Reset password link is expired");
            }
            if (user.password !== null && bcrypt.compareSync(req.body.password, user.password)) {
                return callback("New password should not be same as old password");
            }
            var new_password = bcrypt.hashSync(req.body.password, null, null);
            scope.update_password(user.email, new_password, function (err, data) {
                if (err) {
                    return callback(err);
                }
                if (user.is_verified === 'N') {
                    scope.update_is_verified(user.email, validation_token, function (err, data) {
                        if (err) {
                            return callback(err);
                        }
                        return callback(null, "Your verification is successful");
                    });
                } else {
                    return callback(null, "Password updated successfully");
                }

            });
        });
    },

    /**
     * Fetch the roles
     * @author karthick.k
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    get_roles: function (callback) {
        var select_query = "SELECT * FROM roles";
        db.query(select_query, function (err, rows) {
            if (err) {
                return callback("Error while fetching roles");
            }
            return callback(null, {
                roles: rows
            });
        });
    },

    /**
     * Fetch the users
     * @author karthick.k
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    get_users: function (callback) {
        var select_query = "SELECT users.id, users.email, users.created_at, users.firstname, users.lastname, users.expires_at, roles.role, users.is_verified, users.last_login FROM users LEFT JOIN roles ON users.role_id=roles.id";
        db.query(select_query, function (err, rows) {
            if (err) {
                return callback("Error while fetching users");
            }
            return callback(null, {
                users: rows
            });
        });
    },

    /**
     * Updates the expires_at in the table based on the user id
     * @author karthick.k
     * @param   {object} req            request object
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    modify_expiry: function (req, callback) {
        if (!req || !req.query) {
            return callback("User id is missing, Please retry");
        }
        var expiry = req.query.expires_at;
        var id = req.query.id;
        if (!req.query.id) {
            return callback("User id is missing, Please retry");
        }
        if (expiry === null || expiry === "null") {
            var query = "UPDATE users SET expires_at = NULL WHERE id = ?";
            db.query(query, [id], function (err, rows) {
                if (err) {
                    console.log(err);
                    return callback("Error while updating expiry, Please retry");
                }
                return callback(null, {
                    message: "Expiry is updated"
                });
            });
        } else {
            var update_query = "UPDATE users SET expires_at = ? WHERE id = ?";
            db.query(update_query, [expiry, id], function (err, rows) {
                if (err) {
                    console.log(err);
                    return callback("Error while updating expiry, Please retry");
                }
                return callback(null, {
                    message: "Expiry is updated"
                });
            });
        }
    },

    /**
     * Updates the role in the table based on the user id
     * @author karthick.k
     * @param   {object} req            request object
     * @param   {function} callback         function to be invoked after updating.
     * @returns {function} callback function is invoked after updating the validation token in the table
     */
    modify_role: function (req, callback) {
        if (!req || !req.query) {
            return callback("User id is missing, Please retry");
        }
        if (!req.query.role_id) {
            return callback("Role is required, Please retry");
        }
        var role_id = req.query.role_id;
        if (!req.query.id) {
            return callback("User id is missing, Please retry");
        }
        var id = req.query.id;

        var update_query = "UPDATE users SET role_id = ? WHERE id = ?";
        db.query(update_query, [role_id, id], function (err, rows) {
            if (err) {
                console.log(err);
                return callback("Error while updating role, Please retry");
            }
            return callback(null, {
                message: "Role is updated"
            });
        });
    },

    /**
     * Used to log user action and time 
     * @author karthick.k
     * @param {object} req request object           
     */
    log_user: function (req) {
        if (req && req.user && req.path) {
            var update_query = "INSERT INTO user_logs SET id = ?, path = ?, time = NOW()";
            db.query(update_query, [req.user.id, req.path], function (err, rows) {
                if (err) {
                    console.log(err);
                }
            });
        }
    },

    /**
     * Used to fetch user logs
     * @author karthick.k
     * @param {object} req request object 
     * @returns {object} callback user log                    
     */
    get_user_logs: function (req, callback) {
        if (req && req.query && req.query.user_id) {
            var select_query = "SELECT * FROM user_logs WHERE id = ? ORDER BY time DESC";
            db.query(select_query, [req.query.user_id], function (err, rows) {
                if (err) {
                    return callback("Error while fetching user logs");
                }
                return callback(null, rows);
            });
        } else {
            return callback("User id is missing");
        }
    },

    /**
     * USed to delete user
     * @author karthick.k
     * @param {object} req  request header, query
     * @param {function} callback callback function is invoked after deleting the user in the table
     */
    delete_user: function (req, callback) {
        if (req && req.query && req.query.user_id) {
            var delete_query = "DELETE FROM ?? WHERE id = ?";
            db.query(delete_query, ["users", req.query.user_id], function (err, rows) {
                if (err) {
                    return callback("Error while deleting user");
                }
                return callback(null, "User is deleted successfully");
            });
        } else {
            return callback("User id is missing");
        }
    },
    /**
     * USed to fetch users by ids
     * @author Jatin Seth
     * @param {object} req  users array
     * @param {function} promise provided by db.query
     */
    fetch_users_by_ids: function (ids) {
        return db.query("SELECT id, email FROM users WHERE id IN (" + ids.join() + ");");
    }
};