/* jshint node: true */
"use strict";

var Q = require('q');
var db = require('../../helpers/db');
var _cw = require('../../workers/_common');
var moment = require('moment');


module.exports = {
    save_tag: function (video_id, tags) {
        var self = this;

        // check if we are getting input valid or not...
        if (!video_id || !tags) {
            return "nothing to save";
        }

        // need to prepare the array so that we can easily pass it to query;
        var prepared_array = [];

        tags.map(function (tag) {
            prepared_array.push([tag]);
        });

        var query = "INSERT INTO tags (tag) VALUES ? ON DUPLICATE KEY UPDATE tag = VALUES(tag);";

        return db.query(query, [prepared_array]).then(function (result) {

            //It means the tag has been just updated. Find the id and update the relationship
            var select_tags_query = "SELECT id FROM tags WHERE tag IN (?);";
            return db.query(select_tags_query, [tags]).then(function (result_tags) {
                var data_to_save = [];

                result_tags.map(function (result_tag) {
                    data_to_save.push([result_tag.id, video_id]);
                });

                //save the tag and post relation to tag post relation table
                return self.save_tag_video_relationship(data_to_save);

            }, function (err) {
                throw _cw.generate_worker_error(err, "Error in saving tagsn " + tags + " of video id " + video_id, __filename);
            });

        }, function (err) {
            throw _cw.generate_worker_error(err, "Error in saving tagsa " + tags + " of video id " + video_id, __filename);
        });


    },
    save_tag_video_relationship: function (data_to_save) {
        // check if we are getting input valid or not...
        if (!data_to_save) {
            return "nothing to save";
        }

        var query = "INSERT INTO youtube_tags_videos (tag_id,video_id) VALUES ? ON DUPLICATE KEY UPDATE tag_id = VALUES(tag_id), video_id = VALUES(video_id);";
        //save the tag and post relation to tag post relation table
        return db.query(query, [data_to_save]).then(function (data) {
            return data;
        }, function (err) {
            throw _cw.generate_worker_error(err, {
                message: "Error in saving data in youtube_tags_videos table - save_tag_video_relationship",
                data_to_save: data_to_save
            }, __filename);
        });
    },

    //Data for growth widget
    get_growth: function (tag_id) {

        return Q.all([
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	video_id                                                                                            " +
                    "              FROM	    youtube_tags_videos                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    youtube_videos as Y ON (X.video_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(published_at, '%Y-%m-%d') IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "');    "
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	video_id                                                                                            " +
                    "              FROM	    youtube_tags_videos                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    youtube_videos as Y ON (X.video_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(published_at, '%Y-%m-%d') >= '" + moment().subtract(8, 'd').format('YYYY-MM-DD') + "'      " +
                    "  AND     FROM_UNIXTIME(published_at, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "'; "
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	video_id                                                                                            " +
                    "              FROM	    youtube_tags_videos                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    youtube_videos as Y ON (X.video_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(published_at, '%Y-%m-%d') >= '" + moment().subtract(31, 'd').format('YYYY-MM-DD') + "'" +
                    "  AND     FROM_UNIXTIME(published_at, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "';"
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts, FROM_UNIXTIME(MIN(published_at), '%Y-%m-%d') as oldest_date, FROM_UNIXTIME(MAX(published_at), '%Y-%m-%d') as latest_date, DATEDIFF(FROM_UNIXTIME(MAX(published_at)),FROM_UNIXTIME(MIN(published_at))) as no_days                  " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	video_id                                                                                            " +
                    "              FROM	    youtube_tags_videos                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    youtube_videos as Y ON (X.video_id = Y.id)                                                               " +
                    "  WHERE   published_at >= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 6 MONTH)); "
            })
        ]);

    }

};
