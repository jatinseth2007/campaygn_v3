/* jshint node: true */
"use strict";

var Q = require('q');
var moment = require('moment');
var db = require('../../helpers/db');
var _cw = require("../../workers/_common");
var youtube = require('../../helpers/youtube');


module.exports = {

    /**
     * Get youtube channels based on keyword
     * @author Jatin Seth
     * @param   {string} keyword
     * @returns {object} Object of youtube result
     */
    get_youtube_channels: function (keyword, total_results) {
        var def = Q.defer();

        youtube.shuffle_key();

        youtube.search.list({
            part: 'snippet',
            maxResults: total_results,
            q: keyword,
            type: "channel"
        }, function (err, res) {
            if (err) {
                def.reject(err);
            } else {
                def.resolve(res);
            }
        });
        return def.promise;
    },
    /**
     * Get youtube user details from youtube
     * @author NarayaN Yaduvanshi
     * @param   {string} id youtube Id 
     * @returns {object} Object of youtube user details
     */
    get_youtube_user: function (id) {
        var def = Q.defer();

        youtube.shuffle_key();

        youtube.channels.list({
            part: 'statistics,snippet,contentDetails',
            id: id,
        }, function (err, res) {
            if (err) {
                err.custom_error = {
                    youtube_user_id: id,
                    file: __filename
                };
                def.reject(err);
            } else {
                try {
                    var data_to_send = {
                        id: id,
                        name: res.items[0].snippet.title,
                        description: res.items[0].snippet.description,
                        published_at: moment(res.items[0].snippet.publishedAt).unix(),
                        picture: res.items[0].snippet.thumbnails.high.url,
                        view_count: res.items[0].statistics.viewCount,
                        comment_count: res.items[0].statistics.commentCount,
                        subscriber_count: res.items[0].statistics.subscriberCount,
                        video_count: res.items[0].statistics.videoCount,
                        upload_playlist_id: res.items[0].contentDetails.relatedPlaylists.uploads
                    };
                    def.resolve(data_to_send);
                } catch (e) {
                    def.reject(e);
                }

            }
        });
        return def.promise;
    },
    /**
     * Save the data of a user to database
     * @author NarayaN Yaduvanshi
     * @param   {[[Type]]} id [[Description]]
     * @returns {[[Type]]} [[Description]]
     */
    save_youtube_user: function (user) {
        return db.save('youtube', user).fail(function (err) {
            throw _cw.generate_worker_error(err, {
                user_id: user.id,
                message: "Error in saving user details to youtube table"
            }, __filename);
        });
    },
    /**
     * Saves a users Daily insights like subscribers count ect.
     * @author NarayaN Yaduvanshi
     * @param   {[[Type]]} id [[Description]]
     * @returns {[[Type]]} [[Description]]
     */
    save_youtube_daily_insights: function (user) {
        try {
            return db.save('youtube_insights', {
                id: user.id,
                date: moment().format("YYYY-MM-DD"),
                view_count: user.view_count,
                comment_count: user.comment_count,
                subscriber_count: user.subscriber_count,
                video_count: user.video_count
            });
        } catch (e) {
            e.custom_error = {
                youtube_user_id: user.id,
                file: __filename
            };
            return e;
        }
    },
    /**
     * Saving last 10 videos averages as profile engagement rate
     * @author NarayaN Yaduvanshi
     * @param   {[[Type]]} id [[Description]]
     * @returns {string}   [[Description]]
     */
    update_engagement_rate: function (id) {
        return db.one("SELECT AVG(`engagement_rate`) as `avg_engagement_rate` FROM (SELECT `engagement_rate` FROM `youtube_videos` WHERE `channel_id` = '" + id + "' ORDER BY `published_at` DESC LIMIT 0,10) x")
            .then(function (data) {

                //Update only if engagement rate is available.
                if (data.avg_engagement_rate) {
                    return db.update('youtube', {
                        engagement_rate: data.avg_engagement_rate
                    }, {
                        id: id
                    });
                } else {
                    return "No Youtube engagement rate to update";
                }
            });
    },
    //Data for growth box
    get_growth: function (youtube_id) {
        return Q.all([
            db.query({
                sql: " SELECT GROUP_CONCAT(`view_count` ORDER BY `date` DESC SEPARATOR ',') as media_info, GROUP_CONCAT(`subscriber_count` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info FROM `youtube_insights` WHERE `id` = '" + youtube_id + "' AND `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(2, 'd').format('YYYY-MM-DD') + "');            "
            }),
            db.query({
                sql: " SELECT GROUP_CONCAT(`view_count` ORDER BY `date` DESC SEPARATOR ',') as media_info, GROUP_CONCAT(`subscriber_count` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info FROM `youtube_insights` WHERE `id` = '" + youtube_id + "' AND `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(8, 'd').format('YYYY-MM-DD') + "');                                                                                                      "
            }),
            db.query({
                sql: " SELECT GROUP_CONCAT(`view_count` ORDER BY `date` DESC SEPARATOR ',') as media_info, GROUP_CONCAT(`subscriber_count` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info FROM `youtube_insights` WHERE `id` = '" + youtube_id + "' AND `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(31, 'd').format('YYYY-MM-DD') + "');                                                                                                      "
            }),
            db.query({
                sql: "SELECT media_diff, followers_diff, latest_date, oldest_date, no_days FROM `youtube_profiles_average_view` WHERE `id` = '" + youtube_id + "' ;"
            })
        ]);

    }
};
