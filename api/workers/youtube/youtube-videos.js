/* jshint node: true */
"use strict";

var Q = require('q');
var _ = require('lodash');
var moment = require('moment');
var db = require('../../helpers/db');
var _common_worker = require('../_common');
var youtube = require('../../helpers/youtube');
var async = require("async");
var yt_tags = require('./youtube-tags');

module.exports = {
    /**
     * Gets the full details and stats of a youtube video
     * @author NarayaN Yaduvanshi
     * @param {[[Type]]} id [[Description]]
     */
    get_youtube_video_details: function (id) {
        var def = Q.defer();

        youtube.shuffle_key();

        youtube.videos.list({
            part: 'snippet,statistics',
            id: id,
        }, function (err, res) {
            if (err) {
                def.reject(err);
            } else {
                var data_to_return = {
                    id: res.items[0].id,
                    published_at: moment(res.items[0].snippet.publishedAt).unix(),
                    channel_id: res.items[0].snippet.channelId,
                    title: res.items[0].snippet.title,
                    description: res.items[0].snippet.description,
                    thumbnail: res.items[0].snippet.thumbnails.high.url,
                    category_id: res.items[0].snippet.categoryId,
                    view_count: res.items[0].statistics.viewCount,
                    like_count: res.items[0].statistics.likeCount,
                    dislike_count: res.items[0].statistics.dislikeCount,
                    favorite_count: res.items[0].statistics.favoriteCount,
                    comment_count: res.items[0].statistics.commentCount
                };
                if (res.items[0].snippet.tags) {
                    data_to_return.tags = res.items[0].snippet.tags;
                }
                def.resolve(data_to_return);
            }
        });
        return def.promise;
    },
    /**
     * Gets the ids of recently uploaded youtube videos of a user
     * @author NarayaN Yaduvanshi
     * @param   {[[Type]]} playlistId [[Description]]
     * @returns {[[Type]]} [[Description]]
     */
    get_youtube_playlist_videos: function (playlistId) {
        var def = Q.defer();

        youtube.shuffle_key();

        youtube.playlistItems.list({
            part: 'contentDetails',
            playlistId: playlistId,
            maxResults: 10
        }, function (err, res) {
            if (err) {
                def.reject(err);
            } else {
                if (res.items.length >= 1) {
                    var playlist = res.items.map(function (item) {
                        return item.contentDetails.videoId;
                    });
                    def.resolve(playlist);
                } else {
                    def.resolve("No videos found");
                }

            }
        });
        return def.promise;
    },
    save_youtube_video_details: function (video) {
        return Q.all([db.save('youtube_videos', video), this.save_engagement_rate(video)]);
    },
    save_engagement_rate: function (video) {
        try {
            return db.selectOne('youtube_live_users_view', {
                yt_id: video.channel_id
            }).then(function (channel) {
                    return _.round((_.add(parseInt(video.like_count ? video.like_count : 0), parseInt(video.comment_count ? video.comment_count : 0)) / parseInt(channel.yt_subscribers ? channel.yt_subscribers : 0)), 2);
                },
                function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        youtube_information: JSON.stringify(video),
                        error_type: 'custom',
                        message: "Error in updating video engagement rate in save_engagement_rate in db.selectOne"
                    }, __filename);
                }).then(function (ER) {
                return db.update('youtube_videos', {
                    engagement_rate: (isNaN(ER) || ER === Infinity) ? null : ER
                }, {
                    id: video.id
                }).then(function (data) {
                    return data;
                }, function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        youtube_information: JSON.stringify(video),
                        error_type: 'custom',
                        message: "Error in updating video engagement rate in save_engagement_rate in db.update"
                    }, __filename);
                });
            });
        } catch (error) {
            console.log('lol');
            throw _common_worker.generate_worker_error(error, {
                youtube_information: JSON.stringify(video),
                error_type: 'custom',
                message: "Error in updating video engagement rate in save_engagement_rate"
            }, __filename);
        }
    },
    save_youtube_videos: function (videos) {
        var def = Q.defer();
        var self = this;

        async.eachSeries(videos, function (video_id, callback) {
            //Get each youtube video details and statistics
            self.get_youtube_video_details(video_id).then(function (video_details) {

                // need to check if we are gettig something only then we need to do something.
                if (Object.keys(video_details).length <= 0) callback();

                // need to fetch tags from data
                var video_tags = video_details.tags;

                //deleteting tags from original posts
                delete video_details.tags;

                // save the video first then we will move.
                self.save_youtube_video_details(video_details).then(function () {
                    // need to check if we have any tag to save...
                    if (!video_tags) callback();

                    // save tags begin
                    yt_tags.save_tag(video_id, video_tags).then(function (data) {
                        callback();
                    }, function (err) {
                        callback(err);
                    });
                }, function (err) {
                    callback(err);
                });

            }, function (err) {
                callback(err);
            });
        }, function (err) {
            if (err) {
                def.reject(err);
            } else {
                def.resolve("done");
            }
        });

        return def.promise;
    }
};
