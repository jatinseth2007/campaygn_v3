/* jshint node: true */
"use strict";


var users = require('./youtube-users');
var videos = require('./youtube-videos');
var tags = require('./youtube-tags');


module.exports = {
    users: users,
    videos: videos,
    tags: tags
};
