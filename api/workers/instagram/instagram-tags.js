/* jshint node: true */
"use strict";

var ig = require('../../helpers/instagram');
var Q = require('q');
var common = require('../../helpers/common');
var db = require('../../helpers/db');
var moment = require('moment');

module.exports = {
    save_tag: function (post_id, tag) {

        return db.save('tags', {
            tag: tag
        }).then(function (result) {
            if (result.insertId === 0) {

                //It means the tag has been just updated. Find the id and update the relationship
                return db.selectOne("tags", {
                    tag: tag
                }).then(function (result_tag) {

                    //save the tag and post relation to tag post relation table
                    return db.save("instagram_tags_posts", {
                        post_id: post_id,
                        tag_id: result_tag.id
                    });
                }, function (err) {
                    throw err;
                });

            } else {

                //It means the tag has been just created. take the newly inserted id and update the relationship
                return db.save("instagram_tags_posts", {
                    post_id: post_id,
                    tag_id: result.insertId
                });
            }
        }, function (err) {
            return err;
        });

    },
    save_name_tag: function (post_id, name_tag) {

        return db.save('tags', {
            tag: name_tag
        }).then(function (result) {
            if (result.insertId === 0) {
                //It means the tag has been just updated. Find the id and update the relationship
                return db.selectOne("tags", {
                    tag: name_tag
                }).then(function (result_name_tag) {

                    //save the tag and post relation to tag post relation table
                    return db.save("instagram_name_tags_posts", {
                        post_id: post_id,
                        tag_id: result_name_tag.id
                    });
                }, function (err) {
                    throw err;
                });

            } else {

                //It means the tag has been just created. take the newly inserted id and update the relationship
                return db.save("instagram_name_tags_posts", {
                    post_id: post_id,
                    tag_id: result.insertId
                });
            }
        });


    },

    //Data for growth
    get_growth: function (tag_id) {

        return Q.all([
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    instagram_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    instagram_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(created_time, '%Y-%m-%d') IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "');    "
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    instagram_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    instagram_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(created_time, '%Y-%m-%d') >= '" + moment().subtract(8, 'd').format('YYYY-MM-DD') + "'      " +
                    "  AND     FROM_UNIXTIME(created_time, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "'; "
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts                                                                                                     " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    instagram_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    instagram_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   FROM_UNIXTIME(created_time, '%Y-%m-%d') >= '" + moment().subtract(31, 'd').format('YYYY-MM-DD') + "'" +
                    "  AND     FROM_UNIXTIME(created_time, '%Y-%m-%d') <= '" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "';"
            }),
            db.query({
                sql: " SELECT	COUNT(Y.id) as no_of_posts, FROM_UNIXTIME(MIN(created_time), '%Y-%m-%d') as oldest_date, FROM_UNIXTIME(MAX(created_time), '%Y-%m-%d') as latest_date, DATEDIFF(FROM_UNIXTIME(MAX(created_time)),FROM_UNIXTIME(MIN(created_time))) as no_days                  " +
                    "  FROM	(                                                                                                      " +
                    "              SELECT	post_id                                                                                            " +
                    "              FROM	    instagram_tags_posts                                                                   " +
                    "              WHERE 	tag_id = " + tag_id +
                    "         )as X                                                                                                     " +
                    "  JOIN    instagram_posts as Y ON (X.post_id = Y.id)                                                               " +
                    "  WHERE   created_time >= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 6 MONTH)); "
            })
        ]);

    }

};