/* jshint node: true */
"use strict";

var Q = require('q');
var _ = require("lodash");
var moment = require("moment");

var common = require('../../helpers/common');
var db = require('../../helpers/db');
var ig = require('../../helpers/instagram');
var _common_worker = require('../_common');

module.exports = {
    get_user_post_timestamp: function (user_id, count) {
        var def = Q.defer();

        count = count || 10000000000000; //High number to fetch all medias possible in 1 shot itself.

        ig.user_media_recent(user_id.toString(), {
                count: count
            },
            function (err, medias, pagination, remaining, limit) {
                if (!err) {
                    def.resolve(medias, pagination);
                } else {
                    if (err.status_code === 200) {
                        /*
                         * This error handling is because sometime Instagram is replying with broken JSON
                         */
                        def.resolve([]);
                    }

                    // if guy is private, we need to ignore...
                    if (err.code === 400) {
                        def.resolve("private");
                    }

                    def.reject(_common_worker.generate_worker_error(err, {
                        error_type: 'IG',
                        ig_id: user_id,
                        message: "Error in fetching data from Instagram api get_user_post_timestamp method"
                    }, __filename));
                }
            });
        return def.promise;
    },

    /**
     * save a recently extracted post from instagram to mysql db
     * @param  {onject} post holds the post object containing all the info
     * @return {promise}
     */
    save_post: function (post) {
        try {
            var self = this;

            //First delete comments data because we absolutely dont need in our db
            delete post.comments.data;
            delete post.likes.data;

            var data_to_save = {
                id: post.id.split("_")[0],
                user_id: post.user.id,
                type: post.type,
                comments_count: post.comments.count,
                filter: post.filter,
                created_time: post.created_time,
                created_date: moment.unix(post.created_time).format("YYYY-MM-DD"),
                link: post.link,
                likes_count: post.likes.count,
                caption: post.caption ? post.caption.text : null,
                images_low_resolution_url: post.images.low_resolution.url,
                images_thumbnail_url: post.images.thumbnail.url,
                images_standard_resolution_url: post.images.standard_resolution.url,
                //TODO : Get rid of these tags and save them in new table
                tags: post.tags.join(",")
            };

            if (post.location && post.location === 'null') {
                data_to_save.location_latitude = post.location.latitude;
                data_to_save.location_longitude = post.location.longitude;
                data_to_save.location_id = post.location.id;
                data_to_save.location_street_address = post.location.street_address;
                data_to_save.location_name = post.location.name;
            }

            if (post.type === 'video') {
                data_to_save.videos_low_bandwidth_url = post.videos.low_bandwidth.url;
                data_to_save.videos_standard_resolution_url = post.videos.standard_resolution.url;
                data_to_save.videos_low_resolution_url = post.videos.low_resolution.url;
            }

            /**
             * Mechanism to save engagement rate
             */

            return db.selectOne({
                table: 'instagram',
                fields: ['followed_by']
            }, {
                id: data_to_save.user_id
            }).then(function (result_profile) {
                var engagement_rate = ((data_to_save.comments_count + data_to_save.likes_count) / result_profile.followed_by) * 100;

                // we need to check and then assign it to save in db.
                data_to_save.engagement_rate = (isNaN(engagement_rate) || engagement_rate === Infinity) ? null : engagement_rate;

                return db.save('instagram_posts', data_to_save).then(function (data) {

                    /**
                     * Saving hashtags
                     */
                    var saved_tags = [];
                    if (post.tags.length > 0) {
                        saved_tags = post.tags.map(function (tag) {
                            return instagram_tags.save_tag(data_to_save.id, tag);
                        });

                    }
                    /**
                     * Saving nametags
                     */
                    var saved_name_tags = [];
                    if (post.name_tags) {
                        saved_name_tags = post.name_tags.map(function (tag) {
                            return instagram_tags.save_name_tag(data_to_save.id, tag);
                        });

                    }

                    return Q.all([saved_tags, saved_name_tags]).fail(function (err) {
                        throw _common_worker.generate_worker_error(err, {
                            error_type: 'custom',
                            ig_post_information: JSON.stringify(post),
                            message: "Error in saving data in tags function - save_tag"
                        }, __filename);
                    });

                }, function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        error_type: 'custom',
                        ig_post_information: JSON.stringify(post),
                        message: "Error in saving data in instagram_posts table - save_post"
                    }, __filename);
                });
            }, function (err) {
                throw _common_worker.generate_worker_error(err, {
                    error_type: 'custom',
                    ig_post_information: JSON.stringify(post),
                    message: "Error in selectOne in instagram table - save_post"
                }, __filename);
            });
        } catch (error) {
            throw _common_worker.generate_worker_error(error, {
                error_type: 'custom',
                ig_post_information: JSON.stringify(post),
                message: "Error in saving data for ig post - save_post"
            }, __filename);
        }

    }


};

var instagram_tags = require('./instagram-tags');
var instagram_users = require('./instagram-users');
