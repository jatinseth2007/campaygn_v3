/* jshint node: true */
"use strict";

var Q = require('q');
var moment = require('moment');

var db = require('../../helpers/db');
var ig = require('../../helpers/instagram');
var _common_worker = require('../_common');

module.exports = {
    /**
     * Searches a single user info with username from instagram
     * @param  {string} username [username of a instagram user]
     * @param  {int} count [total number of returned results]
     * @return {[Object]}         [Promise]
     */
    search_users: function (keyword, count) {
        var def = Q.defer();

        ig.user_search(keyword, {
            "count": count
        }, function (err, users, remaining, limit) {
            if (!err) {
                def.resolve(users);
            } else {
                def.reject(err);
            }
        });
        return def.promise;
    },
    /**
     * Get details of a single user
     * @param  {string} user_id [user id of the user]
     * @return {Object}         [Promise] 
     */
    get_user_details: function (user_id) {
        var self = this;

        var def = Q.defer();
        ig.user(user_id.toString(), function (err, result, remaining, limit) {
            if (!err) {
                def.resolve(result);
            } else {
                if (err.code === 400) {
                    def.resolve("private");
                } else {
                    def.reject(_common_worker.generate_worker_error(err, {
                        error_type: 'IG',
                        ig_id: user_id,
                        message: "Error in fetching data from Instagram api get_user_details method"
                    }, __filename));
                }
            }
        });
        return def.promise;
    },
    /**
     * Getting all instagram users
     * @return {result} object
     */
    get_ig_users: function () {
        return db.query('SELECT * FROM `profiles` WHERE `instagram` IS NOT NULL');
    },
    //saving instagram daily statics of a user
    save_instagram_daily_count: function (data_to_save) {
        return db.save('instagram_daily_count', data_to_save);
    },
    update_engagement_rate: function (id) {
        return db.one("SELECT AVG(`engagement_rate`) as `avg_engagement_rate` FROM (SELECT `engagement_rate` FROM `instagram_posts` WHERE `user_id` = " + id + " ORDER BY `created_time` DESC LIMIT 0,10) x")
            .then(function (data) {

                //Update only if engagement rate is available.
                if (data.avg_engagement_rate) {
                    return db.save('instagram', {
                        id: id,
                        engagement_rate: data.avg_engagement_rate
                    });
                } else {
                    return "No Instagram engagement rate to update";
                }
            });
    },
    //Update the profile of a instagram user
    update_instagram_profile: function (user) {
        var data_to_save = {
            id: user.id,
            username: user.username,
            full_name: user.full_name,
            website: user.website,
            bio: user.bio,
            media: user.counts.media,
            followed_by: user.counts.followed_by,
            follows: user.counts.follows,
            profile_picture: user.profile_picture
        };

        //save the user to instagram table.
        return db.save('instagram', data_to_save);
    },
    //Data for growth box
    get_growth: function (instagram_id) {
        return Q.all([
            db.query({
                sql: "SELECT GROUP_CONCAT(`media` ORDER BY `date` DESC SEPARATOR ',') as media_info, GROUP_CONCAT(`followed_by` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info FROM `instagram_daily_count` WHERE `instagram_id` = " + instagram_id + " AND `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(2, 'd').format('YYYY-MM-DD') + "'); "
            }),
            db.query({
                sql: "SELECT GROUP_CONCAT(`media` ORDER BY `date` DESC SEPARATOR ',') as media_info, GROUP_CONCAT(`followed_by` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info FROM `instagram_daily_count` WHERE `instagram_id` = " + instagram_id + " AND `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(8, 'd').format('YYYY-MM-DD') + "');"
            }),
            db.query({
                sql: "SELECT GROUP_CONCAT(`media` ORDER BY `date` DESC SEPARATOR ',') as media_info, GROUP_CONCAT(`followed_by` ORDER BY `date` DESC SEPARATOR ',') as followed_by_info FROM `instagram_daily_count` WHERE `instagram_id` = " + instagram_id + " AND `date` IN ('" + moment().subtract(1, 'd').format('YYYY-MM-DD') + "','" + moment().subtract(31, 'd').format('YYYY-MM-DD') + "');"
            }),
            db.query({
                sql: "SELECT media_diff, followers_diff, latest_date, oldest_date, no_days FROM `instagram_profiles_average_view` WHERE `instagram_id` = '" + instagram_id + "' ;"
            })
        ]);

    }
};