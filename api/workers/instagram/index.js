/* jshint node: true */
"use strict";


var users = require('./instagram-users');
var posts = require('./instagram-posts');
var tags = require('./instagram-tags');


module.exports = {
    users: users,
    posts: posts,
    tags: tags
};
