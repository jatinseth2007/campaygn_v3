/* jshint node: true */
"use strict";

var Q = require('q');
var ig_workers = require('../../workers/instagram/index');
var fb_workers = require('../../workers/facebook/');
var yt_workers = require('../../workers/youtube/');
var db = require('../../helpers/db');


module.exports = {
    /**
     * Updates the Engagement rate of a profile by calculating engagement rate for each platform
     * @author NarayaN Yaduvanshi
     * @param   {object}   profile [[Description]]
     * @returns {[[Type]]} [[Description]]
     */
    update_engagement_rate: function (profile) {

        var update_promises = [];

        if (profile.instagram_id) {
            update_promises.push(ig_workers.users.update_engagement_rate(profile.instagram_id));

        }
        if (profile.facebook_id) {
            update_promises.push(fb_workers.user.update_engagement_rate(profile.facebook_id));
        }

        if (profile.youtube_id) {
            update_promises.push(yt_workers.users.update_engagement_rate(profile.youtube_id));
        }

        return Q.all(update_promises);
    },
    /**
     * It updates the growth of each influencer and saves it in their table for fast access
     * @author NarayaN Yaduvanshi
     * @param {object} profile Contains the whole info of this profile
     */
    update_growth: function (profile) {
        return db.query("SELECT `likes` FROM `facebook_insights` WHERE `user_id` = ? AND `date` = DATE(DATE_SUB(NOW(), INTERVAL 1 MONTH)) ; SELECT `followed_by` FROM `instagram_daily_count` WHERE `instagram_id` = ? AND `date` = DATE(DATE_SUB(NOW(), INTERVAL 1 MONTH)) ; SELECT `subscriber_count` FROM `youtube_insights` WHERE `id` LIKE ? AND `date` = DATE(DATE_SUB(NOW(), INTERVAL 1 MONTH))", [profile.facebook_id, profile.instagram_id, profile.youtube_id]).then(function (result) {
            var fb_growth_update = [];
            var ig_growth_update = [];
            var yt_growth_update = [];

            if (result[0][0]) {
                fb_growth_update = db.update('facebook', {
                    growth: (((profile.facebook_likes - result[0][0].likes) * 100) / profile.facebook_likes)
                }, {
                    id: profile.facebook_id
                });
            }
            if (result[1][0].followed_by) {
                ig_growth_update = db.update('instagram', {
                    growth: (((profile.ig_followed_by - result[1][0].followed_by) * 100) / profile.ig_followed_by)
                }, {
                    id: profile.instagram_id
                });
            }
            if (result[2][0]) {
                yt_growth_update = db.update('youtube', {
                    growth: (((profile.youtube_subscriber_count - result[2][0].subscriber_count) * 100) / profile.youtube_subscriber_count)
                }, {
                    id: profile.youtube_id
                });
            }
            return Q.all([fb_growth_update, ig_growth_update, yt_growth_update]);
        }, function (err) {
            throw err;
        });
    },
    /**
     * Validate the input to enter data of Profile
     * @author Jatin Seth
     * @param   {object}   profile params
     * @returns erors or true
     */
    validate_profile_input: function (input, done) {
        var errors = {};
        var promises = [];

        // name validation...
        if (!input.name || input.name === '') {
            errors.name = "Name is required";
        }

        // kind validation...
        if (!input.kind || input.kind === '') {
            errors.kind = "Profile kind is required";
        }

        // gender validation...
        if (!input.gender || input.gender === '') {
            input.error.gender = "Profile gender is required";
        }

        if ((input.age_min && !input.age_max) || (input.age_max && !input.age_min) || (input.age_min && isNaN(input.age_min)) || (input.age_max && isNaN(input.age_max)) || (input.age_min && input.age_max && input.age_min >= input.age_max)) {
            errors.age_min = "Please provide valid audience age range.";
        }

        if (!input.instagram && !input.facebook && !input.youtube) {
            errors.socialmedia = "Please provide atleast one social media link.";
        } else {
            // check if instagram already exist or not.
            if (input.instagram) {
                promises.push(db.selectOne('profiles', {
                    instagram: input.instagram
                }).then(function (result) {
                    if (result) {
                        errors.instagram = "The instagram you provided is already exist.";
                    }
                    return true;
                }, function (err) {
                    done(err);
                }));
            }

            // check if FB already exist or not.
            if (input.facebook) {
                promises.push(db.selectOne('profiles', {
                    facebook: input.facebook
                }).then(function (result) {
                    if (result) {
                        errors.facebook = "The facebook you provided is already exist.";
                    }
                    return true;
                }, function (err) {
                    done(err);
                }));
            }

            // check if YT already exist or not.
            if (input.youtube) {
                promises.push(db.selectOne('profiles', {
                    youtube: input.youtube
                }).then(function (result) {
                    if (result) {
                        errors.youtube = "The youtube you provided is already exist.";
                    }
                    return true;
                }, function (err) {
                    done(err);
                }));
            }
        }

        Q.all(promises).then(function (data) {
            if (Object.keys(errors).length > 0) {
                done(errors);
            }
            done();
        }, function (err) {
            done(err);
        });
    }
};