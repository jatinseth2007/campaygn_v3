/* jshint node: true */
"use strict";

var moment = require('moment');

module.exports = {
    prepare_condition: function (req) {
        var self = this;

        var profile_kind_query = req.query.profile_kind;
        var active_platform = req.query.active_platform;
        var search_keyword = req.query.search_keyword;
        var search_countries = req.query.search_countries;
        var search_continents = req.query.search_continents;
        var search_gender = req.query.search_gender;
        var search_tags = req.query.search_tags;
        var search_themes = req.query.search_themes;
        var search_languages = req.query.search_languages;
        var min_range = req.query.min_range;
        var max_range = req.query.max_range;
        var min_engagement = req.query.min_engagement;
        var max_engagement = req.query.max_engagement;
        var min_age = req.query.min_age;
        var max_age = req.query.max_age;
        var min_growth = req.query.min_growth;
        var max_growth = req.query.max_growth;
        var where_condition = 'WHERE  1 ';

        /**
         * Need to filter results based on active platform for eg. show only those who are active for FB
         */
        if (active_platform) {
            if (active_platform === 'all') { // when we need to show all records; so nothing need to done
                // do nothing ...    
            } else if (active_platform === 'instagram') { // only instagram valid records
                where_condition += ' AND `a`.`instagram_id` IS NOT NULL ';
            } else if (active_platform === 'facebook') { // only facebook valid records
                where_condition += ' AND `a`.`facebook_id` IS NOT NULL ';
            } else if (active_platform === 'twitter') { // only twitter valid records
                // will add conditions later
            } else if (active_platform === 'youtube') { // only facebook valid records
                where_condition += ' AND `a`.`youtube_id` IS NOT NULL ';
            }
        } //EO-IF

        /**
         *  it's a ternary condition to add condition based on the profile type
         */
        where_condition += (profile_kind_query === 'all') ? '' : " AND `a`.`profile_kind` = '" + profile_kind_query + "' ";
        where_condition += !(search_keyword) ? '' : " AND (`a`.`profile_name` LIKE '%" + search_keyword + "%' OR `a`.`profile_tags` LIKE '%" + search_keyword + "%' OR `a`.`profile_url` LIKE '%" + search_keyword + "%') ";
        where_condition += !(search_countries) ? '' : " AND (`a`.`profile_country` IN ('" + search_countries.replace(",", "','") + "'))  ";
        where_condition += !(search_gender) ? '' : " AND (`a`.`profile_gender` IN ('" + search_gender.replace(",", "','") + "'))  ";
        where_condition += !(search_continents) ? '' : " AND (`a`.`profile_continent` IN ('" + search_continents.replace(",", "','") + "'))  ";

        /**
         * Need to add tags in search conditions
         */
        if (search_tags) {
            where_condition += " AND ( ";
            var tags_arr = search_tags.split(",");
            tags_arr.forEach(function (element, index) {

                where_condition += " FIND_IN_SET ('" + element + "' , `a`.`profile_tags`) ";

                if (index == (tags_arr.length - 1)) {
                    where_condition += " ) ";
                } else {
                    where_condition += " AND  ";
                }
            });
        }

        /**
         * Need to add themes in search conditions
         */
        if (search_themes) {
            where_condition += " AND ( ";
            var themes_arr = search_themes.split(",");
            themes_arr.forEach(function (element, index) {

                where_condition += " FIND_IN_SET ('" + element + "' , `a`.`profile_themes`) ";

                if (index == (themes_arr.length - 1)) {
                    where_condition += " ) ";
                } else {
                    where_condition += " AND  ";
                }
            });
        }

        /**
         * Need to add languages in search conditions
         */
        if (search_languages) {
            where_condition += " AND ( ";
            var languages_arr = search_languages.split(",");
            languages_arr.forEach(function (element, index) {

                where_condition += " FIND_IN_SET ('" + element + "' , `a`.`profile_languages`) ";

                if (index == (languages_arr.length - 1)) {
                    where_condition += " ) ";
                } else {
                    where_condition += " AND  ";
                }
            });
        }
        /**
         * Need to add min and max range
         */
        if (!isNaN(min_range) && !isNaN(max_range)) {
            // need to fetch range factor...
            var range_factor = self.get_reachfactor_by_platform(active_platform);

            where_condition += " AND (`a`.`" + range_factor + "` BETWEEN " + min_range + " AND " + max_range + ")";
        }

        /**
         * Need to add min and max engagement rate
         */
        if (!isNaN(min_engagement) && !isNaN(max_engagement)) {
            // need to fetch range factor...
            var engagement_factor = self.get_engagementfactor_by_platform(active_platform);

            where_condition += " AND (`a`.`" + engagement_factor + "` BETWEEN " + min_engagement + " AND " + max_engagement + ")";
        }

        /**
         * Need to add min and max GROWTH
         */
        if (!isNaN(min_growth) && !isNaN(max_growth)) {
            // need to fetch range factor...
            var growth_factor = self.get_growthfactor_by_platform(active_platform);

            where_condition += " AND (`a`.`" + growth_factor + "` BETWEEN " + min_growth + " AND " + max_growth + ")";
        }

        /**
         * Need to add min and max age rate
         */
        if (!isNaN(min_age) && !isNaN(max_age)) {
            where_condition += " AND a.`profile_age_min` >= " + min_age + " AND a.`profile_age_max` <= " + max_age;
        }

        return where_condition;
    },
    prepare_query_pagination: function (req, where_condition, count_query) {
        var self = this;

        var sort_by = req.query.sort_by;
        var active_platform = req.query.active_platform;
        var sort_order = (req.query.sort_order) ? req.query.sort_order : 'DESC';
        var query = "";

        /**
         * Need to select the sort by param
         */
        if (!sort_by) {
            sort_by = self.get_reachfactor_by_platform(active_platform);
        } //EO-IF

        // this is to pick the fields differently for count query and actual query...
        query += (count_query) ? "SELECT count(a.profile_id) as total " : "SELECT a.`profile_id`, a.`profile_slug`, a.`instagram_id`,a.`facebook_id`,a.`youtube_id`, a.`profile_name`, a.`profile_country`, a.`profile_url`, a.`total_engagement_rate`, a.`total_followers`, a.`ig_followed_by`, a.`facebook_likes`, a.`youtube_subscriber_count`, a.`profile_tags`, a.`ig_profile_picture`, a.`ig_media`, a.`instagram_engagement_rate`, a.`facebook_picture`, a.`facebook_engagement_rate`, a.`youtube_profile_picture`, a.`youtube_engagement_rate`, a.`youtube_video_count`, a.`youtube_view_count`, a.`ig_growth`, a.`fb_growth`, a.`yt_growth`, a.`total_growth`";

        // this is to add WHERE in query...
        query += " FROM profile_view a " + (where_condition) + "  " + ((count_query) ? ';' : ' ');

        // add order by info in query...
        query += ((count_query) ? " " : ("ORDER BY " + sort_by + " " + sort_order + "  "));

        return query;
    },
    get_reachfactor_by_platform: function (active_platform) {
        // need to check the platform and select range accrodingly...
        switch (active_platform) {
            case 'all':
                return 'total_followers';
            case 'instagram':
                return 'ig_followed_by';
            case 'facebook':
                return 'facebook_likes';
            case 'youtube':
                return 'youtube_subscriber_count';
            default:
                throw 'Wrong Platform Provided :- ' + active_platform;
        }
    },
    get_engagementfactor_by_platform: function (active_platform) {
        // need to check the platform and select engagement factor accrodingly...
        switch (active_platform) {
            case 'all':
                return 'total_engagement_rate';
            case 'instagram':
                return 'instagram_engagement_rate';
            case 'facebook':
                return 'facebook_engagement_rate';
            case 'youtube':
                return 'youtube_engagement_rate';
            default:
                throw 'Wrong Platform Provided :- ' + active_platform;
        }
    },
    get_growthfactor_by_platform: function (active_platform) {
        // need to check the platform and select growth factor accrodingly...
        switch (active_platform) {
            case 'all':
                return 'total_growth';
            case 'instagram':
                return 'ig_growth';
            case 'facebook':
                return 'fb_growth';
            case 'youtube':
                return 'yt_growth';
            default:
                throw 'Wrong Platform Provided :- ' + active_platform;
        }
    }
};
