/* jshint node: true */
"use strict";

var db = require('../helpers/db');
var Q = require('q');
var _ = require("lodash");
var _common_workers = require('./_common');
var analytics = require("./analytics");

module.exports = {

    /**
     * Used to create monitors and tags relationship
     * @author karthick.k
     * @param   {Array}    tags       list of selected tags
     * @param   {number} monitor_id saved monitor id
     * @returns {string} success/error message
     */
    create_monitors_tags_relationship: function (tags, monitor_id) {
        var monitors_tags = [];
        //Remove duplicates
        tags = _.uniqBy(tags, 'id');
        //Creating monitor tag relationship
        tags.forEach(function (tag) {
            monitors_tags.push({
                "monitor_id": monitor_id,
                tag_id: tag.id
            });
        });

        //Inserting monitor tag relationship table
        return db.insert("monitors_tags", monitors_tags);
    },

    /**
     * Used to create monitors and lists relationship
     * @author karthick.k
     * @param   {Array}    lists       lists of selected list
     * @param   {number} monitor_id saved monitor id
     * @returns {string} success/error message
     */
    create_monitors_lists_relationship: function (params, monitor_id) {
        if (!params.lists || params.lists.length === 0) {
            return;
        }
        var monitors_lists = [];
        var lists = params.lists || [];
        //Remove duplicates
        lists = _.uniqBy(lists, 'id');

        lists.forEach(function (list) {
            monitors_lists.push({
                monitor_id: monitor_id,
                list_id: list.id
            });
        });
        //Inserting monitor list relationship table
        return db.insert("monitors_lists", monitors_lists);
    },

    /**
     * Used to create monitors and users relationship
     * @author karthick.k
     * @param   {object}   req   request object
     * @param   {number} monitor_id saved monitor id
     * @returns {string} success/error message
     */
    create_monitors_users_relationship: function (req, monitor_id) {
        var monitors_users = [];
        var users = (req.body.users && req.body.users.length > 0) ? req.body.users : [];
        //Including logged in user
        if (req && req.user && req.user.id) {
            users.push({
                id: req.user.id
            });
        }
        //Remove duplicates
        users = _.uniqBy(users, 'id');
        // Creating monitor tag relationship
        users.forEach(function (user) {
            monitors_users.push({
                monitor_id: monitor_id,
                user_id: user.id
            });
        });
        //Inserting monitor user relationship table
        return db.insert("monitors_users", monitors_users);
    },

    /**
     * Used to create monitors and tags influencers
     * @author karthick.k
     * @param   {req}    req request object
     * @param   {number} monitor_id saved monitor id
     * @returns {string} success/error message
     */
    create_monitors_influencers_relationship: function (influencers, monitor_id) {
        if (!influencers.individual_influencers || influencers.individual_influencers.length === 0) {
            return;
        }
        var monitors_influencers = [];
        var individual_influencers = influencers.individual_influencers || [];
        //Remove duplicates
        individual_influencers = _.uniqBy(individual_influencers, 'id');
        // Creating monitor tag relationship
        individual_influencers.forEach(function (influencer) {
            monitors_influencers.push({
                monitor_id: monitor_id,
                influencer_id: influencer.id,
                from_list: "N"
            });
        });
        //Inserting monitor influencer relationship table
        return db.insert("monitors_influencers", monitors_influencers);
    },

    /**
     * Used to search tag based on the keyword
     * @author karthick.k
     * @param   {string} tag search keyword
     * @returns {array} tags List of matched tags
     */
    search_tag: function (tag) {
        var select_query = "SELECT ??, ?? FROM ?? WHERE tag LIKE ? LIMIT 5";
        return db.query(select_query, ["id", "tag", "tags", tag + "%"]);
    },

    /**
     * Used to search group based on the keyword
     * @author karthick.k
     * @param   {string} search keyword
     * @returns {array} group name of matched keyword
     */
    search_group: function (keyword) {
        var select_query = "SELECT distinct ?? FROM ?? WHERE ?? LIKE ? LIMIT 5";
        return db.query(select_query, ["group_name", "monitors", "group_name", keyword + "%"]);
    },

    /**
     * Used to search users based on the search keyword
     * @author karthick.k
     * @param   {object} request_query request query object
     * @returns {array} users List of matched user
     */
    search_users: function (req) {
        var select_query = "SELECT ??, ?? FROM ?? WHERE (email LIKE ?  OR firstname LIKE ? OR lastname LIKE ?) AND id <> ? LIMIT 5";
        var user_id = (req && req.user && req.user.id) ? req.user.id : 0;
        var user_name = req.query.user_name;
        return db.query(select_query, ['email', 'id', "users", user_name + '%', user_name + '%', user_name + '%', user_id]);
    },

    /**
     * Used to search influencers based on the search keyword
     * @author karthick.k
     * @param   {object} request_query request query object
     * @returns {array} influencers List of matched influencers
     */
    search_individual_influencers: function (profile_name) {
        var select_query = "SELECT ?? AS id, ?? AS name, ?? AS profile_picture FROM ?? WHERE profile_name LIKE ? LIMIT 5";
        return db.query(select_query, ['profile_id', 'profile_name', 'ig_profile_picture', "profile_view", profile_name + '%']);
    },

    /**
     * Used to validate form fields
     * @author karthick.k
     * @param   {object}   req     request object, query
     * @returns {string} error/success message
     */
    validate_create_monitor_form: function (req) {
        if (!req || !req.body) {
            throw new Error("Monitor information is missing");
        }
        if (!req.body.group) {
            throw new Error("Monitor group is missing");
        }
        if (!req.body.name) {
            throw new Error("Monitor name is missing");
        }
        if (!req.body.from || !req.body.to) {
            throw new Error("Monitor date is missing");
        }
        if (req.body.from && req.body.to) {
            var from = new Date(req.body.from).getTime();
            var to = new Date(req.body.to).getTime();
            if (from > to) {
                throw "Monitor from date is greater than to date, Please enter dates correctly";
            }
        }
        if (!req.body.tags || req.body.tags.length === 0) {
            throw new Error("Monitor tags are  missing");
        }
        if ((!req.body.individual_influencers || req.body.individual_influencers.length === 0) && (!req.body.lists || req.body.lists.length === 0)) {
            throw new Error("Monitor influencers are  missing");
        }
        return true;
    },

    /**
     * Used to create new monitor
     * @author karthick.k
     * @param {object} request object, with monitor form data
     * @returns {string} success/error message
     */
    create_monitor: function (req) {
        var scope = this;
        return Q.fcall(scope.validate_create_monitor_form, req)
            .then(function () {
                return db.beginTransaction()
                    .then(function () {
                        var monitor = {
                            name: req.body.name,
                            from: req.body.from,
                            to: req.body.to,
                            group_name: req.body.group
                        };
                        return db.save("monitors", monitor);
                    })
                    .then(function (inserted_monitor) {
                        var monitor_id = inserted_monitor.insertId;
                        var promises = [
                            scope.create_monitors_tags_relationship(req.body.tags, monitor_id),
                            scope.create_monitors_users_relationship(req, monitor_id),
                            scope.create_monitors_influencers_relationship(req.body, monitor_id),
                            scope.create_monitors_lists_relationship(req.body, monitor_id)
                        ];
                        return Q.all(promises);
                    })
                    .then(function (inserted_monitor) {
                        //Analytics - register monitor creation
                        analytics.register_event("monitor", "added", "monitor_name", req.body.name, req.user.email, req.user.fullname);
                        return db.commit();
                    })
                    .catch(function (err) {
                        db.rollback();
                        throw new Error("Error while saving monitor, Please retry");
                    });
            }).catch(function (err) {
                throw err;
            });
    },

    /**
     * Used to fetch monitors 
     * @author karthick.k
     * @returns {array} monitor list 
     */
    get_monitors_list: function (req) {
        var select_query = {
            table: 'monitors_influencers_view'
        };
        var filter = {
            user_id: req.user.id
        };
        return db.select(select_query, filter);
    },

    /**
     * Used to delete monitor 
     * @author karthick.k
     * @param {object} req request object
     * @returns {string} success/error message 
     */
    delete_monitor: function (req) {
        return db.beginTransaction()
            .then(function () {
                var monitor_delete_filter = {
                    id: req.query.monitor_id
                };
                return db.delete("monitors", monitor_delete_filter);
            })
            .then(function () {
                var filter = {
                    monitor_id: req.query.monitor_id
                };
                return Q.all([db.delete("monitors_users", filter),
                          db.delete("monitors_influencers", filter),
                          db.delete("monitors_tags", filter)]);
            })
            .then(function () {
                //Analytics - register monitor deletion
                analytics.register_event("monitor", "deleted", "monitor_id", req.query.monitor_id, req.user.email, req.user.fullname);
                return db.commit();
            })
            .catch(function (err) {
                db.rollback();
                throw new Error("Error while deleting monitor, Please retry");
            });
    },

    /**
     * Used to fetch monitor details
     * @author karthick.k
     * @param {object} req   request object with query parameters
     * @returns {string} monitor details
     */
    get_monitor_details: function (req) {
        var monitor_id = req.query.monitor_id;
        var filter = {
            id: monitor_id
        };
        if (req.user && !!req.user.id) {
            filter.user_id = req.user.id;
        }
        return Q.all([db.select("monitors_influencers_view", filter), db.select("monitors_tags_view", {
            id: monitor_id
        }), db.select("monitors_users_view", {
            id: monitor_id
        })]);
    },

    /**
     * Used to modify from or to date of monitor
     * @author karthick.k
     * @param {object} monitor monitor id, from , to date
     * @returns {string} success/error message
     */
    modify_date: function (monitor) {
        var filter = {
            id: monitor.id
        };
        var value = (monitor.is_from_date === true || monitor.is_from_date === "true") ? {
            from: monitor.from,
            updated_at: new Date()
        } : {
            to: monitor.to,
            updated_at: new Date()
        };
        return db.update('monitors', value, filter);
    },

    /**
     * Used to modify name
     * @author karthick.k
     * @param {object} monitor monitor id, name
     * @returns {string} success/error message
     */
    modify_name: function (monitor) {
        var filter = {
            id: monitor.id
        };
        var value = {
            name: monitor.name,
            updated_at: new Date()
        };
        return db.update('monitors', value, filter);
    },

    /**
     * Used to modify group name
     * @author karthick.k
     * @param {object} monitor monitor id, group name
     * @returns {string} success/error message
     */
    modify_group_name: function (monitor) {
        var filter = {
            id: monitor.id
        };
        var value = {
            group_name: monitor.group_name,
            updated_at: new Date()
        };
        return db.update('monitors', value, filter);
    },

    /**
     * Used to fetch the monitor post details
     * @author karthick.k
     * @param   {number} monitor_id monitor id
     * @returns {string} success/error message
     */
    get_monitor_posts_details: function (req, monitor_id, influencer_id, tag_id) {
        var filter = {
            id: monitor_id
        };
        return db.selectOne("monitors", filter).then(function (data) {
            //Analytics - register moniotr open
            if (req.user && req.user.hasOwnProperty("email")) {
                analytics.register_event("monitor", "opened", "monitor_id", monitor_id, req.user.email, req.user.fullname);
            } else {
                analytics.register_event("monitor", "opened", "monitor_id", monitor_id, "unknown", "unknown");
            }
            var query = "SELECT * FROM ?? WHERE ?? = ? AND ?? BETWEEN UNIX_TIMESTAMP(?) AND UNIX_TIMESTAMP(?)";
            var parameters = ["monitor_id", monitor_id, "created_time", data.from, data.to];
            if (!!influencer_id && !!tag_id) {
                query = "SELECT * FROM ?? WHERE ?? = ? AND ?? BETWEEN UNIX_TIMESTAMP(?) AND UNIX_TIMESTAMP(?) AND ?? = ? AND ?? = ?";
                parameters = parameters.concat(["influencer_id", influencer_id, "tag_id", tag_id]);
            } else if (influencer_id !== undefined) {
                query = "SELECT * FROM ?? WHERE ?? = ? AND ?? BETWEEN UNIX_TIMESTAMP(?) AND UNIX_TIMESTAMP(?) AND ?? = ?";
                parameters = parameters.concat(["influencer_id", influencer_id]);
            } else if (tag_id !== undefined) {
                query = "SELECT * FROM ?? WHERE ?? = ? AND ?? BETWEEN UNIX_TIMESTAMP(?) AND UNIX_TIMESTAMP(?) AND ?? = ?";
                parameters = parameters.concat(["tag_id", tag_id]);
            }
            var facebook = ["monitors_facebook_posts_view"].concat(parameters);
            var instagram = ["monitors_instagram_posts_view"].concat(parameters);
            var youtube = ["monitors_youtube_videos_view"].concat(parameters);
            return Q.all([
                db.query(query, facebook),
                db.query(query, instagram),
                db.query(query, youtube)
            ]);
        });
    },

    /**
     * Used to fetch monitor influencers followers details
     * @author karthick.k
     * @param   {number} monitor_id monitor id
     * @returns {array} followers count
     */
    get_monitor_influencers_followers_details: function (monitor_id, influencer_id) {
        var filter = {
            monitor_id: monitor_id
        };
        if (!!influencer_id) {
            filter.influencer_id = influencer_id;
        }
        return db.select("monitors_influencers_followers_view", filter);
    },

    /**
     * Used to modify influencers
     * @author karthick.k
     * @throws {Error} Error is thrown if there is any error while dropping or insert influencers
     * @param   {object}   request_obj influencers information and monitor id
     * @returns {object} error/success message
     */
    modify_influencers: function (request_obj) {
        var scope = this;
        var filter = {
            monitor_id: request_obj.monitor_id
        };
        return db.beginTransaction()
            .then(function () {
                return Q.all([
                    db.delete("monitors_influencers", filter),
                    scope.create_monitors_influencers_relationship({
                        individual_influencers: request_obj.influencers
                    }, request_obj.monitor_id)
                ]);
            })
            .then(function (inserted_influencers) {
                return db.commit();
            })
            .catch(function (err) {
                db.rollback();
                throw new Error("Error while updating influencers, Please retry");
            });
    },

    /**
     * Used to modify tags
     * @author karthick.k
     * @throws {Error} Error is thrown if there is any error while dropping or insert tags
     * @param   {object}   request_obj tag information and monitor id
     * @returns {object} error/success message
     */
    modify_tags: function (request_obj) {
        var scope = this;
        var filter = {
            monitor_id: request_obj.monitor_id
        };
        return db.beginTransaction()
            .then(function () {
                return Q.all([
                    db.delete("monitors_tags", filter),
                    scope.create_monitors_tags_relationship(request_obj.tags, request_obj.monitor_id)
                ]);
            })
            .then(function (inserted_tags) {
                return db.commit();
            })
            .catch(function (err) {
                db.rollback();
                throw new Error("Error while updating tags, Please retry");
            });
    },

    /**
     * Used to clear cache for set of api
     * @author karthick.k
     * @param {number} monitor_id monitor id
     */
    clear_monitor_api_cache: function (monitor_id) {
        _common_workers.clear_cache("/api/monitors/monitor_details?monitor_id=" + monitor_id);
        _common_workers.clear_cache("/api/monitors/monitor_influencers_followers_details?monitor_id=" + monitor_id);
        _common_workers.clear_cache("/api/monitors/monitor_posts_details?monitor_id=" + monitor_id);
        _common_workers.clear_cache("/api/monitors/list");
    },

    /**
     * Used to check whether logged in user is allowed to access this monitor
     * @author karthick.k
     * @param   {object}   req request_obj
     * @returns {object} success/error
     */
    is_allowed: function (req) {
        var select_query = {
            table: 'monitors_users'
        };
        var filter = {
            monitor_id: req.query.monitor_id
        };
        if (req.user && !!req.user.id) {
            filter.user_id = req.user.id;
        }
        return db.select(select_query, filter);
    },

    /**
     * Used to modify users
     * @author karthick.k
     * @throws {Error} Error is thrown if there is any error while dropping or insert users
     * @param   {object}   request_obj user information and monitor id
     * @returns {object} error/success message
     */
    modify_users: function (req) {
        var scope = this;
        var filter = {
            monitor_id: req.body.monitor_id
        };
        return db.beginTransaction()
            .then(function () {
                return Q.all([
                    db.delete("monitors_users", filter),
                    scope.create_monitors_users_relationship(req, filter.monitor_id)
                ]);
            })
            .then(function (inserted_users) {
                return db.commit();
            })
            .catch(function (err) {
                db.rollback();
                throw new Error("Error while updating users, Please retry");
            });
    },

    /**
     * Used to search lists based on the keyword
     * @author karthick.k
     * @param   {string} list search keyword
     * @returns {array} lists List of matched list
     */
    search_lists: function (list) {
        var select_query = "SELECT ??, ?? FROM ?? WHERE name LIKE ? LIMIT 5";
        return db.query(select_query, ["id", "name", "lists", list + "%"]);
    }
};