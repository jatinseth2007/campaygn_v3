/* jshint node: true */
"use strict";

var db = require('../helpers/db');
var ig_workers = require('../workers/instagram/index');
var _common_workers = require('../workers/_common');

module.exports = function (app) {

    /**
     * Find a user on instgram
     */
    app.get('/api/instagram/find_user/:keyword', function (req, res) {
        ig_workers.users.search_users(req.params.keyword, 9).then(function (users) {
            _common_workers.send_res(req, res, users);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Get Instagram user complete details
     */
    app.get('/instagram/user/:user_id', function (req, res) {
        ig_workers.users.get_user_details(req.params.user_id).then(function (user) {
            _common_workers.send_res(req, res, user);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    //Getting growth
    app.get('/api/instagram/get_growth/:instagram_id', function (req, res) {
        ig_workers.users.get_growth(req.params.instagram_id).then(function (data) {
            var data_to_send = {};

            if (data) {
                // add the 1 day diff data
                data_to_send.one_day_diff = (typeof data[0] !== 'undefined' && data[0].length > 0) ? data[0] : {};

                //add 7 days diff data
                data_to_send.seven_day_diff = (typeof data[1] !== 'undefined' && data[1].length > 0) ? data[1] : {};

                //add 30 days diff data
                data_to_send.thirty_day_diff = (typeof data[2] !== 'undefined' && data[2].length > 0) ? data[2] : {};

                // add data from MAX(date) to MIN(date) we have from last 6 Months
                data_to_send.avg_stats = (typeof data[3] !== 'undefined' && data[3].length > 0) ? data[3] : {};
            }

            _common_workers.send_res(req, res, data_to_send);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    //Getting hashtag growth for brands
    app.get('/api/instagram/get_hashtagposts_growth/:instagram_id', function (req, res) {

        db.query("SELECT `id` FROM `tags` WHERE `tag` = (SELECT username FROM `instagram` WHERE	id = '" + req.params.instagram_id + "' ) LIMIT 0,1;").then(function (hashtag_result) {
            if (!hashtag_result || hashtag_result.length <= 0) {
                res.status(404).send("No Result found");
            } else {
                if (hashtag_result[0].id) {
                    ig_workers.tags.get_growth(hashtag_result[0].id).then(function (data) {
                        var data_to_send = {};

                        if (data) {
                            // add the 1 day diff data
                            data_to_send.one_day_diff = (typeof data[0] !== 'undefined' && data[0].length > 0) ? data[0] : {};

                            //add 7 days diff data
                            data_to_send.seven_day_diff = (typeof data[1] !== 'undefined' && data[1].length > 0) ? data[1] : {};

                            //add 30 days diff data
                            data_to_send.thirty_day_diff = (typeof data[2] !== 'undefined' && data[2].length > 0) ? data[2] : {};

                            // add data from MAX(date) to MIN(date) we have from last 6 Months
                            data_to_send.avg_stats = (typeof data[3] !== 'undefined' && data[3].length > 0) ? data[3] : {};
                        }

                        _common_workers.send_res(req, res, data_to_send);
                    }, function (err) {
                        _common_workers.send_err(req, res, err.statusCode, err);
                    });
                } else {
                    _common_workers.send_err(req, res, 404, "No Hashtag found");
                }
            }
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    //Get random images of instagram
    app.get('/api/instagram/get_random_images/:instagram_id/:count', function (req, res) {

        var query = " " +
            "   SELECT     `image`,`link`, `type`                 " +
            "   FROM       (                            " +
            "                   SELECT     `images_low_resolution_url` as image,`link`, `type`  " +
            "                   FROM       `instagram_posts`                            " +
            "                   WHERE      `user_id` = " + req.params.instagram_id +
            "                   ORDER BY    `id` DESC                                        " +
            "                   LIMIT       0," + (parseInt(req.params.count) + 10) +
            "               )as X                                                           " +
            "  ORDER BY     RAND()                                                           " +
            "  LIMIT        0," + req.params.count + ";";

        db.query(query).then(function (result) {
            if (!result)
                _common_workers.send_err(req, res, 404, "No Result found");
            else
                _common_workers.send_res(req, res, result);

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });
    //Get recent images of a profile.
    app.get('/api/instagram/get_recent_images/:instagram_id/:count', function (req, res) {

        db.query("SELECT `images_low_resolution_url` as image,`link`,`likes_count`, `comments_count`, `type` FROM `instagram_posts` WHERE `user_id` = " + req.params.instagram_id + " ORDER BY `id` DESC LIMIT " + req.params.count + ";").then(function (result) {
            if (!result)
                _common_workers.send_err(req, res, 404, "No Result found");
            else
                _common_workers.send_res(req, res, result);

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });
    //Get average number of likes for user.
    app.get('/api/instagram/get_user_average_likes/:instagram_id', function (req, res) {

        db.query("SELECT AVG(likes_count) as averageNumber FROM (SELECT likes_count FROM `instagram_posts` WHERE `user_id` = " + req.params.instagram_id + " ORDER BY `id` DESC LIMIT 0,10) as x;")
            .then(function (result) {
                if (!result)
                    _common_workers.send_err(req, res, 404, "No Result found");
                else
                    _common_workers.send_res(req, res, result);

            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });
    //Get user's most liked post
    app.get('/api/instagram/get_user_most_liked/:instagram_id', function (req, res) {

        var query = "SELECT `images_low_resolution_url` as image,`link`,`likes_count`, `type` FROM `instagram_posts` WHERE `user_id` = " + req.params.instagram_id + " ORDER BY `instagram_posts`.`likes_count` DESC LIMIT 1";

        db.query(query)
            .then(function (result) {
                if (!result)
                    _common_workers.send_err(req, res, 404, "No Result found");
                else
                    _common_workers.send_res(req, res, result);

            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });
};
