/* jshint node: true */
"use strict";

var Q = require("q");
var _ = require("lodash");
var moment = require("moment");
var db = require('../helpers/db');
var config = require('../../config');
var _common_workers = require('../workers/_common');
var tag_worker = require('../workers/tag');

module.exports = function (app) {
    /**
     * return the profiles list
     */
    app.get('/api/tag/:tag', function (req, res) {
        db.selectOne("tags", {
            tag: req.params.tag
        }).then(function (tag_details) {

            if (!tag_details) {
                _common_workers.send_err(req, res, 400, "Tag not found");
            }

            /**
             * Finidng the total post of each platform
             */

            var six_month = moment().subtract(6, 'months').unix();
            var total_posts = db.one("SELECT " +
                "`a`.`tag`,a.`id` as `tag_id`, (IFNULL(`x`.`fb_posts_count`,0) +IFNULL(`y`.`ig_posts_count`,0)+IFNULL(`z`.`yt_videos_count`,0)) AS `total_posts` " +
                "FROM 		`tags`	`a` " +

                "LEFT JOIN	( " +
                "SELECT		`tag_id`,COUNT(`post_id`) as `fb_posts_count` " +
                "FROM		`facebook_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`x` ON (`a`.`id` = `x`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		`tag_id`,COUNT(`post_id`) as `ig_posts_count` " +
                "FROM		`instagram_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`y` ON (`a`.`id` = `y`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		`tag_id`,COUNT(`video_id`) as `yt_videos_count` " +
                "FROM		`youtube_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `video_published_at` > ? " +
                ")`z` ON (`a`.`id` = `z`.`tag_id`) " +
                "WHERE 		`id` = ? ", [tag_details.id, six_month, tag_details.id, six_month, tag_details.id, six_month, tag_details.id]);

            /**
             * Total reach
             */
            var total_reach = db.one("SELECT " +
                "(IFNULL(x.fb_users_likes,0)+ IFNULL(y.ig_users_likes,0)+ IFNULL(z.yt_view_count,0)) as total_reach " +
                "FROM 		`tags`	`a` " +

                "LEFT JOIN	( " +
                "SELECT		DISTINCT(`user_id`), (SUM(`user_likes`)*" + config.facebook.reach_constant + ") as fb_users_likes ,`tag_id` " +
                "FROM		`facebook_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`x` ON (`a`.`id` = `x`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		DISTINCT(`user_id`), (SUM(`user_followed_by`)*" + config.instgram.reach_constant + ") as ig_users_likes,`tag_id` " +
                "FROM		`instagram_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`y` ON (`a`.`id` = `y`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		SUM(`user_view_count`) as `yt_view_count`,`tag_id` " +
                "FROM		`youtube_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `video_published_at` > ? " +
                ")`z` ON (`a`.`id` = `z`.`tag_id`) " +
                "WHERE 		`id` = ? ", [tag_details.id, six_month, tag_details.id, six_month, tag_details.id, six_month, tag_details.id]);

            /**
             * Total comments,likes and shares (interactions)
             */
            var total_interactions = db.one("SELECT " +
                "(IFNULL(`x`.`fb_post_comments_count`,0)+ IFNULL(`y`.`ig_post_comments_count`,0)+ IFNULL(`z`.`yt_video_comment_count`,0)+IFNULL(`x`.`fb_post_likes_count`,0)+ IFNULL(`y`.`ig_post_likes_count`,0)+ IFNULL(`z`.`yt_video_like_count`,0)+ IFNULL(`x`.`fb_post_shares_count`,0)) as `total_interactions` " +
                "FROM 		`tags`	`a` " +

                "LEFT JOIN	( " +
                "SELECT		SUM(`post_comments_count`) AS `fb_post_comments_count`,SUM(`post_likes_count`) AS `fb_post_likes_count`,SUM(`post_shares_count`) AS `fb_post_shares_count`,`tag_id` " +
                "FROM		`facebook_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`x` ON (`a`.`id` = `x`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		SUM(`post_comments_count`) AS `ig_post_comments_count`,SUM(`post_likes_count`) AS `ig_post_likes_count`,`tag_id` " +
                "FROM		`instagram_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`y` ON (`a`.`id` = `y`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		SUM(`video_comment_count`) AS `yt_video_comment_count`,SUM(`video_like_count`) AS `yt_video_like_count`,`tag_id` " +
                "FROM		`youtube_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `video_published_at` > ? " +
                ")`z` ON (`a`.`id` = `z`.`tag_id`) " +
                "WHERE 		`id` = ? ", [tag_details.id, six_month, tag_details.id, six_month, tag_details.id, six_month, tag_details.id]);


            /**
             * Total comments,likes and shares (interactions)
             */
            var total_followers = db.one("SELECT " +
                "(IFNULL(`x`.`fb_users_likes`,0)+ IFNULL(`y`.`ig_users_likes`,0)+ IFNULL(`z`.`yt_user_subscriber`,0)) as `total_followers` " +
                "FROM 		`tags`	`a` " +

                "LEFT JOIN	( " +
                "SELECT		DISTINCT(`user_id`), SUM(`user_likes`) as fb_users_likes ,`tag_id` " +
                "FROM		`facebook_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`x` ON (`a`.`id` = `x`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		DISTINCT(`user_id`), SUM(`user_followed_by`) as ig_users_likes,`tag_id` " +
                "FROM		`instagram_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `post_created_time` > ? " +
                ")`y` ON (`a`.`id` = `y`.`tag_id`) " +

                "LEFT JOIN	( " +
                "SELECT		DISTINCT(`user_id`), SUM(`user_subscriber_count`) as `yt_user_subscriber`,`tag_id` " +
                "FROM		`youtube_tags_view` " +
                "WHERE		`tag_id` = ? " +
                "AND        `video_published_at` > ? " +
                ")`z` ON (`a`.`id` = `z`.`tag_id`) " +
                "WHERE 		`id` = ? ", [tag_details.id, six_month, tag_details.id, six_month, tag_details.id, six_month, tag_details.id]);

            var influencers = db.query(
                "SELECT  DISTINCT(`user_id`), `user_name`,`user_picture`,`profile_slug`,'Facebook' as `type` FROM  `facebook_tags_view` WHERE  `tag_id` = ? AND `post_created_time` > ?  " +
                "UNION " +
                "SELECT  DISTINCT(`user_id`), `user_full_name`,`user_profile_picture`,`profile_slug`,'instagram' as `type` FROM  `instagram_tags_view` WHERE  `tag_id` = ? AND `post_created_time` > ? " +
                "UNION " +
                "SELECT  DISTINCT(`user_id`), `user_name`,`user_picture`,`profile_slug`,'youtube' as `type` FROM  `youtube_tags_view` WHERE  `tag_id` = ? AND `video_published_at` > ? ", [tag_details.id, six_month, tag_details.id, six_month, tag_details.id, six_month, tag_details.id]);


            Q.all([
                total_posts, total_reach, total_interactions, total_followers, influencers
            ]).then(function (data) {
                    var data_to_send = {
                        tag_name: req.params.tag,
                        tag_id: data[0].tag_id,
                        total_posts: data[0].total_posts,
                        total_reach: data[1].total_reach,
                        total_interactions: data[2].total_interactions,
                        total_followers: data[3].total_followers,
                        influencers: data[4]
                    };
                    _common_workers.send_res(req, res, data_to_send);
                },
                function (err) {
                    _common_workers.send_err(req, res, err.statusCode, err);
                }

            );
        });
    });

    //Getting the post counts of each day for 6 months
    app.get('/api/tag/get_daily_post_count/:tag_id', function (req, res) {
        //6 month back
        var six_month = moment().subtract(6, 'months').unix();

        var daily_posts_count = db.query("SELECT COUNT(`post_id`) as post_count, DATE(`post_created_timestamp`) as `date`, 'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY DATE(`post_created_timestamp`) " +
                "UNION " +
                "SELECT COUNT(`post_id`) as post_count, DATE(`post_created_timestamp`) as `date`, 'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY DATE(`post_created_timestamp`) " +
                "UNION " +
                "SELECT COUNT(`video_id`) as video_count, DATE(`video_published_timestamp`) as `date`, 'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? GROUP BY DATE(`video_published_timestamp`) ", [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id]
            )
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
    });

    /**
     * Getting daily interactions of a tag
     */
    app.get('/api/tag/get_daily_interactions/:tag_id', function (req, res) {
        //6 month back
        var six_month = moment().subtract(6, 'months').unix();

        var daily_posts_count = db.query("SELECT (SUM(`post_likes_count`) + SUM(`post_comments_count`) + SUM(`post_shares_count`)) as `interaction_count`, DATE(`post_created_timestamp`) as `date`, 'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY DATE(`post_created_timestamp`) " +
                "UNION " +
                "SELECT (SUM(`post_likes_count`) + SUM(`post_comments_count`)) as `interaction_count` , DATE(`post_created_timestamp`) as `date`, 'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY DATE(`post_created_timestamp`) " +
                "UNION " +
                "SELECT (SUM(`video_like_count`) + SUM(`video_comment_count`)) as `interaction_count`, DATE(`video_published_timestamp`) as `date`, 'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? GROUP BY DATE(`video_published_timestamp`) ", [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id]
            )
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
    });

    /**
     * Getting daily daily reach of a tag
     */
    app.get('/api/tag/get_daily_reach/:tag_id', function (req, res) {
        //6 month back
        var six_month = moment().subtract(6, 'months').unix();

        var daily_posts_count = db.query("SELECT (SUM(`user_likes`)*" + config.facebook.reach_constant + ") as `reach_count`, DATE(`post_created_timestamp`) as `date`, 'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY DATE(`post_created_timestamp`) " +
                "UNION " +
                "SELECT (SUM(`user_followed_by`)*" + config.instgram.reach_constant + ") as `reach_count` , DATE(`post_created_timestamp`) as `date`, 'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY DATE(`post_created_timestamp`) " +
                "UNION " +
                "SELECT (SUM(`video_view_count`)) as `interaction_count`, DATE(`video_published_timestamp`) as `date`, 'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? GROUP BY DATE(`video_published_timestamp`) ", [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id]
            )
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
    });

    //Getting the post counts of each day for 6 months
    app.get('/api/tag/get_post_feed/:tag_id/:page_number', function (req, res) {
        // 6 month back
        var six_month = moment().subtract(6, 'months').unix();
        // res.json(six_month);
        var daily_posts = db.query({
                sql: "SELECT `post_id`,`post_full_picture` as image, DATE(`post_created_timestamp`) as `date`,`post_message`,`user_picture`, `user_name`,'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? " +
                    "UNION " +
                    "SELECT `post_id`,`post_images_standard_resolution_url` as image, DATE(`post_created_timestamp`) as `date`,`post_caption` as `post_message`,`user_profile_picture`,`user_full_name`,'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? " +
                    "UNION " +
                    "SELECT `video_id`,`video_thumbnail` as image, DATE(`video_published_timestamp`) as `date`,`video_description`,`user_picture`, `user_name`,'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? ",
                paginate: {
                    page: req.params.page_number,
                    resultsPerPage: 30
                }
            }, [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id])
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });

    //Getting the post counts of each day for 6 months
    app.get('/api/tag/get_top_reach/:tag_id', function (req, res) {
        // 6 month back
        var six_month = moment().subtract(6, 'months').unix();
        // res.json(six_month);
        var daily_posts = db.query({
                sql: "SELECT `user_id`,`user_picture`, `user_name`,(`user_likes`*" + config.facebook.reach_constant + ") as `reach`, 'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_profile_picture`, `user_full_name`,(`user_followed_by`*" + config.instgram.reach_constant + ") as `reach`,'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_picture`, `user_name`,`user_view_count` as reach, 'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? GROUP BY `user_id` ORDER BY `reach` DESC",
                paginate: {
                    page: 1,
                    resultsPerPage: 10
                }
            }, [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id])
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });


    //Getting the top impressive influencers for 6 months
    app.get('/api/tag/get_top_impression/:tag_id', function (req, res) {
        // 6 month back
        var six_month = moment().subtract(6, 'months').unix();
        // res.json(six_month);
        var daily_posts = db.query({
                sql: "SELECT `user_id`,`user_picture`, `user_name`,(`user_likes`*" + config.facebook.reach_constant + ") * COUNT(`post_id`) as `impression`, 'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_profile_picture`, `user_full_name`,(`user_followed_by`*" + config.instgram.reach_constant + ") * COUNT(`post_id`) as `impression` ,'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_picture`, `user_name`,`user_view_count`* COUNT(`video_id`) as `impression`, 'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? GROUP BY `user_id` ORDER BY `impression` DESC",
                paginate: {
                    page: 1,
                    resultsPerPage: 10
                }
            }, [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id])
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });


    //Getting the top impressive(interaction) influencers for 6 months
    app.get('/api/tag/get_top_interactions/:tag_id', function (req, res) {
        // 6 month back
        var six_month = moment().subtract(6, 'months').unix();

        var daily_posts = db.query({
                sql: "SELECT `user_id`,`user_picture`, `user_name`,(SUM(`post_likes_count`)+SUM(`post_comments_count`)+SUM(`post_shares_count`)) as `interactions`, 'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_profile_picture`, `user_full_name`,(SUM(`post_likes_count`)+SUM(`post_comments_count`)) as `interactions`,'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_picture`, `user_name`,(SUM(`video_like_count`)+SUM(`video_comment_count`)) as `interactions`, 'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? GROUP BY `user_id` ORDER BY `interactions` DESC",
                paginate: {
                    page: 1,
                    resultsPerPage: 10
                }
            }, [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id])
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });

    //Getting the top Engagement rate influencers for 6 months
    app.get('/api/tag/get_top_engagement_rate/:tag_id', function (req, res) {
        // 6 month back
        var six_month = moment().subtract(6, 'months').unix();

        var daily_posts = db.query({
                sql: "SELECT `user_id`,`user_picture`, `user_name`,`user_engagement_rate` as `engagement_rate`, 'facebook' as `type` FROM `facebook_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_profile_picture`, `user_full_name`,`user_engagement_rate` as `engagement_rate`,'instagram' as `type` FROM `instagram_tags_view` WHERE `tag_id` = ? AND `post_created_time` > ? GROUP BY `user_id` " +
                    "UNION " +
                    "SELECT `user_id`,`user_picture`, `user_name`,`user_engagement_rate` as `engagement_rate`, 'youtube' as `type` FROM `youtube_tags_view` WHERE `tag_id` = ? AND `video_published_at` > ? GROUP BY `user_id` ORDER BY `engagement_rate` DESC",
                paginate: {
                    page: 1,
                    resultsPerPage: 10
                }
            }, [req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id, six_month, req.params.tag_id])
            .then(function (data) {
                _common_workers.send_res(req, res, data);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });

    /**
     * Used to fetch post based on the tag name and post creation date (six month old)
     * @author karthick.k
     * @param   {object}   req request header
     * @returns {Array} List of posts
     */
    app.get('/api/tag/get_posts/:tag_name', function (req, res) {
        if (!req.params && !req.params.tag_name) {
            return _common_workers.send_err(req, res, 400, "Tag name is required");
        }
        tag_worker.get_posts(req).then(function (result) {
            var data = [].concat.apply([], result);
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while fetching posts");
        });
    });
};