/* jshint node: true */
"use strict";

var db = require('../helpers/db');
var redis = require('../helpers/redis');
var _common_workers = require('../workers/_common');
var profiles_workers = require('../workers/profile/profiles');
var monitors_workers = require('../workers/monitors');

/**
 * Configure routes and permission
 * @author karthick.k
 * Example : 
 * 1. "/user/login": 0 can be allowed for all users even login is not required, Anonymous users also can access
 * 2. "/user/roles": 100 can be accessed only by admin and required login
 * 3. "/user/logout": 10(visitor) can be accessed only by admin, member, visitor, user basically logged in users only can access
 * admin - 100, member - 60, user - 30, visitor - 10, Anonymous - 0
 */
var api_permission_list = {
    "/api/monitors/monitor_details?monitor_id=53": 0,
    "/api/monitors/monitor_influencers_followers_details?monitor_id=53": 0,
    "/api/monitors/monitor_posts_details?monitor_id=53": 0,
    "/api/user/list_user": 100,
    "/api/user/roles": 100,
    "/api/user/modify_role": 100,
    "/api/user/modify_expiry": 100,
    "/api/user/create_user": 100,
    "/api/user/user_logs": 100,
    "/api/user/delete_user": 100,
    "/api/user/view": 10,
    "/api/_common": 100,
    "/api/_common/get_data/profiles/": 0,
    "/api/profile": 0,
    "/api/profile/add": 0,
    "/api/profile/edit": 100,
    "/api/profile/delete/": 100,
    "/api/profile/edit_instagram": 100,
    "/api/profile/edit_facebook": 100,
    "/api/profile/edit_youtube": 100,
    "/api/profile/generate_slug": 0,
    "/api/instagram": 0,
    "/api/facebook": 0,
    "/api/youtube": 0,
    "/api/monitors/search_tag": 100,
    "/api/monitors/search_individual_influencers": 100,
    "/api/monitors/search_users": 100,
    "/api/monitors/search_lists": 100,
    "/api/monitors/search_group": 100,
    "/api/monitors/list": 100,
    "/api/monitors/create_monitor": 100,
    "/api/monitors/delete_monitor": 100,
    "/api/monitors/monitor_details": 100,
    "/api/monitors/modify_date": 100,
    "/api/monitors/modify_name": 100,
    "/api/monitors/monitor_posts_details": 100,
    "/api/monitors/modify_influencers": 100,
    "/api/monitors/modify_tags": 100,
    "/api/monitors/modify_users": 100,
    "/api/monitors/modify_group_name": 100,
    "/api/monitors/monitor_influencers_followers_details": 100,
    "/api/analytics/events_details": 100,
    "/api/analytics/export": 100,
    "/api/user/logout": 10,
    "/api/user/login": 0,
    "/api/profiles": 0,
    "/api/user/user": 0,
    "/api/user/forgot_password": 0,
    "/api/user/reset_password": 0,
    "/api/lists": 10,
    "/api/list": 10,
    "/api/tag/": 100,
    "/api/tags/": 100,
    "/favicon": 0
};

/**
 * used to check authentication and authorization
 * @author karthick.k
 * @param {object} req request header object
 * @returns {boolean} true/false based the user role
 */
var is_allowed = function (permission_list, req) {
    var role_id = 0;
    if (req.user && req.user.role_id) {
        role_id = req.user.role_id;
    }
    var has_permission = Object.keys(permission_list).some(function (route) {
        return ~req.url.indexOf(route) && (role_id >= permission_list[route]);
    });
    return has_permission;
};

module.exports = function (app) {

    app.all("*", function (req, res, next) {
        if (!!is_allowed(api_permission_list, req)) {
            next();
        } else {
            _common_workers.send_err(req, res, 401, "Not authorized");
        }
    });

    /**
     * Used to check the authorization of the logged in user with the monitor
     * @author karthick.k
     * @param {req} req request object
     * @param {res} res response object
     * @param {next} next function
     */
    app.get("/api/monitors/monitor_*", function (req, res, next) {
        monitors_workers.is_allowed(req).then(function (data) {
            if (!!data.length) {
                next();
            } else {
                _common_workers.send_err(req, res, 401, "Not authorized");
            }
        }).fail(function (err) {
            _common_workers.send_err(req, res, 401, "Not authorized");
        });
    });

    /**
     * Approaching cache for the value first
     */
    app.get('/api/*', function (req, res, next) {
        if (req.query.cache && req.query.cache === 'false') {
            //If cache is false... dont do anyting with redis.
            //Go to db and fetch new records
            next();
        } else {
            redis.get(req.url).then(function (data) {
                //If no data found in redis then go fetch it from db
                if (data) {
                    try {
                        data = JSON.parse(data);
                    } catch (e) {
                        return false;
                    }
                    res.json(data);
                } else {
                    next();
                }

            }, function (err) {
                console.log("redis Error : ", err);
                next();
            });
        }
    });

    //Get attribute of a profile by another attribute
    app.get('/api/_common/get_data/:table/:columns_to_fetch/:column/:value', function (req, res) {
        _common_workers.get_data(req.params.table, req.params.columns_to_fetch, req.params.column, req.params.value).then(function (data) {
            _common_workers.send_res(req, res, data);
        }, function (err) {
            _common_workers.send_err(req, res, err.code, err.msg);
        });
    });


};