/* jshint node: true */
"use strict";

var db = require('../helpers/db');
var FB = require('../helpers/facebook');
var fb_workers = require('../workers/facebook/index');
var moment = require('moment');
var _common_workers = require('../workers/_common');

module.exports = function (app) {

    //find a page on facebook
    app.get('/api/facebook/find_page/:page', function (req, res) {
        FB.randomize_tokens();
        FB.api('/search', 'GET', {
            "q": req.params.page,
            "limit": 10,
            "type": 'page',
            fields: [
                'id', 'username', 'name', 'picture.width(1024).height(1024)', 'fan_count', 'link', 'birthday', 'website', 'bio', 'phone', 'location', 'emails'
            ]
        }, function (result) {
            if (!result || result.error) {
                _common_workers.send_err(req, res, 400, result.error);
            }
            _common_workers.send_res(req, res, result);
        });
    });
    //Get random images of a facebook profile
    app.get('/api/facebook/get_random_images/:facebook_id/:count', function (req, res) {

        var query = " " +
            "   SELECT     `image`,`link`, `type`                 " +
            "   FROM       (                            " +
            "                   SELECT     `full_picture` as image,`link`, `type`  " +
            "                   FROM       `facebook_posts`                            " +
            "                   WHERE      `user_id` = " + req.params.facebook_id +
            "                   AND        `full_picture` IS NOT NULL                   " +
            "                   ORDER BY    `id` DESC                                        " +
            "                   LIMIT       0," + (parseInt(req.params.count) + 10) +
            "               )as X                                                           " +
            "  ORDER BY     RAND()                                                           " +
            "  LIMIT        0," + req.params.count + ";";

        db.query(query).then(function (result) {
            if (!result)
                _common_workers.send_err(req, res, 400, "No Result found");
            else
                _common_workers.send_res(req, res, result);

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });
    //Get recent images of a profile.
    app.get('/api/facebook/get_recent_images/:facebook_id/:count', function (req, res) {

        db.query("SELECT `full_picture` as image,`link`,`likes_count`, `comments_count`, `type` FROM `facebook_posts` WHERE `user_id` = '" + req.params.facebook_id + "' AND `full_picture` IS NOT NULL ORDER BY `id` DESC LIMIT " + req.params.count + ";").then(function (result) {
            if (!result)
                _common_workers.send_err(req, res, 404, "No Result found");
            else
                _common_workers.send_res(req, res, result);

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });
    //Get average number of likes for user.
    app.get('/api/facebook/get_user_average_likes/:facebook_id', function (req, res) {

        db.query("SELECT AVG(likes_count) as averageNumber FROM ( SELECT likes_count FROM `facebook_posts` WHERE `user_id` = " + req.params.facebook_id + " ORDER BY `id` DESC LIMIT 0,10) as x;")
            .then(function (result) {
                if (!result)
                    _common_workers.send_err(req, res, 404, "No Result found");
                else
                    _common_workers.send_res(req, res, result);

            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });
    //Get user's most liked post
    app.get('/api/facebook/get_user_most_liked/:facebook_id', function (req, res) {

        var query = "SELECT `full_picture` as image,`link`,`likes_count`, `type` FROM `facebook_posts` WHERE `user_id` = " + req.params.facebook_id + " ORDER BY `likes_count` DESC LIMIT 1";

        db.query(query)
            .then(function (result) {
                if (!result)
                    _common_workers.send_err(req, res, 404, "No Result found");
                else
                    _common_workers.send_res(req, res, result);

            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });

    //Fetch User's stats for HighMap
    app.get('/api/facebook/get_user_followersbyarea/:facebook_id', function (req, res) {

        var which_date = req.query.which_date && moment(Date.parse(req.query.which_date)).isValid() ? moment(Date.parse(req.query.which_date)).format("YYYY-MM-DD") : moment().format("YYYY-MM-DD");

        var query = "SELECT `country` as code, `fans` as value, '#3b5998' as color FROM `facebook_page_fans_country` WHERE `user_id` ='" + req.params.facebook_id + "'  AND `date` = '" + which_date + "' ;";

        db.query({
            sql: query
        }).then(function (result) {
            if (result) {
                _common_workers.send_res(req, res, result);
            } else {
                _common_workers.send_err(req, res, 404, "No Result found");
            }
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    //Fetch Facebook media posted by user
    app.get('/api/facebook/media_posted/:facebook_id', function (req, res) {

        var query = "SELECT COUNT(id) as number_of_posts, FROM_UNIXTIME(MIN(created_time), '%Y-%m-%d') as min_date FROM facebook_posts WHERE user_id ='" + req.params.facebook_id + "' AND created_time >= UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 6 MONTH));";

        db.query({
            sql: query
        }).then(function (result) {
            if (result) {
                _common_workers.send_res(req, res, result);
            } else {
                _common_workers.send_err(req, res, 404, "No Result found");
            }
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    //Getting growth
    app.get('/api/facebook/get_growth/:facebook_id', function (req, res) {
        fb_workers.user.get_growth(req.params.facebook_id).then(function (data) {
            var data_to_send = {};

            if (data) {
                // add the 1 day diff data
                data_to_send.one_day_diff = (typeof data[0] !== 'undefined' && data[0].length > 0) ? data[0] : {};

                //add 7 days diff data
                data_to_send.seven_day_diff = (typeof data[1] !== 'undefined' && data[1].length > 0) ? data[1] : {};

                //add 30 days diff data
                data_to_send.thirty_day_diff = (typeof data[2] !== 'undefined' && data[2].length > 0) ? data[2] : {};

                // add data from MAX(date) to MIN(date) we have from last 6 Months
                data_to_send.avg_stats = (typeof data[3] !== 'undefined' && data[3].length > 0) ? data[3] : {};

                // add data from MAX(date) to MIN(date) we have from last 6 Months for media posted
                data_to_send.avg_stats_media = (typeof data[4] !== 'undefined' && data[4].length > 0) ? data[4] : {};
            }

            _common_workers.send_res(req, res, data_to_send);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    //Getting hashtag growth for brands
    app.get('/api/facebook/get_hashtagposts_growth/:facebook_id', function (req, res) {

        db.query("SELECT `id` FROM `tags` WHERE `tag` = (SELECT username FROM `facebook` WHERE	id = '" + req.params.facebook_id + "' ) LIMIT 0,1;").then(function (hashtag_result) {
            if (!hashtag_result || hashtag_result.length <= 0) {
                res.status(404).send("No Result found");
            } else {
                if (hashtag_result[0].id) {
                    fb_workers.tags.get_growth(hashtag_result[0].id).then(function (data) {
                        var data_to_send = {};

                        if (data) {
                            // add the 1 day diff data
                            data_to_send.one_day_diff = (typeof data[0] !== 'undefined' && data[0].length > 0) ? data[0] : {};

                            //add 7 days diff data
                            data_to_send.seven_day_diff = (typeof data[1] !== 'undefined' && data[1].length > 0) ? data[1] : {};

                            //add 30 days diff data
                            data_to_send.thirty_day_diff = (typeof data[2] !== 'undefined' && data[2].length > 0) ? data[2] : {};

                            // add data from MAX(date) to MIN(date) we have from last 6 Months
                            data_to_send.avg_stats = (typeof data[3] !== 'undefined' && data[3].length > 0) ? data[3] : {};
                        }

                        _common_workers.send_res(req, res, data_to_send);
                    }, function (err) {
                        _common_workers.send_err(req, res, err.statusCode, err);
                    });
                } else {
                    _common_workers.send_err(req, res, 404, "No Hashtag found");
                }
            }
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });
};
