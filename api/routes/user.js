/* jshint node: true */
"use strict";

var _common_workers = require('../workers/_common');
var user_worker = require('../workers/user');
var _common_routes = require('./_common');
var db = require('../helpers/db');

module.exports = function (app, passport) {

    /**
     * Used to handle login ajax request, establishing session after verifying credentials
     * @author karthick.k
     * @param   {object}   req     consist of header, form data email, password
     * @param   {object} res response header, redirection, and used to respond to ajax request
     * @returns {function} next passes control to next matching route  
     */
    app.post('/api/user/login', function (req, res, next) {
        passport.authenticate('login', function (err, user) {
            if (err) {
                _common_workers.send_err(req, res, 400, err);
            } else {
                delete user.password; //Don't expose password to template or to client side
                req.logIn(user, function (err) {
                    if (err) {
                        return _common_workers.send_err(req, res, 400, "Error while logging, please retry");
                    }
                    user_worker.update_last_login(user.email, function (err, result) {
                        user_worker.log_user(req);
                        _common_workers.send_res(req, res, {
                            email: user.email,
                            id: user.id,
                            role: user.role,
                            role_id: user.role_id
                        });
                    });
                });
            }
        })(req, res, next);
    });

    /**
     * Used to handle signup ajax request, create account, sending confirmation email
     * @author karthick.k
     * @param   {object}   req     consist of header, form data email, password
     * @param   {object} res response header, redirection, and used to respond to ajax request
     * @returns {function} next passes control to next match route  
     */
    app.post('/api/user/create_user', function (req, res, next) {
        passport.authenticate('create_user', function (err, user) {
            if (err) {
                _common_workers.send_err(req, res, 500, err);
            } else {
                user_worker.send_email(req, user, false, function (err, data) {
                    if (err) {
                        return _common_workers.send_err(req, res, 500, err);
                    }
                    var message = "Email is sent to " + user.email + " to verify his/her account";
                    _common_workers.send_res(req, res, {
                        message: message
                    });
                });
            }
        })(req, res, next);
    });

    /**
     * Used to logout/destroy session
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res response header, redirection, and used to respond to ajax request
     */
    app.get('/api/user/logout', function (req, res) {
        user_worker.log_user(req);
        req.logout();
        res.redirect('/');
    });

    /**
     * Used to return the user which is based on the currect session
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns user information, id and email of current session
     */
    app.get('/api/user/user', function (req, res) {
        var user = {};
        if (req && req.user && !!req.user.id) {
            user.id = req.user.id;
            user.email = req.user.email;
            user.role = req.user.role;
            user.role_id = req.user.role_id;
        }
        _common_workers.send_res(req, res, user);
    });

    /**
     * Used to handle forgot password, send an email to reset the password
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns user information, id and email of current session
     */
    app.get('/api/user/forgot_password', function (req, res) {
        user_worker.forgot_password(req, function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, {
                message: data
            });
        });
    });

    /**
     * Used to handle reset password, change the password to new password
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns user information, id and email of current session
     */
    app.post('/api/user/reset_password', function (req, res) {
        user_worker.reset_password(req, function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, {
                message: data
            });
        });
    });

    /**
     * Used to fetch all roles 
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns roles
     */
    app.get('/api/user/roles', function (req, res) {
        user_worker.get_roles(function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, data);
        });
    });

    /**
     * Used to fetch all users 
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns roles
     */
    app.get('/api/user/list_user', function (req, res) {
        user_worker.get_users(function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, data);
        });
    });

    /**
     * Used to modify role of the user 
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns roles
     */
    app.get('/api/user/modify_role', function (req, res) {
        user_worker.modify_role(req, function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, data);
        });
    });

    /**
     * Used to modify expiry of the user 
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns roles
     */
    app.get('/api/user/modify_expiry', function (req, res) {
        user_worker.modify_expiry(req, function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, data);
        });
    });

    /**
     * Used to fetch user logs
     * @author karthick.k
     * @param   {object}   req     consist of header, user information
     * @param   {object} res returns user logs based on the user id
     */
    app.get('/api/user/user_logs', function (req, res) {
        user_worker.get_user_logs(req, function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, data);
        });
    });

    /**
     * Used to delete user
     * @author karthick.k
     * @param   {object}   req consist of header, user id
     * @param   {object}   res response object
     * @returns   {object} res returns success message
     */
    app.get('/api/user/delete_user', function (req, res) {
        user_worker.delete_user(req, function (err, data) {
            if (err) {
                return _common_workers.send_err(req, res, 400, err);
            }
            _common_workers.send_res(req, res, data);
        });
    });

    /**
     * Used to fetch all valid users
     * @author Jatin Seth
     * @param   {object}   req consist of header
     * @param   {object}   res response object
     * @returns   {object} res returns success message
     */
    app.get('/api/user/view', function (req, res) {
        // intiate query...
        var query = " SELECT id, email, firstname, lastname FROM `users` WHERE `is_verified` = 'Y' AND (`expires_at` IS NULL OR `expires_at` >= now()) ";

        //check if list_id provided or not...
        if (req.query.list_id) {
            query += " AND id NOT IN ( SELECT user_id FROM `lists_users` WHERE `list_id` = '" + req.query.list_id + "')";
        }

        db.query(query).then(function (users) {
            _common_workers.send_res(req, res, users);
        }, function (err) {
            _common_workers.send_err(req, res, 400, err);
        });
    });

};