/* jshint node: true */
"use strict";

var _common_workers = require('../workers/_common');
var monitors_worker = require('../workers/monitors');

module.exports = function (app, passport) {


    /**
     * Used to search group
     * @author karthick.k
     * @param   {object} req consist of header, search keyword
     * @param   {object} res response header, group information based on the keyword
     */
    app.get('/api/monitors/search_group', function (req, res) {
        if (!req || !req.query || !req.query.keyword) {
            return _common_workers.send_err(req, res, 400, "Search keyword is required");
        }
        monitors_worker.search_group(req.query.keyword).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while searching group");
        });
    });

    /**
     * Used to search tag
     * @author karthick.k
     * @param   {object} req consist of header, search keyword
     * @param   {object} res response header, tag information based on the keyword
     */
    app.get('/api/monitors/search_tags', function (req, res) {
        if (!req || !req.query || !req.query.tag) {
            return _common_workers.send_err(req, res, 400, "Search keyword is required");
        }
        monitors_worker.search_tag(req.query.tag).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while searching tag");
        });
    });

    /**
     * Used to search individual influencers
     * @author karthick.k
     * @param   {object} req consist of header, search keyword
     * @param   {object} res response header, influencers information based on the keyword
     */
    app.get('/api/monitors/search_individual_influencers', function (req, res) {
        if (!req || !req.query || !req.query.profile_name) {
            return _common_workers.send_err(req, res, 400, "Search keyword is required");
        }
        monitors_worker.search_individual_influencers(req.query.profile_name).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while searching infleuncers");
        });
    });

    /**
     * Used to search list
     * @author karthick.k
     * @param   {object} req consist of header, search keyword
     * @param   {object} res response header, list information based on the keyword
     */
    app.get('/api/monitors/search_lists', function (req, res) {
        if (!req || !req.query || !req.query.keyword) {
            return _common_workers.send_err(req, res, 400, "Search keyword is required");
        }
        monitors_worker.search_lists(req.query.keyword).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while searching lists");
        });
    });

    /**
     * Used to search users based on the keyword
     * @author karthick.k
     * @param   {object} req consist of header, search keyword
     * @param   {object} res response header, users information based on the keyword
     */
    app.get('/api/monitors/search_users', function (req, res) {
        if (!req || !req.query || !req.query.user_name) {
            return _common_workers.send_err(req, res, 400, "Search keyword is required");
        }
        monitors_worker.search_users(req).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while searching users");
        });
    });

    /**
     * Used to create new monitor 
     * @author karthick.k
     * @param   {object} req consist of header, monitor information
     * @param   {object} res response header, error/success message
     */
    app.post('/api/monitors/create_monitor', function (req, res) {
        monitors_worker.create_monitor(req).then(function (data) {
            _common_workers.send_res(req, res, {
                message: "Monitor saved successfully"
            });
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, err.message);
        });
    });

    /**
     * Used to fetch monitors list
     * @author karthick.k
     * @param   {object} req consist of header
     * @param   {object} res response header, monitors list
     */
    app.get('/api/monitors/list', function (req, res) {
        monitors_worker.get_monitors_list(req).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while fetching monitor list");
        });
    });

    /**
     * Used to delete monitor
     * @author karthick.k
     * @param   {object} req consist of header, monitor_id
     * @param   {object} res response header, success/error message
     */
    app.delete('/api/monitors/delete_monitor', function (req, res) {
        if (!req || !req.query || !req.query.monitor_id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        monitors_worker.delete_monitor(req).then(function (data) {
            _common_workers.send_res(req, res, {
                message: "Monitor deleted successfully"
            });
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, err.message);
        });
    });

    /**
     * Used to fetch monitor details
     * @author karthick.k
     * @param   {object} req consist of header, monitor_id
     * @param   {object} res response header, monitor details
     */
    app.get('/api/monitors/monitor_details', function (req, res) {
        if (!req || !req.query || !req.query.monitor_id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        monitors_worker.get_monitor_details(req).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while fetching monitor details");
        });
    });

    /**
     * Used to modify date
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, from, to date
     * @param   {object} res response header, success/error message
     */
    app.post('/api/monitors/modify_date', function (req, res) {
        if (!req || !req.body || !req.body.id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        if (!req.body.from || !req.body.to) {
            return _common_workers.send_err(req, res, 400, "Monitor date is missing");
        }
        var from = new Date(req.body.from).getTime();
        var to = new Date(req.body.to).getTime();
        if (from > to) {
            return _common_workers.send_err(req, res, 400, "Monitor from date is greater than to date");
        }
        monitors_worker.modify_date(req.body).then(function (data) {
            monitors_worker.clear_monitor_api_cache(req.body.id);
            _common_workers.send_res(req, res, "Monitor date is modified successfully");
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while modifying monitor date");
        });
    });

    /**
     * Used to modify group name
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, group name
     * @param   {object} res response header, success/error message
     */
    app.post('/api/monitors/modify_group_name', function (req, res) {
        if (!req || !req.body || !req.body.id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        if (!req.body.group_name) {
            return _common_workers.send_err(req, res, 400, "Monitor group name is missing");
        }
        monitors_worker.modify_group_name(req.body).then(function (data) {
            monitors_worker.clear_monitor_api_cache(req.body.id);
            _common_workers.send_res(req, res, "Monitor group name is modified successfully");
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while modifying monitor group name");
        });
    });

    /**
     * Used to modify name
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, name
     * @param   {object} res response header, success/error message
     */
    app.post('/api/monitors/modify_name', function (req, res) {
        if (!req || !req.body || !req.body.id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        if (!req.body.name) {
            return _common_workers.send_err(req, res, 400, "Monitor name is missing");
        }
        monitors_worker.modify_name(req.body).then(function (data) {
            monitors_worker.clear_monitor_api_cache(req.body.id);
            _common_workers.send_res(req, res, "Monitor name is modified successfully");
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while modifying monitor name");
        });
    });

    /**
     * Used to fetch monitor post count
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, name
     * @param   {object} res response header, success/error message
     */
    app.get('/api/monitors/monitor_posts_details', function (req, res) {
        if (!req || !req.query || !req.query.monitor_id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        monitors_worker.get_monitor_posts_details(req.user, req.query.monitor_id, req.query.influencer_id, req.query.tag_id).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while fetching monitor posts count");
        });
    });

    /**
     * Used to fetch monitor influencers followers count
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, name
     * @param   {object} res response header, success/error message
     */
    app.get('/api/monitors/monitor_influencers_followers_details', function (req, res) {
        if (!req || !req.query || !req.query.monitor_id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        monitors_worker.get_monitor_influencers_followers_details(req.query.monitor_id, req.query.influencer_id).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while fetching monitor influencers followers count");
        });
    });

    /**
     * Used to update influencers
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, influencers
     * @param   {object} res response header, success/error message
     */
    app.post('/api/monitors/modify_influencers', function (req, res) {
        if (!req || !req.body || !req.body.monitor_id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        if (!req.body.influencers || !req.body.influencers.length) {
            return _common_workers.send_err(req, res, 400, "Influencers are missing");
        }
        monitors_worker.modify_influencers(req.body).then(function (data) {
            monitors_worker.clear_monitor_api_cache(req.body.monitor_id);
            _common_workers.send_res(req, res, "Influencers are updated successfully");
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while updating influencers");
        });
    });

    /**
     * Used to update tags
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, tags
     * @param   {object} res response header, success/error message
     */
    app.post('/api/monitors/modify_tags', function (req, res) {
        if (!req || !req.body || !req.body.monitor_id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        if (!req.body.tags || !req.body.tags.length) {
            return _common_workers.send_err(req, res, 400, "Tags are missing");
        }
        monitors_worker.modify_tags(req.body).then(function (data) {
            monitors_worker.clear_monitor_api_cache(req.body.monitor_id);
            _common_workers.send_res(req, res, "Tags are updated successfully");
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while updating tags");
        });
    });

    /**
     * Used to update users
     * @author karthick.k
     * @param   {object} req consist of header, monitor id, users
     * @param   {object} res response header, success/error message
     */
    app.post('/api/monitors/modify_users', function (req, res) {
        if (!req || !req.body || !req.body.monitor_id) {
            return _common_workers.send_err(req, res, 400, "Monitor id is missing");
        }
        if (!req.body.users || !req.body.users.length) {
            return _common_workers.send_err(req, res, 400, "Users are missing");
        }
        monitors_worker.modify_users(req).then(function (data) {
            monitors_worker.clear_monitor_api_cache(req.body.monitor_id);
            _common_workers.send_res(req, res, "Users are updated successfully");
        }).fail(function (err) {
            _common_workers.send_err(req, res, 400, "Error while updating users");
        });
    });

};