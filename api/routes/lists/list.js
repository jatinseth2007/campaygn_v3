/* jshint node: true */
"use strict";

var db = require('../../helpers/db');
var _common_workers = require('../../workers/_common');
var list_worker = require('../../workers/list/list');
var json2xls = require('json2xls');
var moment = require('moment');
var visitor = require('../../helpers/visitor.js');
var analytics = require("../../workers/analytics");

module.exports = function (app) {
    /**
     * Used to return the list information
     * @author jatin
     * @param   {object}   req     consist of header, logged in user information, list_id
     * @param   {object} res returns list info from db
     */
    app.get('/api/list/:list_id', function (req, res) {

        // prepare query to fetch lists assosiated with a user by parent list.
        var query = "SELECT b.`id`, b.`name`, b.`description`, a.`user_role` FROM `lists_users` a JOIN `lists` b ON(a.`list_id` = b.`id`) WHERE b.`id` = '" + req.params.list_id + "' AND a.`user_id` = '" + req.user.id + "' AND b.`deleted` = 'N';";

        db.query(query).then(function (result) {
            _common_workers.send_res(req, res, result);
            //Analytics - register list open
            if (req.user && req.user.hasOwnProperty("email")) {
                analytics.register_event("list", "opened", "list_id", req.params.list_id, req.user.email, req.user.fullname);
            } else {
                analytics.register_event("list", "opened", "list_id", req.params.list_id, "unknown", "unknown");
            }
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });


    });

    /**
     * Used to return the users associated with a list
     * @author jatin
     * @param   {object}   req     consist of header, logged in user information, list_id
     * @param   {object} res returns list info from db
     */
    app.get('/api/list/:list_id/users', function (req, res) {

        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0) {
                // prepare query to fetch lists assosiated with a user by parent list.
                var query = "" +
                    "   SELECT	   a.`id` as list_id, c.`id`, c.`email`, c.`firstname`, c.`lastname`, b.`user_role`   " +
                    "   FROM	   `lists`			a  " +
                    "   JOIN	   `lists_users`	b ON(a.`id` = b.`list_id`)    " +
                    "   JOIN	   `users`			c ON(b.`user_id` = c.`id`)    " +
                    "   WHERE	   a.`id` = " + req.params.list_id +
                    "   ORDER BY   c.`firstname` ASC                             ";

                db.query(query).then(function (result) {
                    _common_workers.send_res(req, res, result);
                }, function (err) {
                    _common_workers.send_err(req, res, err.statusCode, err);
                });
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to update he list information
     * @author Jatin Seth
     * @param   {object}   req     consist of header, list_info
     * @param   {object} res returns confirmation
     */
    app.post('/api/list/update', function (req, res) {

        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.body.id).then(function (user_list_info) {
            if (user_list_info.length > 0 && user_list_info[0].user_role == 'owner') { // must be owner to perform this action...
                list_worker.update(req).then(function (data) {
                    _common_workers.send_res(req, res, 'List has been updated successfully.');
                }, function (err) {
                    _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, "Not authorized to perform this action");
            }
        }, function (err) {
            _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
        });
    });

    /**
     * Used to add new list in the db
     * @author jatin
     * @param   {object}   req     consist of header, logged in user information, list information
     * @param   {object} res returns confirmation or error
     */
    app.post('/api/list/add', function (req, res) {
        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.body.parent_id).then(function (user_list_info) {

            if ((user_list_info.length > 0 && user_list_info[0].user_role === 'owner') || req.body.parent_id === 0) {
                // add a list in DB, everything is handeled in worker only...
                list_worker.add(req).then(function (data) {
                    /**
                     * Google analytics to tracks profile views
                     */
                    visitor.event("List - add", req.user.email, req.body.name, function (err) {
                        if (err) {
                            console.log('Event error for sending analytics in list - new' + err);
                        }
                    });

                    _common_workers.send_res(req, res, 'List has been added successfully.');
                }, function (err) {
                    _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, 'You need to be the owner of list to perform this action.');
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to add/ update user role for list
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, list_id, user_id, role
     * @param   {object} res returns confirmation or error
     */
    app.post('/api/list/:list_id/update_user_role', function (req, res) {
        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0 && ['owner', 'admin'].indexOf(user_list_info[0].user_role) >= 0) {

                // check if user is trying to update his own role or trying to make someone owner
                if (req.body.user_id === req.user.id || req.body.role === 'owner') {
                    _common_workers.send_err(req, res, 401, 'This action is not permitted, you are trying to make someone owner or trying to change your own role.');
                } else {
                    db.save('lists_users', {
                        list_id: req.params.list_id,
                        user_id: req.body.user_id,
                        user_role: req.body.user_role
                    }).then(function (sucess) {
                        _common_workers.send_res(req, res, "User's role has been updated successfully.");
                    }, function (err) {
                        _common_workers.send_err(req, res, err.statusCode, err);
                    });
                }

            } else {
                _common_workers.send_err(req, res, 401, 'You need to be the owner of list to perform this action.');
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to insert users for list
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, list_id, users, role
     * @param   {object} res returns confirmation or error
     */
    app.post('/api/list/:list_id/add_user_role', function (req, res) {
        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0 && ['owner', 'admin'].indexOf(user_list_info[0].user_role) >= 0) {

                // check if user is trying to update his own role or trying to make someone owner
                if (req.body.role === 'owner') {
                    _common_workers.send_err(req, res, 401, 'This action is not permitted, you are trying to make someone owner.');
                } else {
                    // validate the input first to go forward...
                    var validation = list_worker.validate_list_user_params(req.body);

                    if (validation === true) {
                        // prepare data to put into db...
                        var list_users = list_worker.prepare_list_users_obj(req.params.list_id, req.body);

                        list_worker.add_list_user(list_users).then(function (success) {
                            // need to notify users by email so need to send email.
                            if (req.body.notify && req.body.notify === true) {
                                list_worker.send_list_notification(req).then(function (data) {
                                    _common_workers.send_res(req, res, "User(s) has been added into list successfully and email notification has been sent.");
                                }, function (err) {
                                    console.log(err);
                                    _common_workers.send_res(req, res, "User(s) has been added into list successfully, but we are not able to send emails at this moment.");
                                });
                            } else {
                                _common_workers.send_res(req, res, "User(s) has been added into list successfully.");
                            }
                        }, function (err) {
                            _common_workers.send_err(req, res, 400, err);
                        });
                    } else {
                        _common_workers.send_err(req, res, 400, validation);
                    }
                }

            } else {
                _common_workers.send_err(req, res, 401, 'You need to be the owner of list to perform this action.');
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to REMOVE user role for list
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, list_id, user_id
     * @param   {object} res returns confirmation or error
     */
    app.post('/api/list/:list_id/remove_user_role', function (req, res) {
        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0 && ['owner', 'admin'].indexOf(user_list_info[0].user_role) >= 0) {

                // need to check if the user is going to delte is owner or not
                list_worker.check_user_authorization(req.body.user_id, req.params.list_id).then(function (user_list_info) {

                    // check if user is trying to remove himself or owner
                    if (req.body.user_id === req.user.id || user_list_info.length <= 0 || user_list_info[0].user_role === 'owner') {
                        _common_workers.send_err(req, res, 401, 'This action is not permitted, you are trying to remove yourself or the owner.');
                    } else {
                        db.delete('lists_users', {
                            list_id: req.params.list_id,
                            user_id: req.body.user_id
                        }).then(function (sucess) {
                            _common_workers.send_res(req, res, "User has been removed successfully from list.");
                        }, function (err) {
                            _common_workers.send_err(req, res, err.statusCode, err);
                        });
                    }
                }, function (err) {
                    _common_workers.send_err(req, res, err.statusCode, err);
                });

            } else {
                _common_workers.send_err(req, res, 401, 'You need to be the owner of list to perform this action.');
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to delete list from system
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information
     * @param   {object} res returns true or error
     */
    app.delete('/api/list/:list_id/delete', function (req, res) {

        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {
            if (user_list_info.length > 0 && user_list_info[0].user_role == 'owner') { // must be owner to perform this action...
                // delete lits from sytem...
                list_worker.delete_list(req.params.list_id, req.user).then(function (info) {
                    _common_workers.send_res(req, res, "List has been deleted successfully.");
                }, function (err) {
                    _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, "Not authorized to perform this action");
            }
        }, function (err) {
            _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
        });
    });

    /**
     * Used to fetch child lists information for list page
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, parent_list_id
     * @param   {object} res returns array of lists with extra information
     */
    app.get('/api/list/:list_id/children', function (req, res) {

        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0) { // must be a member to perform it...

                // prepre query to fetch information of Sub-lists
                var query = " " +
                    " SELECT    a.`id`,COUNT(c.`profile_id`) as number_of_influencers, a.`name`, a.`description`, SUM(`facebook_likes`) as fb_reach, SUM(`ig_followed_by`) as ig_reach, SUM(`youtube_subscriber_count`) as yt_reach, IFNULL(e.`total_subchilds`, 0) as total_subchilds, b.`user_role`" +
                    " FROM		`lists`			a " +
                    " JOIN		`lists_users`	b ON (b.`user_id` = '" + req.user.id + "' AND a.`parent_id` = '" + req.params.list_id + "' AND a.`deleted` = 'N' AND a.`id` = b.`list_id`) " +
                    " LEFT JOIN	 (       " +
                    "             SELECT    `parent_id`, COUNT(`id`) as total_subchilds      " +
                    "             FROM      `lists`                                           " +
                    "             GROUP BY  `parent_id`                                       " +
                    "            )as e ON (a.`id` = e.`parent_id`)                 " +
                    " LEFT JOIN	`lists_profiles`	c ON (a.`id` = c.`list_id`) " +
                    " LEFT JOIN	`profile_view`	d ON (c.`profile_id` = d.`profile_id`) " +
                    " GROUP BY	a.`id` " +
                    " ORDER BY	a.`id` DESC";

                db.query(query).then(function (lists) {
                    _common_workers.send_res(req, res, lists);
                }, function (err) {
                    _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, "Not authorized to perform this action");
            }

        }, function (err) {
            _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
        });
    });

    /**
     * Used to fetch profiles information for list page
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, list_id
     * @param   {object} res returns array of profiles with extra information
     */
    app.get('/api/list/:list_id/profiles', function (req, res) {

        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0) { // must be a member to perform it...

                // prepre query to fetch information of Sub-lists
                var query = " " +
                    " SELECT        a.`list_id`, b.`profile_id`, b.`profile_slug`, b.`profile_name`, b.`profile_tags`, b.`profile_country`, b.`profile_url`, b.`profile_gender`, b.`instagram_id`, b.`ig_followed_by`, b.`ig_profile_picture`, b.`facebook_id`, b.`facebook_picture`, b.`facebook_likes`, b.`youtube_id`, b.`youtube_profile_picture`, b.`youtube_view_count`, b.`youtube_comment_count`, b.`youtube_subscriber_count`, b.`youtube_video_count`, b.`total_followers`, b.`total_engagement_rate`" +
                    " FROM		    `lists_profiles`   a " +
                    " JOIN	        `profile_view`	   b ON (a.`list_id` = '" + req.params.list_id + "' AND a.`profile_id` = b.`profile_id`) ";

                db.query(query).then(function (lists) {
                    _common_workers.send_res(req, res, lists);
                }, function (err) {
                    _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, "Not authorized to perform this action");
            }

        }, function (err) {
            _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
        });
    });

    /**
     * Used to add comment
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, list_id, comment
     * @param   {object} res returns confirmation or error
     */
    app.post('/api/list/:list_id/add_comment', function (req, res) {
        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0) {

                // need to validate the input first...
                var validations = list_worker.validate_comment_params(req.body);
                if (validations === true) {
                    db.insert('list_messages', {
                        list_id: req.params.list_id,
                        user_id: req.user.id,
                        message: req.body.comment
                    }).then(function (sucess) {
                        _common_workers.send_res(req, res, "Comment has been added successfully.");
                    }, function (err) {
                        _common_workers.send_err(req, res, err.statusCode, err);
                    });
                } else {
                    _common_workers.send_err(req, res, 400, validations);
                }

            } else {
                _common_workers.send_err(req, res, 401, 'You need to be part of list to perform this action.');
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to fetch list comments
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, list_id
     * @param   {object} res returns comments or error
     */
    app.get('/api/list/:list_id/comments', function (req, res) {
        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0) {

                db.query("SELECT a.`id`, a.`message`, a.`created`, b.`id` as user_id, b.`firstname`, b.`lastname`, b.`email` FROM `list_messages` a JOIN `users` b ON(a.`list_id` = '" + req.params.list_id + "' AND a.`user_id` = b.`id`) ORDER BY a.`id` ASC").then(function (comments) {
                    _common_workers.send_res(req, res, comments);
                }, function (err) {
                    _common_workers.send_err(req, res, err.statusCode, err);
                });

            } else {
                _common_workers.send_err(req, res, 401, 'You need to be part of list to perform this action.');
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to export list in file
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, list_id
     * @param   {object} res returns downoadable file.
     */
    app.use(json2xls.middleware);
    app.get('/api/list/:list_id/export', function (req, res) {
        // need to check if user is authorize to perform this action or not
        list_worker.check_user_authorization(req.user.id, req.params.list_id).then(function (user_list_info) {

            if (user_list_info.length > 0) {

                var query = "  " +
                    "   SELECT        a.`id`, a.`name`,c.`instagram_id`,c.`facebook_id`,c.`youtube_id`, c.`profile_name`, c.`profile_country`, c.`profile_url`, c.`total_engagement_rate`, c.`total_followers`, c.`ig_followed_by`, c.`facebook_likes`, c.`youtube_subscriber_count`, a.`total_growth` " +
                    "   FROM		`lists` 					a " +
                    "   LEFT JOIN	`lists_profiles` 			b ON (a.`id` = b.`list_id`) " +
                    "   JOIN		`profile_view`			    c ON (a.`id` = '" + req.params.list_id + "' AND b.`profile_id` = c.`profile_id`) ";

                db.query(query).then(function (result) {
                    // prepare the data as needed by json2xls
                    var file_name = (result[0].name) ? _common_workers.to_slug(result[0].name) : 'no_list_found';

                    res.xls(file_name + '.xlsx', list_worker.prepare_list_export_data(result)); // I have added middleware so I can use res.xls directly...
                }, function (err) {
                    _common_workers.send_err(req, res, 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, 'You need to be part of list to perform this action.');
            }

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

};