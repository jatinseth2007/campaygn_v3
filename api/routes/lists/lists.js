/* jshint node: true */
"use strict";

var db = require('../../helpers/db');
var _common_workers = require('../../workers/_common');
var list_workers = require('../../workers/list/index');
var Q = require("q");

module.exports = function (app) {

    /**
     * Used to return the leaf nodes which is based on the currect session of user by parent list
     * @author jatin
     * @param   {object}   req     consist of header, logged in user information, parend list id, user_role
     * @param   {object} res returns leaf nodes of the parent list provided in req
     */
    app.get('/api/lists/get_child_lists_by_user', function (req, res) {

        var parent_id = (req.query.parent_id) ? req.query.parent_id : 0; // if no parent list_id provided means needs to provide root lists

        var user_role = (req.query.user_role && req.query.user_role == 'all') ? "'admin','owner','member'" : ("'" + req.query.user_role + "'"); // if we need to provide lists for user having role admin or all means user should be a member only.

        // prepare query to fetch lists assosiated with a user by parent list.
        var query = "SELECT b.`id`, b.`name` FROM `lists_users` a JOIN `lists` b ON(a.`list_id` = b.`id`) WHERE a.`user_id` = '" + req.user.id + "' AND a.`user_role` IN (" + user_role + ") AND b.`parent_id` = '" + parent_id + "' AND b.`deleted` = 'N';";

        db.query(query).then(function (result) {
            _common_workers.send_res(req, res, result);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });


    });

    /**
     * Used to return the base nodes assosiated with user
     * @author jatin
     * @param   {object}   req     consist of header, logged in user information, user_role - owner means only nodes whos owner is user, other means - user is member of lists, all means all togeather.
     * @param   {object} res returns leaf nodes of the parent list provided in req
     */
    app.get('/api/lists/get_root_lists_by_user', function (req, res) {

        var promises = []; // will collect promises in this...

        if (req.query.user_role == 'owner') {
            promises.push(list_workers.lists.fetch_user_owner_root_lists(req.user.id));
        } else if (req.query.user_role == 'other') {
            promises.push(list_workers.lists.fetch_user_other_root_lists(req.user.id));
        } else {
            promises.push(list_workers.lists.fetch_user_owner_root_lists(req.user.id));
            promises.push(list_workers.lists.fetch_user_other_root_lists(req.user.id));
        }

        Q.all(promises).then(function (data) {
            var data_to_send = [];
            if (data.length > 0) {
                data.forEach(function (item, index) {
                    data_to_send = data_to_send.concat(item);
                });
            }
            _common_workers.send_res(req, res, data_to_send);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });


    });

    /**
     * Used to remove profiles from a list
     * @author jatin
     * @param   {object}   req     consist of header, logged in user information, profiles
     * @param   {object} res returns confirmation success or error
     */
    app.post('/api/lists/remove_list_profiles', function (req, res) {

        // need to check if user is authorize to perform this action or not
        list_workers.list.check_user_authorization(req.user.id, req.body.parent_id).then(function (user_list_info) {
            if (user_list_info.length > 0) { // user just need to be part of list to perform this action...
                // remove profiles from list
                list_workers.list.remove_list_profiles(req.body).then(function (response) {
                    _common_workers.send_res(req, res, 'Profiles has been removed from list successfully.');
                }, function (err) {
                    _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, "Not authorized to perform this action");
            }
        }, function (err) {
            _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
        }).fail(function (err) {
            console.log(err);
        });
    });

    /**
     * Used to add profiles to a list
     * @author jatin
     * @param   {object}   req     consist of header, logged in user information, profiles
     * @param   {object} res returns confirmation success or error
     */
    app.post('/api/lists/add_list_profiles', function (req, res) {

        // need to check if user is authorize to perform this action or not
        list_workers.list.check_user_authorization(req.user.id, req.body.parent_id).then(function (user_list_info) {
            if (user_list_info.length > 0) { // user just need to be part of list to perform this action...
                // prepare data to add in table
                var influencers = list_workers.list.prepare_influencers_array(req.body.parent_id, req.body.profiles);

                // add profiles to list
                list_workers.list.add_influencers(influencers).then(function (response) {
                    _common_workers.send_res(req, res, 'Profiles has been added from list successfully.');
                }, function (err) {
                    _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 401, "Not authorized to perform this action");
            }
        }, function (err) {
            _common_workers.send_err(req, res, (err.statusCode) ? err.statusCode : 400, err);
        }).fail(function (err) {
            console.log(err);
        });
    });

    /**
     * Used to fetch list information for listing of owner lists
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, sorting information
     * @param   {object} res returns array of lists with extra information
     */
    app.get('/api/lists/fetch_owner_lists', function (req, res) {

        //Addind page and limit funcationality
        var page = _common_workers.check_page(req.query.page);
        var limit = _common_workers.check_limit(req.query.limit);

        //Preparing the count query
        var count_query = list_workers.lists.user_owner_root_lists_query(req, true);

        //Preparing the Query which will get us the reasult based on above filters
        var result_query = list_workers.lists.user_owner_root_lists_query(req, false);

        db.query({
            sql: count_query + result_query,
            paginate: {
                page: page,
                resultsPerPage: limit
            }
        }).then(function (result) {
            var data_to_send = {
                page: page,
                limit: limit,
                total_records: result[0][0].total_records,
                lists: result[1]
            };

            _common_workers.send_res(req, res, data_to_send);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    /**
     * Used to fetch list information for listing of member lists
     * @author Jatin Seth
     * @param   {object}   req     consist of header, logged in user information, sorting information
     * @param   {object} res returns array of lists with extra information
     */
    app.get('/api/lists/fetch_member_lists', function (req, res) {

        list_workers.lists.fetch_user_other_root_lists_details(req.user.id).then(function (result) {
            _common_workers.send_res(req, res, result);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });
};
