/* jshint node: true */
"use strict";

var db = require('../helpers/db');
var _common_workers = require('../workers/_common');
var YT_helper = require('../helpers/youtube');
var yt_workers = require('../workers/youtube/index');

module.exports = function (app) {

    /**
     * Find a channel on YouTube
     */
    app.get('/api/youtube/find_channel/:keyword', function (req, res) {
        yt_workers.users.get_youtube_channels(req.params.keyword, 10).then(function (channels) {
            _common_workers.send_res(req, res, channels);
        }, function (err) {
            _common_workers.send_err(req, res, 400, err);
        });
    });


    //Get random images of a facebook profile
    app.get('/api/youtube/get_random_images/:youtube_id/:count', function (req, res) {

        var query = " " +
            "   SELECT     `image`,`link`, 'video' as type                 " +
            "   FROM       (                            " +
            "                   SELECT     `thumbnail` as image, CONCAT('" + YT_helper.youtube_baseurl + "', `id`) as link  " +
            "                   FROM       `youtube_videos`                            " +
            "                   WHERE      `channel_id` = '" + req.params.youtube_id + "'" +
            "                   AND        `thumbnail` IS NOT NULL                   " +
            "                   ORDER BY    `published_at` DESC                                        " +
            "                   LIMIT       0," + (parseInt(req.params.count) + 10) +
            "               )as X                                                           " +
            "  ORDER BY     RAND()                                                           " +
            "  LIMIT        0," + req.params.count + ";";

        db.query(query).then(function (result) {
            if (!result)
                _common_workers.send_err(req, res, 400, "No Result found");
            else
                _common_workers.send_res(req, res, result);

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });

    //Get recent images of a profile.
    app.get('/api/youtube/get_recent_images/:youtube_id/:count', function (req, res) {

        db.query("SELECT `thumbnail` as image, CONCAT('" + YT_helper.youtube_baseurl + "', `id`) as link,`like_count` as likes_count, `comment_count` as comments_count, `view_count`, 'video' as type FROM `youtube_videos` WHERE `channel_id` = '" + req.params.youtube_id + "' ORDER BY `published_at` DESC LIMIT " + req.params.count + ";").then(function (result) {
            if (!result)
                _common_workers.send_err(req, res, 404, "No Result found");
            else
                _common_workers.send_res(req, res, result);

        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });

    //Get average number of views for user.
    app.get('/api/youtube/get_user_average_views/:youtube_id', function (req, res) {

        db.query("SELECT AVG(`view_count`) as averageNumber FROM ( SELECT `view_count` FROM `youtube_videos` WHERE `channel_id` = '" + req.params.youtube_id + "' ORDER BY `published_at` DESC LIMIT 0,10) as x;")
            .then(function (result) {
                if (!result)
                    _common_workers.send_err(req, res, 404, "No Result found");
                else
                    _common_workers.send_res(req, res, result);

            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });

    //Get user's most viewesd video
    app.get('/api/youtube/get_user_most_viewed/:youtube_id', function (req, res) {

        var query = "SELECT `thumbnail` as image, CONCAT('" + YT_helper.youtube_baseurl + "', `id`) as link, `view_count` as likes_count, 'video' as `type` FROM `youtube_videos` WHERE `channel_id` = '" + req.params.youtube_id + "' ORDER BY `view_count` DESC LIMIT 1";

        db.query(query)
            .then(function (result) {
                if (!result)
                    _common_workers.send_err(req, res, 404, "No Result found");
                else
                    _common_workers.send_res(req, res, result);

            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });

    });

    //Getting growth
    app.get('/api/youtube/get_growth/:youtube_id', function (req, res) {
        yt_workers.users.get_growth(req.params.youtube_id).then(function (data) {
            var data_to_send = {};

            if (data) {
                // add the 1 day diff data
                data_to_send.one_day_diff = (typeof data[0] !== 'undefined' && data[0].length > 0) ? data[0] : {};

                //add 7 days diff data
                data_to_send.seven_day_diff = (typeof data[1] !== 'undefined' && data[1].length > 0) ? data[1] : {};

                //add 30 days diff data
                data_to_send.thirty_day_diff = (typeof data[2] !== 'undefined' && data[2].length > 0) ? data[2] : {};

                // add data from MAX(date) to MIN(date) we have from last 6 Months
                data_to_send.avg_stats = (typeof data[3] !== 'undefined' && data[3].length > 0) ? data[3] : {};
            }

            _common_workers.send_res(req, res, data_to_send);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

    //Getting hashtag growth for brands
    app.get('/api/youtube/get_hashtagposts_growth/:youtube_id', function (req, res) {

        db.query("SELECT `id` FROM `tags` WHERE `tag` = (SELECT name FROM `youtube` WHERE	id = '" + req.params.youtube_id + "' ) LIMIT 0,1;").then(function (hashtag_result) {
            if (!hashtag_result || hashtag_result.length <= 0) {
                _common_workers.send_err(req, res, 404, "No Hashtag found");
            } else {
                if (hashtag_result[0].id) {
                    yt_workers.tags.get_growth(hashtag_result[0].id).then(function (data) {
                        var data_to_send = {};

                        if (data) {
                            // add the 1 day diff data
                            data_to_send.one_day_diff = (typeof data[0] !== 'undefined' && data[0].length > 0) ? data[0] : {};

                            //add 7 days diff data
                            data_to_send.seven_day_diff = (typeof data[1] !== 'undefined' && data[1].length > 0) ? data[1] : {};

                            //add 30 days diff data
                            data_to_send.thirty_day_diff = (typeof data[2] !== 'undefined' && data[2].length > 0) ? data[2] : {};

                            // add data from MAX(date) to MIN(date) we have from last 6 Months
                            data_to_send.avg_stats = (typeof data[3] !== 'undefined' && data[3].length > 0) ? data[3] : {};
                        }

                        _common_workers.send_res(req, res, data_to_send);
                    }, function (err) {
                        _common_workers.send_err(req, res, err.statusCode, err);
                    });
                } else {
                    _common_workers.send_err(req, res, 404, "No Hashtag found");
                }
            }
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });
};
