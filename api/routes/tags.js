/* jshint node: true */
"use strict";

var Q = require("q");
var db = require('../helpers/db');
var _common_workers = require('../workers/_common');

module.exports = function (app) {
    /**
     * Search a tag based on most post counts from 6 months
     * @author NarayaN Yaduvanshi
     */
    app.get('/api/tags/:keyword', function (req, res) {
        var string_to_query = req.params.keyword + '%';
        var query = " " +
            "SELECT      a.`tag`, SUM(b.`posts_count`) as posts_count   " +
            "FROM        `tags`    a                                    " +
            "JOIN        (                                              " +
            "              SELECT      a.`id`, count(c.`id`) AS `posts_count`       " +
            "              FROM        `tags`                          a            " +
            "              JOIN        `instagram_name_tags_posts`     b ON (a.`tag` LIKE ? AND a.`id` = b.`tag_id`)    " +
            "              JOIN        `instagram_posts`               c ON (b.`post_id` = c.`id`)                              " +
            "              WHERE       c.`created_time` > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 6 MONTH))                     " +
            "              GROUP BY    a.`id`                                                                                   " +
            "              UNION ALL                                                                                            " +
            " " +
            "              SELECT      a.`id`, count(c.`id`) AS `posts_count`                                                   " +
            "              FROM        `tags`                          a                                                        " +
            "              JOIN        `instagram_tags_posts`     b ON (a.`tag` LIKE ? AND a.`id` = b.`tag_id`)            " +
            "              JOIN        `instagram_posts`               c ON (b.`post_id` = c.`id`)                                  " +
            "              WHERE       c.`created_time` > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 6 MONTH))                     " +
            "              GROUP BY    a.`id`                                                                                   " +
            "              UNION ALL                                                                                            " +
            " " +
            "              SELECT      a.`id`, count(c.`id`) AS `posts_count`                                                       " +
            "              FROM        `tags`                          a " +
            "              JOIN        `facebook_hash_tags_posts`      b ON (a.`tag` LIKE ? AND a.`id` = b.`tag_id`)        " +
            "              JOIN        `facebook_posts`               c ON (b.`post_id` = c.`id`)                                   " +
            "              WHERE       c.`created_time` > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 6 MONTH))                         " +
            "              GROUP BY    a.`id`                                                                                       " +
            "              UNION ALL                                                                                                         " +
            " " +
            "               SELECT      a.`id`, count(c.`id`) AS `posts_count`                                                              " +
            "               FROM        `tags`                          a                                                                   " +
            "               JOIN        `facebook_name_tags_posts`      b ON (a.`tag` LIKE ? AND a.`id` = b.`tag_id`)               " +
            "               JOIN        `facebook_posts`               c ON (b.`post_id` = c.`id`)                                          " +
            "               WHERE       c.`created_time` > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 6 MONTH))                                " +
            "               GROUP BY    a.`id`                                                                                              " +
            "               UNION ALL                                                                                                                " +
            " " +
            "               SELECT      a.`id`, count(c.`id`) AS `posts_count`                                                                      " +
            "               FROM        `tags`                          a                                                                           " +
            "               JOIN        `youtube_tags_videos`           b ON (a.`tag` LIKE ? AND a.`id` = b.`tag_id`)                       " +
            "               JOIN        `youtube_videos`                c ON (b.`video_id` = c.`id`)                                                " +
            "               WHERE       `published_at` > UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 6 MONTH))                                             " +
            "               GROUP BY    a.`id`                                                                                                  " +
            "             )as b ON (a.`tag` LIKE ? AND a.`id` = b.`id`)                                                     " +
            "GROUP BY    a.`id` " +
            "ORDER BY    SUM(b.`posts_count`) DESC " +
            "LIMIT       0,10; ";

        db.query(query, [string_to_query, string_to_query, string_to_query, string_to_query, string_to_query, string_to_query]).then(function (tags) {
            _common_workers.send_res(req, res, tags);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });
    });

};
