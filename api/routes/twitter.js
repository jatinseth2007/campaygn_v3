/* jshint node: true */
"use strict";

var twitter = require('../helpers/twitter');
module.exports = function(app) {

    //find a user on twitter
    app.get('/twitter/find_user/:user', function(req, res) {
        twitter.get('users/search', {
            q: req.params.user,
            count: 1,
            include_entities: false,
            page: 1
        }, function(error, users, response) {
            //res.json(users);
            if (!error && users.length > 0) {
                res.json({
                    "id": users[0].id,
                    "name": users[0].name,
                    "username": users[0].screen_name,
                    "location": users[0].location,
                    "description": users[0].description,
                    "url": users[0].url,
                    "followers_count": users[0].followers_count,
                    "banner_image": users[0].profile_banner_url,
                    "profile_image": users[0].profile_image_url,
                    "lang": users[0].lang,
                });

            } else {
                res.status(404).json(error);
            }
        });

    });
};
