/* jshint node: true */
"use strict";

module.exports = function (app, passport) {
    //Common/helper routs
    require('./_common')(app);

    // Social Routes
    require('./instagram')(app);
    require('./facebook')(app);
    require('./twitter')(app);
    require('./youtube')(app);

    //Loading tags routes
    require('./tags')(app);
    require('./tag')(app);

    // Local DB routes
    require('./profile/profile')(app);
    require('./profile/profiles')(app);
    require('./lists/lists')(app);
    require('./lists/list')(app);
    require('./user')(app, passport);
    require('../helpers/user/user.js')(passport);
    require('./monitors')(app);
    require('./analytics')(app);
};