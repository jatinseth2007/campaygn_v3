/* jshint node: true */
"use strict";

var _common_workers = require('../workers/_common');
var analytics_worker = require('../workers/analytics');
var json2xls = require('json2xls');

module.exports = function (app) {
    /**
     * Used to fetch events details
     * @author karthick.k
     * @param   {object} req consist of header, etc
     * @param   {object} res response header
     */
    app.get('/api/analytics/events_details', function (req, res) {
        analytics_worker.get_events(req).then(function (data) {
            _common_workers.send_res(req, res, data);
        }).fail(function (err) {
            console.log(err);
            _common_workers.send_err(req, res, 400, "Error while fetching events details");
        });
    });

    /**
     * Used to export the data in xlsx files based on the category
     * @author karthick.k
     * @param   {object} req consist of header, etc
     * @param   {object} res response header
     */
    app.get('/api/analytics/export', function (req, res) {
        if (!req || !req.query) {
            return _common_workers.send_err(req, res, 400, "Please fill mandatory fields");
        }
        if (!req.query.from) {
            return _common_workers.send_err(req, res, 400, "From date is required");
        }
        if (!req.query.to) {
            return _common_workers.send_err(req, res, 400, "To date is required");
        }
        if (!req.query.category) {
            return _common_workers.send_err(req, res, 400, "Category is required");
        }
        analytics_worker.export(req).then(function (data) {
            var form_details = req.query;
            var file_name = "Influencers_added_from_" + form_details.from + "_to_" + form_details.to + ".xlsx";
            res.xls(file_name, data);
        }).fail(function (err) {
            console.log(err);
            _common_workers.send_err(req, res, 400, "Error while exporting category details");
        });
    });
};