/* jshint node: true */
"use strict";

var Q = require('q');
var moment = require('moment');
var db = require('../../helpers/db');
var visitor = require('../../helpers/visitor.js');
var ig_workers = require('../../workers/instagram/index');
var fb_workers = require('../../workers/facebook');
var yt_workers = require('../../workers/youtube');
var _common_workers = require('../../workers/_common');
var profile_worker = require('../../workers/profile/profile');
var analytics = require("../../workers/analytics");

module.exports = function (app) {


    //Get an profile
    app.get('/api/profile/get_profile_data/:id', function (req, res) {
        db.selectOne({
                table: 'profile_view',
            }, {
                profile_id: req.params.id,
            }

        ).then(function (result) {
            if (!result) {
                _common_workers.send_err(req, res, 404, "No Result found");
            } else {
                /**
                 * Google analytics to tracks profile views
                 */
                visitor.event("Profile - view", result.profile_name, function (err) {
                    if (err) {
                        console.log('Event error for sending analytics in profile-view' + err);
                    }
                });
                _common_workers.send_res(req, res, result);
                //Analytics - register profile open
                if (req.user && req.user.hasOwnProperty("email")) {
                    analytics.register_event("profile", "opened", "profile_id", req.params.id, req.user.email, req.user.fullname);
                } else {
                    analytics.register_event("profile", "opened", "profile_id", req.params.id, "unknown", "unknown");
                }
            }
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });

    //Generate slug for profile...
    app.get('/api/profile/generate_slug', function (req, res) {
        // generate slug from name
        var slug = _common_workers.to_slug(req.query.name);

        db.query("SELECT COUNT(`id`) as num FROM `profiles` WHERE `name` LIKE '" + req.query.name.trim() + "';").then(function (result) {
            if (result && result[0].num && result[0].num > 0) {
                slug = slug + '-' + (parseInt(result[0].num) + 1);
            }
            _common_workers.send_res(req, res, slug);
        }, function (err) {
            _common_workers.send_err(req, res, err.statusCode, err);
        });

    });

    //Fetch User's stats for Graph
    app.get('/api/profile/get_user_stats/:user_id', function (req, res) {

        var start_date = req.query.start_date && moment(Date.parse(req.query.start_date)).isValid() ? moment(Date.parse(req.query.start_date)).format("YYYY-MM-DD") : moment().subtract(6, 'months').format("YYYY-MM-DD");
        var end_date = req.query.end_date && moment(Date.parse(req.query.end_date)).isValid() ? moment(Date.parse(req.query.end_date)).format("YYYY-MM-DD") : moment().format("YYYY-MM-DD");
        var query = '';
        var data_to_send = {};
        var promises = [];

        // fetch User's profile data to move forward
        _common_workers.get_data('profiles', 'instagram,facebook,youtube', 'id', req.params.user_id).then(function (profile_data) {
            if (profile_data) {

                // if all platform are null then error
                if (profile_data[0].instagram === null && profile_data[0].facebook === null && profile_data[0].youtube === null) {
                    _common_workers.send_err(req, res, 404, "No Result found");
                }

                // check if we have instagram for the user; if yes then fetch instagram data and add in the output
                if (profile_data[0].instagram && profile_data[0].instagram !== null) {
                    query = "SELECT `date`, `followed_by` FROM `instagram_daily_count` WHERE `instagram_id` ='" + profile_data[0].instagram + "'  AND `date` >= '" + start_date + "' AND `date` <= '" + end_date + "'  ORDER BY `date` ASC;";

                    var instagram_values = db.query({
                        sql: query
                    }).then(function (result) {
                        if (result) {
                            data_to_send.instagram = result;
                        }
                    });
                    promises.push(instagram_values);
                }

                // check if we have facebook for the user; if yes then fetch facebook data and add in the output
                if (profile_data[0].facebook && profile_data[0].facebook !== null) {
                    query = "SELECT `date`, `likes` as followed_by FROM `facebook_insights` WHERE `user_id` ='" + profile_data[0].facebook + "'  AND `date` >= '" + start_date + "' AND `date` <= '" + end_date + "'  ORDER BY `date` ASC;";

                    var facebook_values = db.query({
                        sql: query
                    }).then(function (result) {
                        if (result) {
                            data_to_send.facebook = result;
                        }
                    });

                    promises.push(facebook_values);
                }

                // check if we have youtube for the user; if yes then fetch youtube data and add in the output
                if (profile_data[0].youtube && profile_data[0].youtube !== null) {
                    query = "SELECT `date`, `subscriber_count` as followed_by FROM `youtube_insights` WHERE `id` ='" + profile_data[0].youtube + "'  AND `date` >= '" + start_date + "' AND `date` <= '" + end_date + "'  ORDER BY `date` ASC;";

                    var youtube_values = db.query({
                        sql: query
                    }).then(function (result) {
                        if (result) {
                            data_to_send.youtube = result;
                        }
                    });

                    promises.push(youtube_values);
                }

                /*Response*/
                Q.all(promises).then(function (data) {
                    if (data_to_send)
                        _common_workers.send_res(req, res, data_to_send);
                    else
                        _common_workers.send_err(req, res, 404, "No Result found");
                }, function (err) {
                    _common_workers.send_err(req, res, err.statusCode, err);
                });

            } else {
                _common_workers.send_err(req, res, 404, "Profile not found");
            }
        });
    });

    //Insert an profile
    app.post('/api/profile/add', function (req, res) {
        //Validations are very much required, don't want to break my code
        profile_worker.validate_profile_input(req.body, function (err) {
            if (err) {
                _common_workers.send_err(req, res, 400, err);
            }

            // need to set the sub-kind
            if (req.body.kind !== 'brand' && req.body.kind !== 'influencer') {
                req.body.sub_kind = req.body.kind; // cuz we have only sub-kind for influencers to select
                req.body.kind = 'influencer'; // set kind
            } //EO-if

            //First save or insert his record
            db.insert('profiles', req.body).then(function (db_info) {

                var promises = [];

                /**
                 * Checking if there is an instagram id then find his latest instagram id and save it
                 */

                if (req.body.instagram) {

                    promises.push(ig_workers.users.get_user_details(req.body.instagram).then(function (user) {
                        if (user === 'private') {
                            throw 'Instagram account is private';
                        }

                        return ig_workers.users.update_instagram_profile(user);
                    }, function (err) {
                        throw err;
                    }));
                } else {
                    promises.push(true);
                }

                /**
                 * Checking if there is an facebook id then find his latest instagram id and save it
                 */

                if (req.body.facebook) {
                    promises.push(fb_workers.user.save_facebook_user_details(req.body.facebook));
                } else {
                    promises.push(true);
                }

                /**
                 * Checking for youtube ID
                 */

                if (req.body.youtube) {
                    promises.push(yt_workers.users.get_youtube_user(req.body.youtube).then(function (data) {
                        return yt_workers.users.save_youtube_user(data);
                    }, function (err) {
                        throw err;
                    }));
                } else {
                    promises.push(true);
                }

                Q.allSettled(promises).then(function (results) {
                    var errors = {};

                    // checking instagram response...
                    if (results[0].state && results[0].state === 'rejected') {
                        errors.instagram = (typeof results[0].reason === 'string') ? results[0].reason : 'We are not able to fetch instagram details, please check if profile is valid.';
                    }

                    // checking facebook response...
                    if (results[1].state && results[1].state === 'rejected') {
                        errors.facebook = (typeof results[1].reason === 'string') ? results[1].reason : 'We are not able to fetch facebook details, please check if profile is valid.';
                    }

                    // checking youtube response...
                    if (results[2].state && results[2].state === 'rejected') {
                        errors.youtube = (typeof results[2].reason === 'string') ? results[2].reason : 'We are not able to fetch youtube details, please check if profile is valid.';
                    }

                    if (Object.keys(errors).length > 0) {
                        errors.message = "Profile has been added successfully but we are getting some errors regarding platforms, please contact site admin for more details.";
                        _common_workers.send_err(req, res, 404, errors);
                    }

                    _common_workers.send_res(req, res, "Profile has been saved successfully.");
                });

                /**
                 * Google analytics to tracks profile views
                 */
                visitor.event("Profile - add", req.user.email, req.body.name, function (err) {
                    if (err) {
                        console.log('Event error for sending analytics in profile-view' + err);
                    }
                });

            }, function (err) {
                _common_workers.send_err(req, res, 400, err);
            });

        });
    });

    /**
     * Used to update Profile info
     * @author Jatin Seth
     * @param   {object}   req profile_id, object with field value pair
     * @param   {object} res returns error or confirmation
     */
    app.post('/api/profile/edit/:profile_id', function (req, res) {
        // to save errors...
        var errors = {};

        // need to validate first, don't want to break my code...
        // profile id
        if (!req.params.profile_id || isNaN(req.params.profile_id) || Object.keys(req.body).length === 0) {
            errors.profile_id = "Information you provided is not valid.";
        }

        // birthday
        if (req.body.birthday && !moment(req.body.birthday).isValid()) {
            errors.birthday = "Please provide valid birthday.";
        }

        if (Object.keys(errors).length === 0) {
            db.update('profiles', req.body, {
                id: req.params.profile_id
            }).then(function (data) {
                _common_workers.send_res(req, res, "Profile has been updated successfully.");
            }, function (err) {
                _common_workers.send_err(req, res, 400, err);
            });
        } else {
            _common_workers.send_err(req, res, 400, errors);
        }
    });

    /**
     * Used to update Profile's instagram profile...
     * @author Jatin Seth
     * @param   {object}   req profile_id, object with field value pair
     * @param   {object} res returns error or confirmation
     */
    app.post('/api/profile/edit_instagram/:profile_id', function (req, res) {
        // to save errors...
        var errors = {};
        var promises = [];

        // social profiles - instagram
        if (!req.body.instagram) {
            errors.instagram = "Please provide valid instagram profile.";
        } else {
            // check if instagram already exist or not.
            if (req.body.instagram) {
                promises.push(db.selectOne('profiles', {
                    instagram: req.body.instagram
                }).then(function (result) {
                    if (result) {
                        errors.instagram = "The instagram you provided is already exist.";
                    }
                    return true;
                }, function (err) {
                    return err;
                }));
            }
        }

        Q.all(promises).then(function (data) {
            if (Object.keys(errors).length === 0) {

                // will have to fetch profile data...
                db.selectOne('profiles', {
                    id: req.params.profile_id
                }).then(function (profile_info) {

                    // delete the existing instagram profile data...
                    db.delete('instagram', {
                        id: profile_info.instagram
                    }).then(function (data) {

                        //update profile with new instagram profile...
                        db.update('profiles', {
                            instagram: req.body.instagram
                        }, {
                            id: req.params.profile_id
                        }).then(function (data) {

                            // add new instagram profile in the system...
                            var ig_promises = [];

                            ig_promises.push(ig_workers.users.get_user_details(req.body.instagram).then(function (user) {
                                if (user === 'private') {
                                    throw 'Instagram account is private';
                                }

                                return ig_workers.users.update_instagram_profile(user);
                            }, function (err) {
                                throw err;
                            }));

                            Q.all(ig_promises).then(function (data) {
                                _common_workers.send_res(req, res, "Instagram has been updated successfully.");
                            }, function (err) {
                                _common_workers.send_err(req, res, 400, "Profile has been updated successfully but we are getting some errors from instagram, please contact site admin for more details.");
                            });
                        }, function (err) {
                            _common_workers.send_err(req, res, 400, err);
                        });
                    }, function (err) {
                        _common_workers.send_err(req, res, 400, err);
                    });
                }, function (err) {
                    _common_workers.send_err(req, res, 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 400, errors);
            }
        }, function (err) {
            _common_workers.send_err(req, res, 400, err);
        });
    });

    /**
     * Used to update Profile's facebook profile...
     * @author Jatin Seth
     * @param   {object}   req profile_id, object with field value pair
     * @param   {object} res returns error or confirmation
     */
    app.post('/api/profile/edit_facebook/:profile_id', function (req, res) {
        // to save errors...
        var errors = {};
        var promises = [];

        // social profiles - facebook
        if (!req.body.facebook) {
            errors.facebook = "Please provide valid facebook profile.";
        } else {
            // check if facebook already exist or not.
            if (req.body.facebook) {
                promises.push(db.selectOne('profiles', {
                    facebook: req.body.facebook
                }).then(function (result) {
                    if (result) {
                        errors.facebook = "The facebook you provided is already exist.";
                    }
                    return true;
                }, function (err) {
                    return err;
                }));
            }
        }

        Q.all(promises).then(function (data) {
            if (Object.keys(errors).length === 0) {

                // will have to fetch profile data...
                db.selectOne('profiles', {
                    id: req.params.profile_id
                }).then(function (profile_info) {

                    // delete the existing facebook profile data...
                    db.delete('facebook', {
                        id: profile_info.facebook
                    }).then(function (data) {

                        //update profile with new facebook profile...
                        db.update('profiles', {
                            facebook: req.body.facebook
                        }, {
                            id: req.params.profile_id
                        }).then(function (data) {

                            // add new facebook profile in the system...
                            var fb_promises = [];

                            fb_promises.push(fb_workers.user.save_facebook_user_details(req.body.facebook));

                            Q.all(fb_promises).then(function (data) {
                                _common_workers.send_res(req, res, "Facebook has been updated successfully.");
                            }, function (err) {
                                _common_workers.send_err(req, res, 400, "Profile has been updated successfully but we are getting some errors from facebook, please contact site admin for more details.");
                            });
                        }, function (err) {
                            _common_workers.send_err(req, res, 400, err);
                        });
                    }, function (err) {
                        _common_workers.send_err(req, res, 400, err);
                    });
                }, function (err) {
                    _common_workers.send_err(req, res, 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 400, errors);
            }
        }, function (err) {
            _common_workers.send_err(req, res, 400, err);
        });
    });

    /**
     * Used to update Profile's youtube profile...
     * @author Jatin Seth
     * @param   {object}   req profile_id, object with field value pair
     * @param   {object} res returns error or confirmation
     */
    app.post('/api/profile/edit_youtube/:profile_id', function (req, res) {
        // to save errors...
        var errors = {};
        var promises = [];

        // social profiles - youtube
        if (!req.body.youtube) {
            errors.youtube = "Please provide valid youtube profile.";
        } else {
            // check if youtube already exist or not.
            if (req.body.youtube) {
                promises.push(db.selectOne('profiles', {
                    youtube: req.body.youtube
                }).then(function (result) {
                    if (result) {
                        errors.youtube = "The youtube you provided is already exist.";
                    }
                    return true;
                }, function (err) {
                    return err;
                }));
            }
        }

        Q.all(promises).then(function (data) {
            if (Object.keys(errors).length === 0) {

                // will have to fetch profile data...
                db.selectOne('profiles', {
                    id: req.params.profile_id
                }).then(function (profile_info) {

                    // delete the existing youtube profile data...
                    db.delete('youtube', {
                        id: profile_info.youtube
                    }).then(function (data) {

                        //update profile with new youtube profile...
                        db.update('profiles', {
                            youtube: req.body.youtube
                        }, {
                            id: req.params.profile_id
                        }).then(function (data) {

                            // add new youtube profile in the system...
                            var yt_promises = [];

                            yt_promises.push(yt_workers.users.get_youtube_user(req.body.youtube).then(function (data) {
                                return yt_workers.users.save_youtube_user(data);
                            }, function (err) {
                                throw err;
                            }));

                            Q.all(yt_promises).then(function (data) {
                                _common_workers.send_res(req, res, "Youtube has been updated successfully.");
                            }, function (err) {
                                _common_workers.send_err(req, res, 400, "Profile has been updated successfully but we are getting some errors from youtube, please contact site admin for more details.");
                            });
                        }, function (err) {
                            _common_workers.send_err(req, res, 400, err);
                        });
                    }, function (err) {
                        _common_workers.send_err(req, res, 400, err);
                    });
                }, function (err) {
                    _common_workers.send_err(req, res, 400, err);
                });
            } else {
                _common_workers.send_err(req, res, 400, errors);
            }
        }, function (err) {
            _common_workers.send_err(req, res, 400, err);
        });
    });

    /**
     * Used to delete Profile...
     * @author Jatin Seth
     * @param   {object}   req profile_id
     * @param   {object} res returns error or confirmation
     */
    app.delete('/api/profile/delete/:profile_id', function (req, res) {
        // we are using soft-delete for profiles so just need to update some fields to delete
        db.update('profiles', {
            deleted: 'Y',
            verified: 'N'
        }, {
            id: req.params.profile_id
        }).then(function (data) {
            _common_workers.send_res(req, res, "Profile has been deleted successfully.");
        }, function (err) {
            _common_workers.send_err(req, res, 400, err);
        });

    });

    /**
        API to Re-sink profile data for all social medias
        Input: profile ID 
        Output: none
    */
    app.put('/api/profile/:profile_id', function (req, res) {
        //Validation goes here
        if (!req.params.profile_id) {
            res.status(404).send("Profile not found, please check the profile_id");
        }

        var platforms = (req.body.platform) ? req.body.platform : ['instagram', 'facebook', 'twitter', 'youtube'];

        // need to fetch the profile to move further...
        db.selectOne({
            table: 'profiles',
            fields: platforms
        }, {
            id: req.params.profile_id
        }).then(function (result) {
            var promises = [];

            //check if we have instagram info for profile, if yes, update it
            if (result.instagram) {
                var instagram_prmise = ig_workers.users.get_user_details(result.instagram).then(function (user) {
                    return ig_workers.users.update_instagram_profile(user);
                });

                promises.push(instagram_prmise);
            }

            /*Response*/
            Q.all(promises).then(function (data) {
                res.status(200).send("Profile Saved");
            }, function (err) {
                res.status(err.statusCode).send("Profile not saved " + err);
            });


        }, function (err) {
            res.status(err.statusCode).json(err);
        });
    });
};