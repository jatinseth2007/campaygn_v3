/* jshint node: true */
"use strict";

var db = require('../../helpers/db');
var _common_workers = require('../../workers/_common');
var profiles_workers = require('../../workers/profile/profiles');

module.exports = function (app) {
    /**
     * return the profiles list
     */
    app.get('/api/profiles', function (req, res) {

        // Preparing the on codition from req.queries and req params for Filtering.
        var on_condition = profiles_workers.prepare_condition(req);

        //Preparing the count query
        var count_query = profiles_workers.prepare_query_pagination(req, on_condition, true);

        //Preparing the Query which will get us the reasult based on above filters
        var result_query = profiles_workers.prepare_query_pagination(req, on_condition, false);

        //Addind page and limit funcationality
        var page = _common_workers.check_page(req.query.page);
        var limit = _common_workers.check_limit(req.query.limit);

        db.query({
                sql: count_query + result_query,
                paginate: {
                    page: page,
                    resultsPerPage: limit
                }
            })
            .then(function (users) {
                var data_to_send = {
                    page: page,
                    limit: limit,
                    total_records: users[0][0].total,
                    users: users[1]
                };

                _common_workers.send_res(req, res, data_to_send);
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
    });

    /**
     * return the profiles range only
     */
    app.get('/api/profiles/get_profiles_range/:active_platform', function (req, res) {
        try {
            var range_factor;

            // need to check the platform and select range accrodingly...
            range_factor = profiles_workers.get_reachfactor_by_platform(req.params.active_platform);

            db.one("SELECT MIN(`" + range_factor + "`) as minimum_value,MAX(`" + range_factor + "`) as maximum_value FROM `profile_view` WHERE " + range_factor + " IS NOT NULL").then(function (result) {
                if (!result) {
                    _common_workers.send_err(req, res, 404, "No Result found");
                } else {
                    var data_to_send = {
                        minimum_value: result.minimum_value,
                        maximum_value: result.maximum_value
                    };

                    _common_workers.send_res(req, res, data_to_send);
                }
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
        } catch (error) {
            _common_workers.send_err(req, res, 404, error);
        }
    });

    /**
     * return the profiles engagement range
     */
    app.get('/api/profiles/get_profiles_engagement_rate_range/:active_platform', function (req, res) {
        try {
            var range_factor;

            range_factor = profiles_workers.get_engagementfactor_by_platform(req.params.active_platform);

            db.one("SELECT FLOOR(MIN(`" + range_factor + "`)) as minimum_value, CEIL(MAX(`" + range_factor + "`)) as maximum_value FROM `profile_view` WHERE " + range_factor + " IS NOT NULL").then(function (result) {
                if (!result) {
                    _common_workers.send_err(req, res, 404, "No Result found");
                } else {
                    var data_to_send = {
                        minimum_value: result.minimum_value,
                        maximum_value: result.maximum_value
                    };

                    _common_workers.send_res(req, res, data_to_send);
                }
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
        } catch (error) {
            _common_workers.send_err(req, res, 404, error);
        }
    });

    /**
     * return the profiles age range
     */
    app.get('/api/profiles/get_profiles_age_range', function (req, res) {
        try {

            db.one("SELECT MIN(profile_age_min) as min_age, MAX(profile_age_max) as max_age FROM `profile_view` WHERE profile_age_min IS NOT NULL AND profile_age_max IS NOT NULL").then(function (result) {
                if (!result) {
                    _common_workers.send_err(req, res, 404, "No Result found");
                } else {
                    var data_to_send = {
                        min_age: result.min_age,
                        max_age: result.max_age
                    };

                    _common_workers.send_res(req, res, data_to_send);
                }
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
        } catch (error) {
            _common_workers.send_err(req, res, 404, error);
        }
    });

    /**
     * return the profiles growth range
     */
    app.get('/api/profiles/get_profiles_growth_range/:active_platform', function (req, res) {
        try {
            var range_factor;

            range_factor = profiles_workers.get_growthfactor_by_platform(req.params.active_platform);

            db.one("SELECT FLOOR(MIN(`" + range_factor + "`)) as minimum_value, CEIL(MAX(`" + range_factor + "`)) as maximum_value FROM `profile_view` WHERE " + range_factor + " IS NOT NULL").then(function (result) {
                if (!result) {
                    _common_workers.send_err(req, res, 404, "No Result found");
                } else {
                    var data_to_send = {
                        minimum_value: result.minimum_value,
                        maximum_value: result.maximum_value
                    };

                    _common_workers.send_res(req, res, data_to_send);
                }
            }, function (err) {
                _common_workers.send_err(req, res, err.statusCode, err);
            });
        } catch (error) {
            _common_workers.send_err(req, res, 404, error);
        }
    });


};
