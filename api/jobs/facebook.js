/* jshint node: true */
"use strict";

/**
 * This File hold the list of jobs that can be called to save data to server or retrieve them
 */

var db = require('../helpers/db');
var Q = require("q");
var fb_workers = require('../workers/facebook/index');
var moment = require('moment');
var _ = require("lodash");
var _common_worker = require('../workers/_common');
var async = require("async");

module.exports = {

    get_user_to_sync: function () {
        var def = Q.defer();

        //Using CEIL mysql function to round the number to biggest positive digits so that we never skip any profile to sync
        //Finding all the total facebook
        return db.one("SELECT CEIL(COUNT(`fb_id`)/24) as `total` FROM `facebook_live_users_view`")
            .then(function (data) {

                var hour = moment().get('hour');

                //Selecting users to fetch the data based on the current hour.
                return db.query({
                    sql: "SELECT * FROM `facebook_live_users_view` ORDER BY `fb_likes` DESC",
                    paginate: {
                        page: hour + 1,
                        resultsPerPage: data.total
                    }
                });
            });
    },
    save_facebook_daily_posts: function () {
        var self = this;
        var count = 0;
        var def = Q.defer();

        //Get All the users of facebook...
        self.get_user_to_sync().then(function (fb_users) {

            async.eachSeries(fb_users, function (fb_user, callback) {
                fb_workers.post.get_fb_reccent_posts(fb_user.fb_id).then(function (posts) {
                    if (!posts) {
                        count++;
                        callback();
                    }

                    // save all the posts for this...
                    fb_workers.post.save_posts(posts).then(function (data) {
                        count++;
                        callback();
                    }, function (err) {
                        callback(err);
                    });
                });
            }, function (err) {
                if (err) {
                    console.log("Facebook : Error in saving facebook post", err);
                    def.resolve("done");
                } else {
                    console.log("Facebook : posts saved for", count, "users.");
                    def.resolve("done");
                }
            });

        }, function (err) {
            throw _common_worker.generate_worker_error(err, {
                error_type: 'mysql',
                message: "Error in fetching data from get_user_to_sync method"
            }, __filename);
        }).fail(function (err) {
            console.log("Facebook: error in saving FB posts", err);
            def.resolve("done");
        });

        return def.promise;

    },
    save_facebook_daily_insights: function () {
        //Get All the users of instagram
        return this.get_user_to_sync().then(function (fb_users) {
            //Going through each user
            var saved_fb_users = fb_users.map(function (fb_user) {
                return fb_workers.insights.save_fb_daily_insights(fb_user.fb_id);
            });

            //preparing console
            return Q.all(saved_fb_users).then(function (saved_insights) {
                console.log("Facebook : Daily insights saved for", saved_insights.length, "users.");
                return true;
            }, function (err) {
                console.log("Facebook : Error in saving Daily insights", err);
                return true;
            });
        }, function (err) {
            throw _common_worker.generate_worker_error(err, {
                error_type: 'mysql',
                message: "Error in fetching data from get_user_to_sync method"
            }, __filename);
        }).fail(function (err) {
            console.log("Facebook: Error in saving Daily insights", err);
            return true;
        });
    },
    save_facebook_page_fans_country: function () {

        //Get All the users of instagram
        return this.get_user_to_sync().then(function (fb_users) {

            //Going through each user
            var saved_fb_users = fb_users.map(function (fb_user) {
                return fb_workers.insights.save_fb_page_fans_country(fb_user.fb_id);
            });

            //preparing console
            return Q.all(saved_fb_users).then(function (saved_insights) {
                console.log("Facebook : page_fans_country saved for", saved_insights.length, "users.");
                return true;
            }, function (err) {
                console.log("Facebook : Error in saving page_fans_country", err);
                return true;
            });

        }, function (err) {
            throw _common_worker.generate_worker_error(err, {
                error_type: 'mysql',
                message: "Error in fetching data from get_user_to_sync method"
            }, __filename);
        }).fail(function (err) {
            console.log("Facebook: error in saving page_fans_country", err);
            return true;
        });

    }
};
