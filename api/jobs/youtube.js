/* jshint node: true */
"use strict";

/**
 * This File hold the list of jobs that can be called to save data to server or retrieve them
 */

var Q = require("q");
var _ = require("lodash");
var moment = require('moment');
var db = require('../helpers/db');
var _cw = require('../workers/_common');
var yt_workers = require('../workers/youtube/');
var async = require("async");

module.exports = {
    /**
     * Fetches the info from youtube
     * @author NarayaN Yaduvanshi
     * @returns {[[Type]]} [[Description]]
     */
    get_user_to_sync: function () {

        //Using CEIL mysql function to round the number to biggest positive digits so that we never skip any profile to sync
        //Finding all the total youtubers
        return db.one("SELECT CEIL(COUNT(`yt_id`)/24) as `total` FROM `youtube_live_users_view`")
            .then(function (data) {

                var hour = moment().get('hour');

                //Selecting users to fetch the data based on the current hour.
                return db.query({
                    sql: "SELECT * FROM `youtube_live_users_view` ORDER BY `yt_subscribers` DESC",
                    paginate: {
                        page: hour + 1,
                        resultsPerPage: data.total
                    }
                });
            });
    },

    /**
     * Fetch a youtube user details and saves the data
     * @author NarayaN Yaduvanshi
     * @returns {[[Type]]} [[Description]]
     */
    sync_youtube: function () {
        var self = this;
        var count = 0;
        var def = Q.defer();

        this.get_user_to_sync().then(function (users) {

            async.eachSeries(users, function (user, callback) {
                //Get this youtubers complete details.
                yt_workers.users.get_youtube_user(user.yt_id).then(function (user_details) {

                    //Save details to main table
                    var saved_user = yt_workers.users.save_youtube_user(user_details);

                    //Save insights to insights table
                    var saved_insights = yt_workers.users.save_youtube_daily_insights(user_details);

                    //Get the recently uploaded videos from the playlist ids
                    var saved_videos = yt_workers.videos.get_youtube_playlist_videos(user_details.upload_playlist_id).then(function (videos) {

                        if (!Array.isArray(videos)) return "no videos available";

                        // function to save all the videos
                        return yt_workers.videos.save_youtube_videos(videos);

                    });

                    Q.all([saved_user, saved_insights, saved_videos]).then(function (data) {
                        count++;
                        callback();
                    }, function (err) {
                        callback(err);
                    });
                });
            }, function (err) {
                if (err) {
                    console.log("Youtube : Error in Syncing youtube ", err);
                    def.resolve("done");
                } else {
                    console.log("Youtube : Daily sync for " + count + " users is saved");
                    def.resolve("done");
                }
            });

        }, function (err) {
            console.log(_cw.generate_worker_error(err, "Error in fetching youtube users to sync", __filename));
            def.resolve("done");
        });

        return def.promise;
    }
};
