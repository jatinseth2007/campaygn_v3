/* jshint node: true */
"use strict";

/**
 * This File hold the list of jobs related to profile
 */

var db = require('../helpers/db');
var profile_workers = require('../workers/profile/profile');
var Q = require('q');
var _common_worker = require('../workers/_common');
var moment = require('moment');

module.exports = {
    get_profiles: function () {
        return db.select({
            table: 'profile_view'
        });
    },
    /**
     * Update each profiles engagement rate by taking average of last 10 post engagement rate
     * @author NarayaN Yaduvanshi
     * @returns {[[Type]]} Promise
     */
    update_engagement_rate: function () {

        this.get_profiles().then(function (profiles) {

            //Looping through each user
            var updated_users = profiles.map(function (profile) {
                return profile_workers.update_engagement_rate(profile);
            });

            Q.all(updated_users).then(function (data) {
                console.log("Profiles : Engagement rates updated for", data.length, "profiles");
            }, console.log);
        });
    },
    /**
     * Update growth of each profile
     * @author NarayaN Yaduvanshi
     */
    update_growth: function () {
        this.get_profiles().then(function (profiles) {

            //Looping through each user
            var updated_users = profiles.map(function (profile) {
                return profile_workers.update_growth(profile);
            });

            Q.all(updated_users).then(function (data) {
                console.log("Profiles : growth rates updated for", data.length, "profiles");
            }, console.log);
        }, console.log);
    },


    /*
     * this function to fetch valid profiles
     */
    fetch_valid_profiles: function () {

        return db.one("SELECT CEIL(COUNT(`id`)/720) as `total` FROM `profiles` WHERE verified = 'Y'").then(function (counting) {

            var day_no = 24 * (moment().date() - 1);
            var page = (moment().get('hour') + 1 + day_no);

            return db.select({
                table: 'profiles',
                fields: ['id', 'facebook', 'instagram', 'youtube'],
                paginate: {
                    page: page,
                    resultsPerPage: counting.total
                }
            }, {
                verified: 'Y'
            });
        }, function (err) {
            throw _common_worker.generate_worker_error(err, {
                error_type: 'profiles',
                message: "Error in counting query - fetch_valid_profiles"
            }, __filename);
        });
    },
    /*
     * this function is to update the tags for valid profiles, it's entry point for this job
     * Jatin Seth
     */
    update_most_used_tags: function () {
        return this.fetch_valid_profiles().then(function (profiles) {
            //Going through each profile
            var saved_profile_tags = profiles.map(function (profile) {
                // need to prepare input elements...
                var ig = (profile.instagram) ? profile.instagram : null;
                var fb = (profile.facebook) ? profile.facebook : null;
                var yt = (profile.youtube) ? profile.youtube : null;

                return db.query("CALL fetch_top_tags(" + ig + "," + fb + ",'" + yt + "',@most_used_tags); SELECT @most_used_tags as most_used_tags;").then(function (result) {
                    if (result[1][0].most_used_tags) {
                        return db.update('profiles', {
                            tags: result[1][0].most_used_tags
                        }, {
                            id: profile.id
                        });
                    } else {
                        return true;
                    }
                }, function (err) {
                    throw _common_worker.generate_worker_error(err, {
                        error_type: 'profiles',
                        message: "Error in fetching data from db using Store Procedure."
                    }, __filename);
                });
            });

            //Handling response
            return Q.all(saved_profile_tags).then(function (saved_profile_tags_results) {
                console.log("Profiles : tags saved for", saved_profile_tags_results.length, "profiles.");
                return true;
            }, function (err) {
                console.log("Profiles : Error in saving tags", err);
                return true;
            });

        }, function (err) {
            throw _common_worker.generate_worker_error(err, {
                error_type: 'mysql',
                message: "Error in fetching data from fetch_valid_profiles method"
            }, __filename);
        }).fail(function (err) {
            console.log("Profile - update_tags -", err);
            return true;
        });
    }
};
