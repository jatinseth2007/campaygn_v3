/* jshint node: true */
"use strict";

/**
 * This File hold the list of jobs that can be called to save data to server or retrieve them
 */

var Q = require("q");
var _ = require("lodash");
var moment = require('moment');
var db = require('../helpers/db');
var ig_workers = require('../workers/instagram/');
var _common_worker = require('../workers/_common');

module.exports = {

    get_user_to_sync: function () {

        //Using CEIL mysql function to round the number to biggest positive digits so that we never skip any profile to sync
        //Finding all the total instagram models by checking verified yes
        return db.one("SELECT CEIL(COUNT(`instagram_id`)/24) as `total` FROM `instagram_live_users_view`")
            .then(function (data) {

                var hour = moment().get('hour');

                //Selecting users to fetch the data based on the current hour.
                return db.query({
                    sql: "SELECT * FROM `instagram_live_users_view` ORDER BY `followed_by` DESC",
                    paginate: {
                        page: hour + 1,
                        resultsPerPage: data.total
                    }
                });
            }, function (err) {
                throw err;
            });
    },

    /**
     * Saves the follower count of the user in db
     * @return {[type]} [description]
     */
    save_instagram_daily_count: function () {
        //Get All the users of instagram
        return this.get_user_to_sync().then(function (ig_users) {
            //Going through each user
            var saved_users = ig_users.map(function (ig_user) {
                return ig_workers.users.get_user_details(ig_user.instagram_id.toString()).then(function (user_detail) {
                    try {
                        // If the user is private, then do nothing.
                        if (user_detail === "private") return;

                        //Prepare the data to send
                        var data_to_save = user_detail.counts;
                        data_to_save.instagram_id = user_detail.id;
                        data_to_save.date = moment().format("YYYY-MM-DD");

                        return Q.all([
                                        ig_workers.users.save_instagram_daily_count(data_to_save),
                                        //Along with updating daily count, Update profile pics and other details also to instagram table.
                                        ig_workers.users.update_instagram_profile(user_detail)
                                    ]).then(function (data) {
                            return data;
                        }, function (err) {
                            throw _common_worker.generate_worker_error(err, {
                                error_type: 'custom',
                                ig_user_details: JSON.stringify(ig_user),
                                message: "Error in save/update data fetched from ig apis - save_instagram_daily_count"
                            }, __filename);
                        });
                    } catch (error) {
                        throw _common_worker.generate_worker_error(error, {
                            error_type: 'custom',
                            ig_user_details: JSON.stringify(ig_user),
                            message: "Error in processing data fetched from ig apis - save_instagram_daily_count"
                        }, __filename);
                    }

                });
            });

            return Q.all(saved_users).then(function (data) {
                console.log("Instagram : Daily follower count for " + data.length + " users is saved");
                return true;
            }, function (err) {
                console.log("Instagram : Error saving daily follower count", err);
                return true;
            });



        }, function (err) {
            throw _common_worker.generate_worker_error(err, {
                error_type: 'mysql',
                message: "Error in fetching data from get_user_to_sync method"
            }, __filename);
        }).fail(function (err) {
            console.log("Instagram: Error saving daily follower count", err);
            return true;
        });

    },
    /**
     * This function saves everydays posts made by the instagram users
     * @return {Promise} [description]
     */
    save_instagram_daily_posts: function () {
        //Get All the users of instagram
        return this.get_user_to_sync().then(function (ig_users) {

                //Going through each user
                var saved_users = ig_users.map(function (ig_user) {

                    return ig_workers.posts.get_user_post_timestamp(ig_user.instagram_id).then(function (posts) {

                            // If the user is private, then do nothing.
                            if (posts === 'private') return;

                            //Iterate through each post and upload them.
                            var saved_posts = posts.map(function (post) {

                                /**
                                 * Searching name tags
                                 */
                                if (post.caption) {
                                    post.name_tags = post.caption.text.match(/@[a-zA-Z0-9._]+/g);
                                    if (post.name_tags) {
                                        post.name_tags = _.map(post.name_tags, function (name_tag) {
                                            return _.trim(name_tag, '@');
                                        });
                                    }
                                }

                                return ig_workers.posts.save_post(post);
                            });

                            return Q.all(saved_posts);
                        },
                        function (err) {
                            throw err;
                        });
                });

                return Q.all(saved_users).then(function (data) {
                    console.log("Instagram : Daily post for " + data.length + " users is saved");
                    return true;
                }, function (err) {
                    console.log("Instagram : Error saving daily post", err);
                    return true;
                });

            },
            function (err) {
                throw _common_worker.generate_worker_error(err, {
                    error_type: 'mysql',
                    message: "Error in fetching data from get_user_to_sync method"
                }, __filename);
            }).fail(function (err) {
            console.log("Instagram : Error saving daily post", err);
            return true;
        });

    }

};
