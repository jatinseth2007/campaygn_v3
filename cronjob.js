/* jshint node: true */
"use strict";

/**
 * This file calls all the cronjob realted tasks
 */

if ((!process.env.NODE_ENV || !process.env.PORT)) {
    console.log("Please make sure there is a env and port is defined");
    exit;
}
var Q = require("q");
var async = require("async");


var ig_jobs = require("./api/jobs/instagram");
var yt_jobs = require("./api/jobs/youtube");
var fb_jobs = require("./api/jobs/facebook");
var profile_jobs = require("./api/jobs/profile");

var CronJob = require('cron').CronJob;

//Runs every hour
new CronJob({
    cronTime: '00 00 * * * *',
    onTick: function () {

        // Instagram daily count for users
        ig_jobs.save_instagram_daily_count().then(function (data) {
            // Instagram daily posts for users
            ig_jobs.save_instagram_daily_posts().then(function (data) {
                // facebook daily posts...
                fb_jobs.save_facebook_daily_posts().then(function (data) {
                    // facebook page-fan-country
                    fb_jobs.save_facebook_page_fans_country().then(function (data) {
                        //facebook daily insights...
                        fb_jobs.save_facebook_daily_insights().then(function (data) {
                            // youtube job...
                            yt_jobs.sync_youtube().then(function (data) {
                                // profile update tags...
                                profile_jobs.update_most_used_tags().then(function (data) {
                                    console.log('all done');
                                }, function (err) {
                                    console.log(err);
                                });
                            }, function (err) {
                                console.log(err);
                            });
                        }, function (err) {
                            console.log(err);
                        });
                    }, function (err) {
                        console.log(err);
                    });
                }, function (err) {
                    console.log(err);
                });
            }, function (err) {
                console.log(err);
            });
        }, function (err) {
            console.log(err);
        });
    },
    start: (process.env.NODE_ENV === 'production') ? true : false,
    runOnInit: false
});

//Runs every day at 7 morning
new CronJob({
    cronTime: '00 00 07 * * *',
    onTick: function () {

        profile_jobs.update_engagement_rate();
        profile_jobs.update_growth();

    },
    start: (process.env.NODE_ENV === 'production') ? true : false,
    runOnInit: false
});

// Put the request inside this cron to test the job immidiately.
new CronJob({

    cronTime: '00 00 00 00 00 00',
    onTick: function () {},
    start: (process.env.NODE_ENV === 'dev') ? true : false,
    runOnInit: (process.env.NODE_ENV === 'dev') ? true : false,
});
