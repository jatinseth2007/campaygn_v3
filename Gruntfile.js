// Gruntfile.js
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // JS TASKS ================================================================
        // check all js files for errors
        jshint: {
            all: ['js/**/*.js', 'api/**/*.js']
        },
        //concats all js file and then minify it along with banner
        concat: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */\n',
            },
            dist: {
                src: 'js/**/*.js',
                dest: 'public/js/core.js',
            },
        },
        min: {
            js: {
                src: 'public/js/core.js',
                dest: 'public/js/core.min.js'
            }
        },
        //Complile All stylus files into 1 core css file
        stylus: {
            options: {
                compress: false, //set to false if want beautified css
                linenos: true //set to true if want line numbers for debugging
            },
            compile: {
                files: {
                    'public/css/core.css': 'stylus/**/*.styl'
                }
            }
        },
        // COOL TASKS ==============================================================
        // watch css and js files and process the above tasks
        watch: {
            js: {
                files: 'js/**/*.js',
                tasks: ['jshint', 'concat']
            },
            api: {
                files: 'api/**/*.js',
                tasks: ['jshint']
            },
            css: {
                files: ['stylus/**/*.styl'],
                tasks: ['stylus']
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: ['public/**/*']
            }
        },
        // configure nodemon
        nodemon: {
            dev: {
                script: 'server.js',
                options: {
                    //nodeArgs: ['--debug'],
                    env: {
                        PORT: '9002', //9002 for dev 9001 for staging 9000 for production
                        NODE_ENV: 'dev'
                    },
                    ignore: ['node_modules/**', 'js/**/*', 'stylus/**/*', 'public/**/*'],
                }
            }
        },

        // run watch and nodemon at the same time
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            tasks: ['nodemon', 'watch']
        }

    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-concurrent');

    grunt.registerTask('default', ['jshint', 'concat', 'stylus', 'concurrent']);

};
