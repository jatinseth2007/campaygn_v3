#!/bin/bash

USER="root"
PASSWORD="eom1lohza0eedoh3Ziengah4eepheuN4"
#OUTPUT="/Users/rabino/DBs"

#rm "$OUTPUTDIR/*gz" > /dev/null 2>&1

databases=`mysql -u $USER -p$PASSWORD -e "SHOW DATABASES;" | tr -d "| " | grep -v Database`

for db in $databases; do
    if [[ "$db" != "information_schema" ]] && [[ "$db" != "performance_schema" ]] && [[ "$db" != "mysql" ]] && [[ "$db" != _* ]] ; then
        echo "Dumping database: $db"
        mysqldump -u $USER -p$PASSWORD --default-character-set=utf8mb4 --databases $db > $db.sql
    fi
done


#compress all mysql db files to 1 file
tar -czvf mysql_backup-`date +%F`.tar.gz *.sql

#delete remaining mysql files
find . -name '*.sql' -delete

#Save only 1 months backup and delete rest
DUMPS=`ls mysql_backup-*.tar.gz | sort | wc -l`
if [ $DUMPS -gt 30 ]; then
	OFFSET=$(($DUMPS - 30))
	ls mysql_backup-*.tar.gz | sort | head -$OFFSET | xargs rm
fi