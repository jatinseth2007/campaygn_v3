/* jshint node: true */
"use strict";

var MongoClient = require('mongodb').MongoClient;
var _common_workers = require("./api/workers/_common");
var user_worker = require("./api/workers/user");
var mysql_db = require("./api/helpers/db");
var db_name = "campaygn";
var url = 'mongodb://localhost:27017/' + db_name;

/**
 * Import user from version 2 of campaygn to version 3, Mongodb to mysql, send a email to rest the password since version 2 password and hashing system cannot be reversed for verified users
 * @author karthick.k
 */

MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log(err);
        return;
    }

    var collection = db.collection('users');
    collection.find({}).toArray(function (err, users) {
        if (err) {
            console.log("Error while fetching users from mongodb" + err);
            db.close();
            return;
        } else {
            users.forEach(function (user) {
                console.log("next");
                var query = "INSERT INTO users (email, password, created_at, role, is_verified, newsletter, last_login, language, expires_at) values (?,NULL,?,?,?,?,?,?,?)";
                var created_at = new Date().getTime();
                var role = 0;
                var is_verified = 'N';
                var newsletter = 'N';
                var last_login = null;
                var language = "en";
                var expires_at = null;
                if (!!user.created_at) {
                    created_at = new Date(user.created_at * 1000);
                }
                if (!!user.role) {
                    role = (user.role === 0) ? 10 : user.role;
                }
                if (!!user.status && user.status === "validated") {
                    is_verified = 'Y';
                }
                if (!!user.newsletter) {
                    newsletter = 'Y';
                }
                if (!!user.last_login) {
                    last_login = new Date(user.last_login * 1000);
                }
                if (!!user.expires_at) {
                    expires_at = new Date(user.expires_at * 1000);
                }
                if (!!user.settings && user.settings.language) {
                    language = user.settings.language;
                }
                if (!user.firstname && user.email) {
                    user.firstname = user.email.split("@")[0];
                }
                if (!user.lastname) {
                    user.lastname = "";
                }

                mysql_db.query(query, [user.email, created_at, role, is_verified, newsletter, last_login, language, expires_at], function (err, rows) {
                    if (err) {
                        console.log("Error while inserting user to mysql : ", err);
                        console.log("Failed for : " + user.email);
                    } else {
                        //if (is_verified === 1) {
                        //                        _common_worker.send_email(user, false, function (err, result) {
                        //                            if (err) {
                        //                                console.log(err);
                        //                            } else {
                        //                                console.log(user.email + " created");
                        //
                        //                            }
                        //                        });
                        //}
                        console.log(user.email + " created");
                    }
                });
            });
            db.close();
        }

    });
});