var express = require('express'),
    app = express(),
    q = require('q'),
    bodyParser = require('body-parser'),
    path = require('path'),
    port = process.env.PORT,
    passport = require('passport'),
    flash = require('connect-flash'),
    session = require('express-session');

if ((!process.env.NODE_ENV || !process.env.PORT)) {
    console.log("Please make sure there is a env and port is defined");
    exit;
}

//For SEO
app.use(require('prerender-node'));


/************************************************/
/************ All API Routes goes here **********/
/************************************************/

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

/*//Allowing CORS here
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    next();
});
*/

// required for passport
app.use(session({
    secret: '5151db3f77e7847337000000_ykone',
    resave: true,
    saveUninitialized: false
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


//Cron jobs import for development
if (process.env.NODE_ENV === "dev") {
    require('./cronjob'); // Scheduling our cronjobs
}

//Serving static files and index.html
app.use(express.static(__dirname + '/public'));


// routes ==================================================
require('./api/routes/index')(app, passport); // configure our routes

//All API Routes Ends here =================================








// app.get('/',function(req, res) {
//     console.log(JSON.stringify(req.url));
//      res.send('Hello'+ JSON.stringify(req.url));
//
// });

//Redirect all request to index.html page.. Why Because its SPA
app.get('/*', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

//Listening to port 9000
app.listen(port, function () {
    console.log('Server is running with ' + process.env.NODE_ENV + ' envoirment on ' + port + '...');
});