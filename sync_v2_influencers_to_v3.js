/* jshint node: true */
"use strict";

var MongoClient = require('mongodb').MongoClient,
    request = require("request"),
    _ = require("lodash"),
    _common_workers = require("./api/workers/_common"),
    moment = require("moment"),
    assert = require('assert');

// Connection URL 
var db_name = (process.env.NODE_ENV == "production") ? "campaygn-prod-api" : "campaygn-api";
var url = 'mongodb://localhost:27017/' + db_name;


// Use connect method to connect to the Server 
MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log(err);
    }
    assert.equal(null, err);
    console.log("Connected correctly to server");



    //Finding all blog post
    var collection = db.collection('blogs');

    collection.find({
            admin_validated: true,
            //_slugs: {
            //    $in: ['nima-benati', 'cristina-musacchio', 'nsap', 'roberto-de-rosa', 'jay-strut', 'anna-wilken', 'riccardo-simonetti']
            //}
            created_at: {
                $gt: new Date("2016-10-01") //14-10-2016
            }
            //            _slugs: "dani-stevens",
            //            "social_networks.youtube.account_id": {
            //                $exists: true
            //            }
        })
        //        .skip(0).limit(10)
        .toArray(function (err, blogs) {
            if (err) {
                console.log("error" + err);
            } else {
                _.each(blogs, function (blog) {

                    // console.log(blog);

                    var data_to_save = {
                            name: blog.name,
                            slug: _common_workers.to_slug(blog.name),
                            blog: blog.url,
                            gender: blog.genders[0] || "other",
                            country: blog.orig_country,
                            starred: blog.starred,
                            url: blog.url,
                            phone: blog.phone,
                            verified: "Y"
                        }
                        //console.log(data_to_save);
                        //Bio
                    if (blog.bio_blog != "") {
                        data_to_save.bio = blog.bio_blog;
                    }

                    //Emails
                    if (blog.emails.length > 0 && blog.emails[0].email) {
                        data_to_save.email = blog.emails[0].email;
                    }

                    //weather a Influencer or a Brand
                    if (blog.kind[0] == "blog" || blog.kind[0] == "celebrity") {
                        data_to_save.kind = "influencer";
                    } else if (blog.kind[0] == "magazine") {
                        data_to_save.kind = "brand";
                    }

                    //Tags
                    if (blog.tags.length > 0) {
                        data_to_save.tags = blog.tags.join(",");
                    }

                    //Types
                    if (blog.types.length > 0) {
                        data_to_save.types = blog.types.join(",");
                    }

                    //Themes
                    if (blog.themes.length > 0) {
                        data_to_save.themes = blog.themes.join(",");
                    }

                    //Description
                    if (blog.descriptions && blog.descriptions.en) {
                        data_to_save.description = blog.descriptions.en;
                    }

                    //Languages
                    if (blog.languages.length > 0) {
                        data_to_save.languages = blog.languages.join(",");
                    }

                    //Address
                    if (blog.address.address1 != "") {
                        data_to_save.address_1 = blog.address.address1;
                    }
                    if (blog.address.address2 != "") {
                        data_to_save.address_2 = blog.address.address2;
                    }
                    if (blog.address.number != "") {
                        data_to_save.address_number = blog.address.number;
                    }
                    if (blog.address.city != "") {
                        data_to_save.address_city = blog.address.city;
                    }
                    if (blog.address.state != "") {
                        data_to_save.address_state = blog.address.state;
                    }
                    if (blog.address.country != "") {
                        data_to_save.address_country = blog.address.country;
                    }
                    if (blog.address.zipcode != "") {
                        data_to_save.address_zipcode = blog.address.zipcode;
                    }


                    //Hangling age
                    if (blog.age_ranges.length > 0) {
                        data_to_save.age_min = blog.age_ranges[0].split("-")[0];
                        data_to_save.age_max = blog.age_ranges[0].split("-")[1];
                    }

                    if (blog.social_networks.instagram && blog.social_networks.instagram.account_id) {
                        data_to_save.instagram = blog.social_networks.instagram.account_id;
                    }
                    if (blog.social_networks.facebook && blog.social_networks.facebook.account_id) {
                        data_to_save.facebook = blog.social_networks.facebook.account_id;
                    }
                    if (blog.social_networks.youtube && blog.social_networks.youtube.account_id) {
                        data_to_save.youtube = blog.social_networks.youtube.account_id;
                    }

                    request.post({
                        url: (process.env.NODE_ENV == "production") ? "http://v3.campaygn.com/api/profile/add" : "http://campaygn3.local/api/profile/add",
                        form: data_to_save
                    }, function (error, response, body) {
                        if (!error) {
                            console.log(body);
                        } else {
                            console.log("error", error);
                        }
                    })
                })
            }
            db.close();
        });
});
