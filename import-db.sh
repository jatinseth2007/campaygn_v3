#!/bin/bash

USER="root"
PASSWORD="root"

FILES="/Users/narayan/Backups/MySQL/*"

for f in $FILES
do
  echo "Processing $f file..."
  mysql -u $USER -p$PASSWORD < $f
done