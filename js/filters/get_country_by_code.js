app.filter("get_country_by_code", ["countries", function (countries) {
    return function (code) {
        if (!code) {
            return false;
        }

        // use factory to fetch country name by code...
        var country_name = countries.get_contry_object_by_code(code)[0].name;

        return country_name;
    };
}]);
