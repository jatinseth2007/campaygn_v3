app.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states
    $stateProvider
        .state('homepage', {
            url: "/",
            templateUrl: "./views/homepage.html",
            controller: 'home'
        })
        .state('add_profile', {
            url: "/profiles/add",
            templateUrl: "./views/profile/add_profile.html",
            controller: 'add_profile'
        })
        //For tags
        .state('tags', {
            url: "/tags/:keyword",
            templateUrl: "./views/tag/tags.html",
            controller: 'tags',
            resolve: {
                tags: ['$http', '$stateParams', function ($http, $stateParams) {
                    if ($stateParams.keyword !== '') {
                        return $http.get('/api/tags/' + $stateParams.keyword).then(function (data) {
                            return data.data;
                        });
                    }
                }]
            }
        })
        //For tag
        .state('tag', {
            url: "/tag/:tag?from&to",
            templateUrl: "./views/tag/tag.html",
            controller: 'tag',
            resolve: {
                posts: function (tag_service, $stateParams) {
                    return tag_service.get_posts($stateParams.tag).then(function (result) {
                        return (result && result.data) ? result.data : [];
                    }, function () {
                        return [];
                    });
                }
            }
        })
        .state('profiles', {
            url: "/profiles/?profile_kind&search_keyword&search_countries&search_continents&search_gender&search_tags&search_themes&search_languages&min_range&max_range&min_age&max_age&limit&page&sort_by&sort_order&active_platform&min_engagement&max_engagement&min_growth&max_growth&cache",
            params: {
                profile_kind: {
                    value: 'all',
                },
                active_platform: {
                    value: 'all',
                }
            },
            templateUrl: "./views/profiles/profiles.html",
            controller: 'profiles'
        })
        .state('profile', {
            url: "/profile/:slug?active_platform",
            params: {
                active_platform: {
                    value: 'instagram',
                }
            },
            templateUrl: "./views/profile/profile.html",
            controller: 'profile',
            resolve: {
                user_obj: function ($stateParams, profile_service) {
                    return profile_service.fetch_profile($stateParams.slug).then(function (result) {
                        if (result) {
                            return result.data[0];
                        }
                    });
                }
            }
        })
        .state('analytics', {
            url: "/analytics",
            templateUrl: "./views/analytics/dashboard.html",
            controller: 'analytics_dashboard',
            resolve: {
                events: function ($stateParams, analytics_service) {
                    return analytics_service.fetch_events().then(function (result) {
                        result = (result && result.data.length > 0) ? result.data : [];
                        return result;
                    });
                }
            }
        })
        .state('reset_password', {
            url: "/user/reset_password/?token&email",
            templateUrl: "./views/user/reset_password.html",
            controller: 'user'
        })
        .state('user_verification', {
            url: "/user/verification/?token&email",
            templateUrl: "./views/user/reset_password.html",
            controller: 'user'
        })
        .state('user_dashboard_blogs', {
            url: "/user/dashboard/blogs",
            templateUrl: "./views/user/dashboard/blogs.html",
            controller: 'user'
        })
        .state('user_dashboard_users', {
            url: "/user/dashboard/users",
            templateUrl: "./views/user/dashboard/users.html",
            controller: 'dashboard_users',
            resolve: {
                users: function (user_service) {
                    return user_service.get_users().then(function (result) {
                        if (result && result.data && result.data.users) {
                            return result.data.users;
                        } else {
                            return [];
                        }
                    }, function () {
                        return [];
                    });
                },
                roles: function (user_service) {
                    return user_service.get_roles().then(function (result) {
                        if (result && result.data && result.data.roles) {
                            return result.data.roles;
                        } else {
                            return [];
                        }
                    }, function () {
                        return [];
                    });
                }
            }
        })
        .state('user_dashboard_parameters', {
            url: "/user/dashboard/parameters",
            templateUrl: "./views/user/dashboard/parameters.html",
            controller: 'user'
        })
        .state('user_dashboard_tags', {
            url: "/user/dashboard/tags",
            templateUrl: "./views/user/dashboard/tags.html",
            controller: 'user'
        })
        .state('user_dashboard_users_new', {
            url: "/user/dashboard/users/new",
            templateUrl: "./views/user/dashboard/new_user.html",
            controller: 'dashboard_new_user',
            resolve: {
                roles: function (user_service) {
                    return user_service.get_roles().then(function (result) {
                        return (result && result.data && result.data.roles) ? result.data.roles : [];
                    }, function () {
                        return [];
                    });
                }
            }
        })
        .state('user_dashboard_user_logs', {
            url: "/user/dashboard/users/logs/:id",
            templateUrl: "./views/user/dashboard/user_logs.html",
            controller: 'dashboard_user_logs',
            resolve: {
                logs: function (user_service, $stateParams) {
                    return user_service.get_user_logs($stateParams.id).then(function (result) {
                        return (result && result.data) ? result.data : [];
                    }, function () {
                        return [];
                    });
                }
            }
        })
        .state('monitors', {
            url: "/monitors/?group_name",
            templateUrl: "./views/monitors/monitors.html",
            controller: 'monitors',
            resolve: {
                monitors_list: function (monitors_service, $stateParams) {
                    return monitors_service.get_monitors_list().then(function (result) {
                        return (result && result.data) ? result.data : [];
                    }, function () {
                        return [];
                    });
                }
            }
        })
        .state('monitor', {
            url: "/monitor/:id?influencer_id&tag_id",
            templateUrl: "./views/monitors/monitor.html",
            controller: 'monitor',
            resolve: {
                monitor_details: function (monitors_service, $stateParams) {
                    return monitors_service.get_monitor_details($stateParams.id, $stateParams.influencer_id, $stateParams.tag_id).then(function (result) {
                        return (result && result.data) ? result.data : [];
                    }, function () {
                        return [];
                    });
                },
                posts_details: function (monitors_service, $stateParams) {
                    return monitors_service.get_monitor_posts_details($stateParams.id, $stateParams.influencer_id, $stateParams.tag_id).then(function (result) {
                        return (result && result.data) ? result.data : [];
                    }, function () {
                        return [];
                    });
                },
                followers_details: function (monitors_service, $stateParams) {
                    return monitors_service.get_monitor_influencers_followers_details($stateParams.id, $stateParams.influencer_id, $stateParams.tag_id).then(function (result) {
                        return (result && result.data) ? result.data : [];
                    }, function () {
                        return [];
                    });
                }
            }
        })
        .state('new_monitor', {
            url: "/monitors/new",
            templateUrl: "./views/monitors/new_monitor.html",
            controller: 'new_monitor'
        })
        //to see all my lists
        .state('lists', {
            url: "/lists/",
            templateUrl: "./views/lists/lists.html",
            controller: 'lists'
        })
        //to add new list
        .state('add_list', {
            url: "/lists/add",
            templateUrl: "./views/lists/add.html",
            controller: 'add_list',
            resolve: {
                user_lists: function (list_service, toastr) {
                    return list_service.fetch_user_lists(0, 'owner').then(function (result) {
                        return list_service.prepare_data(result.data, true);
                    }, function (err) {
                        toastr.error(err.data);
                    });
                }
            }
        }) //list page
        .state('list', {
            url: "/list/:list_id",
            templateUrl: "./views/lists/list.html",
            controller: 'list',
            resolve: {
                user_list: function ($stateParams, list_service, toastr) {
                    return list_service.fetch_user_list($stateParams.list_id).then(function (result) {
                        return result.data[0];
                    }, function (err) {
                        toastr.error(err.data);
                    });
                }
            }
        }) // leaderboard
        .state('leaderboard', {
            url: "/leaderboard/?search_countries&search_gender&search_themes&search_social_platform&search_kpi&search_trigger",
            params: {
                search_trigger: {
                    value: 'false'
                }
            },
            templateUrl: "./views/leaderboard/leaderboard.html",
            controller: 'leaderboard'
        })
        .state('404', {
            url: "/404",
            templateUrl: "./views/error/404.html"
        });
});

app.config(function ($locationProvider) {
    $locationProvider.html5Mode({
        enabled: false,
        requireBase: false,
    });
});