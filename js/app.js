var app = angular.module("app", [
	"ui.router",
	"ui.bootstrap",
	'ngAnimate',
	'toastr',
	"angular-loading-bar",
	"ui.select",
	"ngSanitize",
	"rzModule",
	"highcharts-ng",
    "xeditable",
    "angularBootstrapNavTree",
    "angular-confirm",
    'ui.grid',
    'ui.grid.exporter',
    'ngCookies'
]);
