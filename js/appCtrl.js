app.controller('appCtrl', function ($scope, $state, $location, $rootScope, user_service, authentication_settings) {
    $scope.isMenuVisible = function () {
        switch ($state.$current.name) {
        case 'homepage':
            return false;
        default:
            return true;
        }
    };

    /**
     * Fetch logged in user information
     * @author karthick.k
     * @returns {object} set user information in rootscope and set is_logged_in flag
     */
    if (!$rootScope.is_logged_in) {
        user_service.get_user().then(function (result) {
            if (!!result.data && !!result.data.id && !!result.data.email) {
                $rootScope.is_logged_in = true;
                $rootScope.user_info = result.data;
            } else {
                $rootScope.user_info = null;
                $rootScope.is_logged_in = false;
            }
        }, function (result) {
            $rootScope.user_info = null;
            $rootScope.is_logged_in = false;
        });
    }

    /**
     * Used to authorization of the user, function is invoked on every state change
     * @author karthick.k
     */
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (!$rootScope.is_logged_in) {
            $rootScope.is_logged_in = false;
        }
        if ($rootScope.user_info === undefined) {
            user_service.get_user().then(function (result) {
                if (!!result.data && !!result.data.id && !!result.data.email) {
                    $rootScope.is_logged_in = true;
                    $rootScope.user_info = result.data;
                } else {
                    $rootScope.user_info = null;
                    $rootScope.is_logged_in = false;
                }
                if (!authentication_settings.has_permission($rootScope.user_info, toState.url)) {
                    event.preventDefault();
                    $state.go("profiles");
                }
            }, function (result) {
                $rootScope.user_info = null;
                $rootScope.is_logged_in = false;
                if (!authentication_settings.has_permission($rootScope.user_info, toState.url)) {
                    event.preventDefault();
                    $state.go("profiles");
                }
            });
        } else {
            if (!authentication_settings.has_permission($rootScope.user_info, toState.url)) {
                event.preventDefault();
                $state.go("profiles");
            }
        }
    });
});

app.run(function (editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});