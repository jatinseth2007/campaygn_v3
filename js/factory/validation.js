/* jshint -W100 */
/**
 * Used to handle form validation
 * @author karthick.k
 */
app.factory('validation', function () {

    return {
        /**
         * Used to validate form fields and mark the error for each and every fields
         * @author karthick.k
         * @param {object} form form fields value
         */
        validate: function (form) {
            for (var field in form) {
                if (field[0] !== '$' && !!form[field].$pristine) {
                    form[field].$setViewValue(
                        form[field].$modelValue
                    );
                    form[field].$dirty = true;
                    form[field].$setTouched();
                }
            }
        },
        /**
         * Used to check the whether form field values are valid or not
         * @author karthick.k
         * @param {object} user logged in user information id, email, role
         * @returns {boolean} whether form is valid or not, true/false
         */
        is_valid: function (form) {
            var result = true;
            if (!form.$valid) {
                this.validate(form);
                result = false;
            }
            return result;
        }
    };
});