/* jshint -W100 */
app.factory('audience_factory', function () {
    return {
        all_audiences: [{
                name: 'Male',
                value: 'male'
        },
            {
                name: 'Female',
                value: 'female'
        },
            {
                name: 'Both',
                value: 'both'
        }]
    };
});
