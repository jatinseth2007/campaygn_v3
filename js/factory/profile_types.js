/* jshint -W100 */
app.factory('profile_types_factory', function () {
    return {
        all_profile_types: [{
                name: 'Blogger',
                value: 'blogger'
        },
            {
                name: 'Instagramer',
                value: 'instagramer'
        },
            {
                name: 'Youtuber',
                value: 'youtuber'
        },
            {
                name: 'Model',
                value: 'model'
        },
            {
                name: 'Artist',
                value: 'artist'
        },
            {
                name: 'Celebrity',
                value: 'celebrity'
        },
            {
                name: 'Brand',
                value: 'brand'
        },
            {
                name: 'Magazine',
                value: 'magazine'
           }]
    };
});
