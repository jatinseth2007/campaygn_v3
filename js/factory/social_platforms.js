/* jshint -W100 */
app.factory('social_platform_factory', function () {
    return {
        all_social_platforms: [{
                name: 'Instagram',
                value: 'instagram'
        },
            {
                name: 'Facebook',
                value: 'facebook'
        },
            {
                name: 'Youtube',
                value: 'youtube'
        },
            {
                name: 'All',
                value: 'all'
        }]
    };
});
