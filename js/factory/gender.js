/* jshint -W100 */
app.factory('gender', function() {
    return {
        get_gender_by_code: function(inputcode) {
            return $.grep(this.all_gender, function(e,i){
                return (inputcode.toLowerCase().split(",").indexOf(e.code.toLowerCase()) >= 0);
            });
        },
        all_gender: [
            { name: 'Male', code: 'male' },
            { name: 'Female', code: 'female' },
            { name: 'Transgender', code: 'transgender' },
            { name: 'Other', code: 'other' }
        ]
    };
});