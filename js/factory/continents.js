/* jshint -W100 */
app.factory('continents', function() {
    return {
        get_continent_by_code: function(inputcode) {
            return $.grep(this.all_continents, function(e,i){
                return (inputcode.toLowerCase().split(",").indexOf(e.code.toLowerCase()) >= 0);
            });
        },
        all_continents: [
            { name: 'Africa', code: 'AF' },
            { name: 'Antarctica', code: 'AN' },
            { name: 'Asia', code: 'AS' },
            { name: 'Europe', code: 'EU' },
            { name: 'North America', code: 'NA' },
            { name: 'Oceania', code: 'OC' },
            { name: 'South America', code: 'SA' }
        ]
    };
});