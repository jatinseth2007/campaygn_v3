/* jshint -W100 */
app.factory('themes_factory', function () {
    return {
        get_theme_by_code: function (inputcode) {
            return $.grep(this.all_themes, function (e, i) {
                return (inputcode.toLowerCase().split(",").indexOf(e.code.toLowerCase()) >= 0);
            });
        },
        all_themes: [
            {
                name: 'Art',
                code: 'art'
            },
            {
                name: 'Beauty',
                code: 'beauty'
            },
            {
                name: 'Cinema',
                code: 'cinema'
            },
            {
                name: 'Design',
                code: 'design'
            },
            {
                name: 'DIY',
                code: 'diy'
            },
            {
                name: 'Events',
                code: 'events'
            },
            {
                name: 'Family',
                code: 'family'
            },
            {
                name: 'Fashion',
                code: 'fashion'
            },
            {
                name: 'Food',
                code: 'food'
            },
            {
                name: 'Gaming',
                code: 'gaming'
            },
            {
                name: 'Lifestyle',
                code: 'lifestyle'
            },
            {
                name: 'Mom',
                code: 'mom'
            },
            {
                name: 'Music',
                code: 'music'
            },
            {
                name: 'Photography',
                code: 'photography'
            },
            {
                name: 'Sports',
                code: 'sports'
            },
            {
                name: 'Tech',
                code: 'tech'
            },
            {
                name: 'Travel',
                code: 'travel'
            },
            {
                name: 'Vlog',
                code: 'vlog'
            }
        ]
    };
});
