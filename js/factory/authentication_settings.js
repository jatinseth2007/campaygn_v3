/* jshint -W100 */

app.factory('authentication_settings', function () {
    /**
     * Configure routes and permission
     * @author karthick.k
     * Example : 
     * 1. "/": 0 can be allowed for all users even login is not required, Anonymous users also can access
     * 2. "/user/dashboard/blogs": 100 can be accessed only by admin and required login
     * admin - 100, member - 60, user - 30, visitor - 10, Anonymous - 0
     */
    var route_permission_list = {
        // users routes
        "/user/dashboard/blogs": 100,
        "/user/dashboard/tags": 100,
        "/user/dashboard/parameters": 100,
        "/user/dashboard/users/new": 100,
        "/user/dashboard/users": 100,
        "/user/dashboard/users/logs/": 100,
        "/user/reset_password/?": 0,
        "/user/verification/?": 0,
        // monitors routes
        "/monitors/new": 100,
        "/monitors/": 100,
        "/monitor/": 0,
        // analytics routes
        "/analytics": 100,
        // tag routes
        "/tag/": 100,
        "/tags/": 100,
        // lists routes
        "/lists/": 10,
        "/list/:list_id": 10,
        // profiles routes
        "/profiles/add": 100,
        "/profiles/": 0,
        "/profile/:slug": 0,
        //leaderboard
        "/leaderboard/?": 0
    };

    return {
        /**
         * Used to handle authorization of the user, which page can be accessed by whom, Basically who can access what based on the logged user role
         * @author karthick.k
         * @modified Jatin Seth
         * @param {object} user logged in user information id, email, role
         * @param {string} url current route to be changed/accessed
         */
        has_permission: function (user, url) {
            if (url === "/") {
                return true;
            }
            var role_id = 0;
            var selected_route;

            if (user && user.role_id) {
                role_id = user.role_id;
            }
            Object.keys(route_permission_list).some(function (route) {
                var re = route + ".*";
                var regex = new RegExp(re);
                if (regex.test(url)) {
                    selected_route = route;
                    return true;
                }
            });

            return (role_id >= route_permission_list[selected_route]);
        }
    };
});
