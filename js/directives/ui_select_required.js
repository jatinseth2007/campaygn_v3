/**
 * Used to validate ui select for required
 * @author karthick.k
 */
app.directive('uiSelectRequired', function () {
    return {
        scope: true,
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$validators.uiSelectRequired = function (modelValue, viewValue) {
                if (!modelValue) {
                    return false;
                } else {
                    return modelValue.length;
                }

            };
        }
    };
});