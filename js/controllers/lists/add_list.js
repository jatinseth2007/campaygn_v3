app.controller('add_list', ['$scope', '$stateParams', '$http', 'user_lists', 'list_service', 'toastr', '$state',

    function ($scope, $stateParams, $http, user_lists, list_service, toastr, $state) {
        /*
            Initialization for the tree params
        */
        $scope.my_tree = {};
        $scope.user_lists = user_lists;
        $scope.list = {
            parent_id: 0
        };

        // END of Tree params

        /*
         * Add children to the list if available and set parent ID of the selected list
         */
        $scope.add_children = function (branch, user_role) {
            list_service.add_children($scope.list, branch, user_role);
        };

        /*
         * Add new list in system
         */
        $scope.create_new_list = function () {
            if (list_service.validate_list_form($scope.list)) {
                list_service.create_user_list($scope.list).then(function (success) {
                    toastr.success(success.data);
                    $scope.cancel_button();
                }, function (err) {
                    $scope.list_api_error = 'There is some error in data you provied';

                    if (err.data.name) {
                        $scope.error = {};
                        $scope.error.name = err.data.name;
                    }
                });
            }
        };

        // cancel the page
        $scope.cancel_button = function () {
            $state.go('lists', {}, {
                reload: true
            });
        };
    }
]);
