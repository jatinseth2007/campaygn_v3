app.controller('list', ['$state', '$scope', '$stateParams', '$http', 'list_service', '$filter', '$rootScope', 'toastr', 'user_list', '$uibModal', 'profiles_list_service',
    function ($state, $scope, $stateParams, $http, list_service, $filter, $rootScope, toastr, user_list, $uibModal, profiles_list_service) {
        // intialization of params
        $scope.list_info = user_list;
        $scope.list_users = [];
        $scope.list_children = [];
        $scope.list_comments = [];
        $scope.profiles = [];
        $scope.errors = {};
        $scope.tag_limit = 3;
        $scope.propertyName = 'total_followers';
        $scope.reverse = true;
        // END intialization...


        // add list functionality in profiles...
        profiles_list_service.add_popup_functionality($scope, $uibModal, list_service, toastr, function () {
            // need to load profiles again to after succesful action performed...
            load_list_profiles();
        });

        // this is to fetch users assosiated with current list
        var fetch_list_users = function () {
            list_service.fetch_list_users($scope.list_info.id).then(function (list_users) {
                $scope.list_users = list_users.data;
            }, function (err) {
                console.log(err);
            });
        };

        // need to fecth the list_users only for roles mentioned in condition...
        if (['owner', 'admin'].indexOf($scope.list_info.user_role) >= 0) {
            fetch_list_users();
        }

        // need to fecth the sublists assosiated with current user...
        list_service.fetch_list_children($scope.list_info.id).then(function (list_children) {
            if (list_children.data.length > 0) {
                angular.forEach(list_children.data, function (list, index) {
                    /**
                     * need to calculate percentage for each platform
                     */

                    var total_reach = parseInt((list.ig_reach) ? list.ig_reach : 0) + parseInt((list.fb_reach) ? list.fb_reach : 0) + parseInt((list.yt_reach) ? list.yt_reach : 0);

                    // add Instagram percentage
                    if (list.ig_reach) {
                        list.ig_percentage = $filter('number')((list.ig_reach / total_reach) * 100, 0);
                    }
                    // add facebook percentage
                    if (list.fb_reach) {
                        list.fb_percentage = $filter('number')((list.fb_reach / total_reach) * 100, 0);
                    }
                    // add youtube percentage
                    if (list.yt_reach) {
                        list.yt_percentage = $filter('number')((list.yt_reach / total_reach) * 100, 0);
                    }
                });
            }
            $scope.list_children = list_children.data;
        }, function (err) {
            console.log(err);
        });

        // need to fecth the profiles assosiated with current list...
        var load_list_profiles = function () {
            list_service.fetch_list_profiles($scope.list_info.id).then(function (list_profiles) {
                $scope.profiles = list_profiles.data;

                /**
                 * need to perform some actions to each profile...
                 */
                if ($scope.profiles && $scope.profiles.length > 0) {
                    angular.forEach($scope.profiles, function (profile, index) {
                        // very important to do it otherwise sorting will not work at all :(
                        profile.total_followers = (profile.total_followers) ? parseInt(profile.total_followers) : 0;
                        profile.total_engagement_rate = (profile.total_engagement_rate) ? parseFloat(profile.total_engagement_rate) : 0;
                        profile.ig_followed_by = (profile.ig_followed_by) ? parseInt(profile.ig_followed_by) : 0;
                        profile.facebook_likes = (profile.facebook_likes) ? parseInt(profile.facebook_likes) : 0;
                        profile.youtube_subscriber_count = (profile.youtube_subscriber_count) ? parseInt(profile.youtube_subscriber_count) : 0;
                        // parsing over...

                        /**
                         * Splitting profile tags
                         */

                        if (profile.profile_tags) {
                            profile.profile_tags = profile.profile_tags.split(",");
                        }

                        // add Instagram percentage
                        if (profile.ig_followed_by) {
                            profile.ig_total_percentage = $filter('number')((profile.ig_followed_by / profile.total_followers) * 100, 0);
                        }
                        // add facebook percentage
                        if (profile.facebook_likes) {
                            profile.fb_total_percentage = $filter('number')((profile.facebook_likes / profile.total_followers) * 100, 0);
                        }
                        // add youtube percentage
                        if (profile.youtube_subscriber_count) {
                            profile.yt_total_percentage = $filter('number')((profile.youtube_subscriber_count / profile.total_followers) * 100, 0);
                        }

                        // need to select the profile picture in 'all' case
                        if (profile.ig_profile_picture) {
                            profile.available_profile_picture = profile.ig_profile_picture;
                        } else if (profile.facebook_picture) {
                            profile.available_profile_picture = profile.facebook_picture;
                        } else if (profile.youtube_profile_picture) {
                            profile.available_profile_picture = profile.youtube_profile_picture;
                        } else {
                            profile.available_profile_picture = null;
                        }
                    });
                }
            }, function (err) {
                console.log(err);
            });
        };
        // need to call the function imidiatly...
        load_list_profiles();


        // sort the records...
        $scope.sort_by = function (propertyName) {
            $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
            $scope.propertyName = propertyName;
        };

        /*
         * this function is to open pop-up to show Access management section...
         */
        $scope.open_list_users = function () {
            var popup_obj = $uibModal.open({
                animation: true,
                //template: '<h2>hi</h2>',
                templateUrl: './views/lists/list_users.html',
                controller: 'list_users',
                size: 'lg',
                resolve: {
                    list_users: function () {
                        return $scope.list_users;
                    }
                }
            });

            // use the closed promise...
            popup_obj.closed.then(function (data) {
                // need to load users again...
                fetch_list_users();
            }, console.log);
        };

        /*
         * Update list 
         */
        $scope.update = function () {
            list_service.update({
                id: $scope.list_info.id,
                name: $scope.list_info.name,
                description: $scope.list_info.description,
                image: $scope.list_info.image
            }).then(function (success) {
                toastr.success(success.data);
                $scope.errors = {}; // need to reset errors
            }, function (err) {
                toastr.error("There is error in data you provided, please try again.");

                // check if we have any error regrading name.
                if (err.data.name) {
                    $scope.errors.name = err.data.name;
                }
            });
        };

        // need to fecth the profiles assosiated with current list...
        var load_list_comments = function () {
            list_service.fetch_list_comments($scope.list_info.id).then(function (list_comments) {
                $scope.list_comments = list_comments.data;
            }, function (err) {
                console.log(err);
            });
        };
        // need to call the function imidiatly...
        load_list_comments();

        /*
         * Add comments...
         */
        $scope.add_comment = function () {
            list_service.add_comment($scope.list_info.id, $scope.comment).then(function (success) {
                toastr.success(success.data);
                $scope.comment = ""; // need to reset the input also...
                $scope.errors = {}; // need to reset errors
                load_list_comments(); //need to reload comments...
            }, function (err) {
                toastr.error("There is error in data you provided, please try again.");

                // check if we have any error regrading name.
                if (err.data.comment) {
                    $scope.errors.comment = err.data.comment;
                }
            });
        };

        /*
         * Delete list and sublists from system...
         */

        $scope.delete_list = function (list_id) {
            list_service.delete_list(list_id).then(function (success) {
                toastr.success(success.data);

                $state.go('lists', {}, {
                    reload: true
                });
            }, function (err) {
                toastr.error(err.data);
            });
        };

        /*
         * Upload List Image
         */
        $scope.upload_list_image = function () {
            console.log($scope.list_image);
        };
}]);
