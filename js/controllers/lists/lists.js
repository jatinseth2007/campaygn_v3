app.controller('lists', ['$scope', '$stateParams', '$http', 'list_service', '$filter', '$rootScope', 'toastr',
    function ($scope, $stateParams, $http, list_service, $filter, $rootScope, toastr) {
        // intialization of the params
        $scope.owner_lists = $scope.member_lists = null;
        $scope.owner_bigCurrentPage = 1;
        $scope.owner_sort_order = 'DESC';
        $scope.owner_sort_by = 'a.id';

        var prepare_listing_data = function (lists) {
            if (lists.length > 0) {
                angular.forEach(lists, function (list, index) {
                    /**
                     * need to calculate percentage for each platform
                     */

                    var total_reach = parseInt((list.ig_reach) ? list.ig_reach : 0) + parseInt((list.fb_reach) ? list.fb_reach : 0) + parseInt((list.yt_reach) ? list.yt_reach : 0);

                    // add Instagram percentage
                    if (list.ig_reach) {
                        list.ig_percentage = $filter('number')((list.ig_reach / total_reach) * 100, 0);
                    }
                    // add facebook percentage
                    if (list.fb_reach) {
                        list.fb_percentage = $filter('number')((list.fb_reach / total_reach) * 100, 0);
                    }
                    // add youtube percentage
                    if (list.yt_reach) {
                        list.yt_percentage = $filter('number')((list.yt_reach / total_reach) * 100, 0);
                    }
                });

                return lists;
            } else {
                return null;
            }
        };

        var fetch_owner_lists = function () {
            // we need to pack all the params in one object
            var params = {
                page: $scope.owner_bigCurrentPage,
                sort_order: $scope.owner_sort_order,
                sort_by: $scope.owner_sort_by,
                limit: 30
            };

            // fetch user's owner lists
            list_service.fetch_owner_lists(params).then(function (owner_lists) {
                // pagination params...
                $scope.owner_maxSize = 5;
                $scope.owner_bigTotalItems = owner_lists.data.total_records;
                $scope.owner_bigCurrentPage = owner_lists.data.page;
                $scope.owner_itemsPerPage = owner_lists.data.limit;

                $scope.owner_lists = prepare_listing_data(owner_lists.data.lists);
            }, function (err) {
                console.log(err);
                $scope.owner_lists = null;
            });
        };
        /*
         * Function is update results for dufferent pages
         */
        $scope.owner_change_page_value = function (owner_bigCurrentPage) {
            $scope.owner_bigCurrentPage = owner_bigCurrentPage;
            fetch_owner_lists();
        };

        /**
         * Function is responsable to sort the records dynamically
         */
        $scope.sort_results = function (sort_by) {
            $scope.owner_sort_order = ($scope.owner_sort_by && $scope.owner_sort_order && $scope.owner_sort_by == sort_by && $scope.owner_sort_order.toLowerCase() == 'desc') ? 'ASC' : 'DESC';
            $scope.owner_sort_by = sort_by;

            fetch_owner_lists();
        };

        var fetch_member_lists = function () {
            // fetch user's member lists
            list_service.fetch_member_lists().then(function (member_lists) {
                $scope.member_lists = prepare_listing_data(member_lists.data);
            }, function (err) {
                console.log(err);
                $scope.member_lists = null;
            });
        };

        /*
         * Delete list and sublists from system...
         */

        $scope.delete_list = function (list_id) {
            list_service.delete_list(list_id).then(function (success) {
                toastr.success(success.data);

                fetch_owner_lists();
            }, function (err) {
                toastr.error(err.data);
            });
        };

        fetch_owner_lists();
        fetch_member_lists();
}]);
