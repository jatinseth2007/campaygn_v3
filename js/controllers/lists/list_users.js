app.controller('list_users', ['$state', '$scope', '$stateParams', '$http', 'list_service', '$filter', '$rootScope', 'toastr', '$uibModalInstance', 'list_users', 'user_service',

    function ($state, $scope, $stateParams, $http, list_service, $filter, $rootScope, toastr, $uibModalInstance, list_users, user_service) {

        // intialization of params
        $scope.list_users = list_users;
        var list_id = (list_users.length > 0) ? list_users[0].list_id : 0;

        $scope.user_roles = [
            {
                value: 'member',
                text: 'Member'
            },
            {
                value: 'admin',
                text: 'Admin'
            }
        ];

        $scope.all_users = [];

        $scope.list = {};
        $scope.errors = {};

        // END intialization

        /*
         * fetch all valid users
         */
        var fetch_valid_users_list = function () {
            user_service.fetch_valid_users(list_id).then(function (users) {
                $scope.all_users = users.data;
            });
        };
        fetch_valid_users_list(); // need to call it first to fetch data...

        /*
         * This function is to change role of a particular user...
         */
        $scope.modify_role = function (list_id, user_id, role) {
            list_service.modify_role(list_id, user_id, role).then(function (success) {
                toastr.success(success.data);
            }, function (err) {
                toastr.error("We are not able to perform this action at this moment, please try again later.");
            });
        };

        /*
         * This function is to refresh the list_users again...
         */
        var refresh_records = function (list_id) {
            list_service.fetch_list_users(list_id).then(function (list_users) {
                $scope.list_users = list_users.data;
            }, function (err) {
                console.log(err);
            });
        };

        /*
         * This function is to remove user from a list...
         */
        $scope.remove_list_user = function (list_id, user_id) {
            list_service.remove_list_user(list_id, user_id).then(function (success) {
                toastr.success(success.data);
                // refresh records to show updated users.
                refresh_records(list_id);
                //refresh user list also.
                fetch_valid_users_list();
            }, function (err) {
                toastr.error("We are not able to perform this action at this moment, please try again later.");
            });
        };

        /*
         * This function is to add new user into list
         */
        $scope.add_list_users = function () {
            if (validate_new_users_form($scope.list)) {
                list_service.add_list_users(list_id, $scope.list).then(function (success) {
                    toastr.success(success.data);
                    // refresh records to show updated users.
                    refresh_records(list_id);
                    //refresh user list also.
                    fetch_valid_users_list();
                    // reset values of form.
                    $scope.list = {};
                }, function (err) {
                    toastr.error("There is an error in data you provided.");

                    if (err.data.users) {
                        $scope.errors.users = err.data.users;
                    }
                    if (err.data.user_role) {
                        $scope.errors.user_role = err.data.user_role;
                    }
                });

                $scope.errors = {};
            }
        };

        // validate form to proceed...
        var validate_new_users_form = function (input) {
            var errors = true;

            // check if user input is valid
            if (!(input.users) || input.users.length <= 0) {
                $scope.errors.users = "Please select atleast one user.";

                errors = false;
            }

            // check if role input is valid
            if (!(input.user_role) || input.user_role.length <= 0) {
                $scope.errors.user_role = "Please select a role.";

                errors = false;
            }

            return errors;
        };

        // close the popup...
        $scope.close_popup = function () {
            $uibModalInstance.dismiss();
        };
}]);
