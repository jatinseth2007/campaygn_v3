app.controller('pagination', ['$scope', '$stateParams','$state', function($scope, $stateParams,$state) {
    /**
     * change pagination page value on click.
     */
    $scope.change_page_value = function() {
        $stateParams.page = $scope.bigCurrentPage;
        $state.go('profiles', $stateParams, { reload: true });
    };
}]);
