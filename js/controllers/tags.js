app.controller('tags', ['$scope', '$stateParams', '$state', 'tags',
    function ($scope, $stateParams, $state, tags) {

        $scope.stateParams = $stateParams;
        $scope.tags = tags;

        $scope.search = function () {
            if ($stateParams !== '') {
                $state.go("tags", $stateParams, {
                    reload: true
                });
            }

        };
    }]);