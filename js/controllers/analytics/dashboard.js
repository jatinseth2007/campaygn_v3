/*jshint loopfunc: true */

app.controller('analytics_dashboard', ['$scope', '$filter', '$stateParams', 'validation', '$location', '$rootScope', '$state', '$window', 'analytics_service', 'events',

function ($scope, $filter, $stateParams, validation, $location, $rootScope, $state, $window, analytics_service, events) {
        /**************************** General section ***********************************/
        $scope.events = events;
        $scope.filter = {};
        $scope.export = {};
        $scope.category_legend = ["Profiles"];
        $scope.category_legend_filter = "Profiles";
        var default_actions = ["added", "deleted", "opened"];
        /**************************** End of General section *****************************/

        /*********************************** Overview section ****************************************/
        //Highchart configuration for 
        $scope.event_chart_config = {
            options: {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Events'
                },
                xAxis: {
                    categories: [],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of event'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                credits: {
                    enabled: false
                },
            },
            series: []
        };

        /**
         * Used to generate series for event bar chart
         * @author karthick.k
         * @param {Object} events grouped event object
         */
        var generate_event_chart = function (events) {
            events = _(events).groupBy("category").value();
            var event_chart_series = {};
            for (var key in events) {
                $scope.event_chart_config.options.xAxis.categories.push(key);
                var actions = _(events[key]).groupBy("action").value();
                default_actions.forEach(function (action) {
                    if (!event_chart_series.hasOwnProperty(action)) {
                        event_chart_series[action] = [];
                    }
                    if (!!actions[action] && !!actions[action].length) {
                        event_chart_series[action].push(parseInt(actions[action][0].count));
                    } else {
                        event_chart_series[action].push(0);
                    }
                });
            }
            for (var action in event_chart_series) {
                $scope.event_chart_config.series.push({
                    name: action,
                    data: event_chart_series[action]
                });
            }
        };
        generate_event_chart($scope.events);

        /**
         * Used to get events based on the date filter
         * @author karthick.k
         * @param {Object} form Form values ( from and to date)
         */
        $scope.fetch_events = function (form) {
            if (!validation.is_valid(form)) {
                return;
            }
            var filter = $scope.filter;
            filter.from = $filter('date')(new Date(filter.from), 'yyyy-MM-dd');
            filter.to = $filter('date')(new Date(filter.to), 'yyyy-MM-dd');
            analytics_service.fetch_events(filter).then(function (result) {
                generate_event_chart(result.data);
            }, function (result) {
                console.log(result.data);
            });
        };
        /*********************************** End of Overview section *************************************/

        /*********************************** Download section ****************************************/
        /**
         * Used to change the download category legend
         * @author karthick.k
         * @param {String} legend Selected legend for download
         */
        $scope.change_category_legend = function (legend) {
            $scope.category_legend_filter = legend;
        };

        /**
         * Used to export the selected category based on the date
         * @author karthick.k
         * @param {object} form data
         */
        $scope.export = function (form) {
            if (!validation.is_valid(form)) {
                return;
            }
            var form_details = $scope.export;
            $scope.export.from = $filter('date')(new Date(form_details.from), 'yyyy-MM-dd');
            $scope.export.to = $filter('date')(new Date(form_details.to), 'yyyy-MM-dd');
            form_details.category = $scope.category_legend_filter;
            analytics_service.export(form_details);
        };
        /*********************************** End of download section **********************************/
}]);