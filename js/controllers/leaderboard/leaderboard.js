app.controller('leaderboard', ['$state', '$stateParams', '$scope', 'countries', 'gender', 'themes_factory', 'social_platform_factory', 'leaderboard_service',
    function ($state, $stateParams, $scope, countries, gender, themes_factory, social_platform_factory, leaderboard_service) {
        // intialization
        document.title = 'Campaygn - Leaderboard';
        $scope.countries = countries.all_countries; // load all the contries in the dropdown...
        $scope.all_gender = gender.all_gender; // load all the gender options in the dropdown...
        $scope.all_themes = themes_factory.all_themes; // load all the gender options in the dropdown...
        $scope.all_social_platforms = social_platform_factory.all_social_platforms; // load all the gender options in the dropdown...
        $scope.all_kpis = [{
                name: 'Followers',
                value: 'followers'
        },
            {
                name: 'Growth Rate',
                value: 'growth_rate'
                          },
            {
                name: 'Engagement Rate',
                value: 'engagement_rate'
                          }];
        // END

        // fetch $stateParams and assign those into form + fetch profiles according to filters...
        $scope.leaderboard = {}; //to handle form inputs
        $scope.leaderboard.selected_countries = ($stateParams.search_countries) ? $stateParams.search_countries.split(',') : [];
        $scope.leaderboard.selected_gender = ($stateParams.search_gender) ? $stateParams.search_gender.split(',') : [];
        $scope.leaderboard.selected_themes = ($stateParams.search_themes) ? $stateParams.search_themes.split(',') : [];
        $scope.leaderboard.selected_platform = ($stateParams.search_social_platform) ? $stateParams.search_social_platform : '';
        $scope.leaderboard.kpi = ($stateParams.search_kpi) ? $stateParams.search_kpi : '';

        // call the API to fetch results...
        if ($stateParams.search_trigger === true || $stateParams.search_trigger == 'true') {
            if (leaderboard_service.validate($scope.leaderboard)) {
                leaderboard_service.fetch_results();
            }
        }

        // Assign form values to $stateParams and reload page with params...
        $scope.fetch_profiles = function () {
            // validate data first don't want to break my code...
            if (leaderboard_service.validate($scope.leaderboard)) {
                // need to assign params to $satteparams
                $stateParams.search_trigger = true;
                $stateParams.search_countries = $scope.leaderboard.selected_countries.join();
                $stateParams.search_gender = $scope.leaderboard.selected_gender.join();
                $stateParams.search_themes = $scope.leaderboard.selected_themes.join();
                $stateParams.search_social_platform = $scope.leaderboard.selected_platform;
                $stateParams.search_kpi = $scope.leaderboard.kpi;

                $state.go('leaderboard', $stateParams, {
                    reload: true
                });
            }
        };

    }
]);
