app.controller('profiles', ['$scope', '$stateParams', '$location', '$http', 'toastr', 'profile_service', 'list_service', '$state', 'countries', 'gender', 'languages', 'continents', '$filter', '$rootScope', '$uibModal', 'profiles_list_service', 'themes_factory',
    function ($scope, $stateParams, $location, $http, toastr, profile_service, list_service, $state, countries, gender, languages, continents, $filter, $rootScope, $uibModal, profiles_list_service, themes_factory) {

        // add list functionality in profiles...
        profiles_list_service.add_popup_functionality($scope, $uibModal, list_service, toastr, function () {
            // nothing to do...
        });

        /**
         ** Default variable defination
         */

        /**
         * Need the values on template page.
         */
        $scope.stateParams = $stateParams;
        $scope.tag_limit = 3;
        document.title = 'Campaygn - Profiles - ' + $scope.stateParams.profile_kind;
        $scope.slider = {}; // this is for followers reach filter
        $scope.engagement_rate = {}; // this is for engagement rate filter
        $scope.age = {}; // this is for engagement rate filter
        $scope.growth = {}; // this is for growth filter

        /**
         * Slider intialization
         */
        // this is followers range...
        profile_service.fetch_profiles_reach().then(function (max_min) {
            var slider_min = ($stateParams.min_range) ? parseInt($stateParams.min_range) : parseInt(max_min.data.minimum_value);
            var slider_max = ($stateParams.max_range) ? parseInt($stateParams.max_range) : parseInt(max_min.data.maximum_value);

            $scope.slider = {
                minValue: slider_min,
                maxValue: slider_max,
                options: {
                    floor: parseInt(max_min.data.minimum_value),
                    ceil: parseInt(max_min.data.maximum_value),
                    hideLimitLabels: true,
                    translate: function (value) {
                        return $filter('number')(value, 0);
                    }
                }
            };
        }, function (err) {
            $scope.slider = null;
        });

        // this is to Growth range...
        profile_service.fetch_profiles_growth_range().then(function (growth_max_min) {
                var slider_min = ($stateParams.min_growth) ? $stateParams.min_growth : parseInt(growth_max_min.data.minimum_value);
                var slider_max = ($stateParams.max_growth) ? $stateParams.max_growth : parseInt(growth_max_min.data.maximum_value);

                $scope.growth = {
                    minValue: slider_min,
                    maxValue: slider_max,
                    options: {
                        floor: parseInt(growth_max_min.data.minimum_value),
                        ceil: parseInt(growth_max_min.data.maximum_value),
                        step: 0.1,
                        precision: 1,
                        hideLimitLabels: true
                    }
                };
            },
            function (err) {
                $scope.growth = null;
            });

        // this is to manage engagement rate range...
        profile_service.fetch_profiles_engagementrate_range().then(function (egrt_max_min) {
                var slider_min = ($stateParams.min_engagement) ? $stateParams.min_engagement : parseInt(egrt_max_min.data.minimum_value);
                var slider_max = ($stateParams.max_engagement) ? $stateParams.max_engagement : parseInt(egrt_max_min.data.maximum_value);

                $scope.engagement_rate = {
                    minValue: slider_min,
                    maxValue: slider_max,
                    options: {
                        floor: parseInt(egrt_max_min.data.minimum_value),
                        ceil: parseInt(egrt_max_min.data.maximum_value),
                        step: 0.1,
                        precision: 1,
                        hideLimitLabels: true
                    }
                };
            },
            function (err) {
                $scope.engagement_rate = null;
            });

        // this is to manage age range...
        profile_service.fetch_profiles_age_range().then(function (age_max_min) {
                var slider_min = ($stateParams.min_age) ? parseInt($stateParams.min_age) : parseInt(age_max_min.data.min_age);
                var slider_max = ($stateParams.max_age) ? parseInt($stateParams.max_age) : parseInt(age_max_min.data.max_age);

                $scope.age = {
                    minValue: slider_min,
                    maxValue: slider_max,
                    options: {
                        floor: parseInt(age_max_min.data.min_age),
                        ceil: parseInt(age_max_min.data.max_age),
                        hideLimitLabels: true,
                        translate: function (value) {
                            return $filter('number')(value, 0);
                        }
                    }
                };
            },
            function (err) {
                $scope.age = null;
            });


        /**
         *  Intialization for Search params
         */
        $scope.countries = countries.all_countries; // load all the contries in the dropdown...
        $scope.all_continents = continents.all_continents; // load all the continents options in the dropdown...
        $scope.all_gender = gender.all_gender; // load all the gender options in the dropdown...
        $scope.all_tags = []; // load all the gender options in the dropdown...
        $scope.all_themes = themes_factory.all_themes; // load all the gender options in the dropdown...
        $scope.all_languages = languages.all_languages; // load all the countries options in the dropdown...
        //END

        // to make things selected inside form
        $scope.all = {};
        $scope.all.search_keyword = ($stateParams.search_keyword) ? $stateParams.search_keyword : '';
        $scope.all.selected_countries = ($stateParams.search_countries) ? $stateParams.search_countries.split(",") : [];
        $scope.all.selected_continents = ($stateParams.search_continents) ? $stateParams.search_continents.split(",") : [];
        $scope.all.selected_gender = ($stateParams.search_gender) ? $stateParams.search_gender.split(",") : [];
        $scope.all.search_tags = ($stateParams.search_tags) ? $stateParams.search_tags.split(",") : [];
        $scope.all.selected_themes = ($stateParams.search_themes) ? $stateParams.search_themes.split(",") : [];
        $scope.all.selected_languages = ($stateParams.search_languages) ? $stateParams.search_languages.split(",") : [];
        // END SECTION

        /**
         * need to call the service
         */
        profile_service.fetch_profiles().then(function (profiles) {
            $scope.maxSize = 10;
            $scope.bigTotalItems = profiles.data.total_records;
            $scope.bigCurrentPage = profiles.data.page;
            $scope.itemsPerPage = profiles.data.limit;

            $scope.profiles = profiles.data.users;

            /**
             * If no image  then give placeholder
             */
            if ($scope.profiles && $scope.profiles.length !== 0) {
                angular.forEach($scope.profiles, function (profile, index) {
                    /**
                     * Splitting profile tags
                     */

                    if (profile.profile_tags) {
                        profile.profile_tags = profile.profile_tags.split(",");
                    }

                    /**
                     * if we are having all case then need to calculate for each profile ...
                     */
                    if ($stateParams.active_platform === 'all') {
                        // add Instagram percentage
                        if (profile.ig_followed_by) {
                            profile.ig_total_percentage = $filter('number')((profile.ig_followed_by / profile.total_followers) * 100, 0);
                        }
                        // add facebook percentage
                        if (profile.facebook_likes) {
                            profile.fb_total_percentage = $filter('number')((profile.facebook_likes / profile.total_followers) * 100, 0);
                        }
                        // add youtube percentage
                        if (profile.youtube_subscriber_count) {
                            profile.yt_total_percentage = $filter('number')((profile.youtube_subscriber_count / profile.total_followers) * 100, 0);
                        }

                        // need to select the profile picture in 'all' case
                        if (profile.ig_profile_picture) {
                            profile.available_profile_picture = profile.ig_profile_picture;
                        } else if (profile.facebook_picture) {
                            profile.available_profile_picture = profile.facebook_picture;
                        } else if (profile.youtube_profile_picture) {
                            profile.available_profile_picture = profile.youtube_profile_picture;
                        } else {
                            profile.available_profile_picture = null;
                        }

                    }
                });
            } else {
                $scope.profiles = null;
            }

        }, function () {
            $scope.profiles = null;
        });


        /**
         * Search function
         */
        $scope.search = function () {
            $stateParams.search_keyword = $scope.all.search_keyword;
            $stateParams.search_countries = $scope.all.selected_countries.join();
            $stateParams.search_continents = $scope.all.selected_continents.join();
            $stateParams.search_gender = $scope.all.selected_gender.join();
            $stateParams.search_themes = $scope.all.selected_themes.join();
            $stateParams.search_tags = $scope.all.search_tags.join();
            $stateParams.search_languages = $scope.all.selected_languages.join();
            $state.go('profiles', $stateParams, {
                reload: true
            });
        };

        /**
         * Search based on crieteria
         */
        $scope.criterias_search = function () {
            // followers range options...
            $stateParams.min_range = $scope.slider.minValue;
            $stateParams.max_range = $scope.slider.maxValue;

            // followers range options...
            $stateParams.min_engagement = $scope.engagement_rate.minValue;
            $stateParams.max_engagement = $scope.engagement_rate.maxValue;

            // age range options...
            $stateParams.min_age = $scope.age.minValue;
            $stateParams.max_age = $scope.age.maxValue;

            // growth range options...
            $stateParams.min_growth = $scope.growth.minValue;
            $stateParams.max_growth = $scope.growth.maxValue;

            $state.go('profiles', $stateParams, {
                reload: true
            });
        };

        /**
         * Function is responsable to sort the records dynamically
         */
        $scope.sort_results = function (sort_by) {
            $stateParams.sort_order = ($stateParams.sort_by && $stateParams.sort_order && $stateParams.sort_by == sort_by && $stateParams.sort_order.toLowerCase() == 'desc') ? 'ASC' : 'DESC';
            $stateParams.sort_by = sort_by;

            $state.go('profiles', $stateParams, {
                reload: true
            });
        };

        /**
         * Used to navigate to demo page / login page
         * @author karthick.k
         */
        $scope.get_demo = function () {
            setTimeout(function () {
                angular.element(document.getElementById('user-dropdown'))[0].click();
            });
        };
                }
                ]);
