app.controller('profiles_list', ['$scope', '$stateParams', 'toastr', 'list_service', '$uibModalInstance', 'profiles_list_service', 'user_lists', 'list_obj',

    function ($scope, $stateParams, toastr, list_service, $uibModalInstance, profiles_list_service, user_lists, list_obj) {

        // intialize params
        $scope.my_tree = tree = {};
        $scope.user_lists = user_lists;
        $scope.list = list_obj;
        // END intialization...

        // Create new list with selected profiles...
        $scope.create_new_list = function () {
            if (list_service.validate_list_form($scope.list)) {
                list_service.create_user_list($scope.list).then(function (success) {
                    $uibModalInstance.dismiss();
                    toastr.success(success.data);
                }, function (err) {
                    $scope.list_api_error = 'There is some error in data you provied';

                    if (err.data.name) {
                        $scope.error = {};
                        $scope.error.name = err.data.name;
                    }
                });
            }
        };
        // END Create new list

        /*
         * Remove profiles from list
         */
        // remove the selected profile(s) from the list
        $scope.remove_from_list = function () {
            if (validate_remove_list_form()) {
                list_service.remove_profile_list($scope.list).then(function (success) {
                    $uibModalInstance.dismiss();
                    toastr.success(success.data);
                }, function (err) {
                    if (err.status == 401) {
                        $scope.list_api_error = err.data;
                    } else {
                        $scope.list_api_error = 'There is some error in data you provied';
                    }
                });
            }
        };

        // END Remove profiles from list


        /*
         * Add profiles to list
         */
        // add the selected profile(s) to the list
        $scope.add_to_list = function () {
            if (validate_remove_list_form()) {
                list_service.add_profile_list($scope.list).then(function (success) {
                    $uibModalInstance.dismiss();
                    toastr.success(success.data);
                }, function (err) {
                    if (err.status == 401) {
                        $scope.list_api_error = err.data;
                    } else {
                        $scope.list_api_error = 'There is some error in data you provied';
                    }
                });
            }
        };

        // END Remove profiles from list



        /*
         * Common List functions...
         */
        var validate_remove_list_form = function () {
            var error = true;

            if (angular.isUndefined($scope.list.parent_id) || isNaN($scope.list.parent_id) || $scope.list.parent_id <= 0) {
                $scope.list_parent_error = 'Please select a list to proceed';
                error = false;
            }

            return error;
        };

        $scope.add_children = function (branch, user_role) {
            list_service.add_children($scope.list, branch, user_role);
        };

        // To close the open popup
        $scope.cancel_button = function () {
            $uibModalInstance.dismiss();
        };
        // END Common List functions

    }
]);
