/*jshint loopfunc: true */

app.controller('monitor', ['$scope', '$stateParams', '$location', '$rootScope', '$state', '$window',
    'monitor_details', 'monitors_service', '$filter', 'posts_details', 'followers_details', 'validation', '$sce', '$http',
    function ($scope, $stateParams, $location, $rootScope, $state, $window, monitor_details, monitors_service, $filter, posts_details, followers_details, validation, $sce, $http) {


        /****************************** common methods ****************************/

        //Influencers  - monitor_details[0], tags - monitor_details[1], users - monitor_details[2]
        //Finding where individual influencer page or monitor page
        $scope.users = monitor_details[2] || [];
        //Used by angularjs xeditable
        $scope.opened = {};
        $scope.updated = {
            tags: [],
            influencers: [],
            individual_influencers: []
        };
        var default_value = {
            facebook: {
                count: 0,
                percentage: 0
            },
            instagram: {
                count: 0,
                percentage: 0
            },
            youtube: {
                count: 0,
                percentage: 0
            }
        };
        var impression_values = {
            facebook: 0.008,
            instagram: 0.20,
            youtube: 1
        };
        $scope.reach_default_values = impression_values;
        var media_color = {
            instagram: "#956D53",
            facebook: "#39599F",
            youtube: "#b03330"
        };
        var default_media_value = 0.74;

        $scope.is_tag_selected = ($stateParams.tag_id !== undefined) ? true : false;
        if ($scope.is_tag_selected) {
            $scope.tags = _.filter(monitor_details[1], function (tag) {
                return tag.tag_id == $stateParams.tag_id;
            });
        } else {
            $scope.tags = monitor_details[1] || [];
        }

        /**
         * Used to get unique posts and combine the tags
         * @author karthick.k
         * @param   {array} posts list of posts
         * @returns {array} unique posts list
         */
        var get_unique_posts = function (posts) {
            var result = [];
            var group_posts = _.chain(posts)
                .groupBy("post_id")
                .pairs()
                .map(function (item) {
                    return _.object(_.zip(["post_id", "fields"], item));
                })
                .value();
            group_posts.forEach(function (post) {
                var tags = [];
                post.fields.forEach(function (field) {
                    if (tags.indexOf(field.tag)) {

                    }
                    if (field.tag_type === "hash") {
                        tags.push("#" + field.tag);
                    } else if (field.tag_type === "name") {
                        tags.push("@" + field.tag);
                    }
                });
                var obj = post.fields[0];
                obj.tags = tags.toString();
                result.push(obj);
            });
            return result;
        };
        /**
         * USed to get list of posts facebook, instagram, youtube posts
         * @author karthick.k
         * @returns {array} list of posts
         */
        var get_posts_list = function () {
            var posts_details_list = [];
            posts_details.forEach(function (media_posts_details) {
                media_posts_details.forEach(function (post_details) {
                    posts_details_list.push(post_details);
                });
            });
            return get_unique_posts(posts_details_list);
        };

        //To find whether user is in monitor or individual influencer page
        $scope.is_influencer_page = ($stateParams.influencer_id !== undefined) ? true : false;
        if ($scope.is_influencer_page) {
            $scope.influencers = _.filter(monitor_details[0], function (influencers) {
                return influencers.influencer_id == $stateParams.influencer_id;
            });
        } else {
            var influencers = monitor_details[0] || [];
            var posts_by_influencers = _(get_posts_list()).groupBy("influencer_id").value();
            influencers.forEach(function (influencer) {
                if (posts_by_influencers.hasOwnProperty(influencer.influencer_id)) {
                    influencer.post_available = true;
                } else {
                    influencer.post_available = false;
                }
            });
            $scope.influencers = influencers;
        }
        var grouped_influencers = _($scope.influencers).groupBy("from_list").value();
        $scope.individual_influencers = grouped_influencers.N ? grouped_influencers.N : [];
        $scope.influencers_from_list = grouped_influencers.Y ? grouped_influencers.Y : [];


        var posts = _.sortBy(get_posts_list(),
            function (post) {
                return parseInt(post.interactions);
            }).reverse();
        $scope.posts_by_media = _(posts).groupBy("media").value();

        /**
         * Used to get followers list
         * @author karthick.k
         * @returns {array} list of followers
         */
        var get_influencers_list = function () {
            var influencers_followers_details_list = [];
            followers_details.forEach(function (influencer_followers_details) {
                influencers_followers_details_list.push(influencer_followers_details);
            });
            return influencers_followers_details_list;
        };
        var influencers_list = get_influencers_list();

        /**
         * Used to aggregate the influncers, tags, users into monitor
         * @author karthick.k
         * @param   {Array} monitor_details Array of tags, influencers, users
         * @returns {object} monitor information
         */
        var get_monitor_details = function () {
            var influencers = monitor_details[0] || [];
            var tags = monitor_details[1] || [];
            var users = monitor_details[2] || [];
            var monitor = {};
            if (!!users.length || !!tags.length || !!influencers.length) {
                monitor = {
                    influencers: influencers,
                    tags: tags,
                    users: users,
                    name: users[0].name || $scope.tags[0].name || $scope.influencers[0].name,
                    id: users[0].id || $scope.tags[0].id || $scope.influencers[0].id,
                    from: users[0].from || $scope.tags[0].from || $scope.influencers[0].from,
                    to: users[0].to || $scope.tags[0].to || $scope.influencers[0].to,
                    group_name: $scope.influencers[0].group_name
                };
            } else {
                $state.go("monitors");
            }
            return monitor;
        };

        /**
         * Used to get followers media wise
         * @author karthick.k
         * @returns {object} media wise folowers
         */
        var get_followers_by_media = function () {
            var followers_by_media = angular.copy(default_value);
            var total = 0;
            var post_by_media = _(posts).groupBy("media").value();
            for (var media in post_by_media) {
                var count = 0;
                if (media === "youtube") {
                    count = _.sum(post_by_media[media], 'followed_by');
                } else {
                    var post_by_influencer = _(post_by_media[media]).groupBy("influencer_id").value();
                    for (var influencer in post_by_influencer) {
                        count += parseInt(post_by_influencer[influencer][0].followed_by);
                    }
                }
                followers_by_media[media].count = count;
                total += count;
            }
            for (var key in followers_by_media) {
                followers_by_media[key].percentage = ((followers_by_media[key].count / total) * 100);
            }
            followers_by_media.total = total;
            return followers_by_media;
        };
        var followers_by_media = get_followers_by_media();

        /**
         * Used to get no of posts by media wise
         * @author karthick.k
         * @returns {object} media wise no of posts
         */
        var get_post_count_by_media = function () {
            var post_by_media = angular.copy(default_value);
            if (posts.length === 0) {
                post_by_media.total = 0;
                return post_by_media;
            }
            var total = 0;
            var posts_by_media = _(posts).groupBy("media").value();
            for (var key in posts_by_media) {
                var count = posts_by_media[key].length || 0;
                post_by_media[key].count = count;
                total += count;
            }
            for (var media in post_by_media) {
                post_by_media[media].percentage = ((post_by_media[media].count / total) * 100);
            }
            post_by_media.total = total;
            return post_by_media;
        };
        var post_count_by_media = get_post_count_by_media();

        /**
         * Used to get reach by media wise
         * @author karthick.k
         * @returns {object} media wise reach
         */
        var get_reach_by_media = function () {
            var reach_by_media = angular.copy(default_value);
            if (posts.length === 0) {
                reach_by_media.total = 0;
                return reach_by_media;
            }
            var total = 0;
            var posts_by_media = _(posts).groupBy("media").value();
            for (var key in posts_by_media) {
                var count = parseInt(followers_by_media[key].count) * impression_values[key];
                reach_by_media[key].count = count;
                total += count;
            }
            for (var media in reach_by_media) {
                reach_by_media[media].percentage = ((reach_by_media[media].count / total) * 100);
            }
            reach_by_media.total = total;
            return reach_by_media;
        };
        var reach_by_media = get_reach_by_media();

        /**
         * Used to get impression by media wise
         * @author karthick.k
         * @returns {object} media wise impressions
         */
        var get_impressions_by_media = function () {
            var posts_by_media = $scope.posts_by_media;
            var impressions_by_media = angular.copy(default_value);
            var total = 0;
            for (var key in posts_by_media) {
                var count = 0;
                posts_by_media[key].forEach(function (post) {
                    count += parseInt(post.followed_by) * impression_values[key];
                });
                impressions_by_media[key].count = count;
                total += count;
            }
            for (var media in impressions_by_media) {
                impressions_by_media[media].percentage = ((impressions_by_media[media].count / total) * 100);
            }
            impressions_by_media.total = total;
            return impressions_by_media;
        };
        var impressions_by_media = get_impressions_by_media();

        /**
         * Used to get interactions based on the media
         * @author karthick.k
         * @returns {object} media wise interactions
         */
        var get_interactions_by_media = function () {
            var posts_by_media = $scope.posts_by_media;
            var interactions_by_media = angular.copy(default_value);
            var total = 0;
            for (var key in posts_by_media) {
                interactions_by_media[key].count = _.sum(posts_by_media[key], 'interactions');
                total += interactions_by_media[key].count;
            }
            for (var media in interactions_by_media) {
                interactions_by_media[media].percentage = ((interactions_by_media[media].count / total) * 100);
            }
            interactions_by_media.total = total;
            return interactions_by_media;
        };
        var interactions_by_media = get_interactions_by_media();

        /**
         * Used to get media value
         * @author karthick.k
         * @returns {object} media wise media value
         */
        var get_media_value_by_media = function () {
            var media_value_by_media = angular.copy(default_value);
            var posts_by_media = $scope.posts_by_media;
            var total = 0;
            for (var key in posts_by_media) {
                var count = (interactions_by_media[key].count * default_media_value);
                media_value_by_media[key].count = count;
                total += count;
            }
            for (var media in media_value_by_media) {
                media_value_by_media[media].percentage = ((media_value_by_media[media].count / total) * 100);
            }
            media_value_by_media.total = total;
            return media_value_by_media;
        };
        var media_value_by_media = get_media_value_by_media();

        /**
         * Used to get media value
         * @author karthick.k
         * @returns {object} media wise media value
         */
        var get_engagement_rate_by_media = function () {
            var posts_by_media = $scope.posts_by_media;
            var engagement_rate_by_media = angular.copy(default_value);
            var total = 0;
            for (var key in posts_by_media) {
                if (!!interactions_by_media[key] && !!followers_by_media[key]) {
                    var count = ((interactions_by_media[key].count / posts_by_media[key].length) / followers_by_media[key].count) * 100;
                    engagement_rate_by_media[key].count = count;
                    total += count;
                }
            }
            for (var media in engagement_rate_by_media) {
                engagement_rate_by_media[media].percentage = ((engagement_rate_by_media[media].count / total) * 100);
            }
            engagement_rate_by_media.total = total;
            return engagement_rate_by_media;
        };
        var engagement_rate_by_media = get_engagement_rate_by_media();

        /**
         * Used to change url as trustAsResourceUrl
         * @author karthick.k
         * @param   {string} url
         * @returns {string} url
         */
        $scope.trustAsUrl = function (url) {
            return $sce.trustAsResourceUrl(url);
        };

        /**************************** End of common methods ***********************/

        /******************* Monitor details - name, influencers, users, tags **************************/

        /**
         * Used to handle event propagation which is used by angularjs framework xeditable
         * @author karthick.k
         * @param   {object} event
         * @param   {object} elementOpened
         */
        $scope.open = function ($event, elementOpened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[elementOpened] = !$scope.opened[elementOpened];
        };


        /**
         * Used to modify group name of monitors
         * @author karthick.k
         * @param {number} id monitor id
         * @param {string} group_name group name of monitor
         */
        $scope.modify_group_name = function (id, group_name) {
            console.log(id, group_name);
        };

        /**
         * Used to change monitor name
         * @author karthick.k
         * @param {number} id   monitor id
         * @param {string} name new name for monitor
         */
        $scope.modify_name = function (id, name) {
            monitors_service.modify_name({
                id: id,
                name: name
            }).then(function (result) {
                if (result.data && result.data.message) {
                    $scope.success_message = result.data.message;
                }
            }, function (result) {
                $scope.error_message = result.data;
            });
        };

        /**
         * Used to modify the date
         * @author karthick.k
         * @param {number} id monitor id
         * @param {datestring} from_date monitor from date
         * @param {datestring} to_date monitor to date
         */
        $scope.modify_date = function (id, from_date, to_date, is_from_date) {
            if (!from_date || !to_date) {
                return;
            }
            from_date = $filter('date')(new Date(from_date), 'yyyy-MM-dd');
            to_date = $filter('date')(new Date(to_date), 'yyyy-MM-dd');
            if (new Date(from_date).getTime() > new Date(to_date).getTime()) {
                return;
            }
            var monitor = {};
            monitor.id = id;
            monitor.from = from_date;
            monitor.to = to_date;
            monitor.is_from_date = is_from_date;
            monitors_service.modify_date(monitor).then(function (result) {
                if (result.data && result.data.message) {
                    $scope.success_message = result.data.message;
                }
                $state.reload();
            }, function (result) {
                $scope.error_message = result.data;
            });
        };

        /**
         * Used to navigate to influencer page of that monitor
         * @author karthick.k
         * @param {object} influencer influencer id, monitor id, monitor name etc
         */
        $scope.go_to_influencer = function (influencer) {
            $state.go("monitor", {
                id: influencer.id,
                influencer_id: influencer.influencer_id
            });
        };

        /**
         * Used to open layout for influencers
         * @author karthick.k
         */
        $scope.open_edit_layout = function (key) {
            $scope["activate_edit_" + key] = true;
            $scope.updated[key] = angular.copy($scope[key]);
        };

        /**
         * Used to close edit influencers layout
         * @author karthick.k
         */
        $scope.close_edit_layout = function (tags) {
            $scope["activate_edit_" + tags] = false;
        };

        /**
         * Used to update users
         * @author karthick.k
         * @param {object} form form details
         */
        $scope.update_users = function (form) {
            if (!validation.is_valid(form)) {
                return;
            }
            var users = [];
            $scope.updated.users.forEach(function (user) {
                users.push({
                    id: user.user_id
                });
            });
            monitors_service.modify_users({
                monitor_id: $scope.updated.users[0].id,
                users: users
            }).then(function (result) {
                $state.reload();
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to search infuencers
         * @author karthick.k
         * @param {string} keyword search keyword for influencers
         */
        $scope.search_users = function (keyword) {
            if (!keyword) {
                return;
            }
            monitors_service.search_users(keyword).then(function (result) {
                if (result && result.data) {
                    result.data.forEach(function (data) {
                        data.user_id = data.id;
                    });
                }
                $scope.searched_users = result.data;
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to update influencers
         * @author karthick.k
         * @param {object} form form details
         */
        $scope.update_influencers = function (form) {
            if (!validation.is_valid(form)) {
                return;
            }
            var influencers = [];
            $scope.updated.individual_influencers.forEach(function (influencer) {
                influencers.push({
                    id: influencer.influencer_id
                });
            });
            monitors_service.modify_influencers({
                monitor_id: $scope.updated.individual_influencers[0].id || get_monitor_details().id,
                influencers: influencers
            }).then(function (result) {
                $state.reload();
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to search infuencers
         * @author karthick.k
         * @param {string} keyword search keyword for influencers
         */
        $scope.search_influencers = function (keyword) {
            if (!keyword) {
                return;
            }
            monitors_service.search_individual_influencers(keyword).then(function (result) {
                var individual_influencers = [];
                if (result && result.data) {
                    result.data.forEach(function (data) {
                        individual_influencers.push({
                            influencer_id: data.id,
                            profile_name: data.name,
                            profile_picture: data.profile_picture
                        });
                    });
                }
                $scope.searched_individual_influencers = individual_influencers;
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to update tags
         * @author karthick.k
         * @param {object} form form object
         */
        $scope.update_tags = function (form) {
            if (!validation.is_valid(form)) {
                return;
            }
            var tags = [];
            $scope.updated.tags.forEach(function (tag) {
                tags.push({
                    id: tag.tag_id
                });
            });
            monitors_service.modify_tags({
                monitor_id: $scope.updated.tags[0].id,
                tags: tags
            }).then(function (result) {
                $state.reload();
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to update group name
         * @author karthick.k
         * @param {object} form object
         */
        $scope.update_group_name = function (form, monitor_id) {
            if (!validation.is_valid(form)) {
                return;
            }
            monitors_service.modify_group_name({
                id: monitor_id,
                group_name: $scope.updated.group_name
            }).then(function (result) {
                $state.reload();
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to search tags
         * @author karthick.k
         * @param {string} keyword search keyword for tags
         */
        $scope.search_tags = function (keyword) {
            if (!keyword) {
                return;
            }
            monitors_service.search_tags(keyword).then(function (result) {
                var tags = [];
                if (result && result.data) {
                    result.data.forEach(function (tag) {
                        tags.push({
                            tag: tag.tag,
                            tag_id: tag.id
                        });
                    });
                }
                $scope.searched_tags = tags;
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to search monitor group name
         * @author karthick.k
         * @param {string} keyword search keyword for monitor group name
         */
        $scope.search_group = function (keyword) {
            if (!keyword) {
                return;
            }
            monitors_service.search_group(keyword).then(function (result) {
                if (result && result.data) {
                    var group_list = [];
                    result.data.forEach(function (group) {
                        group_list.push(group.group_name);
                    });
                    $scope.group_list = group_list;
                } else {
                    $scope.group_list = [];
                }
            }, function (error) {
                console.log(error);
            });
        };

        /**************************** End of Monitor details section *******************************/

        /**************************** posts count, reach, interactions, etc *******/
        $scope.interactions = interactions_by_media.total || 0;
        $scope.reach = Math.round(reach_by_media.total);
        $scope.impressions = Math.round(impressions_by_media.total);
        $scope.followers = followers_by_media.total;
        $scope.engagement_rate = engagement_rate_by_media.total.toPrecision(2);
        $scope.media_value = Math.round(media_value_by_media.total);
        $scope.monitor = get_monitor_details(monitor_details);
        $scope.posts = posts;
        $scope.no_of_influencers_used_tags = Object.keys(_(posts).groupBy("influencer_id").value()).length;
        /*********************** End of posts count section ********************************/

        /************************* Metrics by day *****************************************/
        $scope.metrics_by_day_legends = ["Number of posts", "Reach", "Impressions", "Interactions", "Engagement rate", "Media value"];
        //Configuration for metrics by day option
        var metrics_by_day_chart_options = {
            "chart": {
                "height": 325,
                "zoomType": "x"
            },
            "title": {
                "text": ""
            },
            "tooltip": {
                "shared": true,
                "borderColor": "#CCC",
                "useHTML": true,
                pointFormat: '{series.name}: <b>{point.y:.0f}</b><br/>'
            },
            "plotOptions": {
                series: {
                    pointWidth: 2
                }
            },
            "xAxis": {
                "id": "days-axis",
                "type": "datetime",
                dateTimeLabelFormats: {
                    month: '%Y',
                    year: '%Y'
                },
                "minRange": 345600000
            },
            "yAxis": [
                {
                    "id": "y-axis",
                    "height": 200,
                    "top": 50,
                    "lineWidth": 2,
                    "offset": 0
                }
            ]
        };
        $scope.metrics_by_day_config = {
            options: metrics_by_day_chart_options
        };

        /**
         * Used to prepare date for daily post statistics
         * @author karthick.k
         * @returns {array} data grouped based on created time based on instagram, facebook, youtube
         */
        var get_daily_posts_stats = function () {
            var aggregated_posts = get_posts_list();
            var posts_by_time = _(aggregated_posts).groupBy("created_time").value();
            var data = {};
            for (var key in impression_values) {
                data[key + "_posts"] = [];
                data[key + "_reach"] = [];
                data[key + "_impressions"] = [];
                data[key + "_interactions"] = [];
                data[key + "_engagement_rate"] = [];
                data[key + "_media_value"] = [];
            }
            for (var time in posts_by_time) {
                var posts = posts_by_time[time];
                var post_by_media = _(posts_by_time[time]).groupBy("media").value();
                var count = {};
                for (var media_name in impression_values) {
                    count[media_name + "_posts_count"] = post_by_media[media_name] ? post_by_media[media_name].length : 0;
                    count[media_name + "_post_reach"] = 0;
                    count[media_name + "_post_impressions"] = 0;
                    count[media_name + "_post_interactions"] = 0;
                    count[media_name + "_post_engagement_rate"] = 0;
                    count[media_name + "_post_media_value"] = 0;
                }
                posts.forEach(function (post) {
                    count[post.media + "_post_reach"] = count[post.media + "_post_reach"] + (parseInt(post.followed_by) * impression_values[post.media]);
                    count[post.media + "_post_impressions"] = count[post.media + "_post_impressions"] + (parseInt(post.followed_by) * impression_values[post.media]);
                    count[post.media + "_post_interactions"] = count[post.media + "_post_interactions"] + parseInt(post.interactions);
                    count[post.media + "_post_engagement_rate"] = count[post.media + "_post_engagement_rate"] + ((parseInt(post.interactions) / parseInt(post.followed_by)) * 100);
                    count[post.media + "_post_media_value"] = count[post.media + "_post_media_value"] + (parseInt(post.interactions) * default_media_value);
                });
                for (var media in impression_values) {
                    data[media + "_posts"].push({
                        y: count[media + "_posts_count"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_reach"].push({
                        y: count[media + "_post_reach"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_impressions"].push({
                        y: count[media + "_post_impressions"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_interactions"].push({
                        y: count[media + "_post_interactions"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_engagement_rate"].push({
                        y: count[media + "_post_engagement_rate"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_media_value"].push({
                        y: count[media + "_post_media_value"],
                        x: parseInt(time) * 1000
                    });
                }
            }
            return data;
        };
        $scope.daily_posts_stats = get_daily_posts_stats();

        /**
         * Used to generate metrics by day chart based on the legend, reach, impression, interactons, etc
         * @author karthick.k
         * @param {string} legend selected legend
         */
        $scope.get_metrics_by_day = function (legend) {
            $scope.metrics_by_day_filter = legend;
            var filter = null;
            switch (legend) {
            case $scope.metrics_by_day_legends[0]:
                filter = "_posts";
                break;
            case $scope.metrics_by_day_legends[1]:
                filter = "_reach";
                break;
            case $scope.metrics_by_day_legends[2]:
                filter = "_impressions";
                break;
            case $scope.metrics_by_day_legends[3]:
                filter = "_interactions";
                break;
            case $scope.metrics_by_day_legends[4]:
                filter = "_engagement_rate";
                break;
            case $scope.metrics_by_day_legends[5]:
                filter = "_media_value";
                break;
            default:
                filter = "_posts";
            }
            var stats = [];
            for (var value in impression_values) {
                stats.push({
                    "id": legend + value,
                    "serieType": legend,
                    "name": value,
                    "yAxis": "y-axis",
                    "type": "bar",
                    "color": media_color[value],
                    data: $scope.daily_posts_stats[value + filter]
                });
            }
            $scope.metrics_by_day_config.series = stats;
        };

        //Initialize the metrics by day chart
        var initialize_metrics_by_day = function () {
            $scope.metrics_by_day_filter = $scope.metrics_by_day_legends[0];
            $scope.get_metrics_by_day($scope.metrics_by_day_legends[0]);
        }();

        /***************************** End of metrics by day section ****************************/

        /********************************* origin of posts section *********************************/

        //Legend for origin of posts chart
        $scope.origin_of_posts_legends = ["Number of posts", "Reach", "Impressions", "Interactions", "Engagement rate", "Media value"];
        //Pie chart configuration for origin of posts
        $scope.origin_of_posts_config = {
            options: {
                chart: {
                    height: 350,
                    marginRight: 120,
                    type: 'pie'
                },
                plotOptions: {
                    pie: {
                        size: 300,
                        shadow: false,
                        dataLabels: {
                            enabled: false
                        },
                        point: {
                            events: {}
                        }
                    },
                    series: {
                        stacking: 'normal',
                        animation: true,
                        showInLegend: true
                    }
                },
                legend: {
                    align: 'right',
                    layout: 'vertical',
                    verticalAlign: 'middle',
                    itemMarginTop: 5,
                    itemMarginBottom: 5,
                    itemStyle: {
                        fontSize: '17px',
                        fontWeight: 'normal',
                        verticalAlign: 'top'
                    },
                    x: -50,
                    useHTML: true,
                    symbolWidth: 0,
                    labelFormatter: function () {
                        var legend;
                        legend = "<span data-network='" + this.network + "'> " + this.icon + " " + this.legend + " </span>";
                        if (this.legend !== null) {
                            return legend;
                        } else {
                            return null;
                        }
                    }
                }
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            }
        };

        /**
         * Used to prepare statistics for pie chart origin by post
         * @author karthick.k
         * @returns {array} [[Description]]
         */
        var get_origin_of_posts_stats = function (data, name) {
            var stats = [
                {
                    "id": "origin_by_posts",
                    "name": name,
                    "data": [
                        {
                            "name": "<b>facebook</b>",
                            "y": data.facebook.count,
                            "color": "#39599f",
                            "icon": "<i class='fa fa-facebook-square fa-2 facebook-color'></i>",
                            "legend": data.facebook.percentage.toFixed(2) + "%"
                        },
                        {
                            "name": "<b>instagram</b>",
                            "y": data.instagram.count,
                            "color": "#AE866C",
                            "icon": "<i class='fa fa-instagram fa-2 instagram-color'></i>",
                            "legend": data.instagram.percentage.toFixed(2) + "%"
                        },
                        {
                            "name": "<b>youtube</b>",
                            "y": data.youtube.count,
                            "color": "#B03330",
                            "icon": "<i class='fa fa-youtube fa-2 youtube-color'></i>",
                            "legend": data.youtube.percentage.toFixed(2) + "%"
                        },
                    ]
                  }
                ];
            return stats;
        };

        /**
         * Used to generate pie chart based on the legend, reach, impression, interactons, etc
         * @author karthick.k
         */
        $scope.generate_origin_of_posts_chart = function (legend) {
            $scope.origin_of_posts_filter = legend;
            var data = null;
            switch (legend) {
            case $scope.origin_of_posts_legends[0]:
                data = post_count_by_media;
                break;
            case $scope.origin_of_posts_legends[1]:
                data = reach_by_media;
                break;
            case $scope.origin_of_posts_legends[2]:
                data = impressions_by_media;
                break;
            case $scope.origin_of_posts_legends[3]:
                data = interactions_by_media;
                break;
            case $scope.origin_of_posts_legends[4]:
                data = engagement_rate_by_media;
                break;
            case $scope.origin_of_posts_legends[5]:
                data = media_value_by_media;
                break;
            default:
                data = null;
            }
            $scope.origin_of_posts_config.series = !!data ? get_origin_of_posts_stats(data, legend) : [];
        };
        //Initialize the chart
        var initialize_origin_of_posts_chart = function () {
            $scope.origin_of_posts_filter = $scope.origin_of_posts_legends[0];
            $scope.generate_origin_of_posts_chart($scope.origin_of_posts_legends[0]);
        }();
        /********************************* end of origin of posts section **************************/

        /********************************* Top potential influencers *******************************/
        $scope.top_potential_influencers_current_page = 0;
        /**
         * Used to prepare number of posts of each influencers
         * @author karthick.k
         * @returns {array} list of influencers with number of posts
         */
        var number_of_posts_by_influencers = function () {
            var list = [];
            var posts_by_influencers = _(posts).groupBy("influencer_id").value();
            var influencers_list = angular.copy(get_influencers_list());
            influencers_list.forEach(function (influencer) {
                var post_by_media = _(posts_by_influencers[influencer.influencer_id]).groupBy("media").value();
                for (var key in post_by_media) {
                    influencer[key] = post_by_media[key].length || 0;
                }
                list.push(influencer);
            });
            return list;
        };

        /**
         * Used to prepare reach of each influencers
         * @author karthick.k
         * @returns {array} list of influencers with reach
         */
        var reach_by_influencers = function () {
            var list = [];
            var posts_by_influencers = _(posts).groupBy("influencer_id").value();
            var influencers_list = angular.copy(get_influencers_list());
            influencers_list.forEach(function (influencer) {
                var post_by_media = _(posts_by_influencers[influencer.influencer_id]).groupBy("media").value();
                for (var key in post_by_media) {
                    if (key === "youtube") {
                        influencer[key] = _.sum(post_by_media[key], 'followed_by').toFixed(2);
                    } else {
                        influencer[key] = (parseInt(influencer[key + "_followers"]) * impression_values[key]).toFixed(2);
                    }
                }
                list.push(influencer);
            });
            return list;
        };

        /**
         * Used to prepare impression of each influencers
         * @author karthick.k
         * @returns {array} list of influencers with impression value
         */
        var impressions_by_influencers = function () {
            var list = [];
            var posts_by_influencers = _(posts).groupBy("influencer_id").value();
            var influencers_list = angular.copy(get_influencers_list());
            influencers_list.forEach(function (influencer) {
                var post_by_media = _(posts_by_influencers[influencer.influencer_id]).groupBy("media").value();
                for (var key in post_by_media) {
                    var count = 0;
                    post_by_media[key].forEach(function (post) {
                        count += parseInt(post.followed_by);
                    });
                    influencer[key] = (count * impression_values[key]).toFixed(2);
                }
                list.push(influencer);
            });
            return list;
        };

        /**
         * Used to prepare interactions/media value value based on influencers
         * @author karthick.k
         * @param   {boolean} media_value media value is calculated based on the calling function
         * @returns {array} interaction/media value based on the influencers
         */
        var interactions_by_influencers = function (media_value) {
            var list = [];
            var posts_by_influencers = _(posts).groupBy("influencer_id").value();
            var influencers_list = angular.copy(get_influencers_list());
            influencers_list.forEach(function (influencer) {
                var post_by_media = _(posts_by_influencers[influencer.influencer_id]).groupBy("media").value();
                for (var key in post_by_media) {
                    if (!!media_value) {
                        influencer[key] = (_.sum(post_by_media[key], 'interactions') * default_media_value).toFixed(2);
                    } else {
                        influencer[key] = _.sum(post_by_media[key], 'interactions').toFixed(2);
                    }
                }
                list.push(influencer);
            });
            return list;
        };

        /**
         * Used to prepare engagement value based on influencers
         * @author karthick.k
         * @returns {array} list of influencers with engagement value
         */
        var engagement_value_by_influencers = function () {
            var list = [];
            var posts_by_influencers = _(posts).groupBy("influencer_id").value();
            var influencers_list = angular.copy(get_influencers_list());
            influencers_list.forEach(function (influencer) {
                var post_by_media = _(posts_by_influencers[influencer.influencer_id]).groupBy("media").value();
                for (var key in post_by_media) {
                    influencer[key] = (((_.sum(post_by_media[key], 'interactions') / post_by_media[key].length) / parseInt(post_by_media[key][0].followed_by)) * 100).toFixed(2);
                }
                list.push(influencer);
            });
            return list;
        };

        /**
         * Used to prepare top potential influencers
         * @author karthick.k
         * @param   {string} legend reach, interactions, etc
         * @returns {array} array of top pontential influencer
         */
        $scope.get_top_potential_influencers = function (legend) {
            $scope.top_potential_infleuncers_filter = legend;
            $scope.top_potential_influencers_current_page = 0;
            var data = [];
            switch (legend) {
            case $scope.origin_of_posts_legends[0]:
                data = number_of_posts_by_influencers();
                break;
            case $scope.origin_of_posts_legends[1]:
                data = reach_by_influencers();
                break;
            case $scope.origin_of_posts_legends[2]:
                data = impressions_by_influencers();
                break;
            case $scope.origin_of_posts_legends[3]:
                data = interactions_by_influencers();
                break;
            case $scope.origin_of_posts_legends[4]:
                data = engagement_value_by_influencers();
                break;
            case $scope.origin_of_posts_legends[5]:
                data = interactions_by_influencers(true);
                break;
            default:
                data = [];
            }
            if (!!data.length) {
                data = _.sortBy(data, function (influencer) {

                    if (!influencer.facebook) {
                        influencer.facebook = 0;
                    }
                    if (!influencer.instagram) {
                        influencer.instagram = 0;
                    }
                    if (!influencer.youtube) {
                        influencer.youtube = 0;
                    }
                    return parseInt(influencer.facebook) + parseInt(influencer.instagram) + parseInt(influencer.youtube);
                }).reverse();
            }
            $scope.top_potential_influencers_size = data.length;
            $scope.potential_influencers = data;
            $scope.top_potential_influencers = angular.copy(data).slice(0, 10);
        };

        /**
         * Used to initialize top potential list
         * @author karthick.k
         */
        var initialize_top_potential_influencers_list = function () {
            $scope.get_top_potential_influencers($scope.origin_of_posts_legends[0]);
            $scope.top_potential_infleuncers_filter = $scope.origin_of_posts_legends[0];
        }();

        $scope.next_page = function (current_page, items_per_page) {
            $scope.top_potential_influencers_current_page = current_page - 1;
            var data = angular.copy($scope.potential_influencers);
            $scope.top_potential_influencers = data.slice((current_page - 1) * items_per_page, ((current_page - 1) * items_per_page) + items_per_page);
        };
        /********************************** End of top potential influencers *************************/
        /********************************** Feed/ Post section *************************************/
        $scope.networks = {
            facebook: true,
            instagram: true,
            youtube: true
        };
        $scope.feed_filter = ["Reach", "Interactions", "Engagement rate", "Date"];

        /**
         * Used to order the post based on the filter
         * @author karthick.k
         * @param   {string} legend      selected order
         * @param   {array} feeds       posts
         * @param   {boolean} filter_done Cascade filter
         * @returns {array} Sorted array based on the order selection
         */
        $scope.post_order_by = function (legend, feeds, filter_done) {
            if ($scope.post_filter_legend !== legend || !!filter_done) {
                $scope.post_filter_legend = legend;
                var data = !!filter_done ? feeds : angular.copy($scope.filtered_posts);
                switch (legend) {
                case $scope.feed_filter[0]:
                    data = _.sortBy(data, function (post) {
                        return parseInt((post.followed_by * impression_values[post.media]).toFixed(0));
                    });
                    break;
                case $scope.feed_filter[1]:
                    data = _.sortBy(data, function (post) {
                        return parseInt(post.interactions);
                    });
                    break;
                case $scope.feed_filter[2]:
                    data = _.sortBy(data, function (post) {
                        return ((post.interactions / post.followed_by) * 100);
                    });
                    break;
                case $scope.feed_filter[3]:
                    data = _.sortBy(data, function (post) {
                        return parseInt(post.created_time);
                    });
                    break;
                default:
                    data = [];
                }
                data.forEach(function (post) {
                    post.reach = (post.followed_by * impression_values[post.media]).toFixed(2);
                });
                $scope.filtered_posts = data.reverse();
            }
        };

        /**
         * Used to filter the posts based on the media selection
         * @author karthick.k
         * @param   {networks/media} networks selected media
         * @returns {array} list of post sorted based the interaction and media selection
         */
        $scope.filter_feed = function (networks) {
            var posts = angular.copy(posts);
            var list = [];
            for (var media in networks) {
                if (!!networks[media]) {
                    list = _.union(list, $scope.posts_by_media[media]);
                }
            }
            $scope.post_order_by($scope.post_filter_legend, list, true);
        };

        //Initializing the posts
        $scope.post_order_by($scope.feed_filter[0], posts, true);

        /********************************** End of feed/ post section ********************************/

        /********************************** Start of country map *************************************/
        $scope.post_limit = 99;
        //Legend for origin of posts chart
        $scope.country_map_legends = ["Number of posts", "Reach", "Impressions", "Interactions", "Engagement rate", "Media value"];
        $scope.country_map_legend_filter = $scope.country_map_legends[0];

        /**
         * Used to prepare reach based on country
         * @author karthick.k
         * @returns {array} list of country wise reach
         */
        var reach_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var posts_by_media = _(posts_by_country[country]).groupBy("media").value();
                var followers = 0;
                for (var media in posts_by_media) {
                    var follower = 0;
                    if (media === "youtube") {
                        follower = _.sum(posts_by_media[media], 'followed_by');
                    } else {
                        var post_by_influencer = _(posts_by_media[media]).groupBy("influencer_id").value();
                        for (var influencer in post_by_influencer) {
                            follower += parseInt(post_by_influencer[influencer][0].followed_by * impression_values[media]);
                        }
                    }
                    followers += follower;
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: followers.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare impression based on country
         * @author karthick.k
         * @returns {array} list of country with impression value
         */
        var impressions_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var posts_by_media = _(posts_by_country[country]).groupBy("media").value();
                var impressions = 0;
                for (var media in posts_by_media) {
                    var impression = 0;
                    posts_by_media[media].forEach(function (post) {
                        impression += parseInt(post.followed_by) * impression_values[media];
                    });
                    impressions += impression;
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: impressions.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare interactions/media value value based on country
         * @author karthick.k
         * @param   {boolean} media_value media value is calculated based on the calling function
         * @returns {array} interaction/media value based on the country
         */
        var interactions_by_country = function (media_value) {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var interactions = 0;
                if (!!media_value) {
                    interactions += _.sum(posts_by_country[country], 'interactions') * default_media_value;
                } else {
                    interactions += _.sum(posts_by_country[country], 'interactions');
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: interactions.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare engagement value based on country
         * @author karthick.k
         * @returns {array} list of country with engagement value
         */
        var engagement_value_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var posts_by_media = _(posts_by_country[country]).groupBy("media").value();
                var engagement = 0;
                for (var media in posts_by_media) {
                    if (media === "youtube") {
                        engagement += (((_.sum(posts_by_media[media], 'interactions') / posts_by_media[media].length) / _.sum(posts_by_media[media], 'followed_by')) * 100);
                    } else {
                        var post_by_influencer = _(posts_by_media[media]).groupBy("influencer_id").value();
                        var total_followers = 0;
                        for (var influencer in post_by_influencer) {
                            total_followers += parseInt(posts_by_media[media][0].followed_by);
                        }
                        engagement += (((_.sum(posts_by_media[media], 'interactions') / posts_by_media[media].length) / total_followers) * 100);
                    }
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: engagement.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare number of posts by country
         * @author karthick.k
         * @returns {array} list of country with posts numbers
         */
        var no_of_posts_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: posts_by_country[country].length
                });
            }
            return list;
        };

        //Country map configuration
        $scope.country_map_config = {
            options: {
                legend: {
                    enabled: false
                },
                mapNavigation: {
                    enabled: true,
                    enableMouseWheelZoom: false
                },
                plotOptions: {
                    map: {
                        mapData: Highcharts.maps['custom/world'],
                        joinBy: ['hc-key']
                    },
                    states: {
                        hover: {
                            color: '#BADA55'
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.value}'
                    },
                    colorAxis: {
                        min: 0
                    }
                },
            },
            chartType: 'map',
            title: {
                text: ''
            }
        };

        /**
         * Used to prepare country map
         * @author karthick.k
         * @param   {string} legend reach, interactions, etc
         * @returns {array} array of country details with reach, interaction count
         */
        $scope.generate_country_map = function (legend) {
            $scope.country_map_legend_filter = legend;
            var data = [];
            switch (legend) {
            case $scope.country_map_legends[0]:
                data = no_of_posts_by_country();
                break;
            case $scope.country_map_legends[1]:
                data = reach_by_country();
                break;
            case $scope.country_map_legends[2]:
                data = impressions_by_country();
                break;
            case $scope.country_map_legends[3]:
                data = interactions_by_country();
                break;
            case $scope.country_map_legends[4]:
                data = engagement_value_by_country();
                break;
            case $scope.country_map_legends[5]:
                data = interactions_by_country(true);
                break;
            default:
                data = [];
            }
            $scope.country_map_config.series = [{
                data: data
            }];
            $scope.country_map_series = data;
        };

        //Initialize the country map
        var initialize_country_map = function () {
            $scope.country_map_legend_filter = $scope.country_map_legends[0];
            $scope.generate_country_map($scope.country_map_legends[0]);
        }();

        $scope.load_more = function () {
            $scope.post_limit += 9;
        };
        /********************************* End of country map ****************************************/

        /********************************* Export section ********************************************/

        /**
         * Used to group by post based on the infleuncer
         * @author karthick.k
         * @returns {array} list of posts based on the influencer
         */
        var get_post_based_influencer = function () {
            var list_of_post = [];
            var posts_by_influencers = _(posts).groupBy("influencer_id").value();
            for (var infleuncer_id in posts_by_influencers) {
                var infleuncer_posts = posts_by_influencers[infleuncer_id];
                infleuncer_posts.forEach(function (post) {
                    list_of_post.push({
                        "Influencer Name": post.inlfluencer_name,
                        "Country": post.country,
                        "Social Platform": post.media,
                        "Url of the Publication": post.link || post.thumbnail,
                        "Date of publication": new Date(post.created_time * 1000),
                        "Reach": (post.followed_by * impression_values[post.media]),
                        "Impression": (post.followed_by * impression_values[post.media]),
                        "Interactions": parseInt(post.interactions),
                        "Engagement Rate": (parseInt(post.interactions) / post.followed_by) * 100,
                        "Media value": parseInt(post.interactions) * default_media_value
                    });
                });
            }
            return list_of_post;
        };

        $scope.gridOptions = {
            exporterLinkLabel: 'get your csv here',
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            },
            data: get_post_based_influencer()
        };

        /**
         * Used to export the posts details
         * @author karthick.k
         */
        $scope.export = function () {
            var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
            $scope.gridApi.exporter.csvExport("all", "all", myElement);
        };


        /********************************* End of export section **************************************/
}]);