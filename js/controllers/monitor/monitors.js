app.controller('monitors', ['$scope', '$stateParams', '$location', '$rootScope', '$state', '$window', 'monitors_list', 'monitors_service',

    function ($scope, $stateParams, $location, $rootScope, $state, $window, monitors_list, monitors_service) {

        //Group by based on the monitor id, _ is lodash
        $scope.monitors_by_group = _(monitors_list).groupBy('group_name').value();
        $scope.is_main_page = ($stateParams.group_name !== undefined) ? false : true;
        if (!$scope.is_main_page) {
            var list = [];
            if ($stateParams.group_name === "Not grouped") {
                list = $scope.monitors_by_group[""] || $scope.monitors_by_group[undefined];
            } else {
                list = $scope.monitors_by_group[$stateParams.group_name];
            }
            $scope.monitors_list = _(list).groupBy('id').value();
            $scope.group_name = $stateParams.group_name;
        }


        /**
         * Used to fetch monitor list
         * @author karthick.k
         */
        var reload = function () {
            monitors_service.get_monitors_list().then(function (result) {
                if (result && result.data) {
                    $scope.monitors_list = _(result.data).groupBy('id').value();
                }
            });
        };

        /**
         * Used to delete monitor
         * @author karthick.k
         * @param {object} $event  event handler
         * @param {object} monitor Selected monitor to delete
         */
        $scope.delete_monitor = function ($event, monitor) {
            $event.preventDefault();
            var monitor_id = monitor[0].id;
            monitors_service.delete_monitor(monitor_id).then(function (result) {
                $location.url('/monitors/');
            }, function (result) {
                console.log(result.data);
            });
        };
}]);