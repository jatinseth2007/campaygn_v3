app.controller('new_monitor', ['$scope', '$filter', '$stateParams', 'validation', '$location', '$rootScope', '$state', '$window', 'monitors_service',

function ($scope, $filter, $stateParams, validation, $location, $rootScope, $state, $window, monitors_service) {


        //Monitor group list
        $scope.group_list = [];
        $scope.monitor = {
            group: ""
        };

        /**
         * Used to clear form messages, Message is based on server/ajax call
         * @author karthick.k
         */
        var clear_message = function () {
            $scope.success_message = "";
            $scope.error_message = "";
        };

        /**
         * Used to reset form validation and form fields
         * @author karthick.k
         * @param   {object} form form object
         */
        var reset_form = function (form) {
            $scope.monitor = null;
            form.$setUntouched();
        };

        /**
         * Used to create new monitor
         * @author karthick.k
         * @param {object} form object contains, monitor name, monitor_from date , monitor_to date, influencers 
         */
        $scope.create_monitor = function (form) {
            clear_message();
            if (form.individual_influencers.$valid || form.lists.$valid) {
                form.individual_influencers.$setValidity("uiSelectRequired", true);
                form.lists.$setValidity("uiSelectRequired", true);
            }
            if (!validation.is_valid(form)) {
                return;
            }
            var monitor = $scope.monitor;
            if (monitor.from) {
                monitor.from = $filter('date')(new Date(monitor.from), 'yyyy-MM-dd');
            } else {
                monitor.from = null;
            }
            if (monitor.to) {
                monitor.to = $filter('date')(new Date(monitor.to), 'yyyy-MM-dd');
            } else {
                monitor.to = null;
            }
            $scope.is_loading = true;
            reset_form(form);
            monitors_service.create_monitor(monitor).then(function (result) {
                $scope.is_loading = false;
                if (result.data && result.data.message) {
                    $scope.success_message = result.data.message;
                }
            }, function (result) {
                $scope.is_loading = false;
                $scope.error_message = result.data;
            });
        };

        /**
         * Used to search tags
         * @author karthick.k
         * @param {string} keyword search keyword for tag
         */
        $scope.search_tag = function (keyword) {
            if (!keyword) {
                $scope.tags = [];
                return;
            }
            monitors_service.search_tags(keyword).then(function (result) {
                $scope.tags = result.data;
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to search infuencers
         * @author karthick.k
         * @param {string} keyword search keyword for influencers
         */
        $scope.search_individual_influencers = function (keyword) {
            if (!keyword) {
                $scope.individual_influencers = [];
                return;
            }
            monitors_service.search_individual_influencers(keyword).then(function (result) {
                $scope.individual_influencers = result.data;
            }, function (error) {
                console.log(error);
            });
        };

        /**
         * Used to search users
         * @author karthick.k
         * @param {string} keyword, search keyword for users
         */
        $scope.search_users = function (keyword) {
            if (!keyword) {
                $scope.users = [];
                return;
            }
            monitors_service.search_users(keyword).then(function (result) {
                $scope.users = result.data;
            }, function (error) {
                console.log(error);
            });
        };


        /**
         * Used to search lists
         * @author karthick.k
         * @param {string} keyword, search keyword for lists
         */
        $scope.search_lists = function (keyword) {
            if (!keyword) {
                $scope.lists = [];
                return;
            }
            monitors_service.search_lists(keyword).then(function (result) {
                $scope.lists = result.data;
            }, function (error) {
                console.log(error);
            });
        };


        /**
         * Used to search monitor group
         * @author karthick.k
         * @param {string} keyword search keyword for monitor group
         */
        $scope.search_group = function (keyword) {
            if (!keyword) {
                $scope.group_list = [];
                return;
            }
            monitors_service.search_group(keyword).then(function (result) {
                if (result && result.data) {
                    var group_list = [];
                    result.data.forEach(function (group) {
                        group_list.push(group.group_name);
                    });
                    $scope.group_list = group_list;
                } else {
                    $scope.group_list = [];
                }
            }, function (error) {
                console.log(error);
            });
        };

}]);