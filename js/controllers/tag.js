/*jshint loopfunc: true */

app.controller('tag', ['$scope', '$stateParams', '$http', '$filter', 'tag_service', 'posts',

    function ($scope, $stateParams, $http, $filter, tag_service, posts) {

        /************************************ common methods **************************************/
        var default_value = {
            facebook: {
                count: 0,
                percentage: 0
            },
            instagram: {
                count: 0,
                percentage: 0
            },
            youtube: {
                count: 0,
                percentage: 0
            }
        };
        $scope.impression_values = {
            facebook: 0.008,
            instagram: 0.20,
            youtube: 1
        };
        var media_color = {
            instagram: "#956D53",
            facebook: "#39599F",
            youtube: "#b03330"
        };
        var default_media_value = 0.74;

        /**
         * Used to get unique posts and combine the tags
         * @author karthick.k
         * @param   {array} posts list of posts
         * @returns {array} unique posts list
         */
        var get_unique_posts = function () {
            var result = [];
            _.chain(posts)
                .groupBy("post_id")
                .pairs()
                .map(function (item) {
                    return _.object(_.zip(["post_id", "fields"], item));
                })
                .value()
                .forEach(function (post) {
                    var tags = [];
                    post.fields.forEach(function (field) {
                        if (field.tag_type === "hash") {
                            tags.push("#" + field.tag_name);
                        } else if (field.tag_type === "name") {
                            tags.push("@" + field.tag_name);
                        }
                    });
                    var obj = post.fields[0];
                    obj.tags = tags.toString();
                    result.push(obj);
                });
            return result;
        };

        $scope.posts = get_unique_posts();
        $scope.tag_name = $scope.posts[0].tag_name;
        $scope.filter_legends = ["Number of posts", "Reach", "Impressions", "Interactions", "Engagement rate", "Media value"];


        /**
         * Used to get followers media wise
         * @author karthick.k
         * @returns {object} media wise folowers
         */
        var get_followers_by_media = function () {
            var followers_by_media = angular.copy(default_value);
            var total = 0;
            var post_by_media = _(posts).groupBy("media").value();
            for (var media in post_by_media) {
                var count = 0;
                if (media === "youtube") {
                    count = _.sum(post_by_media[media], 'followed_by');
                } else {
                    var post_by_influencer = _(post_by_media[media]).groupBy("profile_id").value();
                    for (var influencer in post_by_influencer) {
                        count += parseInt(post_by_influencer[influencer][0].followed_by);
                    }
                }
                followers_by_media[media].count = count;
                total += count;
            }
            for (var key in followers_by_media) {
                followers_by_media[key].percentage = ((followers_by_media[key].count / total) * 100);
            }
            followers_by_media.total = total;
            return followers_by_media;
        };
        var followers_by_media = get_followers_by_media();

        /**
         * Used to get no of posts by media wise
         * @author karthick.k
         * @returns {object} media wise no of posts
         */
        var get_post_count_by_media = function () {
            var post_by_media = angular.copy(default_value);
            var posts = angular.copy($scope.posts);
            var total = 0;
            var posts_by_media = _(posts).groupBy("media").value();
            if (posts.length === 0) {
                post_by_media.total = 0;
                return post_by_media;
            }
            for (var key in posts_by_media) {
                var count = posts_by_media[key].length || 0;
                post_by_media[key].count = count;
                total += count;
            }
            for (var media in post_by_media) {
                post_by_media[media].percentage = ((post_by_media[media].count / total) * 100);
            }
            post_by_media.total = total;
            return post_by_media;
        };
        var post_count_by_media = get_post_count_by_media();
        $scope.posts_by_media = _(posts).groupBy("media").value();

        /**
         * Used to get reach by media wise
         * @author karthick.k
         * @returns {object} media wise reach
         */
        var get_reach_by_media = function () {
            var reach_by_media = angular.copy(default_value);
            var posts = angular.copy($scope.posts);
            if (posts.length === 0) {
                reach_by_media.total = 0;
                return reach_by_media;
            }
            var total = 0;
            var posts_by_media = _(posts).groupBy("media").value();
            for (var key in posts_by_media) {
                var count = parseInt(followers_by_media[key].count) * $scope.impression_values[key];
                reach_by_media[key].count = count;
                total += count;
            }
            for (var media in reach_by_media) {
                reach_by_media[media].percentage = ((reach_by_media[media].count / total) * 100);
            }
            reach_by_media.total = total;
            return reach_by_media;
        };
        var reach_by_media = get_reach_by_media();

        /**
         * Used to get impression by media wise
         * @author karthick.k
         * @returns {object} media wise impressions
         */
        var get_impressions_by_media = function () {
            var posts = angular.copy($scope.posts);
            var posts_by_media = _(posts).groupBy("media").value();
            var impressions_by_media = angular.copy(default_value);
            var total = 0;
            for (var key in posts_by_media) {
                var count = 0;
                posts_by_media[key].forEach(function (post) {
                    count += parseInt(post.followed_by) * $scope.impression_values[key];
                });
                impressions_by_media[key].count = count;
                total += count;
            }
            for (var media in impressions_by_media) {
                impressions_by_media[media].percentage = ((impressions_by_media[media].count / total) * 100);
            }
            impressions_by_media.total = total;
            return impressions_by_media;
        };
        var impressions_by_media = get_impressions_by_media();

        /**
         * Used to get interactions based on the media
         * @author karthick.k
         * @returns {object} media wise interactions
         */
        var get_interactions_by_media = function () {
            var posts = angular.copy($scope.posts);
            var posts_by_media = _(posts).groupBy("media").value();
            var interactions_by_media = angular.copy(default_value);
            var total = 0;
            for (var key in posts_by_media) {
                interactions_by_media[key].count = _.sum(posts_by_media[key], "interactions");
                total += interactions_by_media[key].count;
            }
            for (var media in interactions_by_media) {
                interactions_by_media[media].percentage = ((interactions_by_media[media].count / total) * 100);
            }
            interactions_by_media.total = total;
            return interactions_by_media;
        };
        var interactions_by_media = get_interactions_by_media();

        /**
         * Used to get media value
         * @author karthick.k
         * @returns {object} media wise media value
         */
        var get_media_value_by_media = function () {
            var posts = angular.copy($scope.posts);
            var posts_by_media = _(posts).groupBy("media").value();
            var media_value_by_media = angular.copy(default_value);
            var total = 0;
            for (var key in posts_by_media) {
                var count = (interactions_by_media[key].count * default_media_value);
                media_value_by_media[key].count = count;
                total += count;
            }
            for (var media in media_value_by_media) {
                media_value_by_media[media].percentage = ((media_value_by_media[media].count / total) * 100);
            }
            media_value_by_media.total = total;
            return media_value_by_media;
        };
        var media_value_by_media = get_media_value_by_media();

        /**
         * Used to get media value
         * @author karthick.k
         * @returns {object} media wise media value
         */
        var get_engagement_rate_by_media = function () {
            var posts = angular.copy($scope.posts);
            var posts_by_media = _(posts).groupBy("media").value();
            var engagement_rate_by_media = angular.copy(default_value);
            var total = 0;
            for (var key in posts_by_media) {
                if (!!interactions_by_media[key] && !!followers_by_media[key]) {
                    var count = ((interactions_by_media[key].count / posts_by_media[key].length) / followers_by_media[key].count) * 100;
                    engagement_rate_by_media[key].count = count;
                    total += count;
                }
            }
            for (var media in engagement_rate_by_media) {
                engagement_rate_by_media[media].percentage = ((engagement_rate_by_media[media].count / total) * 100);
            }
            engagement_rate_by_media.total = total;
            return engagement_rate_by_media;
        };
        var engagement_rate_by_media = get_engagement_rate_by_media();
        /*************************************** End of common methods ***********************************/
        /**************************** posts count, reach, interactions, etc *******/
        $scope.interactions = interactions_by_media.total || 0;
        $scope.reach = Math.round(reach_by_media.total);
        $scope.impressions = Math.round(impressions_by_media.total);
        $scope.followers = followers_by_media.total;
        $scope.engagement_rate = engagement_rate_by_media.total.toPrecision(2);
        $scope.media_value = Math.round(media_value_by_media.total);
        $scope.no_of_influencers_used_tags = Object.keys(_($scope.posts).groupBy("profile_id").value()).length;
        /*********************** End of posts count section *****************************************/
        /******************************** Metrics by day *****************************************/
        //Configuration for metrics by day option
        var metrics_by_day_chart_options = {
            "chart": {
                "height": 325,
                "zoomType": "x"
            },
            "title": {
                "text": ""
            },
            "tooltip": {
                "shared": true,
                "borderColor": "#CCC",
                "useHTML": true,
                pointFormat: '{series.name}: <b>{point.y:.0f}</b><br/>'
            },
            "plotOptions": {
                series: {
                    pointWidth: 2
                }
            },
            "xAxis": {
                "id": "days-axis",
                "type": "datetime",
                dateTimeLabelFormats: {
                    month: '%Y',
                    year: '%Y'
                },
                "minRange": 345600000
            },
            "yAxis": [
                {
                    "id": "y-axis",
                    "height": 200,
                    "top": 50,
                    "lineWidth": 2,
                    "offset": 0
                }
            ]
        };
        $scope.metrics_by_day_config = {
            options: metrics_by_day_chart_options
        };

        /**
         * Used to prepare date for daily post statistics
         * @author karthick.k
         * @returns {array} data grouped based on created time based on instagram, facebook, youtube
         */
        var get_daily_posts_stats = function () {
            var aggregated_posts = angular.copy($scope.posts);
            var posts_by_time = _(aggregated_posts).groupBy("post_created_time").value();
            var data = {};
            for (var key in $scope.impression_values) {
                data[key + "_posts"] = [];
                data[key + "_reach"] = [];
                data[key + "_impressions"] = [];
                data[key + "_interactions"] = [];
                data[key + "_engagement_rate"] = [];
                data[key + "_media_value"] = [];
            }
            for (var time in posts_by_time) {
                var posts = posts_by_time[time];
                var post_by_media = _(posts_by_time[time]).groupBy("media").value();
                var count = {};
                for (var media_name in $scope.impression_values) {
                    count[media_name + "_posts_count"] = post_by_media[media_name] ? post_by_media[media_name].length : 0;
                    count[media_name + "_post_reach"] = 0;
                    count[media_name + "_post_impressions"] = 0;
                    count[media_name + "_post_interactions"] = 0;
                    count[media_name + "_post_engagement_rate"] = 0;
                    count[media_name + "_post_media_value"] = 0;
                }
                posts.forEach(function (post) {
                    count[post.media + "_post_reach"] = count[post.media + "_post_reach"] + (parseInt(post.followed_by) * $scope.impression_values[post.media]);
                    count[post.media + "_post_impressions"] = count[post.media + "_post_impressions"] + (parseInt(post.followed_by) * $scope.impression_values[post.media]);
                    count[post.media + "_post_interactions"] = count[post.media + "_post_interactions"] + parseInt(post.interactions);
                    count[post.media + "_post_engagement_rate"] = count[post.media + "_post_engagement_rate"] + ((parseInt(post.interactions) / parseInt(post.followed_by)) * 100);
                    count[post.media + "_post_media_value"] = count[post.media + "_post_media_value"] + (parseInt(post.interactions) * default_media_value);
                });
                for (var media in $scope.impression_values) {
                    data[media + "_posts"].push({
                        y: count[media + "_posts_count"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_reach"].push({
                        y: count[media + "_post_reach"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_impressions"].push({
                        y: count[media + "_post_impressions"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_interactions"].push({
                        y: count[media + "_post_interactions"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_engagement_rate"].push({
                        y: count[media + "_post_engagement_rate"],
                        x: parseInt(time) * 1000
                    });
                    data[media + "_media_value"].push({
                        y: count[media + "_post_media_value"],
                        x: parseInt(time) * 1000
                    });
                }
            }
            return data;
        };
        $scope.daily_posts_stats = get_daily_posts_stats();

        /**
         * Used to generate metrics by day chart based on the legend, reach, impression, interactons, etc
         * @author karthick.k
         * @param {string} legend selected legend
         */
        $scope.get_metrics_by_day = function (legend) {
            $scope.metrics_by_day_filter = legend;
            var filter = null;
            switch (legend) {
            case $scope.filter_legends[0]:
                filter = "_posts";
                break;
            case $scope.filter_legends[1]:
                filter = "_reach";
                break;
            case $scope.filter_legends[2]:
                filter = "_impressions";
                break;
            case $scope.filter_legends[3]:
                filter = "_interactions";
                break;
            case $scope.filter_legends[4]:
                filter = "_engagement_rate";
                break;
            case $scope.filter_legends[5]:
                filter = "_media_value";
                break;
            default:
                filter = "_posts";
            }
            var stats = [];
            for (var value in $scope.impression_values) {
                stats.push({
                    "id": legend + value,
                    "serieType": legend,
                    "name": value,
                    "yAxis": "y-axis",
                    "type": "bar",
                    "color": media_color[value],
                    data: $scope.daily_posts_stats[value + filter],
                    turboThreshold: 20000
                });
            }
            $scope.metrics_by_day_config.series = stats;
        };

        //Initialize the metrics by day chart
        var initialize_metrics_by_day = function () {
            $scope.metrics_by_day_filter = $scope.filter_legends[0];
            $scope.get_metrics_by_day($scope.filter_legends[0]);
        }();

        /***************************** End of metrics by day section **********************************/
        /********************************** Start of country map *************************************/

        $scope.country_map_legend_filter = $scope.filter_legends[0];

        /**
         * Used to prepare reach based on country
         * @author karthick.k
         * @returns {array} list of country wise reach
         */
        var reach_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var posts_by_media = _(posts_by_country[country]).groupBy("media").value();
                var followers = 0;
                for (var media in posts_by_media) {
                    var follower = 0;
                    if (media === "youtube") {
                        follower = _.sum(posts_by_media[media], 'followed_by');
                    } else {
                        var post_by_influencer = _(posts_by_media[media]).groupBy("profile_id").value();
                        for (var influencer in post_by_influencer) {
                            follower += parseInt(post_by_influencer[influencer][0].followed_by * $scope.impression_values[media]);
                        }
                    }
                    followers += follower;
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: followers.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare impression based on country
         * @author karthick.k
         * @returns {array} list of country with impression value
         */
        var impressions_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var posts_by_media = _(posts_by_country[country]).groupBy("media").value();
                var impressions = 0;
                for (var media in posts_by_media) {
                    var impression = 0;
                    posts_by_media[media].forEach(function (post) {
                        impression += parseInt(post.followed_by) * $scope.impression_values[media];
                    });
                    impressions += impression;
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: impressions.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare interactions/media value value based on country
         * @author karthick.k
         * @param   {boolean} media_value media value is calculated based on the calling function
         * @returns {array} interaction/media value based on the country
         */
        var interactions_by_country = function (media_value) {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var interactions = 0;
                if (!!media_value) {
                    interactions += _.sum(posts_by_country[country], 'interactions') * default_media_value;
                } else {
                    interactions += _.sum(posts_by_country[country], 'interactions');
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: interactions.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare engagement value based on country
         * @author karthick.k
         * @returns {array} list of country with engagement value
         */
        var engagement_value_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                var posts_by_media = _(posts_by_country[country]).groupBy("media").value();
                var engagement = 0;
                for (var media in posts_by_media) {
                    if (media === "youtube") {
                        engagement += (((_.sum(posts_by_media[media], 'interactions') / posts_by_media[media].length) / _.sum(posts_by_media[media], 'followed_by')) * 100);
                    } else {
                        var post_by_influencer = _(posts_by_media[media]).groupBy("profile_id").value();
                        var total_followers = 0;
                        for (var influencer in post_by_influencer) {
                            total_followers += parseInt(posts_by_media[media][0].followed_by);
                        }
                        engagement += (((_.sum(posts_by_media[media], 'interactions') / posts_by_media[media].length) / total_followers) * 100);
                    }
                }
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: engagement.toFixed(2)
                });
            }
            return list;
        };

        /**
         * Used to prepare number of posts by country
         * @author karthick.k
         * @returns {array} list of country with posts numbers
         */
        var no_of_posts_by_country = function () {
            var list = [];
            var posts_by_country = _(posts).groupBy("country").value();
            for (var country in posts_by_country) {
                list.push({
                    "hc-key": country.toLowerCase(),
                    value: posts_by_country[country].length
                });
            }
            return list;
        };

        //Country map configuration
        $scope.country_map_config = {
            options: {
                legend: {
                    enabled: false
                },
                mapNavigation: {
                    enabled: true,
                    enableMouseWheelZoom: false
                },
                plotOptions: {
                    map: {
                        mapData: Highcharts.maps['custom/world'],
                        joinBy: ['hc-key']
                    },
                    states: {
                        hover: {
                            color: '#BADA55'
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        format: '{point.value}'
                    },
                    colorAxis: {
                        min: 0
                    }
                },
            },
            chartType: 'map',
            title: {
                text: ''
            }
        };

        /**
         * Used to prepare country map
         * @author karthick.k
         * @param   {string} legend reach, interactions, etc
         * @returns {array} array of country details with reach, interaction count
         */
        $scope.generate_country_map = function (legend) {
            $scope.country_map_legend_filter = legend;
            var data = [];
            switch (legend) {
            case $scope.filter_legends[0]:
                data = no_of_posts_by_country();
                break;
            case $scope.filter_legends[1]:
                data = reach_by_country();
                break;
            case $scope.filter_legends[2]:
                data = impressions_by_country();
                break;
            case $scope.filter_legends[3]:
                data = interactions_by_country();
                break;
            case $scope.filter_legends[4]:
                data = engagement_value_by_country();
                break;
            case $scope.filter_legends[5]:
                data = interactions_by_country(true);
                break;
            default:
                data = [];
            }
            $scope.country_map_config.series = [{
                data: data
            }];
            $scope.country_map_series = data;
        };

        //Initialize the country map
        var initialize_country_map = function () {
            $scope.country_map_legend_filter = $scope.filter_legends[0];
            $scope.generate_country_map($scope.filter_legends[0]);
        }();
        /********************************* End of country map ****************************************/

        /*********************************** origin of post section ************************************/
        //Pie chart configuration for origin of posts
        $scope.origin_of_posts_config = {
            options: {
                chart: {
                    height: 350,
                    marginRight: 120,
                    type: 'pie'
                },
                plotOptions: {
                    pie: {
                        size: 300,
                        shadow: false,
                        dataLabels: {
                            enabled: false
                        },
                        point: {
                            events: {}
                        }
                    },
                    series: {
                        stacking: 'normal',
                        animation: true,
                        showInLegend: true
                    }
                },
                legend: {
                    align: 'right',
                    layout: 'vertical',
                    verticalAlign: 'middle',
                    itemMarginTop: 5,
                    itemMarginBottom: 5,
                    itemStyle: {
                        fontSize: '17px',
                        fontWeight: 'normal',
                        verticalAlign: 'top'
                    },
                    x: -50,
                    useHTML: true,
                    symbolWidth: 0,
                    labelFormatter: function () {
                        var legend;
                        legend = "<span data-network='" + this.network + "'> " + this.icon + " " + this.legend + " </span>";
                        if (this.legend !== null) {
                            return legend;
                        } else {
                            return null;
                        }
                    }
                }
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            }
        };
        /**
         * Used to prepare statistics for pie chart origin by post
         * @author karthick.k
         * @returns {array} [[Description]]
         */
        var get_origin_of_posts_stats = function (data, name) {
            var stats = [
                {
                    "id": "origin_by_posts",
                    "name": name,
                    "data": [
                        {
                            "name": "<b>facebook</b>",
                            "y": data.facebook.count,
                            "color": "#39599f",
                            "icon": "<i class='fa fa-facebook-square fa-2 facebook-color'></i>",
                            "legend": data.facebook.percentage.toFixed(2) + "%"
                        },
                        {
                            "name": "<b>instagram</b>",
                            "y": data.instagram.count,
                            "color": "#AE866C",
                            "icon": "<i class='fa fa-instagram fa-2 instagram-color'></i>",
                            "legend": data.instagram.percentage.toFixed(2) + "%"
                        },
                        {
                            "name": "<b>youtube</b>",
                            "y": data.youtube.count,
                            "color": "#B03330",
                            "icon": "<i class='fa fa-youtube fa-2 youtube-color'></i>",
                            "legend": data.youtube.percentage.toFixed(2) + "%"
                        },
                    ]
                  }
                ];
            return stats;
        };

        /**
         * Used to generate pie chart based on the legend, reach, impression, interactons, etc
         * @author karthick.k
         */
        $scope.generate_origin_of_posts_chart = function (legend) {
            $scope.origin_of_posts_filter = legend;
            var data = null;
            switch (legend) {
            case $scope.filter_legends[0]:
                data = post_count_by_media;
                break;
            case $scope.filter_legends[1]:
                data = reach_by_media;
                break;
            case $scope.filter_legends[2]:
                data = impressions_by_media;
                break;
            case $scope.filter_legends[3]:
                data = interactions_by_media;
                break;
            case $scope.filter_legends[4]:
                data = engagement_rate_by_media;
                break;
            case $scope.filter_legends[5]:
                data = media_value_by_media;
                break;
            default:
                data = null;
            }
            $scope.origin_of_posts_config.series = !!data ? get_origin_of_posts_stats(data, legend) : [];
        };
        //Initialize the chart
        var initialize_origin_of_posts_chart = function () {
            $scope.origin_of_posts_filter = $scope.filter_legends[0];
            $scope.generate_origin_of_posts_chart($scope.filter_legends[0]);
        }();


        /********************************* End of origin of posts **************************************/

        $scope.top_potential_influencers_current_page = 0;
        /**
         * Used to prepare number of posts of each influencers
         * @author karthick.k
         * @returns {array} list of influencers with number of posts
         */
        var number_of_posts_by_influencers = function () {
            var list = [];
            var posts = angular.copy($scope.posts);
            var posts_by_influencers = _(posts).groupBy("profile_id").value();
            for (var influencer in posts_by_influencers) {
                var post_by_media = _(posts_by_influencers[influencer]).groupBy("media").value();
                var profile = {};
                profile.profile_picture = posts_by_influencers[influencer][0].user_profile_picture;
                profile.profile_name = posts_by_influencers[influencer][0].profile_name;
                profile.profile_slug = posts_by_influencers[influencer][0].profile_slug;
                for (var key in post_by_media) {
                    profile[key] = post_by_media[key].length || 0;
                }
                list.push(profile);
            }
            return list;
        };

        /**
         * Used to prepare reach of each influencers
         * @author karthick.k
         * @returns {array} list of influencers with reach
         */
        var reach_by_influencers = function () {
            var list = [];
            var posts = angular.copy($scope.posts);
            var posts_by_influencers = _(posts).groupBy("profile_id").value();
            for (var influencer in posts_by_influencers) {
                var post_by_media = _(posts_by_influencers[influencer]).groupBy("media").value();
                var profile = {};
                profile.profile_picture = posts_by_influencers[influencer][0].user_profile_picture;
                profile.profile_name = posts_by_influencers[influencer][0].profile_name;
                profile.profile_slug = posts_by_influencers[influencer][0].profile_slug;
                for (var key in post_by_media) {
                    if (key === "youtube") {
                        profile[key] = _.sum(post_by_media[key], 'followed_by').toFixed(2);
                    } else {
                        profile[key] = (parseInt(posts_by_influencers[influencer][0].followed_by) * $scope.impression_values[key]).toFixed(2);
                    }
                }
                list.push(profile);
            }
            return list;
        };

        /**
         * Used to prepare impression of each influencers
         * @author karthick.k
         * @returns {array} list of influencers with impression value
         */
        var impressions_by_influencers = function () {
            var list = [];
            var posts = angular.copy($scope.posts);
            var posts_by_influencers = _(posts).groupBy("profile_id").value();
            for (var influencer in posts_by_influencers) {
                var post_by_media = _(posts_by_influencers[influencer]).groupBy("media").value();
                var profile = {};
                profile.profile_picture = posts_by_influencers[influencer][0].user_profile_picture;
                profile.profile_name = posts_by_influencers[influencer][0].profile_name;
                profile.profile_slug = posts_by_influencers[influencer][0].profile_slug;
                for (var key in post_by_media) {
                    var count = _.sum(post_by_media[key], 'followed_by');
                    profile[key] = (count * $scope.impression_values[key]).toFixed(2);
                }
                list.push(profile);
            }
            return list;
        };

        /**
         * Used to prepare interactions/media value value based on influencers
         * @author karthick.k
         * @param   {boolean} media_value media value is calculated based on the calling function
         * @returns {array} interaction/media value based on the influencers
         */
        var interactions_by_influencers = function (media_value) {
            var list = [];
            var posts = angular.copy($scope.posts);
            var posts_by_influencers = _(posts).groupBy("profile_id").value();
            for (var influencer in posts_by_influencers) {
                var post_by_media = _(posts_by_influencers[influencer]).groupBy("media").value();
                var profile = {};
                profile.profile_picture = posts_by_influencers[influencer][0].user_profile_picture;
                profile.profile_name = posts_by_influencers[influencer][0].profile_name;
                profile.profile_slug = posts_by_influencers[influencer][0].profile_slug;
                for (var key in post_by_media) {
                    if (!!media_value) {
                        profile[key] = (_.sum(post_by_media[key], 'interactions') * default_media_value).toFixed(2);
                    } else {
                        profile[key] = _.sum(post_by_media[key], 'interactions').toFixed(2);
                    }
                }
                list.push(profile);
            }
            return list;
        };

        /**
         * Used to prepare engagement value based on influencers
         * @author karthick.k
         * @returns {array} list of influencers with engagement value
         */
        var engagement_value_by_influencers = function () {
            var list = [];
            var posts = angular.copy($scope.posts);
            var posts_by_influencers = _(posts).groupBy("profile_id").value();
            for (var influencer in posts_by_influencers) {
                var post_by_media = _(posts_by_influencers[influencer]).groupBy("media").value();
                var profile = {};
                profile.profile_picture = posts_by_influencers[influencer][0].user_profile_picture;
                profile.profile_name = posts_by_influencers[influencer][0].profile_name;
                profile.profile_slug = posts_by_influencers[influencer][0].profile_slug;
                for (var key in post_by_media) {
                    profile[key] = (((_.sum(post_by_media[key], 'interactions') / post_by_media[key].length) / parseInt(post_by_media[key][0].followed_by)) * 100).toFixed(2);
                }
                list.push(profile);
            }
            return list;
        };

        /**
         * Used to prepare top potential influencers
         * @author karthick.k
         * @param   {string} legend reach, interactions, etc
         * @returns {array} array of top pontential influencer
         */
        $scope.get_top_potential_influencers = function (legend) {
            $scope.top_potential_infleuncers_filter = legend;
            $scope.top_potential_influencers_current_page = 0;
            var data = [];
            switch (legend) {
            case $scope.filter_legends[0]:
                data = number_of_posts_by_influencers();
                break;
            case $scope.filter_legends[1]:
                data = reach_by_influencers();
                break;
            case $scope.filter_legends[2]:
                data = impressions_by_influencers();
                break;
            case $scope.filter_legends[3]:
                data = interactions_by_influencers();
                break;
            case $scope.filter_legends[4]:
                data = engagement_value_by_influencers();
                break;
            case $scope.filter_legends[5]:
                data = interactions_by_influencers(true);
                break;
            default:
                data = [];
            }
            if (!!data.length) {
                data = _.sortBy(data, function (influencer) {

                    if (!influencer.facebook) {
                        influencer.facebook = 0;
                    }
                    if (!influencer.instagram) {
                        influencer.instagram = 0;
                    }
                    if (!influencer.youtube) {
                        influencer.youtube = 0;
                    }
                    return parseInt(influencer.facebook) + parseInt(influencer.instagram) + parseInt(influencer.youtube);
                }).reverse();
            }
            $scope.top_potential_influencers_size = data.length;
            $scope.potential_influencers = data;
            $scope.top_potential_influencers = angular.copy(data).slice(0, 10);
        };

        /**
         * Used to initialize top potential list
         * @author karthick.k
         */
        var initialize_top_potential_influencers_list = function () {
            $scope.get_top_potential_influencers($scope.filter_legends[0]);
            $scope.top_potential_infleuncers_filter = $scope.filter_legends[0];
        }();

        $scope.next_page = function (current_page, items_per_page) {
            $scope.top_potential_influencers_current_page = current_page - 1;
            var data = angular.copy($scope.potential_influencers);
            $scope.top_potential_influencers = data.slice((current_page - 1) * items_per_page, ((current_page - 1) * items_per_page) + items_per_page);
        };
        /********************************** End of top potential influencers *************************/
        /********************************** Feed/ Post section *************************************/
        $scope.post_limit = 99;
        $scope.networks = {
            facebook: true,
            instagram: true,
            youtube: true
        };
        $scope.feed_filter = ["Reach", "Interactions", "Engagement rate", "Date"];

        /**
         * Used to order the post based on the filter
         * @author karthick.k
         * @param   {string} legend      selected order
         * @param   {array} feeds       posts
         * @param   {boolean} filter_done Cascade filter
         * @returns {array} Sorted array based on the order selection
         */
        $scope.post_order_by = function (legend, feeds, filter_done) {
            if ($scope.post_filter_legend !== legend || !!filter_done) {
                $scope.post_filter_legend = legend;
                var data = !!filter_done ? feeds : angular.copy($scope.filtered_posts);
                switch (legend) {
                case $scope.feed_filter[0]:
                    data = _.sortBy(data, function (post) {
                        return parseInt((post.followed_by * $scope.impression_values[post.media]).toFixed(0));
                    });
                    break;
                case $scope.feed_filter[1]:
                    data = _.sortBy(data, function (post) {
                        return parseInt(post.interactions);
                    });
                    break;
                case $scope.feed_filter[2]:
                    data = _.sortBy(data, function (post) {
                        return ((post.interactions / post.followed_by) * 100);
                    });
                    break;
                case $scope.feed_filter[3]:
                    data = _.sortBy(data, function (post) {
                        return parseInt(post.post_created_time);
                    });
                    break;
                default:
                    data = [];
                }
                data.forEach(function (post) {
                    post.reach = (post.followed_by * $scope.impression_values[post.media]).toFixed(2);
                });
                $scope.filtered_posts = data.reverse();
            }
        };

        /**
         * Used to filter the posts based on the media selection
         * @author karthick.k
         * @param   {networks/media} networks selected media
         * @returns {array} list of post sorted based the interaction and media selection
         */
        $scope.filter_feed = function (networks) {
            var posts = angular.copy($scope.posts);
            var posts_by_media = _(posts).groupBy("media").value();
            var list = [];
            for (var media in networks) {
                if (!!networks[media]) {
                    list = _.union(list, posts_by_media[media]);
                }
            }
            $scope.post_order_by($scope.post_filter_legend, list, true);
        };

        //Initializing the posts
        $scope.post_order_by($scope.feed_filter[0], angular.copy($scope.posts), true);
        $scope.load_more = function () {
            $scope.post_limit += 9;
        };

        /********************************** End of feed/ post section ********************************/

        /********************************* Export section ********************************************/

        /**
         * Used to group by post based on the infleuncer
         * @author karthick.k
         * @returns {array} list of posts based on the influencer
         */
        var get_post_based_influencer = function () {
            var list_of_post = [];
            var posts = angular.copy($scope.posts);
            var posts_by_influencers = _(posts).groupBy("profile_id").value();
            for (var infleuncer_id in posts_by_influencers) {
                var infleuncer_posts = posts_by_influencers[infleuncer_id];
                infleuncer_posts.forEach(function (post) {
                    list_of_post.push({
                        "Influencer Name": post.profile_name,
                        "Country": post.country,
                        "Social Platform": post.media,
                        "Url of the Publication": post.post_link || "https://www.youtube.com/watch?v=" + post.video_id,
                        "Date of publication": new Date(post.post_created_time * 1000),
                        "Reach": (post.followed_by * $scope.impression_values[post.media]),
                        "Impression": (post.followed_by * $scope.impression_values[post.media]),
                        "Interactions": parseInt(post.interactions),
                        "Engagement Rate": (parseInt(post.interactions) / post.followed_by) * 100,
                        "Media value": parseInt(post.interactions) * default_media_value
                    });
                });
            }
            return list_of_post;
        };

        $scope.gridOptions = {
            exporterLinkLabel: 'get your csv here',
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            },
            data: get_post_based_influencer()
        };

        /**
         * Used to export the posts details
         * @author karthick.k
         */
        $scope.export = function () {
            var myElement = angular.element(document.querySelectorAll(".custom-csv-link-location"));
            $scope.gridApi.exporter.csvExport("all", "all", myElement);
        };


        /********************************* End of export section **************************************/
}]);