app.controller('profile', ['$scope', '$stateParams', '$http', 'languages', 'countries', '$state', 'profile_service', 'user_obj', 'themes_factory', 'continents', '$filter', 'toastr', 'countries_continents_factory', 'audience_factory', 'profile_types_factory', 'gender', '$uibModal', 'profiles_list_service', 'list_service', function ($scope, $stateParams, $http, languages, countries, $state, profile_service, user_obj, themes_factory, continents, $filter, toastr, countries_continents_factory, audience_factory, profile_types_factory, gender, $uibModal, profiles_list_service, list_service) {

        // sent params to view section to add some conditions...
        $scope.stateParams = $stateParams;

        // check if user hit valid profile or not
        if (!user_obj) {
            $state.go('404', $stateParams, {
                reload: true
            });
        }


        /*
        Intialization
         */
        $scope.user_obj_information = user_obj; // this is to pass values for children

        document.title = 'Campaygn - Profile - ' + $stateParams.slug;
        $scope.tag_limit = 5;

        // add list functionality in profiles...
        $scope.list = {
            profiles: {},
            parent_id: 0
        };
        $scope.list.profiles[$scope.user_obj_information.id] = $scope.user_obj_information.id;

        profiles_list_service.add_popup_functionality($scope, $uibModal, list_service, toastr, function () {
            // need to add this profile again as popup functionality will remove it after close...
            $scope.list.profiles[$scope.user_obj_information.id] = $scope.user_obj_information.id;
        });

        /*
        End of Initialization
        */

        /*
         * X-editable code starts
         */
        $scope.edit_profile = {
            birthday: {
                opened: false
            },
            edit_mode: false,
            all_countries: countries.all_countries,
            all_audiences: audience_factory.all_audiences,
            all_languages: languages.all_languages,
            all_themes: themes_factory.all_themes,
            all_profile_types: profile_types_factory.all_profile_types,
            all_gender: gender.all_gender,
            selected_languages: [],
            selected_themes: []
        };

        $scope.open_birthday = function (input) {
            $scope.edit_profile.birthday.opened = (input) ? false : true;
        };

        $scope.update_profile = function (profile_obj) {

            // adjustement based on fields
            if (profile_obj.hasOwnProperty("birthday") && angular.isDate(profile_obj.birthday)) {
                profile_obj.birthday = $filter('date')(profile_obj.birthday, 'yyyy-MM-dd');

                // need to update age also for this...
                $scope.profile.profile_age = $filter('date')(new Date(), 'yyyy') - $filter('date')(profile_obj.birthday, 'yyyy');
            }
            //country
            if (profile_obj.hasOwnProperty("country")) {
                // need to update country name
                $scope.profile.country_name = (profile_obj.country) ? countries.get_contry_object_by_code(profile_obj.country)[0].name : '';

                //need to update continent front-backend both...
                $scope.profile.profile_continent = (countries_continents_factory.all_countries[profile_obj.country]) ? countries_continents_factory.all_countries[profile_obj.country].continent : null;
                $scope.profile.continent_name = ($scope.profile.profile_continent) ? continents.get_continent_by_code($scope.profile.profile_continent)[0].name : '';

                //add continent field in update section.
                profile_obj.continent = $scope.profile.profile_continent;
            }
            // languages
            if (profile_obj.hasOwnProperty("languages")) {
                profile_obj.languages = profile_obj.languages.join(",");

                // update languages in front
                $scope.profile.profile_languages = profile_obj.languages;
                $scope.profile.languages_details = languages.get_language_object_by_code($scope.profile.profile_languages.toLowerCase());
            }
            // themes
            if (profile_obj.hasOwnProperty("themes")) {
                profile_obj.themes = profile_obj.themes.join(",");

                // update languages in front
                $scope.profile.profile_themes = profile_obj.themes;
                $scope.profile.themes_details = [];
                angular.forEach($scope.edit_profile.selected_themes, function (value, index) {
                    // fetch the theme object by code
                    var theme = themes_factory.get_theme_by_code(value);
                    // if we got the code then add it in front param to show
                    if (theme[0])
                        $scope.profile.themes_details.push(theme[0]);
                });
            }
            // themes
            if (profile_obj.hasOwnProperty("sub_kind")) {
                if (profile_obj.sub_kind === 'brand') {
                    // add main kind as brand
                    $scope.profile.profile_kind = profile_obj.kind = profile_obj.sub_kind;

                    $scope.profile.profile_sub_kind = profile_obj.sub_kind = null;
                } else {
                    // add main kind as brand
                    $scope.profile.profile_kind = profile_obj.kind = 'influencer';
                }
            }

            profile_service.update_profile($scope.profile.profile_id, profile_obj).then(function (success) {
                toastr.success(success.data);
            }, function (err) {
                toastr.error(err.data[Object.keys(err.data)[0]]);
            });
        };

        $scope.toggle_edit_mode = function () {
            $scope.edit_profile.edit_mode = ($scope.edit_profile.edit_mode) ? false : true;
        };

        /*
         * this function is to open pop-up to edit platform assosiated with profile...
         */
        $scope.open_platform = function (platform) {
            var popup_obj = $uibModal.open({
                animation: true,
                //template: '<h2>hi</h2>',
                templateUrl: './views/profile/platform/edit.html',
                controller: 'profile_platform',
                size: 'lg',
                resolve: {
                    platform: function () {
                        return platform;
                    },
                    profile_obj: function () {
                        return $scope.profile;
                    }
                }
            });

            // use the closed promise...
            popup_obj.closed.then(function (data) {
                // need to refresh it again...
                //$state.reload();
            }, console.log);
        };

        /*
         * Synchronize the profile manually
         */
        $scope.synchronize = function (profile_id) {
            $http.put('api/profile/' + profile_id).then(function (result) {
                $state.go('profile', $stateParams, {
                    reload: true
                });
            }, function (err) {
                // do nothing..
            });
        };

        /**
         * Get full profile data
         */

        profile_service.fetch_full_profile($scope.user_obj_information.id).then(function (profile_result) {
            $scope.profile = profile_result.data;

            if ($scope.profile.profile_languages) {
                // for x-editable form
                $scope.edit_profile.selected_languages = ($scope.profile.profile_languages).toLowerCase().split(",");

                // to show full name
                $scope.profile.languages_details = languages.get_language_object_by_code($scope.profile.profile_languages.toLowerCase());
            }

            // need the themes to show
            if ($scope.profile.profile_themes) {
                // array to save the name of themes from factory...
                $scope.profile.themes_details = [];

                // for x-editable form
                $scope.edit_profile.selected_themes = $scope.profile.profile_themes.split(",");

                angular.forEach($scope.edit_profile.selected_themes, function (value, index) {
                    // fetch the theme object by code
                    var theme = themes_factory.get_theme_by_code(value);
                    // if we got the code then add it in front param to show
                    if (theme[0])
                        $scope.profile.themes_details.push(theme[0]);
                });
            }

            if ($scope.profile.profile_tags) {
                $scope.profile.profile_tags = $scope.profile.profile_tags.split(",");
            }

            // need to fetch country name by code from factory
            $scope.profile.country_name = ($scope.profile.profile_country) ? countries.get_contry_object_by_code($scope.profile.profile_country)[0].name : '';
            $scope.profile.continent_name = ($scope.profile.profile_continent) ? continents.get_continent_by_code($scope.profile.profile_continent)[0].name : '';

        });

        /**
         * Get random images for profile page bg
         */

        profile_service.fetch_random_images($scope.user_obj_information).then(function (images_result) {
            $scope.profile_images = images_result.data;
        });

        /**
         * Get recent images for profile page bg
         */

        profile_service.fetch_recent_images($scope.user_obj_information).then(function (result) {
            $scope.recent_posts = result.data;

            /* To fetch most recent image among all images */
            //console.log(result.data[0]);
            $scope.recent_post_single = result.data[0];
        });

        /**
         * Get average number of likes for user
         */

        profile_service.fetch_avg_likes($scope.user_obj_information).then(function (result) {
            //console.log(result.data);
            $scope.user_average_likes = result.data[0].averageNumber;
        });

        /**
         * Get media posted for facebook
         */
        if ($stateParams.active_platform == 'facebook') {
            profile_service.fetch_media_posted($scope.user_obj_information).then(function (result) {
                $scope.FB_media_information = result.data[0];
            }, function (err) {
                console.log(err);
            });
        }


        /**
         * Get user's most liked post
         */

        profile_service.fetch_most_liked_post($scope.user_obj_information).then(function (result) {
            $scope.user_most_liked_post = result.data[0];
        });

        /**
         * Delete profile from system
         */
        $scope.delete_profile = function (profile_id) {
            // check if we are getting profile-id or not
            if (!isNaN(profile_id)) {
                $http.delete('./api/profile/delete/' + profile_id + '/?cache=false').then(function (success) {
                    toastr.success(success.data);

                    // need to redirect also after delete...
                    $state.go('profiles', {
                        cache: false
                    }, {
                        reload: true
                    });

                }, function (error) {
                    toastr.error(error.data);
                });
            }
        };

    }
]);
