app.controller('community', ['$scope', '$stateParams', '$state', 'profile_service', '$filter', function ($scope, $stateParams, $state, profile_service, $filter) {

    // intialization
    $scope.bubbles = [];
    var six_months_older = new Date();
    six_months_older.setMonth(six_months_older.getMonth() - 6);

    $scope.common_date_options = {
        start_date: {
            opened: false
        },
        end_date: {
            opened: false
        },
        max_date: new Date(),
        min_date: six_months_older
    };

    $scope.map = {
        start_date: $scope.common_date_options.min_date,
        end_date: $scope.common_date_options.max_date
    };

    $scope.highchartsNG = {
        options: {
            chart: {
                type: 'line'
            }
        },
        series: [],
        title: {
            text: ''
        },
        loading: true,
        xAxis: {
            type: 'datetime',
            title: {
                text: 'Dates'
            }
        },
        yAxis: {
            type: 'linear',
            title: {
                text: 'No. of followers'
            }
        }
    };
    // end initialization

    /*
     * prepare the data format as accepted by Highchart
     */
    var prepare_highchart_data = function (input) {
        var output = [];

        // run foreach loop for each entry and convert it into desired format...
        angular.forEach(input, function (value, key) {
            var temp = [];
            //var converted_date = new Date(value.date).getTime();
            var converted_date = Date.UTC($filter('date')(value.date, 'yyyy'), $filter('date')(value.date, 'MM') - 1, $filter('date')(value.date, 'dd'));

            //prepare temprary array...
            temp.push(converted_date);
            temp.push(value.followed_by);

            output.push(temp);
        });

        return output;
    };

    /**
     * Prepare the platform object for highchart
     */
    var prepare_platform_object = function (input, platform) {
        var output = {};

        output.data = input;

        switch (platform) {
            case 'instagram':
                output.name = 'Instagram';
                output.color = '#517fa4';
                break;
            case 'facebook':
                output.name = 'Facebook';
                output.color = '#3b5998';
                break;
            case 'youtube':
                output.name = 'Youtube';
                output.color = '#bb0000';
                break;
            default:
                output.name = 'Unknown';
                output.color = '#000000';
                break;
        }

        return output;
    };

    /**
     * Prepare Bubble for Bubble chart
     */
    var prepare_bubble_object = function (followers, platform) {
        var output = {};

        followers = (followers) ? followers : 0;
        var platform_class = '';
        var title = '';

        switch (platform) {
            case 'instagram':
                platform_class = 'instagram-back-color';
                title = 'Instagram followers <br> ' + $filter('number')(followers, 0);
                break;
            case 'facebook':
                platform_class = 'facebook-back-color';
                title = 'Facebook followers <br> ' + $filter('number')(followers, 0);
                break;
            case 'youtube':
                platform_class = 'youtube-back-color';
                title = 'Youtube followers <br> ' + $filter('number')(followers, 0);
                break;
            default:
                platform_class = 'instagram-back-color';
                title = 'Unknown followers <br> ' + $filter('number')(followers, 0);
                break;
        }

        $scope.bubbles.push({
            followers: followers,
            class: platform_class,
            title: title
        });

        return true;
    };

    /**
     * this function is just pushing data into highchartsNG for highchart display
     */

    var push_series_highchart = function (platform_data, platform) {
        // Add series...
        $scope.highchartsNG.series.push(prepare_platform_object(prepare_highchart_data(platform_data), platform));
    };

    // open/ close calender

    $scope.open_calender_startdate = function (input) {
        $scope.common_date_options.start_date.opened = (input) ? false : true;
    };

    $scope.open_calender_enddate = function (input) {
        $scope.common_date_options.end_date.opened = (input) ? false : true;
    };
    // end calender

    /*
     * This function is to handle the changes made in the date range by user...
     */

    $scope.change_range = function () {
        $scope.highchartsNG.loading = true;
        $scope.highchartsNG.series = [];

        profile_service.fetch_map_stats($scope.$parent.user_obj_information.id, $scope.map.start_date, $scope.map.end_date).then(function (result) {
            $scope.highchartsNG.loading = false;

            // add instagram values to graph section...
            if (angular.isArray(result.data.instagram) && result.data.instagram.length > 0) {
                // prepare data in a format suited by highChart
                // push the intagram data
                push_series_highchart(result.data.instagram, 'instagram');
            }

            // add facebook values to graph section...
            if (angular.isArray(result.data.facebook) && result.data.facebook.length > 0) {
                // prepare data in a format suited by highChart
                // push the facebook data
                push_series_highchart(result.data.facebook, 'facebook');
            }

            // add youtube values to graph section...
            if (angular.isArray(result.data.youtube) && result.data.youtube.length > 0) {
                // prepare data in a format suited by highChart
                // push the facebook data
                push_series_highchart(result.data.youtube, 'youtube');
            }
        }, function (err) {
            $scope.highchartsNG.error = err;
        });
    };

    /**
     * Get user's HighChart data stats to show on graph
     */
    profile_service.fetch_map_stats($scope.$parent.user_obj_information.id).then(function (result) {
        $scope.highchartsNG.loading = false;

        // add instagram values to graph section...
        if (angular.isArray(result.data.instagram) && result.data.instagram.length > 0) {
            var instagram_latest_followers = result.data.instagram[result.data.instagram.length - 1].followed_by;

            // prepare data in a format suited by highChart
            // push the intagram data
            push_series_highchart(result.data.instagram, 'instagram');

            // need to add instgram followers to make a bubble...
            prepare_bubble_object(instagram_latest_followers, 'instagram');
        }
        // add facebook values to graph section...
        if (angular.isArray(result.data.facebook) && result.data.facebook.length > 0) {
            var facebook_latest_followers = result.data.facebook[result.data.facebook.length - 1].followed_by;

            // prepare data in a format suited by highChart
            // push the facebook data
            push_series_highchart(result.data.facebook, 'facebook');

            // need to add facebook followers to make a bubble...
            prepare_bubble_object(facebook_latest_followers, 'facebook');
        }

        // add youtube values to graph section...
        if (angular.isArray(result.data.youtube) && result.data.youtube.length > 0) {
            var youtube_latest_followers = result.data.youtube[result.data.youtube.length - 1].followed_by;

            // prepare data in a format suited by highChart
            // push the facebook data
            push_series_highchart(result.data.youtube, 'youtube');

            // need to add youtube followers to make a bubble...
            prepare_bubble_object(youtube_latest_followers, 'youtube');
        }

    }, function (err) {
        $scope.highchartsNG.error = err;
    });
}]);
