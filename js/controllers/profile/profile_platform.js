app.controller('profile_platform', ['$scope', '$uibModalInstance', 'platform', 'profile_obj', 'profile_service', '$http', 'toastr',

    function ($scope, $uibModalInstance, platform, profile_obj, profile_service, $http, toastr) {
        /**
         * Intialization
         */
        $scope.view_obj = {
            platform: platform,
            profile_obj: profile_obj,
            ig_users: [],
            fb_pages: [],
            yt_channels: [],
            search: {
                ig_keyword: '',
                fb_keyword: '',
                yt_keyword: ''
            }
        };

        $scope.profile = {};
        $scope.platform_exist = true;

        // check if pltform that user want to edit exist or not...
        if ((platform === 'instagram' && !(profile_obj.instagram_id)) || (platform === 'facebook' && !(profile_obj.facebook_id)) || (platform === 'youtube' && !(profile_obj.youtube_id))) {
            $scope.platform_exist = false;
        }

        // END 

        // validate form
        var validate_form = function (input) {
            input.error = {};

            // if nothing is selected and user want to update
            if (!input[platform]) {
                input.error[platform] = "Please select an account to update.";
            }

            // when user are selecting same profile again.
            if ((platform === 'instagram' && profile_obj.instagram_id === input.instagram) || (platform === 'facebook' && profile_obj.facebook_id === input.facebook) || (platform === 'youtube' && profile_obj.youtube_id === input.youtube)) {
                input.error[platform] = "You are selecting the existing profile again.";
            }

            if (Object.keys(input.error).length <= 0) {
                return true;
            }

            return false;
        };


        /**
         * Function to handle form submit of edit platform
         */
        $scope.edit_platform = function () {
            if (validate_form($scope.profile)) {
                var data_to_send = {};
                data_to_send[platform] = $scope.profile[platform];

                // send request to server to proceed
                $http.post('./api/profile/edit_' + platform + '/' + profile_obj.profile_id + '/?cache=false', data_to_send).then(function (success) {
                    toastr.success(success.data);

                    // close the pop-up
                    $scope.close_popup();

                }, function (err) {
                    var error_msg = (typeof err.data === 'object') ? err.data[Object.keys(err.data)[0]] : err.data;
                    toastr.error(error_msg);

                    $scope.profile.error = err.data;
                });
            }
        };



        /**
         * Assign value selected by user
         */
        $scope.assign_value = function (platform, value) {
            $scope.profile[platform] = value;
        };

        /**
         * Search Instagram user
         */
        $scope.search_instagram_users = function () {
            profile_service.fetch_instagram_users($scope.view_obj.search.ig_keyword).then(function (ig_users) {
                $scope.view_obj.ig_users = ig_users.data;
            }, function (err) {
                $scope.view_obj.ig_users = [];
            });
        };

        /**
         * getting the facebook page
         */
        $scope.search_facebook_pages = function () {
            profile_service.fetch_facebook_pages($scope.view_obj.search.fb_keyword).then(function (fb_pages) {
                $scope.view_obj.fb_pages = fb_pages.data.data;
            }, function (err) {
                $scope.view_obj.fb_pages = [];
            });
        };

        /**
         * getting the youtube page
         */
        $scope.search_youtube_channels = function () {
            console.log($scope.view_obj.search.yt_keyword);
            profile_service.fetch_youtube_channels($scope.view_obj.search.yt_keyword).then(function (yt_channels) {
                $scope.view_obj.yt_channels = yt_channels.data.items;
            }, function (err) {
                $scope.view_obj.yt_channels = [];
            });
        };

        // close the popup...
        $scope.close_popup = function () {
            $uibModalInstance.dismiss();
        };

}]);
