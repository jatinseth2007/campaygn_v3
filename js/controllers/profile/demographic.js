app.controller('demographic', ['$scope', '$stateParams', '$state', 'profile_service', function ($scope, $stateParams, $state, profile_service) {

    // highmap intialization
    var six_months_older = new Date();
    var one_day_older = new Date();
    var two_day_older = new Date();

    six_months_older.setMonth(six_months_older.getMonth() - 6);
    one_day_older.setDate(one_day_older.getDate() - 1);
    two_day_older.setDate(two_day_older.getDate() - 2);

    $scope.common_date_options = {
        which_date: {
            opened: false
        },
        max_date: new Date(),
        min_date: six_months_older,
        one_day_older: one_day_older
    };

    $scope.map = {
        which_date: two_day_older
    };

    $scope.high_map_options = {
        options: {
            legend: {
                enabled: false
            },
            plotOptions: {
                map: {
                    mapData: Highcharts.maps['custom/world'],
                    joinBy: ['iso-a2', 'code']
                }
            },
            mapNavigation: {
                enabled: true,
                buttonOptions: {
                    align: "right"
                }
            }
        },
        chartType: 'map',
        title: {
            text: 'Followers by Area'
        },
        series: [
            {
                allAreas: true,
                name: 'Facebook followers',
                data: [],
                states: {
                    hover: {
                        color: '#44ac8e'
                    }
                },
                dataLabels: {
                    enabled: true,
                    color: '#ffffff',
                    formatter: function () {
                        if (this.point.value) {
                            return this.point.name;
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.value}</b>'
                }
                }
            ]
    };

    // end initialization

    // to open/ close calender
    $scope.open_calender_which_date = function (input) {
        $scope.common_date_options.which_date.opened = (input) ? false : true;
    };

    /*
     * This function is to handle the changes made in the demographic date by user...
     */

    $scope.change_date = function () {
        $scope.high_map_options.series[0].data = [];
        delete $scope.high_map_options.error;

        profile_service.fetch_highmap_stats($scope.$parent.user_obj_information, $scope.map.which_date).then(function (result) {
            //check if we got the data or not
            if (result.data && result.data.length > 0) {
                $scope.high_map_options.series[0].data = result.data;
            }
        }, function (err) {
            $scope.high_map_options.error = err;
        });
    };

    /**
     * Get user's HighMap data
     */
    profile_service.fetch_highmap_stats($scope.$parent.user_obj_information, $scope.map.which_date).then(function (result) {
        //check if we got the data or not
        if (result.data && result.data.length > 0) {
            $scope.high_map_options.series[0].data = result.data;
        }
    }, function (err) {
        $scope.high_map_options.error = 'No Data Found';
    });
}]);
