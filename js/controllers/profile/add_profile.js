app.controller('add_profile', ['$scope', '$http', 'toastr', 'profile_service', 'themes_factory', 'countries', 'languages', 'gender', 'countries_continents_factory', 'audience_factory', 'profile_types_factory', '$cookies', function ($scope, $http, toastr, profile_service, themes_factory, countries, languages, gender, countries_continents_factory, audience_factory, profile_types_factory, $cookies) {

    /*
     * Initialization
     */
    document.title = 'Campaygn - Profiles - add';

    var intialialize_params = function () {
        $scope.ig_users = [];
        $scope.fb_pages = [];
        $scope.yt_channels = [];

        // serach params
        $scope.search = {};
        $scope.search.ig_keyword = '';
        $scope.search.fb_keyword = '';
        $scope.search.yt_keyword = '';
        //
        // profile params
        $scope.selected = {};
        $scope.selected.themes = [];
        $scope.selected.languages = [];
        $scope.profile = {};

        // to fill last entered data...
        $scope.last_entered_profile = $cookies.getObject('last_added_profile');
    };
    intialialize_params();

    $scope.all_themes = themes_factory.all_themes; // load all the theme options in the dropdown...
    $scope.all_languages = languages.all_languages; // load all the theme options in the dropdown...
    $scope.all_countries = countries.all_countries; // load all the theme options in the dropdown...

    // audience types to create dropdown
    $scope.audience_types = audience_factory.all_audiences;

    // gender to create dropdown
    $scope.gender_types = gender.all_gender;

    // profile types to create dropdown
    $scope.profile_types = profile_types_factory.all_profile_types;
    // END


    /**
     * Search Instagram user
     */
    $scope.search_instagram_users = function () {
        profile_service.fetch_instagram_users($scope.search.ig_keyword).then(function (ig_users) {
            $scope.ig_users = ig_users.data;
        }, function (err) {
            $scope.ig_users = [];
        });
    };

    /**
     * assign Instagram User
     */
    $scope.assign_ig_value = function (ig_id) {
        $scope.profile.instagram = ig_id;
    };

    /**
     * getting the facebook page
     */
    $scope.search_facebook_pages = function () {
        profile_service.fetch_facebook_pages($scope.search.fb_keyword).then(function (fb_pages) {
            $scope.fb_pages = fb_pages.data.data;
        }, function (err) {
            $scope.fb_pages = [];
        });
    };

    /**
     * assign facebook User
     */
    $scope.assign_fb_value = function (fb_info) {
        $scope.profile.facebook = fb_info.id;
        $scope.profile.url = (fb_info.website) ? fb_info.website : null;
        $scope.profile.birthday = (fb_info.birthday) ? fb_info.birthday : null;
    };

    /**
     * getting the youtube page
     */
    $scope.search_youtube_channels = function () {
        profile_service.fetch_youtube_channels($scope.search.yt_keyword).then(function (yt_channels) {
            $scope.yt_channels = yt_channels.data.items;
        }, function (err) {
            $scope.yt_channels = [];
        });
    };

    /**
     * assign youtube User
     */
    $scope.assign_yt_value = function (yt_id) {
        $scope.profile.youtube = yt_id;
    };

    /**
     * Saving a new profile
     */
    $scope.save = function () {
        if (profile_service.validate_profile_input($scope.profile)) {

            // delete errors from object.
            if ($scope.profile.error)
                delete $scope.profile.error;

            // need to create comma separated themes
            if ($scope.selected.themes && Array.isArray($scope.selected.themes))
                $scope.profile.themes = $scope.selected.themes.join(',');

            // need to create comma separated languages
            if ($scope.selected.languages && Array.isArray($scope.selected.languages))
                $scope.profile.languages = $scope.selected.languages.join(',');

            // need to fetch continent from country selection
            if ($scope.profile.country) {
                $scope.profile.continent = (countries_continents_factory.all_countries[$scope.profile.country]) ? countries_continents_factory.all_countries[$scope.profile.country].continent : null;
            } else {
                $scope.profile.continent = null;
            }

            // send request to add profile in system
            $http.post('./api/profile/add', $scope.profile).then(function (res) {
                // put the data in cookie...
                $cookies.putObject('last_added_profile', $scope.profile);

                toastr.success("Profile has been added successfully.");
                intialialize_params();
            }, function (err) {
                var error_message = (err.data.message) ? err.data.message : "Profile could not be added, there are errors in data you provided.";
                toastr.error(error_message);

                $scope.profile.error = err.data;
            });
        }
        return false;
    };

    /*
     * Remove platform from profile...
     */
    $scope.remove_platform = function (platform) {
        switch (platform) {
            case "ig":
                //remove IG data
                delete $scope.profile.instagram; // delete ig profile id from profile data
                $scope.ig_users = []; // empty the results
                $scope.search.ig_keyword = ""; // need to empty serach string also...
                break;
            case "fb":
                //remove FB data
                delete $scope.profile.facebook; // delete fb profile id from profile data
                delete $scope.profile.url;
                delete $scope.profile.birthday;

                $scope.fb_pages = []; // empty the results
                $scope.search.fb_keyword = ""; // need to empty serach string also...
                break;
            case "yt":
                //remove YT data
                delete $scope.profile.youtube; // delete yt profile id from profile data
                $scope.yt_channels = []; // empty the results
                $scope.search.yt_keyword = ""; // need to empty serach string also...
                break;
            default:
                // do nothing ...
        }
    };

    /*
     * generate slug
     */
    $scope.generate_slug = function () {
        if ($scope.profile.name && $scope.profile.name.length > 0) {
            // fetch slug for profile...
            $http.get('./api/profile/generate_slug?cache=false&name=' + $scope.profile.name).then(function (slug_info) {
                if (slug_info.data) {
                    $scope.profile.slug = slug_info.data;
                }
            }, function (err) {
                toastr.error(err);
            });
        }
    };

    /*
     * to fill last entered data...
     */
    $scope.fill_last_entered_data = function () {
        //assign values from cookie...
        $scope.profile.gender = $scope.last_entered_profile.gender; //profile gender
        $scope.profile.kind = $scope.last_entered_profile.kind; //profile kind
        $scope.profile.blog = $scope.last_entered_profile.blog; //blog url
        $scope.selected.themes = $scope.last_entered_profile.themes.split(','); // themes
        $scope.profile.country = $scope.last_entered_profile.country; // country
        $scope.selected.languages = $scope.last_entered_profile.languages.split(','); // country
        $scope.profile.audience = $scope.last_entered_profile.audience; // audience
        $scope.profile.age_min = $scope.last_entered_profile.age_min; // audience age_min
        $scope.profile.age_max = $scope.last_entered_profile.age_max; // audience age_max
        $scope.profile.bio = $scope.last_entered_profile.bio; // bio
        $scope.profile.address_1 = $scope.last_entered_profile.address_1; // audience age_max
        $scope.profile.phone = $scope.last_entered_profile.phone; // audience age_max
        $scope.profile.email = $scope.last_entered_profile.email; // audience age_max
    };

}]);
