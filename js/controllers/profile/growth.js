app.controller('growth', ['$scope', '$stateParams', 'profile_service', function ($scope, $stateParams, profile_service) {

    $scope.profile_growth = {};

    /**
     * Getting growth widget
     */

    profile_service.fetch_growth_widget($scope.user_obj_information).then(function (growth_results) {
        var data = growth_results.data;
        //media avg intialization
        var media_avg_by_day = '-';
        var media_avg_by_week = '-';
        var media_avg_by_month = '-';
        var media_calculation_since = 0;

        // followers avg intialization and calculation
        var followers_avg_by_day = (data.avg_stats[0] && data.avg_stats[0].followers_diff && data.avg_stats[0].no_days) ? (data.avg_stats[0].followers_diff / data.avg_stats[0].no_days) : '-';
        var followers_avg_by_week = (followers_avg_by_day !== '-') ? (followers_avg_by_day * 7) : '-';
        var followers_avg_by_month = (followers_avg_by_day !== '-') ? (followers_avg_by_day * 30) : '-';
        var followers_calculation_since = (data.avg_stats[0] && data.avg_stats[0].oldest_date) ? data.avg_stats[0].oldest_date : 0;

        // media avg calculation based on plateform
        if ($stateParams.active_platform == 'facebook') {
            media_avg_by_day = (data.avg_stats_media[0] && data.avg_stats_media[0].media_diff && data.avg_stats_media[0].no_days) ? (data.avg_stats_media[0].media_diff / data.avg_stats_media[0].no_days) : '-';

            media_avg_by_week = (media_avg_by_day !== '-') ? (media_avg_by_day * 7) : '-';

            media_avg_by_month = (media_avg_by_day !== '-') ? (media_avg_by_day * 30) : '-';

            media_calculation_since = (data.avg_stats_media[0] && data.avg_stats_media[0].oldest_date) ? data.avg_stats_media[0].oldest_date : 0;

        } else if ($stateParams.active_platform == 'instagram' || $stateParams.active_platform == 'youtube') {
            media_avg_by_day = (data.avg_stats[0] && data.avg_stats[0].media_diff && data.avg_stats[0].no_days) ? (data.avg_stats[0].media_diff / data.avg_stats[0].no_days) : '-';
            media_avg_by_week = (media_avg_by_day !== '-') ? (media_avg_by_day * 7) : '-';
            media_avg_by_month = (media_avg_by_day !== '-') ? (media_avg_by_day * 30) : '-';
            media_calculation_since = (data.avg_stats[0] && data.avg_stats[0].oldest_date) ? data.avg_stats[0].oldest_date : 0;
        }

        /**
         * Media
         */

        var media_obj = {};

        media_obj.day = (data.one_day_diff[0] && data.one_day_diff[0].media_info && data.one_day_diff[0].media_info.split(",")[0] && data.one_day_diff[0].media_info.split(",")[1]) ? (data.one_day_diff[0].media_info.split(",")[0] - data.one_day_diff[0].media_info.split(",")[1]) : "-";

        media_obj.day_perc = (media_obj.day !== "-" && media_avg_by_day !== '-') ? ((media_obj.day - media_avg_by_day) / media_avg_by_day) * 100 : "-";

        media_obj.week = (data.seven_day_diff[0] && data.seven_day_diff[0].media_info && data.seven_day_diff[0].media_info.split(",")[0] && data.seven_day_diff[0].media_info.split(",")[1]) ? (data.seven_day_diff[0].media_info.split(",")[0] - data.seven_day_diff[0].media_info.split(",")[1]) : "-";

        media_obj.week_perc = (media_obj.week !== "-" && media_avg_by_week !== '-') ? ((media_obj.week - media_avg_by_week) / media_avg_by_week) * 100 : "-";

        media_obj.month = (data.thirty_day_diff[0] && data.thirty_day_diff[0].media_info && data.thirty_day_diff[0].media_info.split(",")[0] && data.thirty_day_diff[0].media_info.split(",")[1]) ? (data.thirty_day_diff[0].media_info.split(",")[0] - data.thirty_day_diff[0].media_info.split(",")[1]) : "-";

        media_obj.month_perc = (media_obj.month !== "-" && media_avg_by_month !== '-') ? ((media_obj.month - media_avg_by_month) / media_avg_by_month) * 100 : "-";

        media_obj.avg_by_day = parseFloat(media_avg_by_day).toFixed(2);
        media_obj.avg_by_week = parseFloat(media_avg_by_week).toFixed(2);
        media_obj.avg_by_month = parseFloat(media_avg_by_month).toFixed(2);

        media_obj.media_calculation_since = media_calculation_since;



        /**
         * Followers
         */
        var followed_by_obj = {};

        followed_by_obj.day = (data.one_day_diff[0] && data.one_day_diff[0].followed_by_info && data.one_day_diff[0].followed_by_info.split(",")[0] && data.one_day_diff[0].followed_by_info.split(",")[1]) ? (data.one_day_diff[0].followed_by_info.split(",")[0] - data.one_day_diff[0].followed_by_info.split(",")[1]) : "-";

        followed_by_obj.day_perc = (followed_by_obj.day !== "-" && followers_avg_by_day !== '-') ? ((followed_by_obj.day - followers_avg_by_day) / followers_avg_by_day) * 100 : "-";

        followed_by_obj.week = (data.seven_day_diff[0] && data.seven_day_diff[0].followed_by_info && data.seven_day_diff[0].followed_by_info.split(",")[0] && data.seven_day_diff[0].followed_by_info.split(",")[1]) ? (data.seven_day_diff[0].followed_by_info.split(",")[0] - data.seven_day_diff[0].followed_by_info.split(",")[1]) : "-";

        followed_by_obj.week_perc = (followed_by_obj.week !== "-" && followers_avg_by_week !== '-') ? ((followed_by_obj.week - followers_avg_by_week) / followers_avg_by_week) * 100 : "-";

        followed_by_obj.month = (data.thirty_day_diff[0] && data.thirty_day_diff[0].followed_by_info && data.thirty_day_diff[0].followed_by_info.split(",")[0] && data.thirty_day_diff[0].followed_by_info.split(",")[1]) ? (data.thirty_day_diff[0].followed_by_info.split(",")[0] - data.thirty_day_diff[0].followed_by_info.split(",")[1]) : "-";

        followed_by_obj.month_perc = (followed_by_obj.month !== "-" && followers_avg_by_month !== '-') ? ((followed_by_obj.month - followers_avg_by_month) / followers_avg_by_month) * 100 : "-";

        followed_by_obj.avg_by_day = parseFloat(followers_avg_by_day).toFixed(2);
        followed_by_obj.avg_by_week = parseFloat(followers_avg_by_week).toFixed(2);
        followed_by_obj.avg_by_month = parseFloat(followers_avg_by_month).toFixed(2);

        followed_by_obj.followers_calculation_since = followers_calculation_since;

        $scope.profile_growth = {
            media: media_obj,
            followed_by: followed_by_obj
        };

    }, function (err) {
        console.log(err);

        $scope.profile_growth = {
            media: {
                day: '-',
                week: '-',
                month: '-',
                avg_by_day: '-',
                avg_by_week: '-',
                avg_by_month: '-'
            },
            followed_by: {
                day: '-',
                week: '-',
                month: '-',
                avg_by_day: '-',
                avg_by_week: '-',
                avg_by_month: '-'
            }
        };
    });
    /*
     * We need to fetch extra information in case of Brand only
     * This function will fetch the information about the number of posts on that hash tags
     */
    profile_service.fetch_brand_hashtag_growth($scope.user_obj_information).then(function (result) {
        var hashtag_data = result.data;

        // inialization
        // calculation for avg
        var hashtag_avg_by_day = (hashtag_data.avg_stats[0] && hashtag_data.avg_stats[0].no_of_posts && hashtag_data.avg_stats[0].no_days) ? (hashtag_data.avg_stats[0].no_of_posts / hashtag_data.avg_stats[0].no_days) : '-';
        var hashtag_avg_by_week = (hashtag_avg_by_day !== '-') ? (hashtag_avg_by_day * 7) : '-';
        var hashtag_avg_by_month = (hashtag_avg_by_day !== '-') ? (hashtag_avg_by_day * 30) : '-';
        var hashtag_calculation_since = (hashtag_data.avg_stats[0] && hashtag_data.avg_stats[0].oldest_date) ? hashtag_data.avg_stats[0].oldest_date : 0;

        /**
         * Hashtags
         */
        var hashtag_obj = {};

        hashtag_obj.day = (hashtag_data.one_day_diff[0] && hashtag_data.one_day_diff[0].no_of_posts) ? hashtag_data.one_day_diff[0].no_of_posts : "-";
        hashtag_obj.day_perc = (hashtag_obj.day !== "-" && hashtag_avg_by_day !== '-') ? ((hashtag_obj.day - hashtag_avg_by_day) / hashtag_avg_by_day) * 100 : "-";

        hashtag_obj.week = (hashtag_data.seven_day_diff[0] && hashtag_data.seven_day_diff[0].no_of_posts) ? hashtag_data.seven_day_diff[0].no_of_posts : "-";
        hashtag_obj.week_perc = (hashtag_obj.week !== "-" && hashtag_avg_by_week !== '-') ? ((hashtag_obj.week - hashtag_avg_by_week) / hashtag_avg_by_week) * 100 : "-";

        hashtag_obj.month = (hashtag_data.thirty_day_diff[0] && hashtag_data.thirty_day_diff[0].no_of_posts) ? hashtag_data.thirty_day_diff[0].no_of_posts : "-";
        hashtag_obj.month_perc = (hashtag_obj.month !== "-" && hashtag_avg_by_month !== '-') ? ((hashtag_obj.month - hashtag_avg_by_month) / hashtag_avg_by_month) * 100 : "-";

        hashtag_obj.avg_by_day = hashtag_avg_by_day;
        hashtag_obj.avg_by_week = hashtag_avg_by_week;
        hashtag_obj.avg_by_month = hashtag_avg_by_month;

        hashtag_obj.followers_calculation_since = hashtag_calculation_since;


        $scope.profile_growth.hashtag = hashtag_obj;

    }, function (err) {
        console.log(err);

        $scope.profile_growth.hashtag = {
            day: '-',
            week: '-',
            month: '-',
            avg_by_day: '-',
            avg_by_week: '-',
            avg_by_month: '-'
        };
    });

}]);
