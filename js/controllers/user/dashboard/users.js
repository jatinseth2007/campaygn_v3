app.controller('dashboard_users', ['$scope', '$stateParams', 'user_service', 'validation', '$location', '$rootScope', '$state', '$window', '$filter', 'users', 'roles',

    function ($scope, $stateParams, user_service, validation, $location, $rootScope, $state, $window, $filter, users, roles) {

        $scope.users = users;
        $scope.roles = roles;
        $scope.opened = {};

        /**
         * Used to fetch all users from the database
         * @author karthick.k
         * @param   {object} form form object
         */
        var reload_users = function () {
            user_service.get_users().then(function (result) {
                if (result.data && result.data.users) {
                    $scope.users = result.data.users;
                }
            });
        };

        /**
         * Used to load roles for dropdown
         * @author karthick.k
         */
        $scope.load_roles = function () {
            return roles;
        };

        /**
         * Used to handle event propagation which is used by angularjs framework xeditable
         * @author karthick.k
         * @param   {object} event
         * @param   {object} elementOpened
         */
        $scope.open = function ($event, elementOpened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[elementOpened] = !$scope.opened[elementOpened];
        };

        /**
         * Used to modify role for the user
         * @author karthick.k
         * @param   {object} user contains user information id, email, firstname, lastname, created_at, last_login, etc
         */
        $scope.modify_role = function (user) {
            var data = {};
            data.id = user.id;
            data.role_id = user.role_id;
            user_service.modify_role(data).then(function (result) {
                reload_users();
            }, function (error) {
                alert(error.data);
            });
        };

        /**
         * Used to modify expires_at/expiry date of the user
         * @author karthick.k
         * @param   {object} user contains user information id, email, firstname, lastname, created_at, last_login, etc
         */
        $scope.modify_expiry = function (user) {
            var data = {};
            data.id = user.id;
            data.expires_at = user.expires_at ? $filter('date')(new Date(user.expires_at), 'yyyy-MM-dd') : null;
            user_service.modify_expiry(data).then(function (result) {
                reload_users();
            }, function (error) {
                alert(error.data);
            });
        };

        /**
         * Used to change route to user logs
         * @author karthick.k
         * @param {string} id user id
         */
        $scope.go_to_user = function (id) {
            $state.go("user_dashboard_user_logs", {
                id: id
            });
        };

        /**
         * Used to delete user
         * @author karthick.k
         * @param {object} user user id
         */
        $scope.delete = function (user) {
            user_service.delete_user(user.id).then(function (result) {
                reload_users();
            }, function (error) {
                alert(error.data);
            });
        };
}]);