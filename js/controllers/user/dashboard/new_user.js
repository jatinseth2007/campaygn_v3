app.controller('dashboard_new_user', ['$scope', '$stateParams', 'user_service', 'validation', '$location', '$rootScope', '$state', '$window', '$filter', 'roles',

    function ($scope, $stateParams, user_service, validation, $location, $rootScope, $state, $window, $filter, roles) {

        $scope.list_roles = roles;

        /**
         * Used to clear form messages, Message is based on server/ajax call
         * @author karthick.k
         */
        var clear_message = function () {
            $scope.success_message = "";
            $scope.error_message = "";
        };

        /**
         * Used to reset form validation and form fields
         * @author karthick.k
         * @param   {object} form form object
         */
        var reset_form = function (form) {
            $scope.user = null;
            form.$setUntouched();
        };

        /**
         * Used to create new user, after validating form fields
         * @author karthick.k
         * @param   {object} form form object
         */
        $scope.create_user = function (form) {
            clear_message();
            if (!validation.is_valid(form)) {
                return;
            }
            var user = $scope.user;
            if (user.expires_at) {
                user.expires_at = $filter('date')(new Date(user.expires_at), 'yyyy-MM-dd');
            } else {
                user.expires_at = null;
            }
            $scope.is_loading = true;
            reset_form(form);
            user.password = "null";
            user.role_id = user.role.id;
            user_service.create_user(user).then(function (result) {
                $scope.is_loading = false;
                if (result.data && result.data.message) {
                    $scope.success_message = result.data.message;
                }
            }, function (result) {
                $scope.is_loading = false;
                $scope.error_message = result.data;
            });
        };
}]);