app.controller('user', ['$scope', '$stateParams', 'user_service', 'validation', '$location', '$rootScope', '$state', '$window',

    function ($scope, $stateParams, user_service, validation, $location, $rootScope, $state, $window) {
        $scope.user = {};
        $scope.forgot_password = {};
        $scope.template_name = '/views/user/login.html';
        $scope.users = {
            count: 0
        };

        if (!!$rootScope.is_logged_in && $rootScope.user_info && $rootScope.user_info.role_id) {
            $scope.template_name = '/views/user/logout.html';
            $scope.is_admin = ($rootScope.user_info.role_id === 100) ? true : false;
        }

        /**
         * Used to verify user based on the email verification
         * @author karthick.k
         * @param {object} token and email in the state params
         */
        if ($location.path() === "/user/verification/") {
            if ($stateParams.token && $stateParams.email) {
                var user = {
                    token: $stateParams.token,
                    email: $stateParams.email
                };
                user_service.email_verification(user).then(function (result) {
                    $scope.template_name = '/views/user/logout.html';
                }, function (result) {
                    $scope.verification_failure_message = result.data;
                });
            }
        }

        /**
         * Used to clear form fields error/success message
         * @author karthick.k
         */
        var clear_form_message = function () {
            $scope.error_message = "";
            $scope.success_message = "";
        };

        /**
         * Used to login, validate the form fields, make server/ajax call to login
         * @author karthick.k
         * @param   {object} form  form fields contains email and password
         */
        $scope.login = function (form) {
            clear_form_message();
            if (!validation.is_valid(form)) {
                return;
            }
            $scope.is_loading = true;
            user_service.login($scope.user).then(function (result) {
                $scope.is_loading = false;
                $scope.close_dropdown();
                $rootScope.is_logged_in = true;
                $rootScope.user_info = result.data;
                if (!!$rootScope.is_logged_in && $rootScope.user_info && $rootScope.user_info.role_id) {
                    $scope.template_name = '/views/user/logout.html';
                    $scope.is_admin = ($rootScope.user_info.role_id === 100) ? true : false;
                }
            }, function (result) {
                $scope.is_loading = false;
                $rootScope.is_logged_in = false;
                $rootScope.user_info = null;
                $scope.error_message = result.data;
            });
        };

        /**
         * Used to logout/destroy the current logged in session
         * @author karthick.k
         */
        $scope.logout = function () {
            user_service.logout().then(function (result) {
                $rootScope.is_logged_in = false;
                $rootScope.user_info = null;
                $state.go("homepage", {});
            }, function (error) {
                $rootScope.is_logged_in = false;
                $rootScope.user_info = null;
                $state.go("homepage", {});
            });
        };

        /**
         * Used to handle forgot password, make a server/ajax call to send a email to reset the password to user
         * @author karthick.k
         * @param   {object} form  form fields contains email
         */
        $scope.handle_forgot_password = function (form) {
            clear_form_message();
            if (!validation.is_valid(form)) {
                return;
            }
            $scope.is_loading = true;
            user_service.forgot_password($scope.forgot_password).then(function (result) {
                $scope.is_loading = false;
                $scope.success_message = result.data.message;
            }, function (result) {
                $scope.is_loading = false;
                $scope.error_message = result.data;
            });
        };

        /**
         * Used to handle reset password
         * @author karthick.k
         * @param   {object} form  form fields contains new password and confirm password
         */
        $scope.handle_reset_password = function (form) {
            clear_form_message();
            if (!validation.is_valid(form)) {
                return;
            }
            if (!!$stateParams.token && !!$stateParams.email) {
                var user = {
                    token: $stateParams.token,
                    email: $stateParams.email
                };
                user = angular.extend($scope.reset_password, user);
                user_service.reset_password(user).then(function (result) {
                    $state.go("profiles");
                    $scope.template_name = '/views/user/login.html';
                    $scope.open_dropdown();
                }, function (result) {
                    $scope.success_message = result.data;
                });
            } else {
                $scope.error_message = "Reset link is incorrect";
            }

        };

        /**
         * Used to open forgot password dropdown 
         * @author karthick.k
         */
        $scope.open_forgot_password_dropdown = function () {
            clear_form_message();
            $scope.template_name = '/views/user/forgot_password.html';
        };

        /**
         * Used to open login dropdown
         * @author karthick.k
         */
        $scope.open_login_dropdown = function () {
            clear_form_message();
            $scope.template_name = '/views/user/login.html';
        };

        /**
         * Used to go to dashoboard
         * @author karthick.k
         */
        $scope.open_dashboard = function () {
            clear_form_message();
            $state.go("user_dashboard_blogs");
        };

        /**
         * Used to close dropdown
         * @author karthick.k
         */
        $scope.close_dropdown = function () {
            setTimeout(function () {
                angular.element(document.getElementById('user-dropdown'))[0].click();
            });
        };

        /**
         * Used to open dropdown
         * @author karthick.k
         */
        $scope.open_dropdown = function () {
            setTimeout(function () {
                angular.element(document.getElementById('user-dropdown'))[0].click();
            });
        };

        $scope.dropdown_toggled = function (open) {
            if (!!open) {
                clear_form_message();
            }
        };

}]);