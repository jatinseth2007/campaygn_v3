app.service('profiles_list_service', ['$http',

    function ($http) {
        this.add_popup_functionality = function ($scope, $uibModal, list_service, toastr, callback) {

            // need to define this object if it's not defined before...
            if (!$scope.hasOwnProperty('list')) {
                $scope.list = {
                    profiles: {},
                    parent_id: 0
                };
            }

            /*
             * Common List functions...
             */


            // function to check all checkboxes...
            $scope.select_unselect_all_profiles = function () {
                if ($scope.list.select_all) {
                    $scope.profiles.map(function (profile) {
                        $scope.list.profiles[profile.profile_id] = profile.profile_id;
                        return true;
                    });
                } else {
                    $scope.list.profiles = {};
                }
            };

            // function add profile in array...
            $scope.add_remove_profile = function (profile_id) {
                if ($scope.list.profiles.hasOwnProperty(profile_id)) {
                    delete $scope.list.profiles[profile_id];
                } else {
                    $scope.list.profiles[profile_id] = profile_id;
                }
            };

            // open pop code
            var open_popup = function (size, template, user_role, empty_option) {
                var popup_object = $uibModal.open({
                    animation: true,
                    //template: '<h2>hi</h2>',
                    templateUrl: template,
                    controller: 'profiles_list',
                    size: size,
                    resolve: {
                        user_lists: function () {
                            return list_service.fetch_user_lists(0, user_role).then(function (result) {
                                if (result) {
                                    return list_service.prepare_data(result.data, empty_option);
                                }
                            }, function (err) {
                                toastr.error(err.data);
                            });
                        },
                        list_obj: function () {
                            return $scope.list;
                        }
                    }
                });

                // use the closed promise...
                popup_object.closed.then(function (data) {
                    $scope.list = {
                        profiles: {},
                        parent_id: 0,
                        name: '',
                        select_all: false
                    };

                    // need to perform some actions, so, in callback we are doing it :)...
                    callback();
                }, console.log);
            };

            // END Common List functions

            // Create new list with selected profiles...
            $scope.open_create_new_list = function () {
                // check if we have users to add in list...
                if (Object.keys($scope.list.profiles).length > 0) {
                    open_popup('sm', './views/profiles/create_new_list.html', 'owner', true);
                }
            };
            // END Create new list

            /*
             * Remove profiles from list
             */
            $scope.open_remove_from_list = function () {
                // check if we have users to add in list...
                if (Object.keys($scope.list.profiles).length > 0) {
                    open_popup('sm', './views/profiles/remove_from_list.html', 'all', false);
                }
            };

            // END Remove profiles from list

            /*
             * Add profiles to list
             */
            $scope.open_add_to_list = function () {
                // check if we have users to add in list...
                if (Object.keys($scope.list.profiles).length > 0) {
                    open_popup('sm', './views/profiles/add_to_list.html', 'all', false);
                }
            };

            // END Remove profiles from list
        };

    }
]);
