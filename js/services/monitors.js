app.service('monitors_service', ['$http', '$stateParams', '$q', '$rootScope',

    function ($http, $stateParams, $q, $rootScope) {
        /**
         * Used to search tags
         * @author karthick.k
         * @param {string} search keyword 
         * @returns {object} tag information
         */
        this.search_tags = function (keyword) {
            return $http({
                method: 'GET',
                url: "/api/monitors/search_tags?tag=" + keyword
            });
        };

        /**
         * Used to search influencers
         * @author karthick.k
         * @param {string} search keyword              
         * @returns {object} influencers information
         */
        this.search_individual_influencers = function (keyword) {
            return $http({
                method: 'GET',
                url: "/api/monitors/search_individual_influencers?profile_name=" + keyword
            });
        };

        /**
         * Used to search users
         * @author karthick.k
         * @param {string} search keyword              
         * @returns {object} users information
         */
        this.search_users = function (keyword) {
            return $http({
                method: 'GET',
                url: "/api/monitors/search_users?user_name=" + keyword
            });
        };

        /**
         * USed to create new monitor
         * @author karthick.k
         * @param {object} monitor, monitor data
         * @returns {object} success message              
         */
        this.create_monitor = function (monitor) {
            return $http({
                method: 'POST',
                url: "/api/monitors/create_monitor",
                data: monitor
            });
        };

        /**
         * Used to fetch monitors
         * @author karthick.k
         * @returns {Array} monitors list              
         */
        this.get_monitors_list = function () {
            return $http({
                method: 'GET',
                url: "/api/monitors/list",
                params: {
                    cache: false
                }
            });
        };

        /**
         * Used to delete monitor
         * @author karthick.k
         * @param {number} monitor_id selected monitor id to delete
         * @returns {string} success/error message
         */
        this.delete_monitor = function (monitor_id) {
            return $http({
                method: 'DELETE',
                url: "/api/monitors/delete_monitor",
                params: {
                    monitor_id: monitor_id
                }
            });
        };

        /**
         * Used to fetch monitor details
         * @author karthick.k
         * @param {number} monitor_id selected monitor id to delete
         * @returns {object} monitor details
         */
        this.get_monitor_details = function (monitor_id) {
            return $http({
                method: 'GET',
                url: "/api/monitors/monitor_details",
                params: {
                    monitor_id: monitor_id
                }
            });
        };

        /**
         * used to modify from or to date
         * @author karthick.k
         * @param   {object} monitor monitor id, from, to date
         * @returns {string} success/error message
         */
        this.modify_date = function (monitor) {
            return $http({
                method: 'POST',
                url: "/api/monitors/modify_date",
                data: monitor
            });
        };

        /**
         * Used to modify monitor name
         * @author karthick.k
         * @param   {object} monitor monitor id, name
         * @returns {string} success/error message
         */
        this.modify_name = function (monitor) {
            return $http({
                method: 'POST',
                url: "/api/monitors/modify_name",
                data: monitor
            });
        };

        /**
         * Used to fetch monitor posts count
         * @author karthick.k
         * @param   {number} monitor_id monitor id
         * @returns {object} posts count / error message
         */
        this.get_monitor_posts_details = function (monitor_id, influencer_id, tag_id) {
            return $http({
                method: 'GET',
                url: "/api/monitors/monitor_posts_details",
                params: {
                    monitor_id: monitor_id,
                    influencer_id: influencer_id,
                    tag_id: tag_id
                }
            });
        };

        /**
         * Used to fetch monitor influencers followers count
         * @author karthick.k
         * @param   {number} monitor_id monitor id
         * @param   {number} influencer_id influencer id
         * @returns {array} list of monitor influencers followers count
         */
        this.get_monitor_influencers_followers_details = function (monitor_id, influencer_id, tag_id) {
            return $http({
                method: 'GET',
                url: "/api/monitors/monitor_influencers_followers_details",
                params: {
                    monitor_id: monitor_id,
                    influencer_id: influencer_id,
                    tag_id: tag_id
                }
            });
        };

        /**
         * Used to modify influencers
         * @author karthick.k
         * @param   {object} influencers monitor id and influencers
         * @returns {string} sucess/error message
         */
        this.modify_influencers = function (influencers) {
            return $http({
                method: 'POST',
                url: "/api/monitors/modify_influencers",
                data: influencers
            });
        };

        /**
         * Used to modify tags
         * @author karthick.k
         * @param   {object} tags  
         * @returns {string} success/error message
         */
        this.modify_tags = function (tags) {
            return $http({
                method: 'POST',
                url: "/api/monitors/modify_tags",
                data: tags
            });
        };

        /**
         * Used to modify group name of monitor
         * @author karthick.k
         * @param   {object} monitor monitor id and group name
         * @returns {string} success/error message
         */
        this.modify_group_name = function (monitor) {
            return $http({
                method: 'POST',
                url: "/api/monitors/modify_group_name",
                data: monitor
            });
        };

        /**
         * Used to modify users
         * @author karthick.k
         * @param   {object} users
         * @returns {string} success/error messsage
         */
        this.modify_users = function (users) {
            return $http({
                method: 'POST',
                url: "/api/monitors/modify_users",
                data: users
            });
        };

        /**
         * Used to search list
         * @author karthick.k
         * @param {string} search keyword 
         * @returns {object} list information
         */
        this.search_lists = function (keyword) {
            return $http({
                method: 'GET',
                url: "/api/monitors/search_lists?keyword=" + keyword
            });
        };

        /**
         * Used to search group
         * @author karthick.k
         * @param {string} search keyword 
         * @returns {object} group names
         */
        this.search_group = function (keyword) {
            return $http({
                method: 'GET',
                url: "/api/monitors/search_group?cache=false&keyword=" + keyword
            });
        };

    }
]);