app.service('leaderboard_service', ['$http',

    function ($http) {
        /*
         *
         */
        this.fetch_results = function () {
            /**
             * Load top 100 records from DB...
             */
            return $http({
                method: 'GET',
                url: './api/profiles',
                params: {
                    /*profile_kind: $stateParams.profile_kind,
                    search_keyword: $stateParams.search_keyword,
                    search_countries: $stateParams.search_countries,
                    search_continents: $stateParams.search_continents,
                    search_gender: $stateParams.search_gender,
                    search_tags: $stateParams.search_tags,
                    search_themes: $stateParams.search_themes,
                    search_languages: $stateParams.search_languages,
                    min_range: $stateParams.min_range,
                    max_range: $stateParams.max_range,
                    min_age: $stateParams.min_age,
                    max_age: $stateParams.max_age,
                    min_growth: $stateParams.min_growth,
                    max_growth: $stateParams.max_growth,
                    min_engagement: $stateParams.min_engagement,
                    max_engagement: $stateParams.max_engagement,
                    limit: $stateParams.limit,
                    page: $stateParams.page,
                    sort_by: $stateParams.sort_by,
                    sort_order: $stateParams.sort_order,
                    active_platform: $stateParams.active_platform,
                    cache: $stateParams.cache*/
                }
            });
        };

        /*
         * Function to validate input from form...
         */
        this.validate = function (input) {
            input.error = {};

            // we need atleast one filter item to proceed
            if ((!input.selected_countries || input.selected_countries.length <= 0) && (!input.selected_gender || input.selected_gender.length <= 0) && (!input.selected_themes || input.selected_themes.length <= 0)) {
                input.error.country = "Please select atleast one filter";
            }

            // social platform
            if (!input.selected_platform) {
                input.error.social_platform = "Please select social platform";
            }

            // KPI
            if (!input.kpi) {
                input.error.kpi = "Please select KPI";
            }


            if (Object.keys(input.error).length <= 0) {
                return true;
            } else {
                return false;
            }
        };

    }
]);
