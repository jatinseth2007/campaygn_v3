app.service('tag_service', ['$http', '$stateParams', '$q', '$rootScope',
function ($http, $stateParams, $q, $rootScope) {
        /**
         * Used to get posts
         * @author karthick.k
         * @param {string} search keyword 
         * @returns {object} tag information
         */
        this.get_posts = function (tag_id) {
            return $http.get("/api/tag/get_posts/" + tag_id);
        };

}]);