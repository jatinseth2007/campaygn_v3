app.service('user_service', ['$http', '$stateParams', '$q',

    function ($http, $stateParams, $q) {
        /**
         * Used to fetch logged user
         * @author karthick.k
         * @returns {object} user information
         */
        this.get_user = function () {
            return $http({
                method: 'GET',
                url: "/api/user/user?cache=false"
            });
        };

        /**
         * Used to login
         * @author karthick.k
         * @param   {object}   consist of email and password
         * @returns {object} user information id, email, role and role_id
         */
        this.login = function (user) {
            return $http({
                method: 'POST',
                url: "/api/user/login",
                data: user
            });
        };

        /**
         * Used to create new user
         * @author karthick.k
         * @param   {object}   consist of email and password
         * @returns {object} user information id, email, role and role_id
         */
        this.create_user = function (user) {
            return $http({
                method: 'POST',
                url: "/api/user/create_user",
                data: user
            });
        };

        /**
         * Used to verify email
         * @author karthick.k
         * @param   {object}   token and email
         * @returns {object} success message
         */
        this.email_verification = function (user) {
            return $http({
                method: 'GET',
                url: "/api/user/verification?cache=false&token=" + user.token + "&email=" + user.email
            });
        };

        /**
         * Used to handle forgot password
         * @author karthick.k
         * @param   {object}   consist of email
         * @returns {object} success message
         */
        this.forgot_password = function (user) {
            return $http({
                method: 'GET',
                url: "/api/user/forgot_password?cache=false&email=" + user.email
            });
        };

        /**
         * Used to reset password
         * @author karthick.k
         * @param   {object}   consist of password and confirm password
         * @returns {object} success message
         */
        this.reset_password = function (user) {
            return $http({
                method: 'POST',
                url: "/api/user/reset_password",
                data: user
            });
        };

        /**
         * Used to logout current session
         * @author karthick.k
         * @returns {object} success message
         */
        this.logout = function () {
            return $http({
                method: 'GET',
                url: "/api/user/logout?cache=false"
            });
        };

        /**
         * Used to fetch available roles
         * @author karthick.k
         * @returns {array of object} roles-id and role
         */
        this.get_roles = function () {
            return $http({
                method: 'GET',
                url: "/api/user/roles?cache=false"
            });
        };

        /**
         * Used to fetch all users
         * @author karthick.k
         * @returns {array of object} users information id, email, created_at, expires_at
         */
        this.get_users = function () {
            return $http({
                method: 'GET',
                url: "/api/user/list_user?cache=false"
            });
        };

        /**
         * Used to modify role of user
         * @author karthick.k
         * @returns success message
         */
        this.modify_role = function (user) {
            return $http({
                method: 'GET',
                url: "/api/user/modify_role?cache=false&id=" + user.id + "&role_id=" + user.role_id
            });
        };

        /**
         * Used to modify expiry
         * @author karthick.k
         * @returns success message
         */
        this.modify_expiry = function (user) {
            return $http({
                method: 'GET',
                url: "/api/user/modify_expiry?cache=false&id=" + user.id + "&expires_at=" + user.expires_at
            });
        };

        /**
         * Used to fetch user logs
         * @author karthick.k
         * @returns logs
         */
        this.get_user_logs = function (id) {
            return $http({
                method: 'GET',
                url: "/api/user/user_logs?cache=false&user_id=" + id
            });
        };

        /**
         * Used to delete user
         * @author karthick.k
         * @param {number} id, user id
         * @returns success message               
         */
        this.delete_user = function (id) {
            return $http({
                method: 'GET',
                url: "/api/user/delete_user?cache=false&user_id=" + id
            });
        };

        /**
         * Used to fetch all valid users
         * @author Jatin Seth
         * @param
         * @returns all users
         */
        this.fetch_valid_users = function (list_id) {
            var end_point = "./api/user/view?cache=false";

            // if list provided then add
            if (list_id > 0) {
                end_point += "&list_id=" + list_id;
            }

            return $http.get(end_point);
        };
    }
]);