app.service('analytics_service', ['$http', '$stateParams', '$q', '$rootScope', '$window',

function ($http, $stateParams, $q, $rootScope, $window) {
        /**
         * Used to get events
         * @author karthick.k
         * @returns {object} events information
         */
        this.fetch_events = function (filter) {
            var params = {
                cache: 'false'
            };
            if (!!filter && Object.keys(filter).length > 0) {
                params.from = filter.from;
                params.to = filter.to;
            }
            return $http({
                method: 'GET',
                url: "/api/analytics/events_details",
                params: params
            });
        };

        /**
         * Used to export the category based on the date and category selection
         * @author karthick.k
         * @returns {object} form information
         */
        this.export = function (form) {
            $window.open('/api/analytics/export?cache=false&from=' + form.from + "&to=" + form.to + "&category=" + form.category, "_self");
        };
}]);