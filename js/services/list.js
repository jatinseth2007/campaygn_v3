app.service('list_service', ['$http',

    function ($http) {

        /*
         * Owner lists listing...
         */

        this.fetch_owner_lists = function (params) {
            /**
             * Load 30 added records from DB
             */
            return $http({
                method: 'GET',
                url: './api/lists/fetch_owner_lists',
                params: {
                    cache: false,
                    limit: params.limit,
                    page: params.page,
                    sort_order: params.sort_order,
                    sort_by: params.sort_by
                }
            });
        };

        /*
         * Member lists listing...
         */

        this.fetch_member_lists = function () {
            /**
             * Load 30 added records from DB
             */
            return $http({
                method: 'GET',
                url: './api/lists/fetch_member_lists',
                params: {
                    cache: false
                }
            });
        };

        /*
         * Validate add list form
         */
        this.validate_list_form = function (list_scope) {
            var error = true;

            if (angular.isUndefined(list_scope.name) || list_scope.name.length <= 0) {
                list_scope.error = {};
                list_scope.error.name = 'Please enter valid list name';
                error = false;
            }

            return error;
        };

        /*
         * Function is to prepare data in a format compatable to abn-tree
         */
        this.add_children = function (list_scope, branch, user_role) {
            var self = this;


            // when user has selected empty list then no need to do anything, just need to make it empty...
            if (branch.data.id < 0) {
                list_scope.parent_id = 0;
                return true;
            }

            self.fetch_user_child_lists(branch.data.id, user_role).then(function (result) {
                if (result) {
                    branch.children = [];
                    branch.children = self.prepare_data(result.data, false);
                    branch.expanded = true;
                }

                list_scope.parent_id = branch.data.id;
            }, function (err) {
                console.log(err);
            });
        };

        /*
         * Function is to prepare data in a format compatable to abn-tree
         */
        this.prepare_data = function (input, empty_option) {

            var output = [];

            // if need the empty option then add option...
            if (empty_option) {
                output.push({
                    label: 'no parent',
                    data: {
                        id: -1
                    }
                });
            }

            if (input.length > 0) {
                angular.forEach(input, function (child, index) {
                    var temp = {
                        label: child.name,
                        data: {
                            id: child.id
                        }
                    };
                    output.push(temp);
                });
            }

            return output;
        };

        /*
         * Function is to fetch base user's lists
         */

        this.fetch_user_lists = function (parent_id, user_role) {
            /*
             *Checking if we got the user_id or not
             */
            if (!isNaN(parent_id) && user_role.length > 0) {
                return $http.get('./api/lists/get_root_lists_by_user?cache=false&user_role=' + user_role);
            }

            return Promise.reject("invalid input provided");
        };

        /*
         * Function is to fetch user's child lists
         */

        this.fetch_user_child_lists = function (parent_id, user_role) {
            /*
             *Checking if we got the user_id or not
             */
            if (!isNaN(parent_id) && user_role.length > 0) {
                return $http.get('./api/lists/get_child_lists_by_user?cache=false&parent_id=' + parent_id + "&user_role=" + user_role);
            }

            return Promise.reject("invalid input provided");
        };

        /*
         * Function is to save the list for user...
         */

        this.create_user_list = function (list_info) {
            /*
            Checking if profile kind is brand or influencer otherwise it should be set to all.
             */
            if (Object.keys(list_info).length > 0) {
                return $http.post('./api/list/add?cache=false',
                    list_info
                );
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to remove profiles from a list...
         */

        this.remove_profile_list = function (list_info) {
            /*
            Checking if profile kind is brand or influencer otherwise it should be set to all.
             */
            if (Object.keys(list_info).length > 0) {
                return $http.post('./api/lists/remove_list_profiles?cache=false', list_info);
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to add profiles to a list...
         */

        this.add_profile_list = function (list_info) {
            /*
            Checking if profile kind is brand or influencer otherwise it should be set to all.
             */
            if (Object.keys(list_info).length > 0) {
                return $http.post('./api/lists/add_list_profiles?cache=false', list_info);
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to remove list from system...
         */

        this.delete_list = function (list_id) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id) && parseInt(list_id) > 0) {
                return $http.delete('./api/list/' + list_id + '/delete/?cache=false');
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to fetch list info...
         */

        this.fetch_user_list = function (list_id) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id)) {
                return $http.get('./api/list/' + list_id + '?cache=false');
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to fetch list info...
         */

        this.update = function (list) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (Object.keys(list).length > 0) {
                return $http.post('./api/list/update?cache=false', list);
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to fetch lists_users_info...
         */

        this.fetch_list_users = function (list_id) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id)) {
                return $http.get('./api/list/' + list_id + '/users/?cache=false');
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to modify userRole for list...
         */

        this.modify_role = function (list_id, user_id, role) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id) && !isNaN(user_id) && ['admin', 'member'].indexOf(role) >= 0) {
                return $http.post('./api/list/' + list_id + '/update_user_role/?cache=false', {
                    user_id: user_id,
                    user_role: role
                });
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is to remove user from list...
         */

        this.remove_list_user = function (list_id, user_id) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id) && !isNaN(user_id)) {
                return $http.post('./api/list/' + list_id + '/remove_user_role/?cache=false', {
                    user_id: user_id
                });
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Function is add user(s) into list...
         */

        this.add_list_users = function (list_id, list_obj) {
            self = this;
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id) && Object.keys(list_obj).length > 0) {
                var data_to_send = self.prepare_list_users_obj(list_obj); //add other params into object

                return $http.post('./api/list/' + list_id + '/add_user_role/?cache=false', data_to_send);
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Prepare data to add users into list
         */
        this.prepare_list_users_obj = function (list_obj) {
            var output = {};

            if (list_obj.user_role && list_obj.user_role.value) {
                output.role = list_obj.user_role.value;
            }

            if (list_obj.notify) {
                output.notify = list_obj.notify;
            }

            if (list_obj.users.length > 0) {
                output.users = [];
                angular.forEach(list_obj.users, function (user, key) {
                    output.users.push(user.id);
                });
            }


            return output;
        };

        /*
         * fetch list children assosiated with the user
         */
        this.fetch_list_children = function (list_id) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id)) {
                return $http.get('./api/list/' + list_id + '/children/?cache=false');
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * fetch list profiles
         */
        this.fetch_list_profiles = function (list_id) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id)) {
                return $http.get('./api/list/' + list_id + '/profiles/?cache=false');
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * fetch list comments
         */
        this.fetch_list_comments = function (list_id) {
            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (!isNaN(list_id)) {
                return $http.get('./api/list/' + list_id + '/comments/?cache=false');
            }

            return Promise.reject("invalid input provided.");
        };

        /*
         * Add comment for list
         */
        this.add_comment = function (list_id, comment) {
            var error = {};

            /*
            Checking if we are getting right input or not. Don't want to break my code...
             */
            if (isNaN(list_id)) {
                error.list_id = "Please provide valid data.";
            }

            if (isNaN(list_id)) {
                error.comment = "Please provide valid comment.";
            }

            if (Object.keys(error).length <= 0) {
                return $http.post('./api/list/' + list_id + '/add_comment/?cache=false', {
                    comment: comment
                });
            } else {
                return Promise.reject(error);
            }
        };
    }
]);
