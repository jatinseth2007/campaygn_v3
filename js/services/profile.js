app.service('profile_service', ['$http', '$stateParams', '$filter',

    function ($http, $stateParams, $filter) {
        var six_months_older = new Date();
        six_months_older.setMonth(six_months_older.getMonth() - 6);

        /*
         * Profiles listing...
         */

        this.fetch_profiles = function () {
            /*
            Checking if profile kind is brand or influencer otherwise it should be set to all.
             */
            $stateParams.profile_kind = ($stateParams.profile_kind == 'brand' || $stateParams.profile_kind == 'influencer') ? $stateParams.profile_kind : 'all';

            /**
             * Load 30 added records from DB
             */
            //return $http.get('./api/profiles?' + $httpParamSerializer($stateParams));
            return $http({
                method: 'GET',
                url: './api/profiles',
                params: {
                    profile_kind: $stateParams.profile_kind,
                    search_keyword: $stateParams.search_keyword,
                    search_countries: $stateParams.search_countries,
                    search_continents: $stateParams.search_continents,
                    search_gender: $stateParams.search_gender,
                    search_tags: $stateParams.search_tags,
                    search_themes: $stateParams.search_themes,
                    search_languages: $stateParams.search_languages,
                    min_range: $stateParams.min_range,
                    max_range: $stateParams.max_range,
                    min_age: $stateParams.min_age,
                    max_age: $stateParams.max_age,
                    min_growth: $stateParams.min_growth,
                    max_growth: $stateParams.max_growth,
                    min_engagement: $stateParams.min_engagement,
                    max_engagement: $stateParams.max_engagement,
                    limit: $stateParams.limit,
                    page: $stateParams.page,
                    sort_by: $stateParams.sort_by,
                    sort_order: $stateParams.sort_order,
                    active_platform: $stateParams.active_platform,
                    cache: $stateParams.cache
                }
            });
        };

        /*
         * This is to fetch profiles followers range...
         */
        this.fetch_profiles_reach = function () {
            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/profiles/get_profiles_range/instagram');
                case 'facebook':
                    return $http.get('./api/profiles/get_profiles_range/facebook');
                case 'youtube':
                    return $http.get('./api/profiles/get_profiles_range/youtube');
                default:
                    return $http.get('./api/profiles/get_profiles_range/all');
            }
        };

        /*
         * This is to fetch profiles enagagement rate range...
         */
        this.fetch_profiles_engagementrate_range = function () {
            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/profiles/get_profiles_engagement_rate_range/instagram');
                case 'facebook':
                    return $http.get('./api/profiles/get_profiles_engagement_rate_range/facebook');
                case 'youtube':
                    return $http.get('./api/profiles/get_profiles_engagement_rate_range/youtube');
                default:
                    return $http.get('./api/profiles/get_profiles_engagement_rate_range/all');
            }
        };

        /*
         * This is to fetch profiles growth range...
         */
        this.fetch_profiles_growth_range = function () {
            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/profiles/get_profiles_growth_range/instagram');
                case 'facebook':
                    return $http.get('./api/profiles/get_profiles_growth_range/facebook');
                case 'youtube':
                    return $http.get('./api/profiles/get_profiles_growth_range/youtube');
                default:
                    return $http.get('./api/profiles/get_profiles_growth_range/all');
            }
        };

        /*
         * This is to fetch profiles age range...
         */
        this.fetch_profiles_age_range = function () {
            return $http.get('./api/profiles/get_profiles_age_range');
        };

        /*
         * Function to fetch single profile data by slug
         */
        this.fetch_profile = function (slug) {
            if (slug) {
                return $http.get('./api/_common/get_data/profiles/*/slug/' + slug);
            }
            return Promise.reject("No object found");
        };

        /*
         * function to fetch full profile data
         */
        this.fetch_full_profile = function (user_id) {
            if (user_id) {
                return $http.get('./api/profile/get_profile_data/' + user_id + '/?cache=false');
            }
            return Promise.reject("No object found");
        };

        /**
         * Get random images for profile page bg
         */
        this.fetch_random_images = function (user_obj_information) {
            if (!user_obj_information) {
                return Promise.reject("No object found");
            }

            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/instagram/get_random_images/' + user_obj_information.instagram + "/12");
                case 'facebook':
                    return $http.get('./api/facebook/get_random_images/' + user_obj_information.facebook + "/12");
                case 'youtube':
                    return $http.get('./api/youtube/get_random_images/' + user_obj_information.youtube + "/12");
                default:
                    return Promise.reject("Nothing to do");
            }
        };

        /**
         * Get recent images for profile page bg
         */
        this.fetch_recent_images = function (user_obj_information) {
            if (!user_obj_information) {
                return Promise.reject("No object found");
            }
            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('api/instagram/get_recent_images/' + user_obj_information.instagram + "/4");
                case 'facebook':
                    return $http.get('api/facebook/get_recent_images/' + user_obj_information.facebook + "/4");
                case 'youtube':
                    return $http.get('api/youtube/get_recent_images/' + user_obj_information.youtube + "/4");
                default:
                    return Promise.reject("Nothing to do");
            }
        };

        /**
         * Get average number of likes for user
         */
        this.fetch_avg_likes = function (user_obj_information) {
            if (!user_obj_information) {
                return Promise.reject("No object found");
            }
            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/instagram/get_user_average_likes/' + user_obj_information.instagram);
                case 'facebook':
                    return $http.get('./api/facebook/get_user_average_likes/' + user_obj_information.facebook);
                case 'youtube':
                    return $http.get('./api/youtube/get_user_average_views/' + user_obj_information.youtube);
                default:
                    return Promise.reject("Nothing to do");
            }
        };
        /**
         * Get media posted by user
         */
        this.fetch_media_posted = function (user_obj_information) {
            if (!user_obj_information) {
                return Promise.reject("No object found");
            }
            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'facebook':
                    return $http.get('./api/facebook/media_posted/' + user_obj_information.facebook);
                default:
                    return Promise.reject("Nothing to do");
            }
        };
        /**
         * Get user's most liked post
         */
        this.fetch_most_liked_post = function (user_obj_information) {
            if (!user_obj_information) {
                return Promise.reject("No object found");
            }
            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/instagram/get_user_most_liked/' + user_obj_information.instagram);
                case 'facebook':
                    return $http.get('./api/facebook/get_user_most_liked/' + user_obj_information.facebook);
                case 'youtube':
                    return $http.get('./api/youtube/get_user_most_viewed/' + user_obj_information.youtube);
                default:
                    return Promise.reject("Nothing to do");
            }
        };

        /**
         * Getting growth widget
         */
        this.fetch_growth_widget = function (user_obj_information) {
            if (!user_obj_information) {
                return Promise.reject("No object found");
            }

            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/instagram/get_growth/' + user_obj_information.instagram);
                case 'facebook':
                    return $http.get('./api/facebook/get_growth/' + user_obj_information.facebook);
                case 'youtube':
                    return $http.get('./api/youtube/get_growth/' + user_obj_information.youtube);
                default:
                    return Promise.reject("Nothing to do");
            }
        };

        /**
         * Getting Brand hashtag growth information
         */
        this.fetch_brand_hashtag_growth = function (user_obj_information) {
            if (!user_obj_information) {
                return Promise.reject("No object found");
            } else if (user_obj_information.kind !== 'brand') {
                return Promise.reject("This influenser is not a brand.");
            }

            //check the platform and call the service accordingly...
            switch ($stateParams.active_platform) {
                case 'instagram':
                    return $http.get('./api/instagram/get_hashtagposts_growth/' + user_obj_information.instagram);
                case 'facebook':
                    return $http.get('./api/facebook/get_hashtagposts_growth/' + user_obj_information.facebook);
                case 'youtube':
                    return $http.get('./api/youtube/get_hashtagposts_growth/' + user_obj_information.youtube);
                default:
                    return Promise.reject("Nothing to do");
            }
        };

        /*
         * This function is to fetch highChart data regarding a single user...
         */
        this.fetch_map_stats = function (user_id, start_date, end_date) {
            if (user_id) {
                start_date = angular.isDate(start_date) ? $filter('date')(start_date, 'yyyy-MM-dd') : $filter('date')(six_months_older, 'yyyy-MM-dd');
                end_date = angular.isDate(end_date) ? $filter('date')(end_date, 'yyyy-MM-dd') : $filter('date')(new Date(), 'yyyy-MM-dd');

                return $http.get('./api/profile/get_user_stats/' + user_id + '?start_date=' + start_date + '&end_date=' + end_date);
            }
            return Promise.reject("No object found");
        };

        /*
         * This function is to fetch highChart data regarding a single user...
         */
        this.fetch_highmap_stats = function (user_obj_information, which_date) {
            if (user_obj_information) {
                //check the platform and call the service accordingly...
                switch ($stateParams.active_platform) {
                    case 'facebook':
                        return $http.get('./api/facebook/get_user_followersbyarea/' + user_obj_information.facebook + '?which_date=' + which_date);
                    default:
                        return Promise.reject("Nothing to do");
                }
            }

            return Promise.reject("No object found");
        };

        /*
         * This function is to fetch instagram users based on keyword...
         */
        this.fetch_instagram_users = function (keyword) {
            if (keyword === "") {
                return Promise.reject("No object found");
            }

            return $http.get('./api/instagram/find_user/' + keyword);
        };

        /*
         * This function is to fetch facebook pages based on keyword...
         */
        this.fetch_facebook_pages = function (keyword) {
            if (keyword === "") {
                return Promise.reject("No object found");
            }

            return $http.get('./api/facebook/find_page/' + keyword);
        };
        /*
         * This function is to fetch youtube channels based on keyword...
         */
        this.fetch_youtube_channels = function (keyword) {
            if (keyword === "") {
                return Promise.reject("No object found");
            }

            return $http.get('./api/youtube/find_channel/' + keyword);
        };
        /*
         * This function is to validate profile add form...
         */
        this.validate_profile_input = function (input) {
            input.error = {};

            if (!input.name || input.name === '') {
                input.error.name = "Name is required";
            }

            if (!input.kind || input.kind === '') {
                input.error.kind = "Profile kind is required";
            }

            if (!input.gender || input.gender === '') {
                input.error.gender = "Profile gender is required";
            }

            if (!input.instagram && !input.facebook && !input.youtube) {
                input.error.socialmedia = "Please provide atleast one social media link.";
            }

            if ((input.age_min && !input.age_max) || (input.age_max && !input.age_min) || (input.age_min && isNaN(input.age_min)) || (input.age_max && isNaN(input.age_max)) || (input.age_min && input.age_max && input.age_min >= input.age_max)) {
                input.error.age_min = "Please provide valid audience age range.";
            }

            if (Object.keys(input.error).length > 0) {
                return false;
            }

            return true;
        };

        /*
         * Update profile data...
         */
        this.update_profile = function (profile_id, profile_obj) {
            if (profile_id === "" || Object.keys(profile_obj).length <= 0) {
                var error = {
                    data: {
                        profile_id: "Please provide valid input"
                    }
                };

                return Promise.reject(error);
            }

            return $http.post('./api/profile/edit/' + profile_id + '/?cache=false', profile_obj);
        };
    }
]);
